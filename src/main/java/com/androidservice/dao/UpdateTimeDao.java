package com.androidservice.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.UpdateTime;

@Repository
public class UpdateTimeDao extends AbstractDao<UpdateTime>{

	public UpdateTimeDao() {
		super(UpdateTime.class);
	}

	public UpdateTime findUpdateTime(){
		Query query = entityManager.createQuery("select u from UpdateTime u");
		
		return (UpdateTime)query.getResultList().get(0);
	}
}
