package com.androidservice.dao;


import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.CompangInfo;

@Repository
public class CompangInfoDao extends AbstractDao<CompangInfo>{

	protected CompangInfoDao() {
		super(CompangInfo.class);
	}
	
	public CompangInfo findCompangInfoByCid(int cid){
		Query query = entityManager.createQuery("select c from CompangInfo c where c.compang.cid =?1");
		query.setParameter(1, cid);		
		return (CompangInfo) query.getSingleResult();
	}
	
	public CompangInfo findCompangInfoByGid(int gid){
		Query query = entityManager.createQuery("select c from CompangInfo c where c.gid =?1");
		query.setParameter(1, gid);		
		return (CompangInfo) query.getSingleResult();
	}
	
	public void saveNewCompangInfo(CompangInfo info){
		entityManager.persist(info);
	}
	
	public void saveCompangInfo(CompangInfo info){
		entityManager.merge(info);
	}
	
}
