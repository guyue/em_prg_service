package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Contect;

@Repository
public class ContectDao extends AbstractDao<Contect>{

	public ContectDao() {
		super(Contect.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Contect> findContect(){
		Query query = entityManager.createQuery("select c from Contect c");
		
		return query.getResultList();
	}
	
	public Contect findContectByAid(int aid){
		Query query = entityManager.createQuery("select c from Contect c where c.aid = ?1");
		query.setParameter(1, aid);
		
		return (Contect)query.getSingleResult();
	}
	
	public void deleteContectByAid(int aid){
		Query query = entityManager.createQuery("delete from Contect c where c.aid = ?1");
		query.setParameter(1, aid);
		
		query.executeUpdate();
	}

	public void saveContect(Contect contect){
		entityManager.merge(contect);
	}
	
	public void saveNewContect(Contect contect){
		entityManager.persist(contect);
	}
}
