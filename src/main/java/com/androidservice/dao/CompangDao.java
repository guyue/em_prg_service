package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;


import org.springframework.stereotype.Repository;

import com.androidservice.bean.Compang;

@Repository
public class CompangDao extends AbstractDao<Compang>{
	
	public CompangDao(){
		super(Compang.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findAllCompangs(){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus <> '3'");
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findAllCompagsByStatus(){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0'");
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsBySid(int sid){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.sid =?1 and c.checkstatus = '0' order by c.grade desc,c.updateDatetime desc");
		query.setParameter(1, sid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> getCompangsBySid(int sid){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.sid =?1");
		query.setParameter(1, sid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByFid(int fid,int start,int number){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.fourclass.fid =?1 and c.checkstatus = '0' order by c.updateDatetime desc");
		query.setParameter(1, fid);
		query.setFirstResult(start);
		query.setMaxResults(number);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByFidAndLook(int fid,int start,int number){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.fourclass.fid =?1 and c.checkstatus = '0' order by c.looktime desc");
		query.setParameter(1, fid);
		query.setFirstResult(start);
		query.setMaxResults(number);
		
		return query.getResultList();
	}
	
	public int findCompangNumberByFid(int fid){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.fourclass.fid =?1 and c.checkstatus = '0' order by c.updateDatetime desc");
		query.setParameter(1, fid);
		
		return query.getResultList().size();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsBylocation(int sid,String location){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.sid =?1 and c.checkstatus = '0' order by case c.location when" + location +"then 1 when '' then 2 end");
		query.setParameter(1, sid);
		
		return query.getResultList();
	}
	
	public void saveNewCompang(Compang newCompang){
		entityManager.persist(newCompang);
	}
	
	public Compang findCompangByName(String name){
		Query query = entityManager.createQuery("select c from Compang c where c.name = ?1 and c.checkstatus = '0'");
		query.setParameter(1, name);
		
		return (Compang)query.getResultList().get(0);
	}
	
	public Compang findCompangByNameWithoutStatus(String name){
		Query query = entityManager.createQuery("select c from Compang c where c.name = ?1");
		query.setParameter(1, name);
		
		return (Compang)query.getResultList().get(0);
	}
	
	public Compang findCompangByCid(int cid){
		Query query = entityManager.createQuery("select c from Compang c where c.cid = ?1 and c.checkstatus = '0'");
		query.setParameter(1, cid);
		
		return (Compang)query.getSingleResult();
	}
	
	public Compang findCompangByCidWithoutStatus(int cid){
		Query query = entityManager.createQuery("select c from Compang c where c.cid = ?1");
		query.setParameter(1, cid);
		
		return (Compang)query.getSingleResult();
	}
	
	public void saveCompang(Compang comp){
		entityManager.merge(comp);
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsByservice(String service,int sid){
		Query query = entityManager.createQuery("select c from Compang c,Place p where c.smallclass.sid = ?1 and p.pid = c.area and p.area = ?2 and c.checkstatus = '0'");
		
		query.setParameter(1, sid);
		query.setParameter(2, service);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsByserviceAndBid(String area,int bid){
		Query query = entityManager.createQuery("select c from Compang c,Place p where c.smallclass.bigclass.bid = ?1 and p.pid = c.area and p.area = ?2 and c.checkstatus = '0'");
		
		query.setParameter(1, bid);
		query.setParameter(2, area);
		
		return query.getResultList();
	}
	
	public boolean updateCompangLookSize(int cid,int looksize){
		Query query = entityManager.createQuery("update Compang c set c.looktime = ?1 where c.cid = ?2");
		query.setParameter(1, looksize);
		query.setParameter(2, cid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	public boolean updateCompangTelSize(int cid,int telsize){
		Query query = entityManager.createQuery("update Compang c set c.teltime = ?1 where c.cid = ?2");
		query.setParameter(1, telsize);
		query.setParameter(2, cid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	public boolean updateCompangCommentSize(int cid,int commentsize){
		Query query = entityManager.createQuery("update Compang c set c.commentsize = ?1 where c.cid = ?2");
		query.setParameter(1, commentsize);
		query.setParameter(2, cid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByShowFirst(){
		Query query = entityManager.createQuery("select c from Compang c where c.showfirst = '0' and c.checkstatus = '0' and c.authentication='0'");
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByBid(int bid){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.bid = ?1 and c.showfirst = '0' and c.checkstatus = '0'");
		query.setParameter(1, bid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByCheckstatusAndBid(int bid,int start,int num){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.bid = ?1 and c.checkstatus = '0' order by c.teltime desc");
		query.setParameter(1, bid);
		query.setFirstResult(start);
		query.setMaxResults(num);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByCheckstatusAndBid(int bid){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.bid = ?1 and c.checkstatus = '0'");
		query.setParameter(1, bid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByArea(String area){
		Query query = entityManager.createQuery("select c from Compang c where c.serviceAreas.area = ?1 and c.checkstatus = '0'");
		query.setParameter(1, area);
		
		return query.getResultList();
	}
	
	public int findCompangNumberBySid(int sid){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.smallclass.sid =?1");
		query.setParameter(1, sid);
		
		return query.getResultList().size();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangBySize(int sid,int start,int number){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.sid =?1 and c.checkstatus = '0'");
		query.setParameter(1, sid);
		query.setFirstResult(start);
		query.setMaxResults(number);
		
		return query.getResultList();
	}
	
	public void DeleteCompangByCid(int cid){
		Query query = entityManager.createQuery("delete from Compang c where c.cid = ?1");
		query.setParameter(1, cid);
		
		query.executeUpdate();
	}
	
	public void DeleteCompangBySid(int sid){
		Query query = entityManager.createQuery("delete from Compang c where c.smallclass.sid = ?1");
		query.setParameter(1, sid);
		
		query.executeUpdate();
	}
	
	public int findAllCompangNumber(int bid){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.smallclass.bigclass.bid = ?1");
		query.setParameter(1, bid);
		
		return query.getResultList().size();
	}
	
	public int findNumberCompangByAuthent(){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.authentication='0'");
		
		return query.getResultList().size();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsByNumber(int start,int number){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.authentication='0' order by c.teltime desc");
		query.setFirstResult(start);
		query.setMaxResults(number);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> searchCompangs(String search){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.title like '%" + search + "%'");
		
		return query.getResultList();
	}
	
	public boolean findRecommendNumber(){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus = '0' and c.showfirst = '0'");
		if(query.getResultList().size() >= 6){
			return false;
		}
		return true;
	}
	
	public int findMaxNumber(){
		Query query = entityManager.createQuery("select c.number from Compang c order by c.number desc");
		if(query.getResultList().size() > 0){
			return (Integer)query.getResultList().get(0);
		}
		return 0;
	}
	
	public int findNumberByStatus(int checkstatus,int uid){
		Query query = entityManager.createQuery("select count(c) from Compang c where c.checkstatus =?1 and c.user.uid = ?2");
		query.setParameter(1, (byte)checkstatus);
		query.setParameter(2, uid);
		
		return ((Long) query.getSingleResult()).intValue();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByStatusAndUid(int checkstatus,int uid){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.user.uid = ?2");
		query.setParameter(1, (byte)checkstatus);
		query.setParameter(2, uid);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findComapngByCheckStatus(int checkstatus){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1");
		query.setParameter(1, (byte)checkstatus);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findNumberByStatusAndRole(int checkstatus,int role){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.user.role.rid >= ?2");
		query.setParameter(1, (byte)checkstatus);
		query.setParameter(2, role);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findNumberByRole(int role){
		Query query = entityManager.createQuery("select c from Compang c where c.user.role.rid >= ?1");
		query.setParameter(1, role);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findNumberByRoleAndArea(String area){
		Query query = entityManager.createQuery("select c from Compang c where c.area = ?1 and c.checkstatus <> '3'");
		query.setParameter(1, area);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findNumberByStatusAndRoleAndArea(int checkstatus,int role,String area){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.user.role.rid >= ?2 and c.area = ?3");
		query.setParameter(1, (byte)checkstatus);
		query.setParameter(2, role);
		query.setParameter(3, area);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByAreaAndStatus(int status,String area){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.area = ?2");
		query.setParameter(1, (byte)status);
		query.setParameter(2, area);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findComapngByUid(int uid){
		Query query = entityManager.createQuery("select c from Compang c where c.user.uid = ?1 and c.checkstatus <> '3'");
		query.setParameter(1, uid);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findAllCompangsByNum(int start){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus <> '3'");
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findComapngByCheckStatusAndNum(int checkstatus,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1");
		query.setParameter(1, (byte)checkstatus);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByAreaAndStatusAndNum(int status,String area,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.area = ?2");
		query.setParameter(1, (byte)status);
		query.setParameter(2, area);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findNumberByRoleAndAreaAndNum(String area,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.area = ?1 and c.checkstatus <> '3'");
		query.setParameter(1, area);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findComapngByUidAndNum(int uid,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.user.uid = ?1 and c.checkstatus <> '3'");
		query.setParameter(1, uid);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangByStatusAndUidAndNum(int checkstatus,int uid,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.checkstatus =?1 and c.user.uid = ?2");
		query.setParameter(1, (byte)checkstatus);
		query.setParameter(2, uid);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return (List<Compang>)query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Compang> findCompangsByBid(int bid,int start){
		Query query = entityManager.createQuery("select c from Compang c where c.smallclass.bigclass.bid = ?1 and c.checkstatus <> '3'");
		query.setParameter(1, bid);
		query.setMaxResults(10);
		query.setFirstResult(start);
		
		return (List<Compang>)query.getResultList();
	}
}
