package com.androidservice.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Role;

@Repository
public class RoleDao extends AbstractDao<Role>{

	public RoleDao() {
		super(Role.class);
	}
	
	public Role getRoleByRid(int rid){
		Query query = entityManager.createQuery("select r from Role r where r.rid = ?1");
		query.setParameter(1, rid);
		
		return (Role)query.getSingleResult();
	}

}
