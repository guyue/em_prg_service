package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Picture;

@Repository
public class PictureDao extends AbstractDao<Picture>{

	public PictureDao() {
		super(Picture.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Picture> findPictureByCid(int cid){
		Query query = entityManager.createQuery("select p from Picture p where p.compang.cid = ?1");
		query.setParameter(1, cid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Picture> findAllPicture(){
		Query query = entityManager.createQuery("select p from Picture p");
		
		return query.getResultList();
	}
	
	public Picture findPictureByPid(int pid){
		Query query = entityManager.createQuery("select p from Picture p where p.pid = ?1");
		query.setParameter(1, pid);
		
		return (Picture)query.getSingleResult();
	}
	
	public void savePicture(Picture picture){
		entityManager.merge(picture);
	}

	public void saveNewPicture(Picture picture){
		entityManager.persist(picture);
	}
	
	public void deletePictureByPid(int pid){
		Query query = entityManager.createQuery("delete from Picture p where p.pid = ?1");
		query.setParameter(1, pid);
		
		query.executeUpdate();
	}
	
	public void deletePictureByCid(int cid){
		Query query = entityManager.createQuery("delete from Picture p where p.compang.cid = ?1");
		query.setParameter(1, cid);
		
		query.executeUpdate();
	}
}
