package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Shangxun;

@Repository
public class ShangxunDao extends AbstractDao<Shangxun>{

	public ShangxunDao() {
		super(Shangxun.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Shangxun> findShangxunBySid(int sid){
		Query query = entityManager.createQuery("select s from Shangxun s where s.smallclass.sid =?1 order by s.updateTime desc");
		query.setParameter(1, sid);
		
		return query.getResultList();
	}
	
	public Shangxun findShangxunByXid(int xid){
		Query query = entityManager.createQuery("select s from Shangxun s where s.xid =?1");
		query.setParameter(1, xid);
		
		return (Shangxun)query.getSingleResult();
	}
	
	public void saveShangxun(Shangxun shangxun){
		entityManager.persist(shangxun);
	}
	
	public boolean updateShangxunLookSize(int xid,String looksize){
		Query query = entityManager.createQuery("update Shangxun s set s.looktime = ?1 where s.xid = ?2");
		query.setParameter(1, looksize);
		query.setParameter(2, xid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}

	public boolean updateShangxunCommentSize(int xid,String commentsize){
		Query query = entityManager.createQuery("update Shangxun s set s.commentSize = ?1 where s.xid = ?2");
		query.setParameter(1, commentsize);
		query.setParameter(2, xid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<Shangxun> findShangxunByShowFrist(){
		Query query = entityManager.createQuery("select s from Shangxun s where s.showfirst = '0'");
		
		return query.getResultList();
	}
}
