package com.androidservice.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Version;

@Repository
public class VersionDao extends AbstractDao<Version> {

	public VersionDao() {
		super(Version.class);
	}
	
	public Version findVersion(){
		Query query = entityManager.createQuery("select v from Version v");
		return (Version)query.getSingleResult();
	}

}
