package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Place;

@Repository
public class PlaceDao extends AbstractDao<Place>{

	public PlaceDao() {
		super(Place.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Place> findAreaByCity(String city){
		Query query = entityManager.createQuery("select p from Place p where p.city = ?1");
		query.setParameter(1, city);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Place> findCityByProvince(String province){
		Query query = entityManager.createQuery("select p from Place p where p.province = ?1");
		query.setParameter(1, province);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Place> findAllPlace(){
		Query query = entityManager.createQuery("select p from Place p");
		
		return query.getResultList();
	}
	
	public Place findPlaceByPid(int pid){
		Query query = entityManager.createQuery("select p from Place p where p.pid = ?1");
		query.setParameter(1, pid);
		
		return (Place)query.getSingleResult();
	}
}
