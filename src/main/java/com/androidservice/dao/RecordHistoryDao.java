package com.androidservice.dao;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.RecordHistory;

@Repository
public class RecordHistoryDao extends AbstractDao<RecordHistory>{

	public RecordHistoryDao() {
		super(RecordHistory.class);
	}
	
	public void saveNewRecordHistory(RecordHistory record){
		entityManager.persist(record);
	}

}
