package com.androidservice.dao;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.CompangRegister;

@Repository
public class CompangRegisterDao extends AbstractDao<CompangRegister>{

	public CompangRegisterDao() {
		super(CompangRegister.class);
	}
	
	public void addCompangInfo(CompangRegister compang){
		entityManager.persist(compang);
	}

}
