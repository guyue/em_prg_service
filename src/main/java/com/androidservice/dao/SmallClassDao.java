package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.SmallClass;

@Repository
public class SmallClassDao extends AbstractDao<SmallClass> {

	public SmallClassDao() {
		super(SmallClass.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<SmallClass> getSmallClassByBid(int bid){
		Query query = entityManager.createQuery("select s from SmallClass s where s.bigclass.bid =?1 order by s.sort");
		query.setParameter(1, bid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SmallClass> findAllSmallClassByFid(int fid){
		Query query = entityManager.createQuery("select s from SmallClass s where s.bigclass.fourclass.fid = ?1");
		query.setParameter(1, fid);
		
		return query.getResultList();
	}
	
	public int findSmallClassIdByName(String name){
		Query query = entityManager.createQuery("select s.sid from SmallClass s where s.name =?1");
		query.setParameter(1, name);
		
		return (Integer) query.getResultList().get(0);
	}
	
	public SmallClass findSmallClassBySid(int sid){
		Query query = entityManager.createQuery("select s from SmallClass s where s.sid =?1");
		query.setParameter(1, sid);
		
		return (SmallClass) query.getResultList().get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<SmallClass> findSmallClassShowFirst(){
		Query query = entityManager.createQuery("select s from SmallClass s where s.showfirst = '0'");
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SmallClass> findSmallClassByBigClassName(String name){
		Query query = entityManager.createQuery("select s from SmallClass s,BigClass b where b.bid = s.bid and b.name = 1?");
		query.setParameter(1, name);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SmallClass> findAllSmallClass(){
		Query query = entityManager.createQuery("select s from SmallClass s order by s.sort");
		
		return query.getResultList();
	}
	
	public void deleteSmallClassBySid(int sid){
		Query query = entityManager.createQuery("delete from SmallClass s where s.sid = ?1");
		query.setParameter(1, sid);
		
		query.executeUpdate();
	}
	
	public void deleteSmallClassByBid(int bid){
		Query query = entityManager.createQuery("delete from SmallClass s where s.bigclass.bid = ?1");
		query.setParameter(1, bid);
		
		query.executeUpdate();
	}
	
	public boolean findSmallClassBySort(int sort,int bid){
		Query query = entityManager.createQuery("select s from SmallClass s where s.sort = ?1 and s.bigclass.bid = ?2");
		query.setParameter(1, sort);
		query.setParameter(2, bid);
		
		if(query.getResultList().size() > 0){
			return false;
		}
		return true;
	}
	
	public boolean getSmallClassBySortAndSid(int sort,int sid,int bid){
		Query query = entityManager.createQuery("select s from SmallClass s where s.sort = ?1 and s.sid <> ?2 and s.bigclass.bid = ?3");
		query.setParameter(1, sort);
		query.setParameter(2, sid);
		query.setParameter(3, bid);
		
		if(query.getResultList().size() > 0){
			return false;
		}
		return true;
	}
	
	public void saveSmallClass(SmallClass smallclass){
		entityManager.merge(smallclass);
	}
	
	public void saveNewSmallClass(SmallClass smallclass){
		entityManager.persist(smallclass);
	}
}
