package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.OpenArea;

@Repository
public class OpenAreaDao extends AbstractDao<OpenArea>{

	protected OpenAreaDao() {
		super(OpenArea.class);
	}

	@SuppressWarnings("unchecked")
	public List<OpenArea> findAllOpenArea(){
		Query query = entityManager.createQuery("select o from OpenArea o");
		
		return query.getResultList();
	}
	
	public void saveOpenArea(OpenArea area){
		entityManager.merge(area);
	}
	
	public void saveNewOpenArea(OpenArea area){
		entityManager.persist(area);
	}
	
	public OpenArea findOpenAreaByAbridge(String abridge){
		Query query = entityManager.createQuery("select o from OpenArea o where o.abridge = ?1");
		query.setParameter(1, abridge);
		
		return (OpenArea)query.getSingleResult();
	}
	
	public boolean updateOpenAreaVisitNum(String abridge,int num){
		Query query = entityManager.createQuery("update OpenArea o set o.visit = ?1 where o.abridge = ?2");
		query.setParameter(1, num);
		query.setParameter(2, abridge);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
}
