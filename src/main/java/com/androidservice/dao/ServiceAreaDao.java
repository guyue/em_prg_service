package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.ServiceArea;

@Repository
public class ServiceAreaDao extends AbstractDao<ServiceArea>{
	
	public ServiceAreaDao(){
		super(ServiceArea.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<ServiceArea> findServideAresByCid(int cid){
		Query query = entityManager.createQuery("select s from ServiceArea s where s.compang.cid =?1");
		query.setParameter(1, cid);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<ServiceArea> findAllServiceArea(){
		Query query = entityManager.createQuery("select s from ServiceArea s");
		
		return query.getResultList();
	}
	
	public void saveNewServiceArea(ServiceArea area){
		entityManager.persist(area);
	}
	
	public void saveServiceArea(ServiceArea area){
		entityManager.merge(area);
	}
	
	public boolean deleteServiceAreaBySid(int sid){
		Query query = entityManager.createQuery("delete from ServiceArea s where s.sid = ?1");
		query.setParameter(1, sid);
		
		if(query.executeUpdate() > 0){
			return true;
		}
		return false;
	}
	
	public ServiceArea findServiceAreaBySid(int sid){
		Query query = entityManager.createQuery("select s from ServiceArea s where s.sid =?1");
		query.setParameter(1, sid);
		return (ServiceArea)query.getSingleResult();
	}

}
