package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.androidservice.bean.FourClass;

@Repository
public class FourClassDao extends AbstractDao<FourClass>{

	protected FourClassDao() {
		super(FourClass.class);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<FourClass> findFourClass(){
		Query query = entityManager.createQuery("select f from FourClass f order by f.fid");
		return query.getResultList();
	}
	
}
