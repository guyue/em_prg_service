package com.androidservice.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Opinion;

@Repository
public class OpinionDao extends AbstractDao<Opinion>{

	protected OpinionDao() {
		super(Opinion.class);
	}
	
	public void saveNewOPinionInfo(Opinion info){
		entityManager.persist(info);
	}
	
	public List<Opinion> findOpinionByPhone(String phone){
		return null;
	}

}
