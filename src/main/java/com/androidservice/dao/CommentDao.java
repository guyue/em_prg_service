package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.androidservice.bean.Comment;

@Repository
public class CommentDao extends AbstractDao<Comment>{

	public CommentDao() {
		super(Comment.class);
	}

	@SuppressWarnings("unchecked")
	public List<Comment> findShangxunCommentsByXid(int xid){
		Query query = entityManager.createQuery("select c from Comment c where c.type =?1 and c.typeId = ?2 order by c.updateTime desc");
		query.setParameter(1, "1".toString());
		query.setParameter(2, xid);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Comment> findCompangCommentsByGid(int gid){
		Query query = entityManager.createQuery("select c from Comment c where c.type =?1 and c.typeId = ?2 order by c.updateTime desc");
		query.setParameter(1, "0".toString());
		query.setParameter(2, gid);
		
		return query.getResultList();
	}
	
	public void saveNewComment(Comment comment){
		entityManager.persist(comment);
	}
	
}
