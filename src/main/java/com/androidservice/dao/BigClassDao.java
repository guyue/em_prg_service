package com.androidservice.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.androidservice.bean.BigClass;

@Repository
public class BigClassDao extends AbstractDao<BigClass> {
	
	public BigClassDao() {
		super(BigClass.class);
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<BigClass> getAllBigclass(){
		Query query = entityManager.createQuery("select b from BigClass b order by b.sort");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<BigClass> findBigclassByFid(int fid){
		Query query = entityManager.createQuery("select b from BigClass b where b.fourclass.fid = ?1 order by b.sort");
		query.setParameter(1, fid);
		return query.getResultList();
	}
	
	public BigClass findBigClassByBid(int bid){
		Query query = entityManager.createQuery("select b from BigClass b where b.bid = ?1");
		query.setParameter(1, bid);
		
		return (BigClass)query.getSingleResult();
	}
	
	public void DeleteBigClassByBid(int bid){
		Query query = entityManager.createQuery("delete from BigClass b where b.bid = ?1");
		query.setParameter(1, bid);
		
		query.executeUpdate();
	}
	
	public boolean getBigClassBySort(int sort){
		Query query = entityManager.createQuery("select b from BigClass b where b.sort = ?1");
		query.setParameter(1, sort);
		
		if(query.getResultList().size() > 0){
			return false;
		}
		return true;
	}
	
	public boolean getBigClassBySortAndBid(int sort,int bid){
		Query query = entityManager.createQuery("select b from BigClass b where b.sort = ?1 and b.bid <> ?2");
		query.setParameter(1, sort);
		query.setParameter(2, bid);
		
		if(query.getResultList().size() > 0){
			return false;
		}
		return true;
	}
	
	public void saveBigClass(BigClass bigclass){
		entityManager.merge(bigclass);
	}
	
	public void saveNewBigClass(BigClass bigclass){
		entityManager.persist(bigclass);
	}
}
