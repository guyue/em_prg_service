package com.androidservice.backstage.action;

//import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.RequestMap;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.androidservice.bean.Advertisement;
import com.androidservice.bean.BigClass;
import com.androidservice.bean.Commodity;
import com.androidservice.bean.Compang;
import com.androidservice.bean.Contect;
import com.androidservice.bean.FourClass;
import com.androidservice.bean.OpenArea;
import com.androidservice.bean.Picture;
import com.androidservice.bean.RecordHistory;
import com.androidservice.bean.Role;
import com.androidservice.bean.ServiceArea;
import com.androidservice.bean.SmallClass;
import com.androidservice.bean.User;
import com.androidservice.dao.AdvertisementDao;
import com.androidservice.dao.BigClassDao;
import com.androidservice.dao.CommodityDao;
import com.androidservice.dao.CompangDao;
import com.androidservice.dao.ContectDao;
import com.androidservice.dao.FourClassDao;
import com.androidservice.dao.OpenAreaDao;
import com.androidservice.dao.PictureDao;
import com.androidservice.dao.RecordHistoryDao;
import com.androidservice.dao.RoleDao;
import com.androidservice.dao.ServiceAreaDao;
import com.androidservice.dao.SmallClassDao;
import com.androidservice.dao.UserDao;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateDataAction extends ActionSupport implements SessionAware,ServletResponseAware, RequestAware{
	private static final long serialVersionUID = 2721916376072222628L;
	
	private HttpServletResponse response;
	
	@SuppressWarnings("rawtypes")
	private SessionMap session;

	private RequestMap request;
	
	private String aid;
	
	private String cid;
	
	private String yid;
	
	private String pid;
	
	private String bid;
	
	private String sid;
	
	private String fid;
	
	private String uid;
	
	private String oid;
	
	private String typeid;
	
	private String filePath;
	
	private String fileName;
	
	private String com_name;
	
	private String title;
	
	private String address;
	
	private String longitude;
	
	private String latitude;
	
	private String teltime;
	
	private String looktime;
	
	private String content_str;
	
	private String tel;
	
	private String contact;
	
	private String showfirst;
	
	private String checkstatus;
	
	private String comm_name;
	
	private String code;
	
	private String norm;
	
	private String price;
	
	private String comm_content;
	
	private String setcid;
	
	private String filePath2;
	
	private String fileName2;
	
	private String setsid;
	
	private String description;
	
	private String setbid;
	
	private String twomenu;
	
	private String threemenu;
	
	private String number;
	
	private String area;
	
	private String deals;
	
	private String reason;
	
	private String sort;
	
	private String role;
	
	private String username;
	
	private String password;
	
	private String authentication;
	
	private String expiration_time;
	
	private String old_password;
	
	private String new_password;
	
	private String long_lat;
	
	private String isopen;
	
	private String status;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private AdvertisementDao adverDao;
	
	@Autowired
	private FourClassDao fourDao;
	
	@Autowired
	private BigClassDao bigDao;
	
	@Autowired
	private SmallClassDao smallDao;
	
	@Autowired
	private CompangDao compangDao;
	
	@Autowired
	private CommodityDao commodityDao;
	
	@Autowired
	private PictureDao pictureDao;
	
	@Autowired
	private ContectDao contectDao;
	
	@Autowired
	private RecordHistoryDao recordDao;
	
	@Autowired
	private ServiceAreaDao serviceDao;
	
	@Autowired
	private OpenAreaDao openAreaDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void setRequest(Map map) {
		this.request = (RequestMap) map;
	}

	public RequestMap getRequest() {
		return request;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map  map) {
		this.session = (SessionMap) map;
	}

	@SuppressWarnings("rawtypes")
	public SessionMap getSession() {
		return session;
	}

	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getYid() {
		return yid;
	}

	public void setYid(String yid) {
		this.yid = yid;
	}
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getFid() {
		return fid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getTypeid() {
		return typeid;
	}

	public void setTypeid(String typeid) {
		this.typeid = typeid;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getCom_name() {
		return com_name;
	}

	public void setCom_name(String com_name) {
		this.com_name = com_name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getTeltime() {
		return teltime;
	}

	public void setTeltime(String teltime) {
		this.teltime = teltime;
	}

	public String getLooktime() {
		return looktime;
	}

	public void setLooktime(String looktime) {
		this.looktime = looktime;
	}

	public String getContent_str() {
		return content_str;
	}

	public void setContent_str(String content_str) {
		this.content_str = content_str;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getShowfirst() {
		return showfirst;
	}

	public void setShowfirst(String showfirst) {
		this.showfirst = showfirst;
	}

	public String getCheckstatus() {
		return checkstatus;
	}

	public void setCheckstatus(String checkstatus) {
		this.checkstatus = checkstatus;
	}

	public String getComm_name() {
		return comm_name;
	}

	public void setComm_name(String comm_name) {
		this.comm_name = comm_name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNorm() {
		return norm;
	}

	public void setNorm(String norm) {
		this.norm = norm;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getComm_content() {
		return comm_content;
	}

	public void setComm_content(String comm_content) {
		this.comm_content = comm_content;
	}

	public String getSetcid() {
		return setcid;
	}

	public void setSetcid(String setcid) {
		this.setcid = setcid;
	}

	public String getFilePath2() {
		return filePath2;
	}

	public void setFilePath2(String filePath2) {
		this.filePath2 = filePath2;
	}

	public String getFileName2() {
		return fileName2;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public String getSetsid() {
		return setsid;
	}

	public void setSetsid(String setsid) {
		this.setsid = setsid;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSetbid() {
		return setbid;
	}

	public void setSetbid(String setbid) {
		this.setbid = setbid;
	}

	public String getTwomenu() {
		return twomenu;
	}

	public void setTwomenu(String twomenu) {
		this.twomenu = twomenu;
	}

	public String getThreemenu() {
		return threemenu;
	}

	public void setThreemenu(String threemenu) {
		this.threemenu = threemenu;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDeals() {
		return deals;
	}

	public void setDeals(String deals) {
		this.deals = deals;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	public String getExpiration_time() {
		return expiration_time;
	}

	public void setExpiration_time(String expiration_time) {
		this.expiration_time = expiration_time;
	}

	public String getOld_password() {
		return old_password;
	}

	public void setOld_password(String old_password) {
		this.old_password = old_password;
	}

	public String getNew_password() {
		return new_password;
	}

	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	public String getLong_lat() {
		return long_lat;
	}

	public void setLong_lat(String long_lat) {
		this.long_lat = long_lat;
	}

	public String getIsopen() {
		return isopen;
	}

	public void setIsopen(String isopen) {
		this.isopen = isopen;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private void RecordUserAction(String action){
		User user = (User)session.get("user");
		RecordHistory record = new RecordHistory();
		record.setUsername(user.getName());
		record.setOperation(action);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		record.setUpdatetime(now);
		recordDao.saveNewRecordHistory(record);
	}
	
	@SuppressWarnings("unchecked")
	public String updateAdvert(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		int type_id = 0;
		if(null != typeid){
			type_id = Integer.parseInt(typeid);
		}
		
		Advertisement adver = null;
		if(null == aid){
			adver = new Advertisement();
		}else{
			adver = adverDao.findAdvertByAid(id);
		}
		adver.setTypeId(type_id);
		if(null != status){
			adver.setType(status);
		}
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(adver.getImage())){
				if(null != adver.getImage()){
					String path = ServletActionContext.getServletContext().getRealPath(adver.getPath() + adver.getImage());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			adver.setImage(fileName);
		}
		if(null != filePath && filePath.length() > 0){
			adver.setPath(filePath);
		}
		if(null == aid){
//			adver.setType("0");
			adver.setCheckstatus(1);
			adverDao.saveNewAdvertisement(adver);
			RecordUserAction("Add a new advertisement for type_id is " + typeid);
		}else{
			int status = 0;
			if(null != checkstatus){
				status = Integer.parseInt(checkstatus);
			}
			adver.setCheckstatus(status);
			adverDao.saveAdvertisement(adver);
			RecordUserAction("Update a advertisement id is " + aid);
		}
		List<Advertisement> ad = null;
		if(null != status){
			if(status.equals("0")){
				ad = adverDao.findAdvrByType("0");
			}else if(status.equals("1")){
				ad = adverDao.findAdvrByType("1");
			}
		}
		session.put("advers", ad);
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("ad_compang", compang);
		request.put("ad_type", status);
		return "success";
	}
	
	public String updateCompang(){	
		User user = (User) session.get("user");
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		Compang compang = null;
		if(null == cid){
			compang = new Compang();
		}else{
			compang = compangDao.findCompangByCidWithoutStatus(id);
		}
		int id2 = 0;
		if(null != setsid){
			id2 = Integer.parseInt(setsid);
		}
		SmallClass smallclass = smallDao.findSmallClassBySid(id2);
		compang.setSmallClass(smallclass);
		
		compang.setAddress(address);
		compang.setLocation(address);
		compang.setContact(contact);
		compang.setContent(content_str);
		compang.setArea(area);
		
		int num = 0;
		if(null != number && number.length() > 0){
			num = Integer.parseInt(number);
		}
		compang.setNumber(num);
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(compang.getImage())){
				if(null != compang.getImage()){
					String path = ServletActionContext.getServletContext().getRealPath(compang.getPath() + compang.getImage());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			compang.setImage(fileName);
		}
		if(null != filePath && filePath.length() > 0){
			filePath = filePath.trim();
			compang.setPath(filePath);
		}
		float lat = 0;
		if(null != long_lat){
			String[] lat_lon = long_lat.split(",");
			if(lat_lon[0].length() > 0){
				lat = Float.parseFloat(lat_lon[0]);
				lat =(float)(lat - 0.006);
				compang.setLongitude(lat);
			}
			if(lat_lon[1].length() > 0){
				lat = Float.parseFloat(lat_lon[1]);
				lat =(float)(lat - 0.006);
				compang.setLatitude(lat);
			}
		}
		
//		if(null != latitude && latitude.length() > 0){
//			lat = Float.parseFloat(latitude);
//			compang.setLatitude(lat);
//		}
//		
//		float lon = 0;
//		if(null != longitude && longitude.length() > 0){
//			lon = Float.parseFloat(longitude);
//			compang.setLongitude(lon);
//		}
		
		if(null != checkstatus){
			if(checkstatus.equals("0")){
				compang.setCheckStatus((byte)0);
			}else{
				
				compang.setCheckStatus((byte)1);
			}
		}
		if(null != authentication){
			if(authentication.equals("0")){
				compang.setAuthentication((byte)0);
			}else{
				compang.setAuthentication((byte)1);
			}
		}
		if(null != expiration_time){
			DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
			format.setLenient(false);
			try {
				Timestamp  ts = new Timestamp(format.parse(expiration_time).getTime());
				compang.setExpirationtime(ts);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		if(null != showfirst){
			if(showfirst.equals("0")){
				compang.setShowfirst((byte)0);
			}else{
				compang.setShowfirst((byte)1);
			}
		}
		compang.setName(com_name);
		if(null != looktime){
			compang.setLooktime(Integer.parseInt(looktime));
		}
		compang.setSlogan(title);
		compang.setTel(tel);
		if(null != teltime){
			compang.setTeltime(Integer.parseInt(teltime));
		}
		compang.setTitle(title);
		
		if(null == cid){
			compang.setUser(user);
			Timestamp now = new Timestamp(System.currentTimeMillis());
			compang.setUpdateDatetime(now);
			compang.setAuthentication((byte)1);
			compang.setCommentsize(0);
			compang.setDistance("");
			compang.setGrade(0);
			compang.setManage("");
			compang.setPreferential((byte)1);
			compang.setRecommend((byte)1);
			compang.setStars(0);
			//compang.setShowfirst((byte)1);
			compang.setCheckStatus((byte)1);
			compangDao.saveNewCompang(compang);
			RecordUserAction("Add a new compang name for " + com_name);
		}else{
			compangDao.saveCompang(compang);
			RecordUserAction("Update a compang id is " + cid);
		}
		
		if(null == cid){
			Compang newcompang = compangDao.findCompangByNameWithoutStatus(com_name);
			List<BigClass> big = bigDao.getAllBigclass();
			List<SmallClass> small = smallDao.getSmallClassByBid(newcompang.getSmallClass().getBigclass().getBid());
			request.put("bigclass", big);
			request.put("smallclass", small);
			request.put("compang", newcompang);
			return "success1";
		}
		request.put("compang", compang); 
		return "success";
	}
	
	public String updateCommodity(){
		int id1 = 0;
		if(null != setcid){
			id1 = Integer.parseInt(setcid);
		}
		int id2 = 0;
		if(null != yid){
			id2 = Integer.parseInt(yid);
		}
		Commodity commodity = null;
		Compang compang = compangDao.findCompangByCidWithoutStatus(id1);
		if(yid == null){
			commodity = new Commodity();
		}else{
			commodity = commodityDao.findCommodityByYid(id2);
		}
		commodity.setCode(code);
		commodity.setCompang(compang);
		commodity.setContent(comm_content);
		if(null != fileName2 && fileName2.length() > 0){
			if(!fileName.equals(commodity.getDetail_img())){
				if(null != commodity.getDetail_img()){
					String path = ServletActionContext.getServletContext().getRealPath(commodity.getDetail_img() + commodity.getDetail_img());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			commodity.setDetail_img(fileName2);
		}
		if(null != filePath2 && filePath2.length() > 0){
			commodity.setDetail_path(filePath2);
		}
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(commodity.getImage())){
				if(null != compang.getImage()){
					String path = ServletActionContext.getServletContext().getRealPath(commodity.getPath() + commodity.getImage());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			commodity.setImage(fileName);
		}
		if(null != filePath && filePath.length() > 0){
			commodity.setPath(filePath);
		}
		commodity.setName(comm_name);
		commodity.setNorm(norm);
		commodity.setPrice(price);
		commodity.setDeals(deals);
		if(null == yid){
			commodityDao.saveNewCommodity(commodity);
			RecordUserAction("Add a new commodity name for " + comm_name);
		}else{
			commodityDao.saveCommodity(commodity);
			RecordUserAction("Update a commodity id is " + yid);
		}
		
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.getSmallClassByBid(compang.getSmallClass().getBigclass().getBid());
		request.put("bigclass", big);
		request.put("smallclass", small);
		request.put("compang", compang);
		
//		List<Compang> compang_list = null;
//		User user = (User)session.get("user");
//		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
//			compang_list = compangDao.findAllCompangs();
//		}else{
//			if(user.getRole().getRid() == 4){
//				compang_list = compangDao.findNumberByRoleAndArea(user.getAddress());
//			}else{
//				compang_list = compangDao.findComapngByUid(user.getUid());
//			}
//		}
//		session.put("compang", compang_list);
		
		return "success";
	}
	
	public String updatePicture(){
		int id1 = 0;
		if(null != setcid){
			id1 = Integer.parseInt(setcid);
		}
		int id2 = 0;
		if(null != pid){
			id2 = Integer.parseInt(pid);
		}
		Picture picture = null;
		Compang compang = compangDao.findCompangByCidWithoutStatus(id1);
		if(null == pid){
			picture = new Picture();
		}else{
			picture = pictureDao.findPictureByPid(id2);
		}
		picture.setCompang(compang);
		picture.setDescription(description);
		if(null != fileName2 && fileName2.length() > 0){
			picture.setBig(fileName2);
		}else{
			if(null != fileName && fileName.length() > 0){
				picture.setBig(fileName);
			}
		}
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(picture.getSmall())){
				if(null != picture.getSmall()){
					String path = ServletActionContext.getServletContext().getRealPath(picture.getPath() + picture.getSmall());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			picture.setSmall(fileName);
		}
		if(null != filePath && filePath.length() > 0){
			picture.setPath(filePath);
		}
		if(null == pid){
			pictureDao.saveNewPicture(picture);
			RecordUserAction("Add a new picture for compang name is " + compang.getName());
		}else{
			pictureDao.savePicture(picture);
			RecordUserAction("Update a picture id is " + pid);
		}
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.getSmallClassByBid(compang.getSmallClass().getBigclass().getBid());
		request.put("bigclass", big);
		request.put("smallclass", small);
		request.put("compang", compang);
		return "success";
	}
	
	public String updateContect(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		Contect contect = null;
		if(aid == null){
			contect = new Contect();
		}else{
			contect = contectDao.findContectByAid(id);
		}
		contect.setAddress(area);
		contect.setContent(content_str);
		contect.setTelphone(tel);
		
		if(aid == null){
			contectDao.saveNewContect(contect);
			RecordUserAction("Add a new contect for phone is " + tel);
		}else{
			contectDao.saveContect(contect);
			RecordUserAction("Update a contect id is " + aid);
		}
		
		List<Contect> contect2 = contectDao.findContect();
		request.put("contect", contect2);
		return "success";
	}
	
	public String updateTwoMenu(){
		int id1 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
//		int id2 = 0;
//		if(null != fid){
//			id2 = Integer.parseInt(fid);
//		}
		BigClass bigclass = null;
		
		if(bid == null){
			bigclass = new BigClass();
		}else{
			bigclass = bigDao.findBigClassByBid(id1);
		}
		bigclass.setName(twomenu);
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(bigclass.getImage())){
				if(null != bigclass.getImage()){
					String path = ServletActionContext.getServletContext().getRealPath(bigclass.getPath() + bigclass.getImage());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			bigclass.setImage(fileName);
		}
		if(null != filePath && filePath.length() > 0){
			bigclass.setPath(filePath);
		}
		if(null != sort){
			int num = 0;
			num = Integer.parseInt(sort);
			bigclass.setSort(num);
		}
		if(null == bid){
			FourClass four = fourDao.find(1);
			bigclass.setFourclass(four);
			bigDao.saveNewBigClass(bigclass);
			RecordUserAction("Add a new two menu for name is " + twomenu);
		}else{
			bigDao.saveBigClass(bigclass);
			RecordUserAction("Update a two menu id is " + bid);
		}
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = null;
		if(null == bid){
			if(big.get(0).getBid() != 1){
				 request.put("showbid", big.get(0).getBid());
				 small =  smallDao.getSmallClassByBid(big.get(0).getBid());
			 }else{
				 if(big.size() > 1){
					 request.put("showbid", big.get(1).getBid()); 
					 small =  smallDao.getSmallClassByBid(big.get(1).getBid());
				 }
			 }
		}else{
			small =  smallDao.getSmallClassByBid(id1);
			request.put("showbid", id1);
		}
		
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
	public String updateThreeMenu(){
		int id1 = 0;
		if(null != sid){
			id1 = Integer.parseInt(sid);
		}
		int id2 = 0;
		if(null != bid){
			id2 = Integer.parseInt(bid);
		}
		SmallClass smallclass = null;
		if(null == sid){
			smallclass = new SmallClass();
		}else{
			smallclass = smallDao.findSmallClassBySid(id1);
		}
		smallclass.setName(threemenu);
		if(null != fileName && fileName.length() > 0){
			if(!fileName.equals(smallclass.getImage())){
				if(null != smallclass.getImage()){
					String path = ServletActionContext.getServletContext().getRealPath(smallclass.getPath() + smallclass.getImage());
					File dir = new File(path);
					if (dir.exists()) {
						dir.delete();
					}
				}
			}
			smallclass.setImage(fileName);
		}else{
			smallclass.setImage("");
		}
		if(null != filePath && filePath.length() > 0){
			smallclass.setPath(filePath);
		}
		int num = 0;
		if(null != sort && sort.length() > 0){
			num = Integer.parseInt(sort);
		}
		smallclass.setSort(num);
		
		if(null != showfirst){
			if(showfirst.equals("0")){
				smallclass.setShowfirst(0);
			}else{
				smallclass.setShowfirst(1);
			}
		}
		if(null == sid){
			BigClass big = bigDao.findBigClassByBid(id2);
			smallclass.setBigclass(big);
			smallDao.saveNewSmallClass(smallclass);
			RecordUserAction("Add a new three menu for name is " + threemenu);
		}else{
			smallDao.saveSmallClass(smallclass);
			RecordUserAction("Update a three menu id is " + sid);
		}
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = null;
		if(null == bid){
			if(big.get(0).getBid() != 1){
				 request.put("showbid", big.get(0).getBid());
				 small = smallDao.getSmallClassByBid(big.get(0).getBid());
			 }else{
				 if(big.size() > 1){
					 request.put("showbid", big.get(1).getBid()); 
					 small = smallDao.getSmallClassByBid(big.get(1).getBid());
				 }
			 }
		}else{
			request.put("showbid", id2);
			small = smallDao.getSmallClassByBid(id2);
		}
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
	public String updateServiceArea(){
		int id1 = 0;
		if(null != sid){
			id1 = Integer.parseInt(sid);
		}
		int id2 = 0;
		if(null != setcid){
			id2 = Integer.parseInt(setcid);
		}
		ServiceArea service = null;
		if(null == sid){
			service = new ServiceArea();
		}else{
			service = serviceDao.findServiceAreaBySid(id1);
		}
		Compang compang = compangDao.findCompangByCidWithoutStatus(id2);
		service.setArea(area);
		service.setCompang(compang);
		if(null == sid){
			serviceDao.saveNewServiceArea(service);
			RecordUserAction("Add a new service area for compang name is " + compang.getName());
		}else{
			serviceDao.saveServiceArea(service);
			RecordUserAction("Update a service area id is " + sid);
		}
		List<ServiceArea> service2 = serviceDao.findAllServiceArea();
		request.put("service", service2);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String checkAdver(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		int status = 1;
		if(null != checkstatus){
			status = Integer.parseInt(checkstatus);
		}
		Advertisement adver = adverDao.findAdvertByAid(id);
		adver.setCheckstatus(status);
		adverDao.saveAdvertisement(adver);
		RecordUserAction("Update a adver's status id is " + aid);
		
		List<Advertisement> ad = adverDao.findAllAdvertisement();
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		session.put("advers", ad);
		request.put("ad_compang", compang);
		return "success";
	}

	@SuppressWarnings("unchecked")
	public String updateCompangCheckstatus(){
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		int status = 1;
		if(null != checkstatus){
			status = Integer.parseInt(checkstatus);
		}
		String open_check = (String)session.get("open_check");
		
		Compang  compang = compangDao.findCompangByCidWithoutStatus(id);
		int oldstatus = compang.getCheckStatus();
		boolean recover = false;
		if(compang.getCheckStatus() == 3 && status == 0){
			recover = true;
		}
			
		compang.setCheckStatus((byte)status);
		compang.setReason(reason);
		compangDao.saveCompang(compang);
		RecordUserAction("Update a compang's status id is " + cid);
		
		User user = (User)session.get("user");
		List<Compang> companglist = null;
		int start = 0;
		String page_num = (String)session.get("page");
		if(null != page_num && page_num.length() > 0){
			start = (Integer.parseInt(page_num) - 1) * 10;
		}
		int totalcompang = 0;
		if(recover){
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				totalcompang = compangDao.findComapngByCheckStatus(3).size();
				companglist = compangDao.findComapngByCheckStatusAndNum(3, start);
			}else if(user.getRole().getRid() == 4){
				totalcompang = compangDao.findCompangByAreaAndStatus(3,user.getAddress()).size();
				companglist = compangDao.findCompangByAreaAndStatusAndNum(3,user.getAddress(), start);
			}
			session.put("totalcompang",totalcompang);
			request.put("compang", companglist);
			return "success1";
		}else{
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				if(null != open_check && open_check.equals("0")){
					totalcompang = compangDao.findComapngByCheckStatus(oldstatus).size();
					companglist = compangDao.findComapngByCheckStatusAndNum(oldstatus,start);
				}else{
					totalcompang = compangDao.findAllCompangs().size();
					companglist = compangDao.findAllCompangsByNum(start);
				}
			}else{
				if(user.getRole().getRid() == 4){
					if(null != open_check && open_check.equals("0")){
						totalcompang = compangDao.findCompangByAreaAndStatus(oldstatus,user.getAddress()).size();
						companglist = compangDao.findCompangByAreaAndStatusAndNum(oldstatus,user.getAddress(),start);
					}else{
						totalcompang = compangDao.findNumberByRoleAndArea(user.getAddress()).size();
						companglist = compangDao.findNumberByRoleAndAreaAndNum(user.getAddress(),start);
					}
				}else{
					totalcompang = compangDao.findComapngByUid(user.getUid()).size();
					companglist = compangDao.findComapngByUidAndNum(user.getUid(),start);
				}
			}
		}
		session.put("totalcompang",totalcompang);
		session.put("compang", companglist);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String UpdateAdverStatus(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		int new_status = 1;
		if(null != checkstatus){
			new_status = Integer.parseInt(checkstatus);
		}
		Advertisement adver = adverDao.findAdvertByAid(id);
		adver.setCheckstatus(new_status);
		adver.setReason(reason);
		adverDao.saveAdvertisement(adver);
		RecordUserAction("Update a adver's status id is " + cid);
		
		List<Advertisement> ad = null;
		if(null != status){
			ad = adverDao.findAdvrByType(status);
		}
		session.put("advers", ad);
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("ad_compang", compang);
		request.put("ad_type", status);
		return "success";
	}
	
	public String RecommendCompang(){
//		User user = (User)session.get("user");
		int show = 1;
		if(null != showfirst && showfirst.length() > 0){
			show = Integer.parseInt(showfirst);
		}
		int id = 0;
		if(null != cid && cid.length() > 0){
			id = Integer.parseInt(cid);
		}
		Compang com = compangDao.findCompangByCidWithoutStatus(id);
		com.setShowfirst((byte)show);
		compangDao.saveCompang(com);
		RecordUserAction("Update a compang's showfirst id is " + cid);
		
		List<Compang> compang = compangDao.findAllCompagsByStatus();
//		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2)){
//			compang = compangDao.findAllCompagsByStatus();
//		}else{
//			if(user.getRole().getRid() == 3){
//				compang = compangDao.findNumberByStatusAndRole(0,3);
//			}
//		}
				
		request.put("recommend_compang", compang);
		return "success";
	}
	
	public String ChangeUserRole(){
		int id = 0;
		if(null != uid && uid.length() > 0){
			id = Integer.parseInt(uid);
		}
		User user = userDao.findUserByUid(id);
		Role ro = null;
		if(null != role){
			int rid = 0;
			rid = Integer.parseInt(role);
			ro = roleDao.getRoleByRid(rid);
		}
		user.setRole(ro);
		userDao.saveUser(user);
		RecordUserAction("Update a user's role id is " + uid);
		
		List<User> user_list = userDao.findAllUser();
		request.put("all_user", user_list);
		return "success";
	}
	
	public String UpdateUser(){
		int id = 0;
		if(null != uid && uid.length() > 0){
			id = Integer.parseInt(uid);
		}
		User user = null;
		if(null != uid){
			user = userDao.findUserByUid(id);
		}else{
			user = new User();
		}
		user.setName(username);
		user.setPassword(password);
		user.setPhone(tel);
		user.setAddress(address);
		Role ro = null;
		if(null != role){
			int rid = 0;
			rid = Integer.parseInt(role);
			ro = roleDao.getRoleByRid(rid);
		}
		user.setRole(ro);
		user.setEmail("");
		if(null != uid){
			userDao.saveUser(user);
			RecordUserAction("Update a user's content id is " + uid);
		}else{
			userDao.saveNewUser(user);
			RecordUserAction("Add a new user name id is " + username);
		}
		
		List<User> user_list = userDao.findAllUser();
		request.put("all_user", user_list);
		return "success";
	}
	
	public String ChangePassword(){
		User user = (User)session.get("user");
		if(null != old_password){
			if(userDao.findUserByName(user.getName(), old_password)){
				user.setPassword(new_password);
				userDao.saveUser(user);
				request.put("modifyPassword_status", "0");
			}else{
				request.put("modifyPassword_status", "1");
			}
		}
		return "success";
	}
	
	public String SetThreeMenu(){
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		SmallClass smallclass = smallDao.findSmallClassBySid(id);
		if(null != showfirst){
			int show = 0;
			show = Integer.parseInt(showfirst);
			smallclass.setShowfirst(show);
		}
		smallDao.saveSmallClass(smallclass);
		
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.findAllSmallClass();
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
	public String SetOpenPlace(){
		int id = 0;
		if(null != oid){
			id = Integer.parseInt(oid);
		}
		OpenArea open = openAreaDao.find(id);
		if(null != isopen){
			if(isopen.equals("1")){
				open.setIsopen((byte)1);
			}else{
				open.setIsopen((byte)0);
			}
		}
		openAreaDao.saveOpenArea(open);
		
		List<OpenArea> openArea = openAreaDao.findAllOpenArea();
		
		String url = "";
		String http = "";
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
//		http = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort();
		http = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + "8080";
		
		for(int i = 0;i < openArea.size();i ++){
			if(!openArea.get(i).getAbridge().equals("nj")){
				url = http + "/" + openArea.get(i).getPackagename() + httpRequest.getServletPath() + "?oid=" + oid + "&isopen=" + isopen;
				try {
//					url = http + "/tz" + httpRequest.getServletPath() + "?oid=" + oid + "&isopen=" + isopen;
					URL connection = new URL(url);
					HttpURLConnection urlConnection = (HttpURLConnection)connection.openConnection();
					urlConnection.setRequestMethod("POST");  
					urlConnection.connect();
					InputStream in = urlConnection.getInputStream();
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
//		try {
//			url = http + "/tz" + httpRequest.getServletPath() + "?oid=" + oid + "&isopen=" + isopen;
//			URL connection = new URL(url);
//			HttpURLConnection urlConnection = (HttpURLConnection)connection.openConnection();
//			urlConnection.setRequestMethod("POST");  
//			urlConnection.connect();
//			InputStream in = urlConnection.getInputStream();
//			in.close();
//			
//			url = http + "/nt" + httpRequest.getServletPath() + "?oid=" + oid + "&isopen=" + isopen;
//			URL connection_nt = new URL(url);
//			HttpURLConnection urlConnection_nt = (HttpURLConnection)connection_nt.openConnection();
//			urlConnection_nt.setRequestMethod("POST");  
//			urlConnection_nt.connect();
//			InputStream in_nt = urlConnection_nt.getInputStream();
//			in_nt.close();
//			
//			url = http + "/ha" + httpRequest.getServletPath() + "?oid=" + oid + "&isopen=" + isopen;
//			URL connection_ha = new URL(url);
//			HttpURLConnection urlConnection_ha = (HttpURLConnection)connection_ha.openConnection();
//			urlConnection_ha.setRequestMethod("POST");  
//			urlConnection_ha.connect();
//			InputStream in_ha = urlConnection_ha.getInputStream();
//			in_ha.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		return "success";
	}
}
