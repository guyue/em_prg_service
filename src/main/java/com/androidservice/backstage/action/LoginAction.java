package com.androidservice.backstage.action;

//import java.io.PrintWriter;
import java.io.File;
import java.io.PrintWriter;
//import java.io.UnsupportedEncodingException;
//import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.RequestMap;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;

import com.androidservice.bean.Advertisement;
import com.androidservice.bean.BigClass;
import com.androidservice.bean.Commodity;
import com.androidservice.bean.Compang;
import com.androidservice.bean.Contect;
import com.androidservice.bean.OpenArea;
import com.androidservice.bean.Place;
//import com.androidservice.bean.FourClass;
import com.androidservice.bean.Picture;
import com.androidservice.bean.RecordHistory;
import com.androidservice.bean.ServiceArea;
import com.androidservice.bean.SmallClass;
import com.androidservice.bean.User;
import com.androidservice.dao.AdvertisementDao;
import com.androidservice.dao.BigClassDao;
import com.androidservice.dao.CommodityDao;
import com.androidservice.dao.CompangDao;
import com.androidservice.dao.ContectDao;
import com.androidservice.dao.FourClassDao;
import com.androidservice.dao.OpenAreaDao;
import com.androidservice.dao.PictureDao;
import com.androidservice.dao.PlaceDao;
import com.androidservice.dao.RecordHistoryDao;
import com.androidservice.dao.ServiceAreaDao;
import com.androidservice.dao.SmallClassDao;
import com.androidservice.dao.UserDao;
import com.androidservice.util.MiscUtils;
//import com.androidservice.util.MiscUtils;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport implements SessionAware,ServletResponseAware, RequestAware{
	private static final long serialVersionUID = 1L;
	
	private HttpServletResponse response;

	private String name;
	
	private String password;
	
	private String aid;
	
	private String cid;
	
	private String yid;
	
	private String pid;
	
	private String sid;
	
	private String bid;
	
	private String fid;
	
	private String uid;
	
	private String start;
	
	private String number;
	
	private String selectnum;
	
	private String showbylook;
	
	private String area;
	
	private String areanum;
	
	private String check;
	
	private String sort;
	
	private String city;
	
	private String checkname;
	
	private String province;
	
	private String status;
	
	private String type;
	
	private String pagenumber;
	
	private String search;
	
	private String showpage;
	
	private String page;
	
	private String enterbtnnum;
	
	private String showfirst;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private AdvertisementDao adverDao;
	
	@Autowired
	private FourClassDao fourDao;
	
	@Autowired
	private BigClassDao bigDao;
	
	@Autowired
	private SmallClassDao smallDao;
	
	@Autowired
	private CompangDao compangDao;
	
	@Autowired
	private CommodityDao commodityDao;
	
	@Autowired
	private PictureDao pictureDao;
	
	@Autowired
	private ContectDao contectDao;
	
	@Autowired
	private RecordHistoryDao recordDao;
	
	@Autowired
	private ServiceAreaDao serviceDao;
	
	@Autowired
	private PlaceDao placeDao;
	
	@Autowired
	private OpenAreaDao openareaDao;
	
	@SuppressWarnings("rawtypes")
	private SessionMap session;

	private RequestMap request;

	@SuppressWarnings("rawtypes")
	@Override
	public void setRequest(Map map) {
		this.request = (RequestMap) map;
	}

	public RequestMap getRequest() {
		return request;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map  map) {
		this.session = (SessionMap) map;
	}

	@SuppressWarnings("rawtypes")
	public SessionMap getSession() {
		return session;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getAid() {
		return aid;
	}

	public void setAid(String aid) {
		this.aid = aid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getYid() {
		return yid;
	}

	public void setYid(String yid) {
		this.yid = yid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSelectnum() {
		return selectnum;
	}

	public void setSelectnum(String selectnum) {
		this.selectnum = selectnum;
	}

	public String getShowbylook() {
		return showbylook;
	}

	public void setShowbylook(String showbylook) {
		this.showbylook = showbylook;
	}
	
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAreanum() {
		return areanum;
	}

	public void setAreanum(String areanum) {
		this.areanum = areanum;
	}

	public String getCheck() {
		return check;
	}

	public void setCheck(String check) {
		this.check = check;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCheckname() {
		return checkname;
	}

	public void setCheckname(String checkname) {
		this.checkname = checkname;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPagenumber() {
		return pagenumber;
	}

	public void setPagenumber(String pagenumber) {
		this.pagenumber = pagenumber;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getShowpage() {
		return showpage;
	}

	public void setShowpage(String showpage) {
		this.showpage = showpage;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getEnterbtnnum() {
		return enterbtnnum;
	}

	public void setEnterbtnnum(String enterbtnnum) {
		this.enterbtnnum = enterbtnnum;
	}

	public String getShowfirst() {
		return showfirst;
	}

	public void setShowfirst(String showfirst) {
		this.showfirst = showfirst;
	}

	@SuppressWarnings("unchecked")
	public String Login(){
		if(userDao.findUserByName(name, password)){
			User user = userDao.findUserByName(name);
			int num1= 0;
			int num2= 0;
			int num3= 0;
			int num4= 0;
			int num5= 0;
			int num6= 0;
			Calendar start_time = Calendar.getInstance();
			Calendar end_time = Calendar.getInstance();
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				num1 = compangDao.findComapngByCheckStatus(0).size();
				num2 = compangDao.findComapngByCheckStatus(1).size();
				num3 = compangDao.findComapngByCheckStatus(2).size();
				num4 = compangDao.findComapngByCheckStatus(3).size();
				num5 = compangDao.findComapngByCheckStatus(4).size();
				List<Compang> compang = compangDao.findComapngByCheckStatus(0);
				for(int i = 0;i < compang.size();i ++){
					start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
					end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
					int month = 0;
					int start_year = start_time.get(Calendar.YEAR);
					int start_month = start_time.get(Calendar.MONTH);
					int end_year = end_time.get(Calendar.YEAR);
					int end_month = end_time.get(Calendar.MONTH);
					if(end_year > start_year){
						month = end_month + (end_year - start_year) * 12 - start_month;
					}else{
						if(end_month > start_month){
							month = end_month - start_month;
						}
					}
					if(month <= 1){
						num6 ++;
					}
				}
			}else{
				if(user.getRole().getRid() == 4){
					num1 = compangDao.findCompangByAreaAndStatus(0,user.getAddress()).size();
					num2 = compangDao.findCompangByAreaAndStatus(1,user.getAddress()).size();
					num3 = compangDao.findCompangByAreaAndStatus(2,user.getAddress()).size();
					num4 = compangDao.findCompangByAreaAndStatus(3,user.getAddress()).size();
					num5 = compangDao.findCompangByAreaAndStatus(4,user.getAddress()).size();
					List<Compang> compang = compangDao.findCompangByAreaAndStatus(0,user.getAddress());
					for(int i = 0;i < compang.size();i ++){
						start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
						end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
						int month = 0;
						int start_year = start_time.get(Calendar.YEAR);
						int start_month = start_time.get(Calendar.MONTH);
						int end_year = end_time.get(Calendar.YEAR);
						int end_month = end_time.get(Calendar.MONTH);
						if(end_year > start_year){
							month = end_month + (end_year - start_year) * 12 - start_month;
						}else{
							if(end_month > start_month){
								month = end_month - start_month;
							}
						}
						if(month <= 1){
							num6 ++;
						}
					}
				}else{
					num1 = compangDao.findCompangByStatusAndUid(0, user.getUid()).size();
					num2 = compangDao.findCompangByStatusAndUid(1, user.getUid()).size();
					num3 = compangDao.findCompangByStatusAndUid(2, user.getUid()).size();
					num4 = compangDao.findCompangByStatusAndUid(3, user.getUid()).size();
					num5 = compangDao.findCompangByStatusAndUid(4, user.getUid()).size();
					List<Compang> compang = compangDao.findCompangByStatusAndUid(0, user.getUid());
					for(int i = 0;i < compang.size();i ++){
						start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
						end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
						int month = 0;
						int start_year = start_time.get(Calendar.YEAR);
						int start_month = start_time.get(Calendar.MONTH);
						int end_year = end_time.get(Calendar.YEAR);
						int end_month = end_time.get(Calendar.MONTH);
						if(end_year > start_year){
							month = end_month + (end_year - start_year) * 12 - start_month;
						}else{
							if(end_month > start_month){
								month = end_month - start_month;
							}
						}
						if(month <= 1){
							num6 ++;
						}
					}
				}
			}
			request.put("num1", num1);
			request.put("num2", num2);
			request.put("num3", num3);
			request.put("num4", num4);
			request.put("num5", num5);
			request.put("num6", num6);
			session.put("user", user);
			return "success";
		}else{
			if(null == userDao.findUserByName(name)){
				request.put("login_status", "1");
			}else{
				request.put("login_status", "2");
			}
		}
		return "false";
	}
	
	public String getCompangByStatus(){
		User user = (User) session.get("user");
		int num1= 0;
		int num2= 0;
		int num3= 0;
		int num4= 0;
		int num5= 0;
		int num6= 0;
		Calendar start_time = Calendar.getInstance();
		Calendar end_time = Calendar.getInstance();
		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
			num1 = compangDao.findComapngByCheckStatus(0).size();
			num2 = compangDao.findComapngByCheckStatus(1).size();
			num3 = compangDao.findComapngByCheckStatus(2).size();
			num4 = compangDao.findComapngByCheckStatus(3).size();
			num5 = compangDao.findComapngByCheckStatus(4).size();
			List<Compang> compang = compangDao.findComapngByCheckStatus(0);
			for(int i = 0;i < compang.size();i ++){
				start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
				end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
				int month = 0;
				int start_year = start_time.get(Calendar.YEAR);
				int start_month = start_time.get(Calendar.MONTH);
				int end_year = end_time.get(Calendar.YEAR);
				int end_month = end_time.get(Calendar.MONTH);
				if(end_year > start_year){
					month = end_month + (end_year - start_year) * 12 - start_month;
				}else{
					if(end_month > start_month){
						month = end_month - start_month;
					}
				}
				if(month <= 1){
					num6 ++;
				}
			}
		}else{
			if(user.getRole().getRid() == 4){
				num1 = compangDao.findCompangByAreaAndStatus(0,user.getAddress()).size();
				num2 = compangDao.findCompangByAreaAndStatus(1,user.getAddress()).size();
				num3 = compangDao.findCompangByAreaAndStatus(2,user.getAddress()).size();
				num4 = compangDao.findCompangByAreaAndStatus(3,user.getAddress()).size();
				num5 = compangDao.findCompangByAreaAndStatus(4,user.getAddress()).size();
				List<Compang> compang = compangDao.findCompangByAreaAndStatus(0,user.getAddress());
				for(int i = 0;i < compang.size();i ++){
					start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
					end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
					int month = 0;
					int start_year = start_time.get(Calendar.YEAR);
					int start_month = start_time.get(Calendar.MONTH);
					int end_year = end_time.get(Calendar.YEAR);
					int end_month = end_time.get(Calendar.MONTH);
					if(end_year > start_year){
						month = end_month + (end_year - start_year) * 12 - start_month;
					}else{
						if(end_month > start_month){
							month = end_month - start_month;
						}
					}
					if(month <= 1){
						num6 ++;
					}
				}
			}else{
				num1 = compangDao.findCompangByStatusAndUid(0, user.getUid()).size();
				num2 = compangDao.findCompangByStatusAndUid(1, user.getUid()).size();
				num3 = compangDao.findCompangByStatusAndUid(2, user.getUid()).size();
				num4 = compangDao.findCompangByStatusAndUid(3, user.getUid()).size();
				num5 = compangDao.findCompangByStatusAndUid(4, user.getUid()).size();
				List<Compang> compang = compangDao.findCompangByStatusAndUid(0, user.getUid());
				for(int i = 0;i < compang.size();i ++){
					start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
					end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
					int month = 0;
					int start_year = start_time.get(Calendar.YEAR);
					int start_month = start_time.get(Calendar.MONTH);
					int end_year = end_time.get(Calendar.YEAR);
					int end_month = end_time.get(Calendar.MONTH);
					if(end_year > start_year){
						month = end_month + (end_year - start_year) * 12 - start_month;
					}else{
						if(end_month > start_month){
							month = end_month - start_month;
						}
					}
					if(month <= 1){
						num6 ++;
					}
				}
			}
		}
		request.put("num1", num1);
		request.put("num2", num2);
		request.put("num3", num3);
		request.put("num4", num4);
		request.put("num5", num5);
		request.put("num6", num6);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getCompangByCheckStatus(){
		User user = (User) session.get("user");
		int checknum = 0;
		List<Compang> compang = null;
		ArrayList<Compang> new_compang = new ArrayList<Compang>();
		if(null != check){
			checknum = Integer.parseInt(check);
			session.put("open_check", "0");
			session.put("open_checkstatus", checknum);
			
			session.remove("page");
			session.remove("enterbtnnum");
		}
		int totalcompang = 0;
		if(checknum == 6){
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				totalcompang = compangDao.findComapngByCheckStatus(0).size();
				compang = compangDao.findComapngByCheckStatusAndNum(0, 0);
			}else{
				if(user.getRole().getRid() == 4){
					totalcompang = compangDao.findCompangByAreaAndStatus(0,user.getAddress()).size();
					compang = compangDao.findCompangByAreaAndStatusAndNum(0, user.getAddress(), 0);
				}else{
					totalcompang = compangDao.findCompangByStatusAndUid(0,user.getUid()).size();
					compang = compangDao.findCompangByStatusAndUidAndNum(0,user.getUid(),0);
				}
			}

			for(int i = 0;i < compang.size();i ++){
				Calendar start_time = Calendar.getInstance();
				Calendar end_time = Calendar.getInstance();
				start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
				end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
				int month = 0;
				int start_year = start_time.get(Calendar.YEAR);
				int start_month = start_time.get(Calendar.MONTH);
				int end_year = end_time.get(Calendar.YEAR);
				int end_month = end_time.get(Calendar.MONTH);
				if(end_year > start_year){
					month = end_month + (end_year - start_year) * 12 - start_month;
				}else{
					if(end_month > start_month){
						month = end_month - start_month;
					}
				}
				if(month <= 1){
					new_compang.add(compang.get(i));
				}
			}
			session.put("totalcompang",totalcompang);
			session.put("compang", new_compang);
		}else{
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				totalcompang = compangDao.findComapngByCheckStatus(checknum).size();
				compang = compangDao.findComapngByCheckStatusAndNum(checknum, 0);
			}else{
				if(user.getRole().getRid() == 4){
					totalcompang = compangDao.findCompangByAreaAndStatus(checknum,user.getAddress()).size();
					compang = compangDao.findCompangByAreaAndStatusAndNum(checknum, user.getAddress(), 0);
				}else{
					totalcompang = compangDao.findCompangByStatusAndUid(checknum,user.getUid()).size();
					compang = compangDao.findCompangByStatusAndUidAndNum(checknum,user.getUid(),0);
				}
			}
			if(checknum == 3){
				request.put("compang", compang);
				return "success1";
			}
			session.put("totalcompang",totalcompang);
			session.put("compang", compang);
		}
		 
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getAdvert(){
		List<Advertisement> ad = null;
		if(null != status){
			ad = adverDao.findAdvrByType(status);
		}
//		ad = adverDao.findAllAdvertisement();
		session.put("advers", ad);
		if(null != check && check.contains("1")){
			return "success1";
		}
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("ad_compang", compang);
		request.put("ad_type", status);
		return "success";
	}
	
	public String getMenu(){
		 List<BigClass> big = bigDao.getAllBigclass();
		 List<SmallClass> small = null;
		 if(big.size() > 0){
			 if(big.get(0).getBid() != 1){
				 request.put("showbid", big.get(0).getBid());
				 small = smallDao.getSmallClassByBid(big.get(0).getBid());
			 }else{
				 if(big.size() > 1){
					 request.put("showbid", big.get(1).getBid()); 
					 small = smallDao.getSmallClassByBid(big.get(1).getBid());
				 }
			 }
		 }else{
			 request.put("showbid", null);
		 }
		 request.put("bigclass", big);
		 request.put("smallclass", small);
		return "success";
	}
	
	public String getSmallMenu(){
		List<BigClass> big = bigDao.getAllBigclass();
		int id = 0;
		if(null != bid){
			id = Integer.parseInt(bid);
		}
		List<SmallClass> small = smallDao.getSmallClassByBid(id);
		request.put("bigclass", big);
		request.put("smallclass", small);
		request.put("showbid", id);
		return "success";
	}
	
	public String getSortBigMenu(){
		int num = 0;
		if(null != sort){
			num = Integer.parseInt(sort);
		}
		boolean onlyone = false;
		if(null != bid){
			int id = Integer.parseInt(bid);
			BigClass big = bigDao.findBigClassByBid(id);
			if(big.getSort() == num){
				onlyone = true;
			}else{
				onlyone = bigDao.getBigClassBySortAndBid(num, id);
			}
		}else{
			onlyone = bigDao.getBigClassBySort(num);
		}
		
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		if(onlyone){
			ret.put("onlyone", "0");
		}else{
			ret.put("onlyone", "1");
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getSortSmallMenu(){
		int num = 0;
		if(null != sort){
			num = Integer.parseInt(sort);
		}
		boolean onlyone = false;
		int id2 = 0;
		if(null != bid){
			id2 = Integer.parseInt(bid); 
		}
		if(null != sid){
			int id = Integer.parseInt(sid);
			SmallClass small = smallDao.findSmallClassBySid(id);
			if(small.getSort() == num){
				onlyone = true;
			}else{
				onlyone = smallDao.getSmallClassBySortAndSid(num, id,id2);
			}
		}else{
			onlyone = smallDao.findSmallClassBySort(num,id2);
		}
		
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		if(onlyone){
			ret.put("onlyone", "0");
		}else{
			ret.put("onlyone", "1");
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String getFirstPageMenu(){
		List<BigClass> big = bigDao.findBigclassByFid(1);
		List<SmallClass> small = smallDao.findAllSmallClassByFid(1);
		List<Compang> compang = compangDao.findCompangByFid(1,0,5);
		int total = compangDao.findCompangNumberByFid(1);
				 
		session.put("bigclass", big);
		session.put("smallclass", small);
		session.put("compang", compang);
		
		request.put("total", String.valueOf(total));
		request.put("fid", "1");
		request.put("start", "0");
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getPageMenu(){
		int id = 0;
		if(null != fid){
			id = Integer.parseInt(fid);
		}
		List<BigClass> big = bigDao.findBigclassByFid(id);
		List<SmallClass> small = smallDao.findAllSmallClassByFid(id);
		List<Compang> compang = compangDao.findCompangByFid(id,0,5);
		int total = compangDao.findCompangNumberByFid(id);
		
		session.put("bigclass", big);
		session.put("smallclass", small);
		session.put("compang", compang);
		
		request.put("total", String.valueOf(total));
		request.put("fid", fid);
		request.put("start", "0");
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getCompangsByLook(){
		int id = 0;
		if(null != fid){
			id = Integer.parseInt(fid);
		}
		List<BigClass> big = bigDao.findBigclassByFid(id);
		List<SmallClass> small = smallDao.findAllSmallClassByFid(id);
		List<Compang> compang = compangDao.findCompangByFidAndLook(id,0,5);
		int total = compangDao.findCompangNumberByFid(id);
		
		session.put("bigclass", big);
		session.put("smallclass", small);
		session.put("compang", compang);
		
		request.put("fid", fid);
		request.put("total", String.valueOf(total));
		request.put("start", "0");
		request.put("showbylook", "1");
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getNextCompangs(){
		int id = 0;
		int begin = 0;
		int num = 0;
		if(null != fid){
			id = Integer.parseInt(fid);
		}
		if(null != start){
			begin = Integer.parseInt(start);
		}
		if(null != number){
			num = Integer.parseInt(number);
		}
		List<BigClass> big = bigDao.findBigclassByFid(id);
		List<SmallClass> small = smallDao.findAllSmallClassByFid(id);
		List<Compang> compang = null;
		if(null != showbylook && showbylook.contains("1")){
			compang = compangDao.findCompangByFidAndLook(id,begin,num);
		}else{
			compang = compangDao.findCompangByFid(id,begin,num);
		}
		
		int total = compangDao.findCompangNumberByFid(id);
		
		session.put("bigclass", big);
		session.put("smallclass", small);
		session.put("compang", compang);
		
		request.put("total", String.valueOf(total));
		request.put("fid", fid);
		request.put("start", start);
		return "success";
	}
	
	public String getSmallClassByBid(){
		int id1 = 0;
		int id2 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
		if(null != sid){
			id2 = Integer.parseInt(sid);
		}
		List<SmallClass> small = smallDao.getSmallClassByBid(id1);
		List<Compang> compang = null;
		if(null != sid){
			compang = compangDao.findCompangsBySid(id2);
			request.put("sid", sid);
		}else{
			compang = compangDao.findCompangByBid(id1);
		}
		
		request.put("smallclass", small);
		request.put("compang", compang);
		request.put("sid", sid);
		return "success";
	}
	
	public String getCompangByArea(){
		int id1 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
		List<SmallClass> small = smallDao.getSmallClassByBid(id1);
		List<Compang> compang = null;
		if(null != area){
			compang = compangDao.findCompangByArea(area);
		}else{
			compang = compangDao.findCompangByBid(id1);
		}
		
		request.put("compang", compang);
		request.put("smallclass", small);
		request.put("area", areanum);
		request.put("sid", sid);
		return "success";
	}
	
	public String getSmall(){
		int id1 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
		List<SmallClass> small = smallDao.getSmallClassByBid(id1);
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		for(int i = 0;i < small.size();i ++){
			String str = "<option value=\""+ small.get(i).getSid()+"\">"+small.get(i).getName()+"</option>";
			ret.add(str);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getCompangInfo(){
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		Compang compang = compangDao.findCompangByCidWithoutStatus(id);
		
		request.put("compang", compang);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getAllCompangs(){
		User user = (User)session.get("user");
		List<Compang> compang = null;
		int start = 0;
		if(null != showfirst && showfirst.equals("0")){
			start = 0;
			session.remove("page");
			session.remove("enterbtnnum");
			session.remove("open_check");
			session.remove("open_checkstatus");
		}else{
			String page_num = (String)session.get("page");
			if(null != page_num && page_num.length() > 0){
				start = (Integer.parseInt(page_num) - 1) * 10;
			}
		}
		int totalcompang = 0;
		String open_check = (String)session.get("open_check");
		Integer open_checkstatus = (Integer)session.get("open_checkstatus");
		
		if(null != status && status.equals("3")){
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				totalcompang = compangDao.findComapngByCheckStatus(3).size();
				compang = compangDao.findComapngByCheckStatusAndNum(3, start);
			}else if(user.getRole().getRid() == 4){
				totalcompang = compangDao.findCompangByAreaAndStatus(3,user.getAddress()).size();
				compang = compangDao.findCompangByAreaAndStatusAndNum(3,user.getAddress(), start);
			}
			session.put("totalcompang",totalcompang);
			request.put("compang", compang);
			return "success1";
		}else{
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				if(null != open_check && open_check.equals("0")){
					totalcompang = compangDao.findComapngByCheckStatus(open_checkstatus).size();
					compang = compangDao.findComapngByCheckStatusAndNum(open_checkstatus,start);
				}else{
					totalcompang = compangDao.findAllCompangs().size();
					compang = compangDao.findAllCompangsByNum(start);
				}
			}else{
				if(user.getRole().getRid() == 4){
					if(null != open_check && open_check.equals("0")){
						totalcompang = compangDao.findCompangByAreaAndStatus(open_checkstatus,user.getAddress()).size();
						compang = compangDao.findCompangByAreaAndStatusAndNum(open_checkstatus,user.getAddress(),start);
					}else{
						totalcompang = compangDao.findNumberByRoleAndArea(user.getAddress()).size();
						compang = compangDao.findNumberByRoleAndAreaAndNum(user.getAddress(),start);
					}
				}else{
					totalcompang = compangDao.findComapngByUid(user.getUid()).size();
					compang = compangDao.findComapngByUidAndNum(user.getUid(),start);
				}
			}
		}
		session.put("compang", compang);
		session.put("totalcompang",totalcompang);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getCompangsByPage(){
		User user = (User)session.get("user");
		List<Compang> compang = null;
		int start = 0;
		int showpage = 0;
		if(null != pagenumber){
			start = Integer.parseInt(pagenumber);
		}else{
			String page_num = (String)session.get("page");
			if(null != page_num && page_num.length() > 0){
				start = (Integer.parseInt(page_num) - 1) * 10;
			}
		}
		if(null != page){
			session.put("page", page);
		}
		if(null != enterbtnnum){
			session.put("enterbtnnum", enterbtnnum);
		}
		String open_check = (String)session.get("open_check");
		Integer open_checkstatus = (Integer)session.get("open_checkstatus");
		
		ArrayList<Compang> new_compang = new ArrayList<Compang>();
		if(null != status && status.equals("3")){
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				compang = compangDao.findComapngByCheckStatusAndNum(3, start);
			}else if(user.getRole().getRid() == 4){
				compang = compangDao.findCompangByAreaAndStatusAndNum(3,user.getAddress(), start);
			}
			if(null != search && search.length() > 0){
				for(int i = 0;i < compang.size();i ++){
					if(compang.get(i).getName().contains(search)){
						new_compang.add(compang.get(i));
					}
				}
				compang = new_compang;
			}else{
				showpage = 1;
			}
		}else{
			if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
				if(null != open_check && open_check.equals("0")){
					compang = compangDao.findComapngByCheckStatusAndNum(open_checkstatus,start);
				}else{
					compang = compangDao.findAllCompangsByNum(start);
				}
			}else{
				if(user.getRole().getRid() == 4){
					if(null != open_check && open_check.equals("0")){
						compang = compangDao.findCompangByAreaAndStatusAndNum(open_checkstatus,user.getAddress(),start);
					}else{
						compang = compangDao.findNumberByRoleAndAreaAndNum(user.getAddress(),start);
					}
				}else{
					compang = compangDao.findComapngByUidAndNum(user.getUid(),start);
				}
			}
			if(null != search && search.length() > 0){
				for(int i = 0;i < compang.size();i ++){
					if(compang.get(i).getName().contains(search)){
						new_compang.add(compang.get(i));
					}
				}
				compang = new_compang;
			}else{
				showpage = 1;
			}
		}
		StringBuilder html = new StringBuilder("");
		boolean showcompang = false;
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		if(null != compang && compang.size() > 0){
			html.append("<tbody>");
			for(int i = 0; i < compang.size();i ++){
				if(compang.get(i).getCheckStatus() != 3){
					showcompang = true;
					html.append("<tr class=\"gradeX\" height=\"40px\">");
					html.append("<td>"+String.valueOf(compang.get(i).getNumber())+"</td>");
					html.append("<td>"+compang.get(i).getName()+"</td>");
					html.append("<td>"+compang.get(i).getSmallClass().getBigclass().getName()+"</td>");
					html.append("<td>"+compang.get(i).getSmallClass().getName()+"</td>");
					
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
					Calendar start_time = Calendar.getInstance();
					Calendar end_time = Calendar.getInstance();
					start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
					end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
					int month = 0;
					int start_year = start_time.get(Calendar.YEAR);
					int start_month = start_time.get(Calendar.MONTH);
					int end_year = end_time.get(Calendar.YEAR);
					int end_month = end_time.get(Calendar.MONTH);
					if(end_year > start_year){
						month = end_month + (end_year - start_year) * 12 - start_month;
					}else{
						if(end_month > start_month){
							month = end_month - start_month;
						}
					}
					List<Picture> picture = pictureDao.findPictureByCid(compang.get(i).getCid());
					List<Commodity> commodity = commodityDao.findCommoditysByCid(compang.get(i).getCid());
					
					html.append("<td>"+formatter.format(compang.get(i).getUpdateDatetime())+"</td>");
					html.append("<td>"+month+"</td>");
					html.append("<td>"+formatter.format(compang.get(i).getExpirationtime())+"</td>");
					
					int pid = Integer.parseInt(compang.get(i).getArea());
					Place pl = placeDao.findPlaceByPid(pid);
					html.append("<td>"+pl.getArea()+"</td>");
					html.append("<td>"+picture.size()+"</td>");
					html.append("<td>"+commodity.size()+"</td>");
					if(compang.get(i).getAuthentication() == 0){
						html.append("<td>yirenzheng</td>");
					}else{
						html.append("<td>weirenzheng</td>");
					}
					if(compang.get(i).getCheckStatus() == 0){
						if(month <= 1){
							html.append("<td>kuaidaoqi</td>");
						}else{
							html.append("<td>fabuzhong</td>");
						}
					}
					if(compang.get(i).getCheckStatus() == 1){
						html.append("<td><font color=\"blue\">daishenhe</font></td>");
					}
					if(compang.get(i).getCheckStatus() == 2){
						html.append("<td>butongguo</td>");
					}
//					if(compang.get(i).getCheckStatus() == 3){
//						html.append("<td>yiguanbi</td>");
//					}
					if(compang.get(i).getCheckStatus() == 4){
						html.append("<td>yiguoqi</td>");
					}
					html.append("<td class=\"taskOptions\">");
					html.append("<a href=\""+httpRequest.getContextPath()+"/browser/viewcompang.do?cid=" + compang.get(i).getCid()+ "\"class=\"tip-top\" data-original-title=\"yulan\"><i class=\"icon-eye-open\"></i></a>");
					if(user.getRole().getRid() == 1){
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/getcompang.do?cid=" + compang.get(i).getCid()+"\"class=\"tip-top\" data-original-title=\"xiugai\"><i class=\"icon-edit\"></i></a>");
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/reviewcompang.do?cid=" + compang.get(i).getCid() + "\"class=\"tip-top\" data-original-title=\"shenhe\"><i class=\"icon-check\"></i></a>");
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/compangstatus.do?cid=" + compang.get(i).getCid() + "&checkstatus=3" + "\"class=\"tip-top\" data-original-title=\"shanchu\"><i class=\"icon-trash\"></i></a>");
					}
					if(user.getRole().getRid() == 2){
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/reviewcompang.do?cid=" + compang.get(i).getCid() + "\"class=\"tip-top\" data-original-title=\"shenhe\"><i class=\"icon-check\"></i></a>");
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/compangstatus.do?cid=" + compang.get(i).getCid() + "&checkstatus=3" + "\"class=\"tip-top\" data-original-title=\"shanchu\"><i class=\"icon-trash\"></i></a>");
					}
					if(user.getRole().getRid() == 3){
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/getcompang.do?cid=" + compang.get(i).getCid()+ "\"class=\"tip-top\" data-original-title=\"xiugai\"><i class=\"icon-edit\"></i></a>");
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/compangstatus.do?cid=" + compang.get(i).getCid() + "&checkstatus=3" + "\"class=\"tip-top\" data-original-title=\"shanchu\"><i class=\"icon-trash\"></i></a>");
					}
					if(user.getRole().getRid() == 4){
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/getcompang.do?cid=" + compang.get(i).getCid()+ "\"class=\"tip-top\" data-original-title=\"xiugai\"" + "<i class=\"icon-edit\"></i></a>");
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/compangstatus.do?cid=" + compang.get(i).getCid() + "&checkstatus=3" + "\"class=\"tip-top\" data-original-title=\"shanchu\"<i class=\"icon-trash\"></i></a>");
					}
					if(user.getRole().getRid() == 5){
						html.append("<a href=\""+httpRequest.getContextPath()+ "/browser/getcompang.do?cid=" + compang.get(i).getCid() + "\"class=\"tip-top\" data-original-title=\"xiugai\"" + "<i class=\"icon-edit\"></i></a>");
					}
					html.append("<td></td>");
					html.append("</tr>");
				}
			}
			if(!showcompang){
				html.append("<tbody>");
				html.append("<tr class=\"gradeX\" height=\"40px\">");
				html.append("<td colspan=\"4\">No data available in table</td>");
				html.append("</tr>");
				html.append("</tbody>");
			}
			html.append("</tbody>");
		}else{
			html.append("<tbody>");
			html.append("<tr class=\"gradeX\" height=\"40px\">");
			html.append("<td colspan=\"4\">No data available in table</td>");
			html.append("</tr>");
			html.append("</tbody>");
		}
		try {
			PrintWriter out = response.getWriter();
			JSONObject ret = new JSONObject();
			ret.put("html", html.toString());
			ret.put("showpage", showpage);
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public String getAllCommodity(){
		List<Commodity> commodity = commodityDao.findAllCommoditys();
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		session.put("compang2", compang);
		session.put("commodity", commodity);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getAllPicture(){
		List<Picture> picture = pictureDao.findAllPicture();
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		session.put("compang3", compang);
		request.put("picture", picture);
		return "success";
	}
	
	public String getContect(){
		List<Contect> contect = contectDao.findContect();
		request.put("contect", contect);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String getAllService(){
		List<ServiceArea> service = serviceDao.findAllServiceArea();
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		session.put("compang4", compang);
		request.put("service", service);
		return "success";
	}
	
	private void RecordUserAction(String action){
		User user = (User)session.get("user");
		RecordHistory record = new RecordHistory();
		record.setUsername(user.getName());
		record.setOperation(action);
		Timestamp now = new Timestamp(System.currentTimeMillis());
		record.setUpdatetime(now);
		recordDao.saveNewRecordHistory(record);
	}
	
	public String DeleteTwoMenu(){
		int id1 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
		List<SmallClass> small_list = smallDao.getSmallClassByBid(id1);
		for(int i = 0;i < small_list.size();i ++){
			SmallClass smallclass = smallDao.findSmallClassBySid(1);
			List<Compang> compang = compangDao.getCompangsBySid(small_list.get(i).getSid());
			for(int j = 0;j< compang.size(); j ++){
				compang.get(j).setSmallClass(smallclass);
				compangDao.saveCompang(compang.get(j));
				
//				pictureDao.deletePictureByCid(compang.get(j).getCid());
//				commodityDao.DeleteCommodityByCid(compang.get(j).getCid());
//				compangDao.DeleteCompangByCid(compang.get(j).getCid());
//				if(adverDao.findAdverByBid(compang.get(j).getCid())){
//					adverDao.deleteAdverByBid(compang.get(j).getCid());
//				}
			}
			//delete picture
			String path = ServletActionContext.getServletContext().getRealPath(small_list.get(i).getPath() + small_list.get(i).getImage());
			File dir = new File(path);
			if (dir.exists()) {
				dir.delete();
			}
		}
		//delete picture
		BigClass delbig = bigDao.findBigClassByBid(id1);
		String path = ServletActionContext.getServletContext().getRealPath(delbig.getPath() + delbig.getImage());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		
		smallDao.deleteSmallClassByBid(id1);
		bigDao.DeleteBigClassByBid(id1);
		
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = null;
		if(big.size() > 0){
			if(big.get(0).getBid() != 1){
				small = smallDao.getSmallClassByBid(big.get(0).getBid());
				 request.put("showbid", big.get(0).getBid());
			 }else{
				 if(big.size() > 1){
					 request.put("showbid", big.get(1).getBid()); 
					 small = smallDao.getSmallClassByBid(big.get(1).getBid());
				 }
			 }
		}else{
			request.put("showbid", null);
		}
		request.put("bigclass", big);
		request.put("smallclass", small);
		
		RecordUserAction("Delete a two menu id for " + bid);
		return "success";
	}
	
	public String DeleteThreeMenu(){
		int id1 = 0;
		if(null != sid){
			id1 = Integer.parseInt(sid);
		}
		List<Compang> compang = compangDao.getCompangsBySid(id1);
		for(int j = 0;j< compang.size(); j ++){
			SmallClass smallclass = smallDao.findSmallClassBySid(1);
			compang.get(j).setSmallClass(smallclass);
			compangDao.saveCompang(compang.get(j));
			
//			pictureDao.deletePictureByCid(compang.get(j).getCid());
//			commodityDao.DeleteCommodityByCid(compang.get(j).getCid());
//			compangDao.DeleteCompangByCid(compang.get(j).getCid());
//			if(adverDao.findAdverByBid(compang.get(j).getCid())){
//				adverDao.deleteAdverByBid(compang.get(j).getCid());
//			}
		}
		//delete picture
		SmallClass delsmall = smallDao.findSmallClassBySid(id1);
		String path = ServletActionContext.getServletContext().getRealPath(delsmall.getPath() + delsmall.getImage());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		
		smallDao.deleteSmallClassBySid(id1);
		
		List<BigClass> big = bigDao.getAllBigclass();
		int id2 = 1;
		if(null != bid){
			id2 = Integer.parseInt(bid);
		}
		List<SmallClass> small = smallDao.getSmallClassByBid(id2);
		request.put("bigclass", big);
		request.put("smallclass", small);
		request.put("showbid", id2);
		RecordUserAction("Delete a three menu id for " + sid);
		return "success";
	}
	
	@SuppressWarnings("unchecked")
	public String DeleteAdvert(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		//delete picture
		Advertisement deladver = adverDao.findAdvertByAid(id);
		String path = ServletActionContext.getServletContext().getRealPath(deladver.getPath() + deladver.getImage());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		
		adverDao.DeleteAdvertByAid(id);
		RecordUserAction("Delete a advert id for " + aid);
		
		List<Advertisement> ad = null;
		if(null != status){
			ad = adverDao.findAdvrByType(status);
		}
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("ad_compang", compang);
		request.put("ad_type", status);
		session.put("advers", ad);
		return "success";
	}
	
	public String DeleteCompang(){
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		
		//delete picture
		List<Commodity> delcommodity = commodityDao.findCommoditysByCid(id);
		List<Picture> delpicture = pictureDao.findPictureByCid(id);
		Compang delcompang = compangDao.findCompangByCidWithoutStatus(id);
		for(int i = 0;i < delcommodity.size();i ++){
			String path = ServletActionContext.getServletContext().getRealPath(delcommodity.get(i).getPath() + delcommodity.get(i).getImage());
			File dir = new File(path);
			if (dir.exists()) {
				dir.delete();
			}
		}
		for(int j = 0;j < delpicture.size();j ++){
			String path1 = ServletActionContext.getServletContext().getRealPath(delpicture.get(j).getPath() + delpicture.get(j).getBig());
			File dir1 = new File(path1);
			if (dir1.exists()) {
				dir1.delete();
			}
			
			String path2 = ServletActionContext.getServletContext().getRealPath(delpicture.get(j).getPath() + delpicture.get(j).getSmall());
			File dir2 = new File(path2);
			if (dir2.exists()) {
				dir2.delete();
			}
		}
		String path = ServletActionContext.getServletContext().getRealPath(delcompang.getPath() + delcompang.getImage());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		
		
		commodityDao.DeleteCommodityByCid(id);
		pictureDao.deletePictureByCid(id);
		compangDao.DeleteCompangByCid(id);
		if(adverDao.findAdverByBid(id)){
			List<Advertisement> deladver = adverDao.findAdverBycid(id);
			for(int k = 0;k < deladver.size();k ++){
				String path3 = ServletActionContext.getServletContext().getRealPath(deladver.get(k).getPath() + deladver.get(k).getImage());
				File dir3 = new File(path3);
				if (dir3.exists()) {
					dir3.delete();
				}
			}
			adverDao.deleteAdverByBid(id);
		}
		RecordUserAction("Delete a compang id for " + cid);
		RecordUserAction("Delete compang's picture id for " + cid);
		RecordUserAction("Delete a compang's commodity id for " + cid);
		
		User user = (User)session.get("user");
		List<Compang> compang = null;
		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
			compang = compangDao.findComapngByCheckStatus(3);
		}else if(user.getRole().getRid() == 4){
			compang = compangDao.findCompangByAreaAndStatus(3,user.getAddress());
		}
		request.put("compang", compang);
		return "success";
	}
	
	public String DeleteCommodity(){
		int id = 0;
		if(null != yid){
			id = Integer.parseInt(yid);
		}
		int id2 = 0;
		if(null != cid){
			id2 = Integer.parseInt(cid);
		}
		Commodity delcommodity = commodityDao.findCommodityByYid(id);
		String path = ServletActionContext.getServletContext().getRealPath(delcommodity.getPath() + delcommodity.getImage());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		String path2 = ServletActionContext.getServletContext().getRealPath(delcommodity.getPath() + delcommodity.getDetail_img());
		File dir2 = new File(path2);
		if (dir2.exists()) {
			dir2.delete();
		}
		
		
		commodityDao.DeleteCommodityByYid(id);
		RecordUserAction("Delete a commodity id for " + yid);
		
//		List<Commodity> commodity = commodityDao.findAllCommoditys();
//		List<Compang> compang = compangDao.findAllCompagsByStatus();
		Compang compang = compangDao.findCompangByCidWithoutStatus(id2);
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.getSmallClassByBid(id2);
		ArrayList<BigClass> big_list = new ArrayList<BigClass>();
		for(int i = 0;i < big.size();i ++){
			if(big.get(i).getSmallclasses().size() > 0){
				big_list.add(big.get(i));
			}
		}
		
		request.put("bigclass", big_list);
		request.put("smallclass", small);
		request.put("compang", compang);
//		session.put("commodity", commodity);
		
		return "success";
	}
	
	public String DeleteContect(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		contectDao.deleteContectByAid(id);
		RecordUserAction("Delete a contect id for " + aid);
		
		List<Contect> contect = contectDao.findContect();
		request.put("contect", contect);
		return "success";
	}
	
	public String DeletePicture(){
		int id = 0;
		if(null != pid){
			id = Integer.parseInt(pid);
		}
		Picture picture = pictureDao.findPictureByPid(id);
		String path = ServletActionContext.getServletContext().getRealPath(picture.getPath() + picture.getSmall());
		File dir = new File(path);
		if (dir.exists()) {
			dir.delete();
		}
		
		pictureDao.deletePictureByPid(id);
		RecordUserAction("Delete a picture id for " + pid);
		
		int id1 = 0;
		if(null != cid){
			id1 = Integer.parseInt(cid);
			Compang compang = compangDao.findCompangByCidWithoutStatus(id1);
			request.put("compang", compang);
		}

		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.getSmallClassByBid(1);
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
	public String DeleteService(){
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		serviceDao.deleteServiceAreaBySid(id);
		RecordUserAction("Delete a service area id for " + sid);
		
		List<ServiceArea> service = serviceDao.findAllServiceArea();
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("service", service);
		request.put("compang4", compang);
		return "success";
	}
	
	public String DeleteUser(){
		int id1 = 0;
		if(null != uid){
			id1 = Integer.parseInt(uid);
		}
		List<Compang> compang = compangDao.findComapngByUid(id1);
		List<User> user = userDao.findUserByRid(1);
		for(int i = 0;i < compang.size();i ++){
			compang.get(i).setUser(user.get(0));
			compangDao.saveCompang(compang.get(i));
		}
		userDao.deleteUserByUid(id1);
		List<User> user_list = userDao.findAllUser();
		
		request.put("all_user", user_list);
		return "success";
	}
	
	public String getAdvertByAid(){
		int id = 0;
		if(null != aid){
			id = Integer.parseInt(aid);
		}
		Advertisement advert = adverDao.findAdvertByAid(id);
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("advert", advert);
		request.put("compang", compang);
		return "success";
	}
	
	public String getCompangByCid(){
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		Compang compang = compangDao.findCompangByCidWithoutStatus(id);
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.getSmallClassByBid(compang.getSmallClass().getBigclass().getBid());
		ArrayList<BigClass> big_list = new ArrayList<BigClass>();
		for(int i = 0;i < big.size();i ++){
			if(big.get(i).getSmallclasses().size() > 0){
				big_list.add(big.get(i));
			}
		}
		
		request.put("bigclass", big_list);
		request.put("smallclass", small);
		request.put("compang", compang);
		return "success";
	}
	
	public String getCommodityByYid(){
		int id1 = 0;
		if(null != yid){
			id1 = Integer.parseInt(yid);
		}
		Commodity commodity = commodityDao.findCommodityByYid(id1);
		int id2 = 0;
		if(null != cid){
			id2 = Integer.parseInt(cid);
		}
		Compang compang = compangDao.findCompangByCidWithoutStatus(id2);
		request.put("compang", compang);
		request.put("commodity", commodity);
		return "success";
	}
	
	public String getPictureByPid(){
		int id1 = 0;
		if(null != pid){
			id1 = Integer.parseInt(pid);
		}
		Picture picture = pictureDao.findPictureByPid(id1);
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("picture", picture);
		request.put("compang", compang);
		return "success";
	}
	
	public String getContectByAid(){
		int id1 = 0;
		if(null != aid){
			id1 = Integer.parseInt(aid);
		}
		Contect contect = contectDao.findContectByAid(id1);
		request.put("contect", contect);
		return "success";
	}
	
	public String getMenuByBidAndSid(){
		int id1 = 0;
		if(null != bid){
			id1 = Integer.parseInt(bid);
		}
		int id2 = 0;
		if(null != sid){
			id2 = Integer.parseInt(sid);
		}
		BigClass big = bigDao.findBigClassByBid(id1);
		SmallClass small = smallDao.findSmallClassBySid(id2);
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
	public String getServiceAreaBySid(){
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		ServiceArea service = serviceDao.findServiceAreaBySid(id);
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		request.put("service", service);
		request.put("compang4", compang);
		return "success";
	}
	
	public String getRecommendCompang(){
		User user = (User) session.get("user");
		List<Compang> compang = null;
		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
			compang = compangDao.findAllCompagsByStatus();
		}else{
			if(user.getRole().getRid() == 4){
				compang = compangDao.findCompangByAreaAndStatus(0,user.getAddress());
			}else{
				compang = compangDao.findCompangByStatusAndUid(0, user.getUid());
			}
		}
		
		request.put("recommend_compang", compang);
		return "success";
	}
	
	public String getRecommendNumber(){
		boolean recommend = compangDao.findRecommendNumber();
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		if(recommend){
			ret.put("canadd", "0");
		}else{
			ret.put("canadd", "1");
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return "success";
	}
	
	public String getUser(){
		List<User> user = userDao.findAllUser();
		request.put("all_user", user);
		
		return "success";
	}
	
	public String getMenuByNewCompang(){
		int maxnum = compangDao.findMaxNumber();
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.findAllSmallClass();
		request.put("bigclass", big);
		request.put("smallclass", small);
		request.put("maxnum", maxnum);
		return "success";
	}
	
	public String getAdverNum(){
		boolean addavert = adverDao.findNumberAvert();
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		if(addavert){
			ret.put("canadd", "0");
		}else{
			ret.put("canadd", "1");
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getReviewCompang(){
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		Compang compang = compangDao.findCompangByCidWithoutStatus(id);
		request.put("compang", compang);
		return "success";
	}
	
	public String getPlaceArea(){
		List<Place> area = null;
		if(null != city){
//			try {
//				city = new String(city.getBytes("iso-8859-1"),"UTF-8");
//			} catch (UnsupportedEncodingException e) {
//				e.printStackTrace();
//			}
			area = placeDao.findAreaByCity(city);
		}
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		if(area != null){
			for(int i = 0;i < area.size();i ++){
				String str = "<option value=\""+ area.get(i).getPid()+"\">"+area.get(i).getArea()+"</option>";
				ret.add(str);
			}
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getPlaceCity(){
		List<Place> city = null;
		if(null != province){
			city = placeDao.findCityByProvince(province);
		}
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		List<OpenArea> openarea = openareaDao.findAllOpenArea();
		if(null != city){
			ArrayList<String> city_list = new ArrayList<String>();
			for(int i = 0;i < city.size();i ++){
				if(!city_list.contains(city.get(i).getCity())){
					city_list.add(city.get(i).getCity());
				}
			}
			for(int i = 0;i < openarea.size();i ++){
				for(int j = 0;j < city_list.size();j ++){
					if(openarea.get(i).getArea().equals(city_list.get(i))){
						if(openarea.get(i).getIsopen() == 0){
							String str = "<option value=\""+ openarea.get(i).getAbridge()+"\">"+city_list.get(i)+"</option>";
							ret.add(str);
						}
					}
				}
			}
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String CheckUserName(){
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		if(null != uid){
			int id = Integer.parseInt(uid);
			if(null != checkname){
				if(userDao.findUserByCheckNameAndUid(checkname, id)){
					ret.put("ckeck", "0");
				}else{
					ret.put("ckeck", "1");
				}
			}
		}else{
			if(null != checkname){
//				try {
//					checkname = new String(checkname.getBytes("iso-8859-1"),"UTF-8");
//				} catch (UnsupportedEncodingException e) {
//					e.printStackTrace();
//				}
				if(userDao.findUserByCheckName(checkname)){
					ret.put("ckeck", "0");
				}else{
					ret.put("ckeck", "1");
				}
			}
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getRecoverCompang(){
		User user = (User)session.get("user");
		List<Compang> compang = null;
		if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){
			compang = compangDao.findComapngByCheckStatus(3);
		}else if(user.getRole().getRid() == 4){
			compang = compangDao.findCompangByAreaAndStatus(3,user.getAddress());
		}
		
		request.put("compang", compang);
		return "success";
	}
	
	public String getThreeMenuByShowfirst(){
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		List<SmallClass> small = smallDao.findSmallClassShowFirst();
		if(small.size() > 8){
			ret.put("add", 1);
		}else{
			ret.put("add", 0);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getHotSmallClass(){
		List<BigClass> big = bigDao.getAllBigclass();
		List<SmallClass> small = smallDao.findAllSmallClass();
		request.put("bigclass", big);
		request.put("smallclass", small);
		return "success";
	}
	
//	@SuppressWarnings("unchecked")
	public String SearchCompangBySome(){
//		List<Compang> compang = (List<Compang>)session.get("compang");
		
		return null;
	}
}
