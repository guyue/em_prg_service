package com.androidservice.backstage.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//import java.io.OutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.Map;

//import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.swing.JOptionPane;

import org.apache.struts2.ServletActionContext;
//import org.apache.struts2.dispatcher.RequestMap;
//import org.apache.struts2.dispatcher.SessionMap;
//import org.apache.struts2.interceptor.RequestAware;
//import org.apache.struts2.interceptor.ServletResponseAware;
//import org.apache.struts2.interceptor.SessionAware;
//import org.springframework.beans.factory.annotation.Autowired;
//import javax.servlet.http.HttpServletResponse;

//import org.apache.struts2.ServletActionContext;
//import org.apache.struts2.dispatcher.SessionMap;
//import org.apache.struts2.interceptor.RequestAware;
//import org.apache.struts2.interceptor.ServletResponseAware;
//import org.apache.struts2.interceptor.SessionAware;
//import org.lxh.smart.File;
//import org.lxh.smart.SmartUpload;
//import org.lxh.smart.SmartUploadException;

import com.androidservice.common.EMProperties;
//import com.androidservice.bean.Advertisement;
//import com.androidservice.bean.Compang;
//import com.androidservice.dao.AdvertisementDao;
//import com.androidservice.dao.CompangDao;
//import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;

public class ImageUploadAction extends ActionSupport{
	private static final long serialVersionUID = 2721916376072222628L;
	
	private File myFile;
	
	private File myFile2;

	private String fileName;
	
	private String filePath;
	
	private String fileName2;
	
	private String filePath2;
	
	private static final EMProperties EMProp = EMProperties.getInstance();

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName2() {
		return fileName2;
	}

	public void setFileName2(String fileName2) {
		this.fileName2 = fileName2;
	}

	public String getFilePath2() {
		return filePath2;
	}

	public void setFilePath2(String filePath2) {
		this.filePath2 = filePath2;
	}

	public File getMyFile2() {
		return myFile2;
	}

	public void setMyFile2(File myFile2) {
		this.myFile2 = myFile2;
	}

	public String Upload() {
		HttpServletRequest request = ServletActionContext.getRequest();
		FileInputStream fis = null;
		FileOutputStream os = null;
		StringBuffer outPath = new StringBuffer();
		outPath.append(EMProp.getProperty("img_path"));
		outPath.append(request.getContextPath());
		outPath.append("/");
		if(null != filePath2 && filePath2.length() > 0){
			outPath.append(filePath2);
			String path = ServletActionContext.getServletContext().getRealPath(filePath2);
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
		}else{
			outPath.append(filePath);
			String path = ServletActionContext.getServletContext().getRealPath(filePath);
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
		}
		if(null != fileName2 && fileName2.length() > 0){
			outPath.append(fileName2);
		}else{
			outPath.append(fileName);
		}
		
		File file = new File(outPath.toString());
		
		try {
			if(null != myFile2){
				fis = new FileInputStream(myFile2);
			}else{
				fis = new FileInputStream(myFile);
			}
			
			os = new FileOutputStream(file);
			int count = 0;
			byte[] buffer = new byte[1024 * 8];
			try {
				while ((count = fis.read(buffer)) != -1) {
					try {
						os.write(buffer, 0, count);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
//		JOptionPane.showInputDialog(this,"OK!");
		return null;
	}
}
