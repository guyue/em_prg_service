package com.androidservice.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.androidservice.util.MiscUtils;

public class EMProperties extends Properties {
	private static final long serialVersionUID = 6826276411016192863L;
	private static EMProperties emProperties = new EMProperties();

	public static EMProperties getInstance() {
		return emProperties;
	}

	private EMProperties() {
		MiscUtils.getLogger().debug("PHOENIX PROPS CONSTRUCTOR");
		try {
			readFromFile("/androidService.properties");
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error", e);
		}
	}

	public void readFromFile(String url) throws IOException {
		InputStream is = getClass().getResourceAsStream(url);
		if (is == null)
			is = new FileInputStream(url);

		try {
			load(is);
		} finally {
			is.close();
		}
	}

	@Override
	public String getProperty(String key) {
		String tmp = super.getProperty(key);
		if (null == tmp) {
			return "";
		}
		return tmp.trim();
	}

	public int getIntProperty(String key) {
		try {
			String value = emProperties.getProperty(key);
			return Integer.parseInt(value);
		} catch (Exception e) {
			return 0;
		}
	}

	public boolean getBooleanProperty(String key) {
		try {
			String value = emProperties.getProperty(key);
			return Boolean.parseBoolean(value);
		} catch (Exception e) {
			return false;
		}
	}
}
