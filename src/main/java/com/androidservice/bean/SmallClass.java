package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the smallclass database table.
 * 
 */
@Entity
@Table(name = "smallclass")
public class SmallClass extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int sid;

	private String image;

	private String name;

	private String path;
	
	private int sort;
	
	private int showfirst;

	//bi-directional many-to-one association to Compang
	@OneToMany(mappedBy="smallclass")
	private List<Compang> compangs;

	//bi-directional many-to-one association to Bigclass
	@ManyToOne
	@JoinColumn(name="bid")
	private BigClass bigclass;

	public SmallClass() {
	}

	public int getSid() {
		return this.sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<Compang> getCompangs() {
		return this.compangs;
	}

	public void setCompangs(List<Compang> compangs) {
		this.compangs = compangs;
	}

	public BigClass getBigclass() {
		return this.bigclass;
	}

	public void setBigclass(BigClass bigclass) {
		this.bigclass = bigclass;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public int getShowfirst() {
		return showfirst;
	}

	public void setShowfirst(int showfirst) {
		this.showfirst = showfirst;
	}

	@Override
	public Integer getId() {
		return sid;
	}

}
