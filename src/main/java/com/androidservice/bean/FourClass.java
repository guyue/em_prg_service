package com.androidservice.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
/**
 * The persistent class for the fourclass database table.
 * 
 */
@Entity
@Table(name="fourclass")
public class FourClass extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int fid;

	private String name;
	
	//bi-directional many-to-one association to Smallclass
	@OneToMany(mappedBy="fourclass")
	private List<BigClass> Bigclasses;

	public FourClass() {
	}

	public int getFid() {
		return this.fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public List<BigClass> getBigclasses() {
		return Bigclasses;
	}

	public void setBigclasses(List<BigClass> bigclasses) {
		Bigclasses = bigclasses;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Integer getId() {
		return fid;
	}

}