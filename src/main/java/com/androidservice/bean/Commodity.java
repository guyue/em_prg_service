package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the commodity database table.
 * 
 */
@Entity
@Table(name="commodity")
public class Commodity extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int yid;

	private String code;

	private String name;

	private String norm;

	private String price;

	private String deals;
	
	@Lob
	private String content;
	
	private String image;
	
	private String path;
	
	private String detail_img;
	
	private String detail_path;

	//bi-directional many-to-one association to Compang
	@ManyToOne
	@JoinColumn(name="cid")
	private Compang compang;

	public Commodity() {
	}

	public int getYid() {
		return this.yid;
	}

	public void setYid(int yid) {
		this.yid = yid;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNorm() {
		return this.norm;
	}

	public void setNorm(String norm) {
		this.norm = norm;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDeals() {
		return deals;
	}

	public void setDeals(String deals) {
		this.deals = deals;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Compang getCompang() {
		return this.compang;
	}

	public void setCompang(Compang compang) {
		this.compang = compang;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getDetail_img() {
		return detail_img;
	}

	public void setDetail_img(String detail_img) {
		this.detail_img = detail_img;
	}

	public String getDetail_path() {
		return detail_path;
	}

	public void setDetail_path(String detail_path) {
		this.detail_path = detail_path;
	}

	@Override
	public Integer getId() {
		return yid;
	}

}