package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the shangxun database table.
 * 
 */
@Entity
@Table(name="shangxun")
public class Shangxun extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int xid;

	@Column(name="comment_size")
	private String commentSize;

	@Column(name="shangxun_name")
	private String shangxunName;

	@Lob
	private String content;

	private String image;

	private String looktime;

	private String path;

	private String source;

	private String title;

	@Column(name="update_time")
	private Timestamp updateTime;
	
	//bi-directional many-to-one association to Smallclass
	@ManyToOne
	@JoinColumn(name="sid")
	private SmallClass smallclass;
	
	private byte showfirst;
	
	private byte checkstatus;

	public Shangxun() {
	}

	public int getXid() {
		return this.xid;
	}

	public void setXid(int xid) {
		this.xid = xid;
	}

	public String getCommentSize() {
		return this.commentSize;
	}

	public void setCommentSize(String commentSize) {
		this.commentSize = commentSize;
	}

	public String getShangxunName() {
		return this.shangxunName;
	}

	public void setShangxunName(String shangxunName) {
		this.shangxunName = shangxunName;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLooktime() {
		return this.looktime;
	}

	public void setLooktime(String looktime) {
		this.looktime = looktime;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSource() {
		return this.source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public SmallClass getSmallclass() {
		return smallclass;
	}

	public void setSmallclass(SmallClass smallclass) {
		this.smallclass = smallclass;
	}

	public byte getCheckStatus() {
		return checkstatus;
	}

	public void setCheckStatus(byte checkstatus) {
		this.checkstatus = checkstatus;
	}

	public byte getShowfirst() {
		return showfirst;
	}

	public void setShowfirst(byte showfirst) {
		this.showfirst = showfirst;
	}

	@Override
	public Integer getId() {
		return xid;
	}

}
