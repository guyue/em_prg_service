package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the bigclass database table.
 * 
 */
@Entity
@Table(name = "bigclass")
public class BigClass extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int bid;

	private String name;
	
	private String image;
	
	private String path;
	
	private int sort;
	
	//bi-directional many-to-one association to Bigclass
	@ManyToOne
	@JoinColumn(name="fid")
	private FourClass fourclass;

	//bi-directional many-to-one association to Smallclass
	@OneToMany(mappedBy="bigclass", fetch = FetchType.EAGER)
	private List<SmallClass> smallclasses;

	public BigClass() {
	}

	public int getBid() {
		return this.bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public List<SmallClass> getSmallclasses() {
		return this.smallclasses;
	}

	public void setSmallclasses(List<SmallClass> smallclasses) {
		this.smallclasses = smallclasses;
	}

	public FourClass getFourclass() {
		return fourclass;
	}

	public void setFourclass(FourClass fourclass) {
		this.fourclass = fourclass;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	@Override
	public Integer getId() {
		// TODO Auto-generated method stub
		return bid;
	}

}
