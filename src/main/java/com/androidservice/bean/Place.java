package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the place database table.
 * 
 */
@Entity
@Table(name="place")
public class Place extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int pid;

	private String area;

	private String city;

	private String province;

	public Place() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@Override
	public Integer getId() {
		return pid;
	}

}
