package com.androidservice.bean;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the picture database table.
 * 
 */
@Entity
@Table(name="recordhistory")
public class RecordHistory extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int rid;
	
	private String username;

	private String operation;
	
	private Timestamp updatetime;

	public int getRid() {
		return rid;
	}




	public void setRid(int rid) {
		this.rid = rid;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getOperation() {
		return operation;
	}




	public void setOperation(String operation) {
		this.operation = operation;
	}




	public Timestamp getUpdatetime() {
		return updatetime;
	}




	public void setUpdatetime(Timestamp updatetime) {
		this.updatetime = updatetime;
	}




	@Override
	public Integer getId() {
		return rid;
	}
}
