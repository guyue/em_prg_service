package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the feedbacks database table.
 * 
 */
@Entity
@Table(name="feedbacks")
public class Feedback implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int fid;

	@Lob
	private String content;

	private String name;

	private String phone;

	public Feedback() {
	}

	public int getFid() {
		return this.fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
