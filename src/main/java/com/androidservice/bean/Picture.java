package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the picture database table.
 * 
 */
@Entity
@Table(name="picture")
public class Picture extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int pid;

	private String big;

	private String description;

	private String path;

	private String small;

	//bi-directional many-to-one association to Compang
	@ManyToOne
	@JoinColumn(name="cid")
	private Compang compang;

	public Picture() {
	}

	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getBig() {
		return this.big;
	}

	public void setBig(String big) {
		this.big = big;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSmall() {
		return this.small;
	}

	public void setSmall(String small) {
		this.small = small;
	}

	public Compang getCompang() {
		return this.compang;
	}

	public void setCompang(Compang compang) {
		this.compang = compang;
	}
	@Override
	public Integer getId() {
		return pid;
	}

}
