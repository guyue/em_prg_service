package com.androidservice.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
/**
 * The persistent class for the commodity database table.
 * 
 */
@Entity
@Table(name="updatetime")
public class UpdateTime extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int uid;
	
	private String domain;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;
	
	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	@Override
	public Integer getId() {
		return uid;
	}
	
}
