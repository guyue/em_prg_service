package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the version database table.
 * 
 */
@Entity
@Table(name="version")
public class Version extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int vid;

	@Column(name="become_effective")
	private byte becomeEffective;

	@Lob
	@Column(name="update_content")
	private String updateContent;

	private String url;

	private String ver;

	public Version() {
	}

	public int getVid() {
		return this.vid;
	}

	public void setVid(int vid) {
		this.vid = vid;
	}

	public byte getBecomeEffective() {
		return this.becomeEffective;
	}

	public void setBecomeEffective(byte becomeEffective) {
		this.becomeEffective = becomeEffective;
	}

	public String getUpdateContent() {
		return this.updateContent;
	}

	public void setUpdateContent(String updateContent) {
		this.updateContent = updateContent;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getVer() {
		return this.ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	@Override
	public Integer getId() {
		return vid;
	}

}