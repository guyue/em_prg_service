package com.androidservice.bean;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * The persistent class for the comments database table.
 * 
 */
@Entity
@Table(name="contect")
public class Contect extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	private int aid;
	
	private String address;
	
	private String telphone;
	
	@Lob
	private String content;
	
	@Override
	public Integer getId() {
		return aid;
	}

	public int getAid() {
		return aid;
	}

	public void setAid(int aid) {
		this.aid = aid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
