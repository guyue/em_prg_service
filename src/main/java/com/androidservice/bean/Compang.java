package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the compangs database table.
 * 
 */
@Entity
@Table(name="compangs")
public class Compang  extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int cid;

	private String address;

	private byte authentication;

	private byte checkstatus;

	private int commentsize;

	@Lob
	private String content;
	
	private String contact;

	private String distance;

	private int grade;

	private String image;

	private float latitude;

	private String location;

	private float longitude;

	private int looktime;

	private String manage;

	private String name;

	private String path;

	private byte preferential;

	private byte recommend;

	private int stars;
	
	private String slogan;

	private String tel;

	private int teltime;

	private String title;
	
	private byte showfirst;
	
	private int number;
	
	private String area;
	
	private String reason;

	@Column(name="update_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDatetime;
	
	@Column(name="expiration_time")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationtime;

	//bi-directional many-to-one association to Commodity
	@OneToMany(mappedBy="compang")
	private List<Commodity> commodities;

	//bi-directional many-to-one association to Smallclass
	@ManyToOne
	@JoinColumn(name="sid")
	private SmallClass smallclass;

	//bi-directional many-to-one association to Picture
	@OneToMany(mappedBy="compang")
	private List<Picture> pictures;

	//bi-directional many-to-one association to ServiceArea
	@OneToMany(mappedBy="compang")
	private List<ServiceArea> serviceAreas;
	
	@ManyToOne
	@JoinColumn(name="uid")
	private User user;

	public Compang() {
	}

	public int getCid() {
		return this.cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public byte getAuthentication() {
		return this.authentication;
	}

	public void setAuthentication(byte authentication) {
		this.authentication = authentication;
	}

	public byte getCheckStatus() {
		return this.checkstatus;
	}

	public void setCheckStatus(byte checkstatus) {
		this.checkstatus = checkstatus;
	}

	public int getCommentsize() {
		return this.commentsize;
	}

	public void setCommentsize(int commentsize) {
		this.commentsize = commentsize;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getDistance() {
		return this.distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public int getGrade() {
		return this.grade;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public float getLatitude() {
		return this.latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getLongitude() {
		return this.longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public int getLooktime() {
		return this.looktime;
	}

	public void setLooktime(int looktime) {
		this.looktime = looktime;
	}

	public String getManage() {
		return this.manage;
	}

	public void setManage(String manage) {
		this.manage = manage;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public byte getPreferential() {
		return this.preferential;
	}

	public void setPreferential(byte preferential) {
		this.preferential = preferential;
	}

	public byte getRecommend() {
		return this.recommend;
	}

	public void setRecommend(byte recommend) {
		this.recommend = recommend;
	}

	public int getStars() {
		return this.stars;
	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public String getTel() {
		return this.tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getTeltime() {
		return this.teltime;
	}

	public void setTeltime(int teltime) {
		this.teltime = teltime;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public byte getShowfirst() {
		return showfirst;
	}

	public void setShowfirst(byte showfirst) {
		this.showfirst = showfirst;
	}
	
	public Date getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(Date updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public Date getExpirationtime() {
		return expirationtime;
	}

	public void setExpirationtime(Date expirationtime) {
		this.expirationtime = expirationtime;
	}

	public List<Commodity> getCommodities() {
		return this.commodities;
	}

	public void setCommodities(List<Commodity> commodities) {
		this.commodities = commodities;
	}

	public SmallClass getSmallClass() {
		return this.smallclass;
	}

	public void setSmallClass(SmallClass smallclass) {
		this.smallclass = smallclass;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Picture> getPictures() {
		return this.pictures;
	}

	public void setPictures(List<Picture> pictures) {
		this.pictures = pictures;
	}

	public List<ServiceArea> getServiceAreas() {
		return this.serviceAreas;
	}

	public void setServiceAreas(List<ServiceArea> serviceAreas) {
		this.serviceAreas = serviceAreas;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public Integer getId() {
		return cid;
	}

}
