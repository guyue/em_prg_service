package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the openarea database table.
 * 
 */
@Entity
@Table(name="openarea")
public class OpenArea extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int oid;

	private String area;
	
	private String abridge;

	private String dataname;

	private byte isopen;
	
	private int visit;

	@Column(name="packagename")
	private String packagename;

	public OpenArea() {
	}

	public int getOid() {
		return this.oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getAbridge() {
		return abridge;
	}

	public void setAbridge(String abridge) {
		this.abridge = abridge;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getDataname() {
		return this.dataname;
	}

	public void setDataname(String dataname) {
		this.dataname = dataname;
	}

	public byte getIsopen() {
		return this.isopen;
	}

	public void setIsopen(byte isopen) {
		this.isopen = isopen;
	}

	public String getPackagename() {
		return this.packagename;
	}

	public void setPackagename(String packagename) {
		this.packagename = packagename;
	}

	public int getVisit() {
		return visit;
	}

	public void setVisit(int visit) {
		this.visit = visit;
	}

	@Override
	public Integer getId() {
		return oid;
	}
}

