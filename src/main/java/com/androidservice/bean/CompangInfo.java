package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the compang_info database table.
 * 
 */
@Entity
@Table(name="compang_info")
public class CompangInfo  extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int gid;

	private String address;

	private String image;

	private String info;
	
	private int stars;
	
	private int assess;

	private byte isAuthentication;

	private String looksize;
	
	private String tel_time;
	
	private String commentsize;

	private String name;

	private String path;

	private String updatetime;

	//bi-directional many-to-one association to Compang
	@ManyToOne
	@JoinColumn(name="cid")
	private Compang compang;

	public CompangInfo() {
	}

	public int getGid() {
		return this.gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getInfo() {
		return this.info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getStars() {
		return stars;
	}

	public void setStars(int stars) {
		this.stars = stars;
	}

	public int getAssess() {
		return assess;
	}

	public void setAssess(int assess) {
		this.assess = assess;
	}

	public byte getIsAuthentication() {
		return this.isAuthentication;
	}

	public void setIsAuthentication(byte isAuthentication) {
		this.isAuthentication = isAuthentication;
	}

	public String getLooksize() {
		return this.looksize;
	}

	public void setLooksize(String looksize) {
		this.looksize = looksize;
	}

	public String getTel_time() {
		return tel_time;
	}

	public void setTel_time(String tel_time) {
		this.tel_time = tel_time;
	}

	public String getCommentsize() {
		return commentsize;
	}

	public void setCommentsize(String commentsize) {
		this.commentsize = commentsize;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(String updatetime) {
		this.updatetime = updatetime;
	}

	public Compang getCompang() {
		return this.compang;
	}

	public void setCompang(Compang compang) {
		this.compang = compang;
	}

	@Override
	public Integer getId() {
		return gid;
	}

}
