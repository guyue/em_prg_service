package com.androidservice.bean;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the service_area database table.
 * 
 */
@Entity
@Table(name="service_area")
public class ServiceArea extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int sid;

	private String area;

	//bi-directional many-to-one association to Compang
	@ManyToOne
	@JoinColumn(name="cid")
	private Compang compang;

	public ServiceArea() {
	}

	public int getSid() {
		return this.sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getArea() {
		return this.area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Compang getCompang() {
		return this.compang;
	}

	public void setCompang(Compang compang) {
		this.compang = compang;
	}

	@Override
	public Integer getId() {
		return sid;
	}

}
