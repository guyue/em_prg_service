package com.androidservice.android.action;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.struts2.dispatcher.RequestMap;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import com.androidservice.common.EMProperties;

import com.opensymphony.xwork2.Action;

public class DownloadAction implements Action,SessionAware,ServletResponseAware, RequestAware{
	private HttpServletResponse response;
	
	private static final EMProperties EMProp = EMProperties.getInstance();
	
	@SuppressWarnings("rawtypes")
	private SessionMap session;

	private HttpServletRequest request;

	
	public String download(){
		try {
			doPost(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
//	private String tempName;

	@SuppressWarnings("static-access")
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		String pathsavefile="E:/apache-tomcat-6.0.36/webapps/mobile/apk/YiminAndroid_v0.4.apk";
//		String pathsavefile="/var/lib/tomcat6/webapps/mobile/apk/YiminAndroid_v0.4.apk";
		String pathsavefile= EMProp.getProperty("APK_PATH");
		String fileName= EMProp.getProperty("APK_NAME");
//		String fileName="YiminAndroid.apk";
		
		try{
	       response.reset();
	       response.setContentType("APPLICATION/OCTET-STREAM");
//	       fileName=response.encodeURL(new String(fileName.getBytes(),"ISO8859_1"));
	       response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");

	       ServletOutputStream out = response.getOutputStream();
	       InputStream inStream=new FileInputStream(pathsavefile);

	       byte[] b = new byte[1024];
	       int len;
	       while((len=inStream.read(b)) >0)
	    	   out.write(b,0,len);
	       response.setStatus(response.SC_OK);
	       response.flushBuffer();

	       out.close();
	       inStream.close();
       }
       catch (Exception e){
    	   System.out.println(e);
       }
	}
	
	 public void doGet(HttpServletRequest request, HttpServletResponse response) throws
     ServletException, IOException {
		 doPost(request, response);
	 }
	
//	public InputStream getInputStream() throws Exception {
//		return new java.io.FileInputStream(tempName);
//	}
	@Override
	public String execute() throws Exception {
		return SUCCESS;
	}


	public void setRequest(HttpServletRequest map) {
		this.request = (HttpServletRequest) map;
	}

	public HttpServletRequest getRequest() {
		return request;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map  map) {
		this.session = (SessionMap) map;
	}

	@SuppressWarnings("rawtypes")
	public SessionMap getSession() {
		return session;
	}

	@Override
	public void setRequest(Map<String, Object> arg0) {
		
	}

}
