package com.androidservice.android.action;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.ArrayList;
//import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts2.ServletActionContext;
//import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.RequestMap;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.androidservice.android.service.IEMService;
import com.androidservice.bean.Advertisement;
import com.androidservice.bean.BigClass;
import com.androidservice.bean.Comment;
import com.androidservice.bean.Commodity;
import com.androidservice.bean.Compang;
import com.androidservice.bean.Contect;
import com.androidservice.bean.FourClass;
import com.androidservice.bean.OpenArea;
import com.androidservice.bean.Opinion;
import com.androidservice.bean.Picture;
import com.androidservice.bean.Place;
import com.androidservice.bean.ServiceArea;
import com.androidservice.bean.Shangxun;
import com.androidservice.bean.SmallClass;
import com.androidservice.bean.UpdateTime;
import com.androidservice.bean.User;
import com.androidservice.bean.Version;
import com.androidservice.common.EMProperties;
import com.androidservice.dao.AdvertisementDao;
import com.androidservice.dao.BigClassDao;
import com.androidservice.dao.CompangDao;
import com.androidservice.dao.CompangInfoDao;
import com.androidservice.dao.ContectDao;
import com.androidservice.dao.OpenAreaDao;
import com.androidservice.dao.PlaceDao;
import com.androidservice.dao.ShangxunDao;
import com.androidservice.dao.SmallClassDao;
import com.androidservice.dao.UpdateTimeDao;
import com.androidservice.dao.UserDao;
import com.androidservice.dao.VersionDao;
import com.opensymphony.xwork2.ActionSupport;
import com.androidservice.util.MiscUtils;

@SuppressWarnings("restriction")
@Component
public class CompangAction extends ActionSupport implements SessionAware,ServletResponseAware, RequestAware{
	
	private static final long serialVersionUID = 1L;
	
	private static final EMProperties EMProp = EMProperties.getInstance();
	
	private String bid;
	private String sid;
	private String cid;
	private String gid;
	private String xid;
	private String uuid;
	
	private String compangname;
	private String address;

	private String contacts;

	private String description;

	private String location;

	private String propaganda;

	private String service;

	private String serviceArea;
	
	private String slogan;

	private String tel;
	
	private String companglook;
	
	private String compangtel; 
	
	private String compangcomment;
	
	private String shangxunlook;
	
	private String shangxuncomment;
	
	private String content;
	
	private String opinionname;
	
	private String opinionphone;
	
	private String score;
	
	private String number;
	
	private String ver;
	
	private String start;
	
	private String search;
	
	private String lat;
	
	private String lng;
	
	private String status;
	
	private String place;
	
	private String visit;
	
	@Autowired
	private AdvertisementDao adverDao;
	
	@Autowired
	private CompangInfoDao comInfoDao;
	
	@Autowired
	private CompangDao compangDao;
	
	@Autowired
	private ShangxunDao shangxunDao;
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private VersionDao versionDao;
	
	@Autowired
	private BigClassDao bigClassDao;
	
	@Autowired
	private SmallClassDao smallClassDao;
	
	@Autowired
	private ContectDao contectDao;
	
	@Autowired
	private UpdateTimeDao updatetimeDao;
	
	@Autowired
	private OpenAreaDao openareaDao;
	
	@Autowired
	private PlaceDao placeDao;
	
	@Resource(name = "eMServiceImpl")
	private IEMService iemService;
	
	private HttpServletResponse response;
	
	@SuppressWarnings("rawtypes")
	private SessionMap session;

	private RequestMap request;

	@SuppressWarnings("rawtypes")
	@Override
	public void setRequest(Map map) {
		this.request = (RequestMap) map;
	}

	public RequestMap getRequest() {
		return request;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	public HttpServletResponse getResponse() {
		return response;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setSession(Map  map) {
		this.session = (SessionMap) map;
	}

	@SuppressWarnings("rawtypes")
	public SessionMap getSession() {
		return session;
	}
	
	public String getBid() {
		return bid;
	}

	public void setBid(String bid) {
		this.bid = bid;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getXid() {
		return xid;
	}

	public void setXid(String xid) {
		this.xid = xid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContacts() {
		return contacts;
	}

	public void setContacts(String contacts) {
		this.contacts = contacts;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPropaganda() {
		return propaganda;
	}

	public void setPropaganda(String propaganda) {
		this.propaganda = propaganda;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServiceArea() {
		return serviceArea;
	}

	public void setServiceArea(String serviceArea) {
		this.serviceArea = serviceArea;
	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getCompangname() {
		return compangname;
	}

	public void setCompangname(String compangname) {
		this.compangname = compangname;
	}

	public String getCompanglook() {
		return companglook;
	}

	public void setCompanglook(String companglook) {
		this.companglook = companglook;
	}

	public String getCompangtel() {
		return compangtel;
	}

	public void setCompangtel(String compangtel) {
		this.compangtel = compangtel;
	}

	public String getCompangcomment() {
		return compangcomment;
	}

	public void setCompangcomment(String compangcomment) {
		this.compangcomment = compangcomment;
	}

	public String getShangxunlook() {
		return shangxunlook;
	}

	public void setShangxunlook(String shangxunlook) {
		this.shangxunlook = shangxunlook;
	}

	public String getShangxuncomment() {
		return shangxuncomment;
	}

	public void setShangxuncomment(String shangxuncomment) {
		this.shangxuncomment = shangxuncomment;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getOpinionname() {
		return opinionname;
	}

	public void setOpinionname(String opinionname) {
		this.opinionname = opinionname;
	}

	public String getOpinionphone() {
		return opinionphone;
	}

	public void setOpinionphone(String opinionphone) {
		this.opinionphone = opinionphone;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getSearch() {
		return search;
	}
	
	public void setSearch(String search) {
		this.search = search;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getVisit() {
		return visit;
	}

	public void setVisit(String visit) {
		this.visit = visit;
	}

	public String getCompangsByBid(){
		
		return null;
	}
	
	public String getAdvertisement(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		List<Advertisement> ad = null;
		if(null != status){
			ad = adverDao.findAdvrByType(status);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		for(int i = 0;i < ad.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", ad.get(i).getAid());
			map.put("type", ad.get(i).getType());
			map.put("type_id", ad.get(i).getTypeId());
			map.put("url", url + ad.get(i).getPath() + ad.get(i).getImage());
			ret.add(map);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getCompangsShowFirst(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		List<Compang> compang = compangDao.findCompangByShowFirst();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		for(int i = 0;i < compang.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", compang.get(i).getCid());
			map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
			map.put("name", compang.get(i).getName());
			map.put("title", compang.get(i).getTitle());
			Integer pid = Integer.parseInt(compang.get(i).getArea());
			Place place = placeDao.findPlaceByPid(pid);
			map.put("service area",place.getArea());
			map.put("longitude", compang.get(i).getLongitude());
			map.put("latitude", compang.get(i).getLatitude());
			map.put("looktime", compang.get(i).getLooktime());
			map.put("teltime", compang.get(i).getTeltime());
			map.put("tel", compang.get(i).getTel());
			map.put("user", compang.get(i).getContact());
			ret.add(map);
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getThreeMenuByShowFirst(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		List<SmallClass> small = smallClassDao.findSmallClassShowFirst();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		for(int i = 0;i < small.size();i ++){
			int compang_num = compangDao.findCompangNumberBySid(small.get(i).getSid());
			List<Compang> compang = compangDao.findCompangsBySid(small.get(i).getSid());
			int au_num = 0;
			for(int j = 0;j < compang.size();j ++){
				if(compang.get(j).getAuthentication() == 0){
					au_num ++;
				}
			}
			JSONObject map = new JSONObject();
			map.put("id", small.get(i).getSid());
			map.put("name", small.get(i).getName());
			map.put("url", url + small.get(i).getPath() + small.get(i).getImage());
			map.put("compang_num", compang_num);
			map.put("auth_num", au_num);
			ret.add(map);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	public String getCompangsFirstByBid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != bid){
			id = Integer.parseInt(bid);
		}else{
			return null;
		}
		
		int begin = 0;
		int num = 0;
		int total = 0;
		if(null != start){
			begin = Integer.parseInt(start);
		}
		if(null != number){
			num = Integer.parseInt(number);
		}
		total = compangDao.findAllCompangNumber(id);
		if(num == 0){
			num = total;
		}
		
//		List<Compang> compang = compangDao.findCompangByCheckstatusAndBid(id, begin, num);
		List<Compang> compang = compangDao.findCompangByCheckstatusAndBid(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < compang.size(); i ++){			
			JSONObject map = new JSONObject();
			map.put("id", Integer.toString(compang.get(i).getCid()));
			if(null != compang.get(i).getImage() && compang.get(i).getImage().length() > 0){
				map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
			}else{
				map.put("url", url + "img/" + "default_img.png");
			}
			map.put("title", compang.get(i).getTitle());
			map.put("name", compang.get(i).getName());
			map.put("manage", compang.get(i).getManage());
			map.put("location", compang.get(i).getLocation());
			
			Integer pid = Integer.parseInt(compang.get(i).getArea());
			Place place = placeDao.findPlaceByPid(pid);
			map.put("service area",place.getArea());
			
			map.put("longitude", compang.get(i).getLongitude());
			map.put("latitude", compang.get(i).getLatitude());
			map.put("looktime", compang.get(i).getLooktime());
			map.put("teltime", compang.get(i).getTeltime());
			map.put("tel", compang.get(i).getTel());
			map.put("authentication", compang.get(i).getAuthentication());
			map.put("user", compang.get(i).getContact());
			info.add(map);
		}
		ret.put("Total", total);
		ret.put("compangs", info.toString());
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getAllSmallClass(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONObject bigclasslist = new JSONObject();
		List<BigClass> bigclass = bigClassDao.getAllBigclass();
		for(int i =0;i < bigclass.size();i ++){
			if(bigclass.get(i).getBid() != 1){
				int compang_num = compangDao.findAllCompangNumber(bigclass.get(i).getBid());
				List<SmallClass> smallclass = iemService.getSmallclassByBid(bigclass.get(i).getBid());
				JSONObject middleclasslist = new JSONObject();
				middleclasslist.put("id", bigclass.get(i).getBid());
				middleclasslist.put("name", bigclass.get(i).getName());
				middleclasslist.put("url",url + bigclass.get(i).getPath() + bigclass.get(i).getImage());
				JSONArray small = new JSONArray();
				for(int j = 0;j < smallclass.size(); j ++){
					JSONObject map = new JSONObject();
					map.put("sid",Integer.toString(smallclass.get(j).getSid()));
					map.put("name",smallclass.get(j).getName());
					map.put("url", url + smallclass.get(j).getPath() + smallclass.get(j).getImage());
					small.add(map);
				}
				middleclasslist.put("smallClass", small);
				middleclasslist.put("compang_num", compang_num);
				bigclasslist.put("middleClass" + i, middleclasslist);
			}
		}
		ret = bigclasslist;
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	private static final double EARTH_RADIUS = 6378.137 * 1000;
//	private static final double dis = 50;
	private static double rad(double d){ 
       return d * Math.PI / 180.0; 
    }
	public static double GetDistance(double lat1, double lng1, double lat2, double lng2){ 
       double radLat1 = rad(lat1); 
       double radLat2 = rad(lat2); 
       double a = radLat1 - radLat2; 
       double b = rad(lng1) - rad(lng2); 
       double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) +  
    		   Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2))); 
       s = s * EARTH_RADIUS ; 
       s = Math.round(s * 10000) / 10000; 
       return s; 
    }
	
	public String getFirstPageCompang(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int begin = 0;
		int num = 0;
		int total = 0;
		if(null != start){
			begin = Integer.parseInt(start);
		}
		if(null != number){
			num = Integer.parseInt(number);
		}
		total = compangDao.findNumberCompangByAuthent();
		if(num == 0){
			num = total;
		}
		List<Compang> compang = compangDao.findCompangsByNumber(begin, num);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < compang.size(); i ++){			
			JSONObject map = new JSONObject();
			map.put("id", Integer.toString(compang.get(i).getCid()));
			map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
			map.put("title", compang.get(i).getTitle());
			map.put("name", compang.get(i).getName());
			map.put("location", compang.get(i).getLocation());
			
			Integer pid = Integer.parseInt(compang.get(i).getArea());
			Place place = placeDao.findPlaceByPid(pid);
			map.put("service area",place.getArea());
			
			map.put("longitude", compang.get(i).getLongitude());
			map.put("latitude", compang.get(i).getLatitude());
			map.put("looktime", compang.get(i).getLooktime());
			map.put("teltime", compang.get(i).getTeltime());
			map.put("tel", compang.get(i).getTel());
			map.put("authentication", compang.get(i).getAuthentication());
			info.add(map);
		}
		
		ret.put("Total", total);
		ret.put("compangs", info.toString());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getCompangsBySid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}else{
			return null;
		}
		List<Compang> compang = compangDao.findCompangsBySid(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < compang.size(); i ++){			
			JSONObject map = new JSONObject();
			map.put("id", Integer.toString(compang.get(i).getCid()));
			map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
			map.put("title", compang.get(i).getTitle());
			map.put("name", compang.get(i).getName());
			map.put("location", compang.get(i).getLocation());
			
			Integer pid = Integer.parseInt(compang.get(i).getArea());
			Place place = placeDao.findPlaceByPid(pid);
			map.put("service area",place.getArea());
			
			map.put("longitude", compang.get(i).getLongitude());
			map.put("latitude", compang.get(i).getLatitude());
			map.put("looktime", compang.get(i).getLooktime());
			map.put("teltime", compang.get(i).getTeltime());
			map.put("tel", compang.get(i).getTel());
			map.put("authentication", compang.get(i).getAuthentication());
			map.put("user", compang.get(i).getContact());
			info.add(map);
		}
		ret.put("compangs", info.toString());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getShangxunBySid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		List<Shangxun> shangxun = iemService.getShangxunBySid(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < shangxun.size();i ++){
			if(shangxun.get(i).getCheckStatus() == 0){
				JSONObject map = new JSONObject();
				map.put("id", shangxun.get(i).getXid());
				map.put("name", shangxun.get(i).getShangxunName());
				map.put("title", shangxun.get(i).getTitle());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				map.put("updatetime", formatter.format(shangxun.get(i).getUpdateTime()));
				map.put("looktime", shangxun.get(i).getLooktime());
				map.put("commentsize", shangxun.get(i).getCommentSize());
				map.put("url", url + shangxun.get(i).getPath() + shangxun.get(i).getImage());
				
				info.add(map);
			}
		}
		ret.put("Shangxun", info.toString());
		
		List<Shangxun> showfirst = shangxunDao.findShangxunByShowFrist();
		JSONArray showfirst_list = new JSONArray();
		for(int i = 0;i < showfirst.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", showfirst.get(i).getXid());
			map.put("url", url + showfirst.get(i).getPath() + showfirst.get(i).getImage());
			
			showfirst_list.add(map);
		}
		ret.put("guanggao", info.toString());
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getCompangInfoByCid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != cid){
			id = Integer.parseInt(cid);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		//Compang
		Compang info = iemService.getCompangInfoByCid(id);
		JSONObject map = new JSONObject();
		
		map.put("id", info.getCid());
		map.put("title", info.getTitle());
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		map.put("updatetime", formatter.format(info.getUpdateDatetime()));
		map.put("name", info.getName());
		
		Integer pid = Integer.parseInt(info.getArea());
		Place place = placeDao.findPlaceByPid(pid);
		map.put("area",place.getArea());
		
		map.put("address", info.getAddress());
		map.put("looksize", info.getLooktime());
		map.put("telsize", info.getTeltime());
		map.put("latitude", info.getLatitude());
		map.put("longitude", info.getLongitude());
		map.put("content", info.getContent());
		map.put("tel", info.getTel());
		map.put("contact", info.getContact());
		map.put("authentication", info.getAuthentication());
		if(null != info.getImage() && info.getImage().length() > 0){
			map.put("url", url + info.getPath() + info.getImage());
		}else{
			map.put("url", url + "img/" + "default_img.png");
		}
		//map.put("url", url + info.getPath() + info.getImage());
		ret.put("compang", map.toString());
		
		//Picture
		List<Picture> picture = iemService.getPictureByCid(info.getCid());
		JSONArray picture_list = new JSONArray();
		for(int i = 0;i < picture.size();i ++){
			JSONObject picture_map = new JSONObject();
			picture_map.put("id", picture.get(i).getPid());
			picture_map.put("description", picture.get(i).getDescription());
			picture_map.put("path", picture.get(i).getPath());
			picture_map.put("smallurl", url + picture.get(i).getPath() + picture.get(i).getSmall());
			picture_map.put("bigurl", url + picture.get(i).getPath() + picture.get(i).getBig());
			
			picture_list.add(picture_map);
		}
		ret.put("pictures", picture_list);
		
		
		//Commodity
		List<Commodity> commoditys = iemService.getCommoditysByCid(info.getCid());
		JSONArray comm_list = new JSONArray();
		for(int i = 0;i < commoditys.size();i ++){
			JSONObject comm_map = new JSONObject();
			comm_map.put("yid", commoditys.get(i).getYid());
			comm_map.put("name", commoditys.get(i).getName());
			comm_map.put("commodity_explain", commoditys.get(i).getCode());
//			comm_map.put("norm", commoditys.get(i).getNorm());
			comm_map.put("now_price",commoditys.get(i).getDeals());
			comm_map.put("old_price", commoditys.get(i).getPrice());
			comm_map.put("use_explain", commoditys.get(i).getContent());
			comm_map.put("url", url + commoditys.get(i).getPath() + commoditys.get(i).getImage());
			comm_map.put("detail_url",url + commoditys.get(i).getDetail_path() + commoditys.get(i).getDetail_img());
			
			comm_list.add(comm_map);
		}
		ret.put("commoditys", comm_list.toString());
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getShangxunInfoByXid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != xid && xid.length() > 0){
			id = Integer.parseInt(xid);
		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		//Shangxun
		Shangxun shangxun = iemService.getShangxunByXid(id);
		if(shangxun.getCheckStatus() == 0){
			JSONObject map = new JSONObject();
			map.put("id", shangxun.getXid());
			map.put("name", shangxun.getShangxunName());
			map.put("title", shangxun.getTitle());
			map.put("source", shangxun.getSource());
			map.put("content", shangxun.getContent());
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			map.put("updatetime", formatter.format(shangxun.getUpdateTime()));
			
			map.put("looktime", shangxun.getLooktime());
			map.put("commentsize", shangxun.getCommentSize());
			map.put("url", url + shangxun.getPath() + shangxun.getImage());
			
			ret.put("info",map);
		}
		
		//Comment
		List<Comment> comments = iemService.getShangxunCommentByXid(shangxun.getXid());
		JSONArray comm_list = new JSONArray();
		for(int i = 0;i < comments.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", comments.get(i).getCid());
//			User user = iemService.getUserByUid(comments.get(i).getUser().getUid());
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
			map.put("updatetime", formatter.format(comments.get(i).getUpdateTime()));
			map.put("content", comments.get(i).getContent());
			
			comm_list.add(map);
		}
		ret.put("comments", comm_list);
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getCompangComment(){
		int id = 0;
		if(null != gid && gid.length() > 0){
			id = Integer.parseInt(gid);
		}
		List<Comment> comments = iemService.getCompangCommentByGid(id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		for(int i = 0;i < comments.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", comments.get(i).getCid());
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd");
			map.put("updatetime", formatter.format(comments.get(i).getUpdateTime()));
			map.put("content", comments.get(i).getContent());
			map.put("score", comments.get(i).getScore());
			
			ret.add(map);
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String putCompangInfo(){
		Compang compang = new Compang();
		String compang_name = "";
		String compang_address = "";
		String compang_content = "";
		String compang_tel = "";
		String compang_service = "";
		String compang_contact = "";
		String compang_slogan = "";
		String compang_serviceArea = "";
		
		if(null != compangname){
//				compang_name = new String(compangname.getBytes("iso-8859-1"),"UTF-8");
			compang_name = compangname;
		}
		if(null != address){
//				compang_address = new String(address.getBytes("iso-8859-1"),"UTF-8");
			compang_address = address;
		}
		if(null != content){
//				compang_content = new String(content.getBytes("iso-8859-1"),"UTF-8");
			compang_content = content;
		}
		if(null != tel){
//				compang_tel = new String(tel.getBytes("iso-8859-1"),"UTF-8");
			compang_tel = tel;
		}
		if(null != service){
//				compang_service = new String(service.getBytes("iso-8859-1"),"UTF-8");
			compang_service = service;
		}
		if(null != contacts){
//				compang_contact = new String(contacts.getBytes("iso-8859-1"),"UTF-8");
			compang_contact = contacts;
		}
		if(null != slogan){
//				compang_slogan = new String(slogan.getBytes("iso-8859-1"),"UTF-8");
			compang_slogan = slogan;
		}
		if(null != serviceArea){
//				compang_serviceArea = new String(serviceArea.getBytes("iso-8859-1"),"UTF-8");
			compang_serviceArea = serviceArea;
		}
			
		compang.setCheckStatus((byte)1);
		compang.setAuthentication((byte)1);
		compang.setCommentsize(0);
		compang.setDistance("");
		compang.setGrade(0);
		compang.setImage("");
		compang.setLatitude(0);
		compang.setLocation("");
		compang.setLongitude(0);
		compang.setLooktime(0);
		compang.setManage("");
		compang.setPath("");
		compang.setPreferential((byte)1);
		compang.setRecommend((byte)1);
		compang.setStars(0);
		compang.setTeltime(0);
		Timestamp now = new Timestamp(System.currentTimeMillis()); 
		compang.setUpdateDatetime(now);
		
		compang.setName(compang_name);
		compang.setAddress(compang_address);
		compang.setContent(compang_content);
		compang.setTel(compang_tel);
		int sid = iemService.getSmallClassIdByName(compang_service);
		SmallClass small = iemService.getSmallClassBySid(sid);
		compang.setSmallClass(small);
		compang.setContact(compang_contact);
		compang.setSlogan(compang_slogan);
		compang.setTitle(compang_slogan);
		compangDao.persist(compang);
		
		Compang com = iemService.getCompangByName(compang_name);
		String[] new_area = compang_serviceArea.split(",");
		for(int i = 0;i < new_area.length;i ++){
			ServiceArea area = new ServiceArea();
			area.setCompang(com);
			area.setArea(new_area[i]);
			iemService.saveNewCompangArea(area);
		}
		
		sendEmail();
		
		return null;
	}
	
	public String sendEmail() {
		String content = "Released new businesses registered, please look it and audit it";
		String subject = "Post new businesses";
		String emailbox = "875161690@qq.com";
		Properties pro = new Properties();
		pro.put("mail.smtp.auth",EMProp.getProperty("smtp_ssl"));
		pro.put("mail.transport.protocol", EMProp.getProperty("smtp_protocol"));
		pro.put("mail.smtp.host", EMProp.getProperty("smtp_url"));
		Session session = Session.getInstance(pro,new javax.mail.Authenticator() {
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
				return new javax.mail.PasswordAuthentication(EMProp.getProperty("smtp_user"), EMProp.getProperty("smtp_pass"));
				}
			});
		session.setDebug(true);
		
		Message message = new MimeMessage(session);
		try {
			message.setFrom(new InternetAddress(EMProp.getProperty("smtp_user")));
			message.setSubject(subject.toString());
			message.setRecipients(RecipientType.TO,InternetAddress.parse(emailbox.toString()));
			message.setContent(content.toString(), "text/html;charset=utf-8");
			Transport.send(message);
			session.getTransport("smtp").close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String putLookAndTelAndCommentSize(){
		int id1 = 0;
		int id2 = 0;
		int oldnum = 0;
		int newnum = 0;
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		boolean status = false;
		JSONObject ret = new JSONObject();
		
		if(null != cid && cid.length() > 0){
			id1 = Integer.parseInt(cid);
			if(null != companglook){
				Compang comp = compangDao.findCompangByCid(id1);
				oldnum = comp.getLooktime();
				newnum = oldnum + Integer.parseInt(companglook);
				status = compangDao.updateCompangLookSize(id1, newnum);
			}
			if(null != compangtel){
				Compang comp = iemService.getCompangByCid(id1);
				oldnum = comp.getTeltime();
				newnum = oldnum + Integer.parseInt(compangtel);
				status = compangDao.updateCompangTelSize(id1, newnum);
			}
			if(null != compangcomment){
				Compang comp = iemService.getCompangByCid(id1);
				oldnum = comp.getCommentsize();
				newnum = oldnum + Integer.parseInt(compangcomment);
				status = compangDao.updateCompangCommentSize(id1, newnum);
			}
		}
		if(null != xid && xid.length() > 0){
			id2 = Integer.parseInt(xid);
			if(null != shangxunlook){
				Shangxun shangxun = iemService.getShangxunByXid(id2);
				oldnum = Integer.parseInt(shangxun.getLooktime());
				newnum = oldnum + Integer.parseInt(shangxunlook);
				status = shangxunDao.updateShangxunLookSize(id2, String.valueOf(newnum));
			}
			if(null != shangxuncomment){
				Shangxun shangxun = iemService.getShangxunByXid(id2);
				oldnum = Integer.parseInt(shangxun.getCommentSize());
				newnum = oldnum + Integer.parseInt(shangxuncomment);
				status = shangxunDao.updateShangxunCommentSize(id2, String.valueOf(newnum));
			}
		}
		ret.put("Put_stauts", status);
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getSearchCompangsByService(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
//		try {
//			service = new String(service.getBytes("iso-8859-1"),"UTF-8");
//		} catch (UnsupportedEncodingException e1) {
//			e1.printStackTrace();
//		}
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		List<Compang> compang = compangDao.findCompangsByservice(service,id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < compang.size(); i ++){			
			if(compang.get(i).getCheckStatus() == 0){
				JSONObject map = new JSONObject();
				map.put("id", Integer.toString(compang.get(i).getCid()));
				map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
				map.put("title", compang.get(i).getTitle());
				map.put("name", compang.get(i).getName());
				map.put("location", compang.get(i).getLocation());
				
				Integer pid = Integer.parseInt(compang.get(i).getArea());
				Place place = placeDao.findPlaceByPid(pid);
				map.put("area",place.getArea());
				
				map.put("longitude", compang.get(i).getLongitude());
				map.put("latitude", compang.get(i).getLatitude());
				map.put("looktime", compang.get(i).getLooktime());
				map.put("teltime", compang.get(i).getTeltime());
				map.put("tel", compang.get(i).getTel());
				map.put("authentication", compang.get(i).getAuthentication());
				map.put("user", compang.get(i).getContact());
				
				info.add(map);
			}
		}
		ret.put("compangs", info.toString());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getSearchByServiceAndBid(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		int id = 0;
		if(null != bid){
			id = Integer.parseInt(bid);
		}
//		try {
//			service = new String(service.getBytes("iso-8859-1"),"UTF-8");
//		} catch (UnsupportedEncodingException e1) {
//			e1.printStackTrace();
//		}
		List<Compang> compang = compangDao.findCompangsByserviceAndBid(service,id);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		JSONArray info = new JSONArray();
		for(int i = 0;i < compang.size(); i ++){			
			if(compang.get(i).getCheckStatus() == 0){
				JSONObject map = new JSONObject();
				map.put("id", Integer.toString(compang.get(i).getCid()));
				map.put("url", url + compang.get(i).getPath() + compang.get(i).getImage());
				map.put("title", compang.get(i).getTitle());
				map.put("name", compang.get(i).getName());
				map.put("location", compang.get(i).getLocation());
				
				Integer pid = Integer.parseInt(compang.get(i).getArea());
				Place place = placeDao.findPlaceByPid(pid);
				map.put("area",place.getArea());
				
				map.put("longitude", compang.get(i).getLongitude());
				map.put("latitude", compang.get(i).getLatitude());
				map.put("looktime", compang.get(i).getLooktime());
				map.put("teltime", compang.get(i).getTeltime());
				map.put("tel", compang.get(i).getTel());
				map.put("authentication", compang.get(i).getAuthentication());
				map.put("user", compang.get(i).getContact());
				
				info.add(map);
			}
		}
		ret.put("compangs", info.toString());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String putOpinionInfo() throws UnsupportedEncodingException{
		String con = "";
		String name = "";
		String phone = "";
		if(content != null){
//				con = new String(content.getBytes("iso-8859-1"),"UTF-8");
			con  = content;
		}
		if(null != opinionname){
//				name = new String(opinionname.getBytes("iso-8859-1"),"UTF-8");
			name = opinionname;
		}
		if(null != opinionphone){
//				phone = new String(opinionphone.getBytes("iso-8859-1"),"UTF-8");
			phone = opinionphone;
		}
		Opinion opinion = new Opinion();
		opinion.setContent(con);
		opinion.setName(name);
		opinion.setPhone(phone);
		iemService.saveOpinionInfo(opinion);
		return null;
	}
	
	public String putCompangCommet(){
		Comment comment =new Comment();
		comment.setType("0");
		comment.setTypeId(Integer.parseInt(gid));
		String con = "";
		if(null != content){
			try {
				con = new String(content.getBytes("iso-8859-1"),"UTF-8");
				con = content;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		comment.setContent(con);
		Timestamp now = new Timestamp(System.currentTimeMillis()); 
		comment.setUpdateTime(now);
		comment.setScore(Integer.parseInt(score));
		comment.setCommentator("");
//		int num = 0;
//		if(null != number && number.length() > 0){
//			num = Integer.parseInt(number);
//		}
//		User user = iemService.getUserByNumber(num);
//		comment.setUser(user);
		iemService.saveNewComment(comment);
		
		List<Comment> comments = iemService.getCompangCommentByGid(Integer.parseInt(gid));
		int sun = 0;
		for(int i = 0;i < comments.size();i ++){
			sun = sun + comments.get(i).getScore();
		}
		int average = 0;
		int a = 0;
		if(sun > 0){
			average = sun / comments.size();
			a = sun % comments.size();
		}
		if(a >= 5){
			average = average + 1;
		}
//		CompangInfo compangInfo = iemService.getCompangInfoByGid(Integer.parseInt(gid));
		Compang compang = iemService.getCompangByCid(Integer.parseInt(cid));
		compang.setStars(average);
		iemService.saveCompang(compang);
		return null;
	}
	
	public String putShangxunComment(){
		Comment comment =new Comment();
		comment.setType("1");
		comment.setTypeId(Integer.parseInt(xid));
		String con = "";
		if(null != content){
			try {
				con = new String(content.getBytes("iso-8859-1"),"UTF-8");
				con = content;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		comment.setContent(con);
		Timestamp now = new Timestamp(System.currentTimeMillis()); 
		comment.setUpdateTime(now);
		comment.setScore(0);
		comment.setCommentator("");
//		int num = 0;
//		if(null != number && number.length() > 0){
//			num = Integer.parseInt(number);
//		}
//		User user = iemService.getUserByNumber(num);
//		comment.setUser(user);
		iemService.saveNewComment(comment);
		
		return null;
	}
	
	public String putNewUser(){
		User user = new User();
//		int num = userDao.findMaxNumber();
//		if(num == 0){
//			user.setNumber(10001);
//		}else{
//			num = num + 1;
//			user.setNumber(num);
//		}
//		user.setUuid(uuid);
		user.setEmail("");
		user.setName("");
		user.setPhone("");
		user.setPassword("");
		userDao.persist(user);
//		response.setContentType("application/json");
//		response.setCharacterEncoding("utf-8");
//		JSONObject ret = new JSONObject();
//		ret.put("number", num);
//		try {		
//			PrintWriter out = response.getWriter();
//			out.print(ret.toString());
//			out.flush();
//			out.close();
//		} catch (Exception e) {
//			MiscUtils.getLogger().info(e.toString());
//		}
		return null;
	}
	
	public String getUserNumberByUuid(){
//		int num = 0;
//		User user = userDao.findUserByUuid(uuid);
//		num = user.getNumber();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
//		ret.put("number", num);
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getVersion(){
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		Version version = versionDao.findVersion();
		if(version.getVer().contains(ver)){
			ret.put("update", "False");
		}else{
			if(version.getBecomeEffective() == 0){
				ret.put("update", "True");
				JSONObject map = new JSONObject();
				map.put("version", version.getVer());
				map.put("updateContemt", version.getUpdateContent());
				map.put("url", version.getUrl());
				
				ret.put("Version", map);
			}else{
				ret.put("update", "False");
			}
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getAllSimplifyClass(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		List<FourClass> fourclass = iemService.getFourClass();
		for(int k = 0;k < fourclass.size();k ++){
			List<BigClass> bigclass = iemService.getBigClassByFid(fourclass.get(k).getFid());
			for(int i =0;i < bigclass.size();i ++){
				JSONObject middleclasslist = new JSONObject();
				List<SmallClass> smallclass = smallClassDao.getSmallClassByBid(bigclass.get(i).getBid());
				middleclasslist.put("id",bigclass.get(i).getBid());
				middleclasslist.put("name", bigclass.get(i).getName());
				middleclasslist.put("url",url + bigclass.get(i).getPath() + bigclass.get(i).getImage());
				JSONArray small_list = new JSONArray();
				for(int j = 0;j < smallclass.size();j ++){
					JSONObject map = new JSONObject();
					map.put("id", smallclass.get(j).getSid());
					map.put("name", smallclass.get(j).getName());
					map.put("url", url + smallclass.get(j).getPath() + smallclass.get(j).getImage());
					
					small_list.add(map);
				}
				middleclasslist.put("SmallClass", small_list);
				ret.put("middleClass" + i, middleclasslist);
			}
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getSmallClass(){
		HttpServletRequest httpRequest = ServletActionContext.getRequest();
		String url = httpRequest.getScheme() + "://" + httpRequest.getServerName() + ":" + httpRequest.getServerPort() + httpRequest.getContextPath() + "/";
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		int id = 0;
		if(null != bid){
			id = Integer.parseInt(bid);
		}else{
			return null;
		}
		List<SmallClass> smallclass = smallClassDao.getSmallClassByBid(id);
		for(int i = 0;i < smallclass.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", smallclass.get(i).getSid());
			map.put("name", smallclass.get(i).getName());
			map.put("url", url + smallclass.get(i).getPath() + smallclass.get(i).getImage());
			
			ret.put("SmallClass" + i, map);
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getContect(){
		List<Contect> contect = contectDao.findContect();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		for(int i = 0; i < contect.size();i ++){
			JSONObject map = new JSONObject();
			map.put("address", contect.get(i).getAddress());
			map.put("content", contect.get(i).getContent());
			map.put("tel", contect.get(i).getTelphone());
			ret.add(map);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getOpenArea(){
		List<OpenArea> openarea = openareaDao.findAllOpenArea();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		JSONArray ret = new JSONArray();
		
		for(int i = 0;i < openarea.size();i ++){
			if(openarea.get(i).getIsopen() == 0){
				JSONObject map = new JSONObject();
				map.put("city", openarea.get(i).getArea());
				List<Place>  place = placeDao.findAreaByCity(openarea.get(i).getArea());
				JSONArray area  = new JSONArray();
				for(int j = 0;j < place.size();j ++){
					area.add(place.get(j).getArea());
				}
				map.put("area", area);
				map.put("abridge", openarea.get(i).getAbridge());
				map.put("data", openarea.get(i).getDataname());
				map.put("package", openarea.get(i).getPackagename());
				map.put("isopen", openarea.get(i).getIsopen());
				ret.add(map);
			}
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getUpdateTime(){
		UpdateTime updatetime = updatetimeDao.findUpdateTime();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		ret.put("domain",updatetime.getDomain());
		ret.put("updatetime", updatetime.getUpdatetime().getTime());
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public String getSearchResults(){
		if(null == search){
			return null;
		}
//		try {
//			search = new String(search.getBytes("iso-8859-1"),"UTF-8");
//		} catch (UnsupportedEncodingException e1) {
//			e1.printStackTrace();
//		}
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		List<Compang> compang = compangDao.searchCompangs(search);
		ArrayList sid = new ArrayList<List>();
		for(int i = 0;i < compang.size();i ++){
			if(!sid.contains(compang.get(i).getSmallClass().getSid())){
				sid.add(compang.get(i).getSmallClass().getSid());
			}
		}
		int[] total = new int[sid.size()];
		for(int j = 0;j < sid.size();j ++){
			for(int k = 0;k < compang.size();k ++){
				if(compang.get(k).getSmallClass().getSid() == Integer.parseInt(sid.get(j).toString())){
					total[j] ++;
				}
			}
		}
		for(int p = 0;p < sid.size();p ++){
			JSONObject map = new JSONObject();
			SmallClass small = smallClassDao.findSmallClassBySid(Integer.parseInt(sid.get(p).toString()));
			map.put("bid",small.getBigclass().getBid());
			map.put("id", small.getSid());
			map.put("name", small.getName());
			map.put("total", total[p]);
			ret.add(map);
		}
		
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getSmallClassBySid(){
		int id = 0;
		if(null != sid){
			id = Integer.parseInt(sid);
		}
		BigClass big = smallClassDao.findSmallClassBySid(id).getBigclass();
		int findbid = big.getBid();
		List<SmallClass> small = smallClassDao.getSmallClassByBid(findbid);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONArray ret = new JSONArray();
		
		JSONObject bigmap = new JSONObject();
		bigmap.put("bid", findbid);
		bigmap.put("name", big.getName());
		ret.add(bigmap);
		
		for(int i = 0;i < small.size();i ++){
			JSONObject map = new JSONObject();
			map.put("id", small.get(i).getSid());
			map.put("name", small.get(i).getName());
			
			ret.add(map);
		}
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getAllCompangNum(){
		List<Compang> compang = compangDao.findAllCompagsByStatus();
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		ret.put("total", compang.size());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String putVisitNum(){
		int oldnum = 0;
		int newnum = 0;
		boolean status = false;
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		if(null != visit && visit.length() > 0){
			OpenArea openarea = openareaDao.findOpenAreaByAbridge(place);
			oldnum = openarea.getVisit();
			newnum =oldnum + Integer.parseInt(visit);
			status = openareaDao.updateOpenAreaVisitNum(place, newnum);
		}
		
		ret.put("Put_stauts", status);
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
	
	public String getVisitNum(){
		OpenArea openarea = openareaDao.findOpenAreaByAbridge(place);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		JSONObject ret = new JSONObject();
		
		ret.put("visitnum", openarea.getVisit());
		try {		
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
		return null;
	}
}
