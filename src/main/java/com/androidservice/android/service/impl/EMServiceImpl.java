package com.androidservice.android.service.impl;

import java.util.List;

//import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.androidservice.android.service.IEMService;
import com.androidservice.bean.Advertisement;
import com.androidservice.bean.BigClass;
import com.androidservice.bean.Comment;
import com.androidservice.bean.Commodity;
import com.androidservice.bean.Compang;
import com.androidservice.bean.CompangInfo;
import com.androidservice.bean.CompangRegister;
import com.androidservice.bean.FourClass;
import com.androidservice.bean.Opinion;
import com.androidservice.bean.Picture;
import com.androidservice.bean.ServiceArea;
import com.androidservice.bean.Shangxun;
import com.androidservice.bean.SmallClass;
import com.androidservice.bean.User;
import com.androidservice.dao.AdvertisementDao;
import com.androidservice.dao.BigClassDao;
import com.androidservice.dao.CommentDao;
import com.androidservice.dao.CommodityDao;
import com.androidservice.dao.CompangDao;
import com.androidservice.dao.CompangInfoDao;
import com.androidservice.dao.CompangRegisterDao;
import com.androidservice.dao.FourClassDao;
import com.androidservice.dao.OpinionDao;
import com.androidservice.dao.PictureDao;
import com.androidservice.dao.ServiceAreaDao;
import com.androidservice.dao.ShangxunDao;
import com.androidservice.dao.SmallClassDao;
import com.androidservice.dao.UserDao;

@Service("eMServiceImpl")
public class EMServiceImpl implements IEMService{
	
	@Autowired
	BigClassDao bigDao;
	
	@Autowired
	SmallClassDao smallDao;
	
	@Autowired
	CompangDao compangDao;
	
	@Autowired
	ServiceAreaDao serviceAreaDao;
	
	@Autowired
	CompangInfoDao compangInfoDao;
	
	@Autowired
	FourClassDao fourDao;
	
	@Autowired
	CompangRegisterDao compangRegisterDao;
	
	@Autowired
	CommodityDao commodityDao;
	
	@Autowired
	OpinionDao opinionDao;
	
	@Autowired
	AdvertisementDao advertisementDao;
	
	@Autowired
	PictureDao pictureDao;
	
	@Autowired
	ShangxunDao shangxunDao;
	
	@Autowired
	CommentDao commentDao;
	
	@Autowired
	UserDao userDao;

	@Override
	public List<BigClass> getAllBig() {
		return bigDao.getAllBigclass();
	}

	@Override
	public List<SmallClass> getSmallclassByBid(int bid) {
		return smallDao.getSmallClassByBid(bid);
	}

	@Override
	public List<Compang> getCompangsBySid(int sid) {
		return compangDao.findCompangsBySid(sid);
	}
	
	@Override
	public List<ServiceArea> getServiceAreaByCid(int cid){
		return serviceAreaDao.findServideAresByCid(cid);
	}
	
	@Override
	public Compang getCompangInfoByCid(int cid){
//		return compangInfoDao.findCompangInfoByCid(cid);
		return compangDao.findCompangByCid(cid);
	}
	
	@Override
	public List<FourClass> getFourClass(){
		return fourDao.findFourClass();
	}
	
	@Override
	public List<BigClass> getBigClassByFid(int fid){
		return bigDao.findBigclassByFid(fid);
	}
	
	@Override
	public void SaveRegisterComoangInfo(CompangRegister info){
		compangRegisterDao.addCompangInfo(info);
	}
	
	@Override
	public void saveNewCompang(Compang newcompang){
		compangDao.saveNewCompang(newcompang);
	}
	
	@Override
	public void saveNewCompangInfo(CompangInfo newinfo){
		compangInfoDao.saveNewCompangInfo(newinfo);
	}
	
	@Override
	public void saveNewCompangArea(ServiceArea newarea){
		serviceAreaDao.saveNewServiceArea(newarea);
	}
	
	@Override
	public int getSmallClassIdByName(String small){
		return smallDao.findSmallClassIdByName(small);
	}
	
	@Override
	public SmallClass getSmallClassBySid(int sid){
		return smallDao.findSmallClassBySid(sid);
	}
	
	@Override
	public Compang getCompangByName(String name){
		return compangDao.findCompangByName(name);
	}
	
	@Override
	public Compang getCompangByCid(int cid){
		return compangDao.findCompangByCid(cid);
	}
	
	@Override
	public void saveCompang(Compang comp){
		compangDao.saveCompang(comp);
	}
	
	@Override
	public CompangInfo getCompangInfoByGid(int gid){
		return compangInfoDao.findCompangInfoByGid(gid);
	}
	
	@Override
	public void saveCompangInfo(CompangInfo info){
		compangInfoDao.saveCompangInfo(info);
	}
	
	@Override
	public List<Commodity> getCommoditysByCid(int cid){
		return commodityDao.findCommoditysByCid(cid);
	}
	
	@Override
	public void saveOpinionInfo(Opinion info){
		opinionDao.saveNewOPinionInfo(info);
	}
	
	@Override
	public List<Advertisement> getAllAdvertisement(){
		return advertisementDao.findAllAdvert();
	}
	
	@Override
	public List<SmallClass> getSmallclassShowFirst(){
		return smallDao.findSmallClassShowFirst();
	}
	
	@Override
	public List<Compang> searchCompangByservice(String service,int sid){
		return compangDao.findCompangsByservice(service,sid);
	}
	
	@Override
	public List<Picture> getPictureByCid(int cid){
		return pictureDao.findPictureByCid(cid);
	}
	
	@Override
	public List<Shangxun> getShangxunBySid(int sid){
		return shangxunDao.findShangxunBySid(sid);
	}
	
	@Override
	public Shangxun getShangxunByXid(int xid){
		return shangxunDao.findShangxunByXid(xid);
	}
	
	@Override
	public List<Comment> getShangxunCommentByXid(int xid){
		return commentDao.findShangxunCommentsByXid(xid);
	}
	
	@Override
	public User getUserByUid(int uid){
		return userDao.findUserByUid(uid);
	}
	
	@Override
	public List<Comment> getCompangCommentByGid(int gid){
		return commentDao.findCompangCommentsByGid(gid);
	}
	
	@Override
	public void saveNewComment(Comment comment){
		commentDao.saveNewComment(comment);
	}
	
	@Override
	public void saveShangxun(Shangxun shangxun){
		shangxunDao.saveShangxun(shangxun);
	}

}