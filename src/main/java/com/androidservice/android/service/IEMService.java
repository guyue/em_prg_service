package com.androidservice.android.service;

import java.util.List;

import com.androidservice.bean.Advertisement;
import com.androidservice.bean.BigClass;
import com.androidservice.bean.Comment;
import com.androidservice.bean.Commodity;
import com.androidservice.bean.Compang;
import com.androidservice.bean.CompangInfo;
import com.androidservice.bean.CompangRegister;
import com.androidservice.bean.FourClass;
import com.androidservice.bean.Opinion;
import com.androidservice.bean.Picture;
import com.androidservice.bean.ServiceArea;
import com.androidservice.bean.Shangxun;
import com.androidservice.bean.SmallClass;
import com.androidservice.bean.User;

public interface IEMService{
	public List<BigClass> getAllBig();
	
	public List<SmallClass> getSmallclassByBid(int bid);
	
	public List<Compang> getCompangsBySid(int sid);
	
	public List<Shangxun> getShangxunBySid(int sid);
	
	public List<ServiceArea> getServiceAreaByCid(int cid);
	
	public Compang getCompangInfoByCid(int cid);
	
	public List<FourClass> getFourClass();
	
	public List<BigClass> getBigClassByFid(int fid);
	
	public void SaveRegisterComoangInfo(CompangRegister info);
	
	public void saveNewCompang(Compang newcompang);
	
	public void saveNewCompangInfo(CompangInfo newinfo);
	
	public void saveNewCompangArea(ServiceArea newarea);
	
	public int getSmallClassIdByName(String small);
	
	public SmallClass getSmallClassBySid(int sid);
	
	public Compang getCompangByName(String name);
	
	public Compang getCompangByCid(int cid);
	
	public void saveCompang(Compang comp);
	
	public CompangInfo getCompangInfoByGid(int gid);
	
	public void saveCompangInfo(CompangInfo info);
	
	public List<Commodity> getCommoditysByCid(int cid);
	
	public void saveOpinionInfo(Opinion info);
	
	public List<Advertisement> getAllAdvertisement();
	
	public List<SmallClass> getSmallclassShowFirst();
	
	public List<Compang> searchCompangByservice(String service,int sid);
	
	public List<Picture> getPictureByCid(int cid);
	
	public Shangxun getShangxunByXid(int xid);
	
	public List<Comment> getShangxunCommentByXid(int xid);
	
	public User getUserByUid(int uid);
	
	public List<Comment> getCompangCommentByGid(int gid);
	
	public void saveNewComment(Comment comment);
	
	public void saveShangxun(Shangxun shangxun);
}