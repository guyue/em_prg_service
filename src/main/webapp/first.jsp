<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	
	User user = (User) session.getAttribute("user");
	Integer num1 = (Integer)request.getAttribute("num1");
	Integer num2 = (Integer)request.getAttribute("num2");
	Integer num3 = (Integer)request.getAttribute("num3");
	Integer num4 = (Integer)request.getAttribute("num4");
	Integer num5 = (Integer)request.getAttribute("num5");
	Integer num6 = (Integer)request.getAttribute("num6");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String area = "";
	int pid = Integer.parseInt(user.getAddress());
	Place pl = placeDao.findPlaceByPid(pid);
	if(user.getRole().getRid() <= 3){
		area = "全市";
	}else{
		area = pl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>首页</title>
	</head>
	<style type="text/css">
	.form-horizontal .control-label {
		padding-top: 15px;
		width: 80px;
	}
	.form-horizontal .control-group {
		border-bottom: 1px solid #eeeeee;
		border-top: 0px solid #ffffff;
		margin-bottom: 0;
	}
	#header .tile {
		height: 61px;
		left: 215px;
		line-height: 600px;
		overflow: hidden;
		position: relative;
		top: -20px;
		width: 391px;
	}
	</style>
	<script type="text/javascript">
	</script>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4>			
			</div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
			<!--<br>
			<font color="#999" size="2px"><%=role%></font>-->
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>首页</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
			</div>		
			<div class="container-fluid">
				<form action="" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal" >
					<div class="control-group" style="display:none">
						<label class="control-label">权限：</label>
						<label class="control-label">
							<%if(user.getRole().getRid() == 1){%>
								一级权限
							<%}%>
							<%if(user.getRole().getRid() == 2){%>
								二级权限
							<%}%>
							<%if(user.getRole().getRid() == 3){%>
								三级权限
							<%}%>
							<%if(user.getRole().getRid() == 4){%>
								四级权限
							<%}%>
							<%if(user.getRole().getRid() == 5){%>
								五级权限
							<%}%>
						</label>
					</div>
					<div class="control-group" style="display:none">
						<label class="control-label">范围：</label>
						<%
							
							String address = "";
							if(user.getRole().getRid() == 1){
								address ="所有区域"; 
							}
							if(user.getRole().getRid() == 2){
								address = pl.getProvince(); 
							}
							if(user.getRole().getRid() == 3){
								if(pid == 1){
									address = pl.getProvince();
								}else{
									address =pl.getCity(); 
								}
							}
							if(user.getRole().getRid() == 4){
								address =pl.getArea(); 
							}
							if(user.getRole().getRid() == 5){
								address ="商户"; 
							}
						%>
						<label class="control-label"><%=address%></label>
					</div><br><br><br><br><br><br>
					<div class="control-group" style="text-align: center;" align="center">	
					<!--	<label class="control-label">状态：</label>		-->
						<ul class="stat-boxes">
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=1" ><strong><%=num2%></strong></a>
									待审核
								</div>
							</li>
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=2" ><strong><%=num3%></strong></a>
									不通过
								</div>
							</li>
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=0" ><strong><%=num1%></strong></a>
									发布中
								</div>
							</li>
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=6" ><strong><%=num6%></strong></a>
									快到期
								</div>
							</li>
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=4" ><strong><%=num5%></strong></a>
									已过期
								</div>
							</li>
							<%if(user.getRole().getRid() != 5){%>
							<li>
								<div class="right">
									<a href="<%=request.getContextPath()%>/browser/compangcheckstatus.do?check=3" ><strong><%=num4%></strong></a>
									回收站
								</div>
							</li>
							<%}%>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#home_menu"));
		});
	</script>
</html>