<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.*"%>
<html>
<%
	String ctx = (String) request.getContextPath();
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String login_status = (String)request.getAttribute("login_status");
%>
	<head>
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.login.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		<title>Login</title>
		<style type="text/css">
			#g
			{
				position:relative;
				display:none;
			}
			#hou{
				width:100%;
				height:100%;
				position:absolute;
				overflow:hidden;
				z-index:90;

				filter: alpha(Opacity=70); 
				-moz-opacity:0.7; 
				opacity:0.7; 
			}
			#close
			{
				float:right;
				width:20px;
				height:20px;
				top:0px;
				text-align:center;
				margin-right:10px;
				margin-top:10px;
			}
			#loginbox {
				width:400px;
				height:250px;
				background:white;
				position:relative;
				z-index:99;
				border-radius:4px;
				margin:auto;
			}
			label {
				display: inline-block;
				margin-bottom: 5px;
			}
			.form-horizontal .control-label {
				padding-top: 5px;
				width: 80px;
			}
			.form-horizontal input[type="text"], .form-horizontal input[type="password"], .form-horizontal textarea {
				width: 50%;
			}
			.form-horizontal .control-group {
				border-bottom: 0px solid #eeeeee;
				border-top: 1px solid #ffffff;
				margin-bottom: 0;
			}
			.form-search input, .form-inline input, .form-horizontal input, .form-search textarea, .form-inline textarea, .form-horizontal textarea, .form-search select, .form-inline select, .form-horizontal select, .form-search .help-inline, .form-inline .help-inline, .form-horizontal .help-inline, .form-search .uneditable-input, .form-inline .uneditable-input, .form-horizontal .uneditable-input, .form-search .input-prepend, .form-inline .input-prepend, .form-horizontal .input-prepend, .form-search .input-append, .form-inline .input-append, .form-horizontal .input-append {
				margin-bottom: 0;
			}
			#loginbox .form-actions {
				padding: 16px 10px 20px;
			}
			body{
				background:gray;
				font-size:15px;
				background-image:url(<%=ctx%>/img/bei.jpg);
				-moz-background-size: 100%;  
				-o-background-size: 100%;  
				-webkit-background-size: 100%;  
				background-size: 100% ; 
			 }
		</style>
		<script type="text/javascript">
		function changecity(){
			var province = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			$.ajax({
					url:"<%=request.getContextPath()%>/browser/getarea.do?province=" + province,
					type:"post",
					dataType: "JSON",
					async:false,
					timeout: 3000,
					success: function(ret) {
						if(ret.length > 0){
							document.getElementById("sel2").options.length = 1;
							for(var i = 0; i < ret.length; i ++){
								$("#sel2").append(ret[i]);
							}
						}
					},
					error: function(XMLRequest, textInfo) {
						if (textInfo != null) {
							alert(textInfo);
						}
					}
			});
		}
		function setInfo(){
			var place = document.getElementById("sel2").options[document.getElementById("sel2").selectedIndex].value;
			document.getElementById("place").value = place;
			var name = document.getElementById("name").value;
			var password = document.getElementById("password").value;
			
			var form = document.forms['loginform'];
			form.action = '<%=request.getContextPath() %>/browser/login?name=' + name + '&password=' + password;
			form.target = "_self";
			form.submit();
		}
		</script>
	</head>
	<body>
	<div id="g" >
	  <div id="hou">
	  </div>
		<div id="loginbox">
			
		<!--	<div class="widget-content nopadding">	-->
				
				
				<form id="loginform" method="post" class="form-horizontal" enctype="multipart/form-data" action="<%=request.getContextPath()%>/browser/login.do">
					<div id="close" > <a href="javascript:void(0)"  onclick="closew()" style="color:gray" ></a></div>
					<div style="width:390;height:30px;border-bottom:1px solid rgb(200,200,200); padding-top:10px; padding-left:10px; margin-bottom:15px;"><span style="color:red;float: left;"><b>易民后台</b></span></div>
					<div class="control-group">
						<label class="control-label"><b>地区:</b></label>
						<select id="sel1" style="width:80px" onchange="">
							<option value="江苏">江苏</option>
						</select>
						<%
							List<Place> place = placeDao.findCityByProvince("江苏");
							ArrayList<String> city = new ArrayList<String>();
							for(int i = 0;i < place.size();i ++){
								if(!city.contains(place.get(i).getCity())){
									city.add(place.get(i).getCity());
								}
							}
							List<OpenArea> openarea = openareaDao.findAllOpenArea();
						%>
						<select id="sel2" style="width:80px">
						<%
							for(int i = 0;i < openarea.size();i ++){
								for(int j = 0;j < city.size();j ++){
									if(openarea.get(i).getArea().equals(city.get(j))){
										if(openarea.get(i).getIsopen() == 0){
						%>
										<option value="<%=openarea.get(i).getAbridge()%>"><%=city.get(j)%></option>
						<%		
										}
									}
								}
							}
						%>
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div class="control-group">
						<label class="control-label"><b>用户名:</b></label>
						<input id="name" name="name" type="text" tabindex="1" />
						<%
							if(null != login_status){
								if("1".equals(login_status)){
									out.println("<div style='color: #F00'><span >用户名不存在！</span></div>");
								}
							}
						%>
					</div>
					<div class="control-group">
						<label class="control-label"><b>密码:</b></label>
						<input id="password" name="password" type="password" tabindex="2" />
						<%
							if(null != login_status){
								if("2".equals(login_status)){
									out.println("<div style='color: #F00'><span >密码错误！</span></div>");
								}
							}
						%>
					</div>
					<br>
					<div class="form-actions">
						<span class="pull-center">
							<input type="button" class="btn btn-danger" onclick="setInfo()" id="btn" value="登录" tabindex="3" />
							<input type="button" class="btn btn-danger" id="clean" value="重置" tabindex="4" />
						</span>
						<input id="place" name="place" type="hidden">
					</div>
				</form>
				<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
				</iframe>
		<!--	</div>-->
		</div>
	</div>
	</body>
	<script type="text/javascript">
	function houtai()
	{
		$("#g").css("display","block");
		var bh=$(window).height();
		var gw=$(window).width();
		var hc=bh-300;
		var th=hc/2+50;
		$("#loginbox").css("margin-top",th+"px");
	}
	houtai();
	</script>
</html>