<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	
	Compang compang = (Compang)request.getAttribute("compang");
	User user = (User) session.getAttribute("user");
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>

<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<style type="text/css">
			.form-horizontal .control-label {
				float: left;
				padding-top: 5px;
				text-align: left;
				width: 160px;
			}
			
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
		</style>
		<script type="text/javascript">
			function closeviewcompang(){
				document.getElementById("back_to_list").click();
				//history.back();
			}
		</script>
	</head>
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>编辑商户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/compang.do" class="tip-bottom">商户列表</a>
				<a href="#" class="current">预览商户</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>编辑商户</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal" />
									<span style="text-align:left">
									<div class="control-group">
										<!--	<ul class="thumbnails"> -->
											<li class="span2">
												<a href="#" class="thumbnail">
													<img src="<%=request.getContextPath() %>/<%=compang.getPath() + compang.getImage()%>" alt="" />
												</a>
												<div class="actions" style="display:none">
													<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
													<a title="" href="#"><i class="icon-remove icon-white"></i></a>
												</div>
											</li>
										<!--</ul>-->
									</div>
									<div class="control-group">
										<label class="control-label">编号：</label>
										<label class="control-label"><%=String.valueOf(compang.getNumber())%></label>
									</div>
									<div class="control-group">
										<label class="control-label">所属大类：</label>
										<label class="control-label"><%=compang.getSmallClass().getBigclass().getName()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">所属子类：</label>
										<label class="control-label"><%=compang.getSmallClass().getName()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">名称：</label>
										<label class="control-label"><%=compang.getName()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">宣传语：</label>
										<label class="control-label"><%=compang.getTitle()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">地址：</label>
										<label class="control-label"><%=compang.getAddress()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">经纬度：</label>
										<label class="control-label"><%=compang.getLongitude()%>,<%=compang.getLatitude()%></label>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">纬度：</label>
										<label class="control-label"><%=compang.getLatitude()%></label>
									</div>
									<%
										int pid = Integer.parseInt(compang.getArea());
										Place pl = placeDao.findPlaceByPid(pid);
									%>
									<div class="control-group">
										<label class="control-label">所在城市：</label>
										<label class="control-label"><%=pl.getCity()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">所在区域：</label>
										<label class="control-label"><%=pl.getArea()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">拨打次数：</label>
										<label class="control-label"><%=compang.getTeltime()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">浏览次数：</label>
										<label class="control-label"><%=compang.getLooktime()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">内容：</label>
										<%
											String content_Str = compang.getContent();
											content_Str = content_Str.replaceAll(" ","&nbsp;&nbsp;");
										%>
										<label class="control-label" style="width:600px"><%=content_Str%></label>
									</div>
									<div class="control-group">
										<label class="control-label">电话号码：</label>
										<label class="control-label"><%=compang.getTel()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">联系人：</label>
										<label class="control-label"><%=compang.getContact()%></label>
									</div>
									<div class="control-group">
										<label class="control-label">发布日期：</label>
										<label class="control-label"><%=formatter.format(compang.getUpdateDatetime())%></label>
									</div>
									<div class="control-group">
										<label class="control-label">到期时间：</label>
										<label class="control-label"><%=formatter.format(compang.getExpirationtime())%></label>
									</div>
									<div class="control-group">
										<label class="control-label">是否认证：</label>
										<label class="control-label">
										<%if(compang.getAuthentication() == 1){%>
										未认证
										<%}else{%>
										已认证
										<%}%>
										</label>
									</div>
									<div class="control-group">
										<label class="control-label">是否推荐：</label>
										<label class="control-label">
										<%if(compang.getShowfirst() == 1){%>
										未推荐
										<%}else{%>
										已推荐
										<%}%>
										</label>
									</div>
									<div class="control-group">
										<label class="control-label">审核状态：</label>
										<label class="control-label">
										<%
										Calendar start_time = Calendar.getInstance();
										Calendar end_time = Calendar.getInstance();
										start_time.setTimeInMillis(compang.getUpdateDatetime().getTime());
										end_time.setTimeInMillis(compang.getExpirationtime().getTime());
										int month = 0;
										int start_year = start_time.get(Calendar.YEAR);
										int start_month = start_time.get(Calendar.MONTH);
										int end_year = end_time.get(Calendar.YEAR);
										int end_month = end_time.get(Calendar.MONTH);
										if(end_year > start_year){
											month = end_month + (end_year - start_year) * 12 - start_month;
										}else{
											if(end_month > start_month){
												month = end_month - start_month;
											}
										}
										if(compang.getCheckStatus() == 0){
											if(month <= 1){
										%>
										快到期
										<%
											}else{
										%>
										发布中
										<%
											}
										}
										if(compang.getCheckStatus() == 1){
										%>
										待审核
										<%
										}
										if(compang.getCheckStatus() == 2){
										%>
										不通过
										<%
										}
										if(compang.getCheckStatus() == 3){
										%>
										已关闭
										<%
										}
										if(compang.getCheckStatus() == 4){
										%>
										已过期
										<%
										}
										%>
										</label>
									</div>
								</span>
								</form>
							</div>
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商户外景</h5>
							</div>
							<div class="widget-content nopadding" id="poicture_list">
								<ul class="thumbnails">
									<%
										PictureDao pictureDao = (PictureDao)SpringUtils.getBean(PictureDao.class);
										List<Picture> picture = pictureDao.findPictureByCid(compang.getCid());
										for(int i = 0;i < picture.size();i ++){
									%>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="<%=request.getContextPath() %>/<%=picture.get(i).getPath() + picture.get(i).getSmall()%>" alt="" />
										</a>
										<div class="actions" style="display:none">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<%}%>
								</ul>
								<%if(picture.size() == 0){%>
								<div class="control-group">
									<label class="control-label">&nbsp;&nbsp;无外景图片</label>
								</div>
								<%}%>
							</div>
							
							<div class="widget-title" style="display:none">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商品列表</h5>
							</div>
							<div class="row-fluid" id="commodity_list" style="display:none">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>图片</th>
											<th>商品名称</th>
											<th>商品说明</th>
											<th>现价</th>
											<th>原价</th>
											<th>使用说明</th>
										<!--	<th>详情图片</th>	-->
										</tr>
									</thead>
									<tbody>
										<%
											CommodityDao commodityDao = (CommodityDao)SpringUtils.getBean(CommodityDao.class);
											List<Commodity> commodity = commodityDao.findCommoditysByCid(compang.getCid());
											if(commodity != null){
												for(int i = 0;i < commodity.size();i ++){
													String code = commodity.get(i).getCode();
													if(code.length() > 15){
														code = code.substring(0,15) + "......";
													}
													String content = commodity.get(i).getContent();
													if(content.length() > 15){
														content = content.substring(0,15) + "......";
													}
										%>
										<tr>
											<td><img src="<%=request.getContextPath() %>/<%=commodity.get(i).getPath() + commodity.get(i).getImage()%>" width="80px"/></td>
											<td><%=commodity.get(i).getName()%></td>
											<td><%=code%></td>
											<td><%=commodity.get(i).getDeals()%></td>
											<td><%=commodity.get(i).getPrice()%></td>
											<td><%=content%></td>
										<!--	<td><img src="<%=request.getContextPath() %>/<%=commodity.get(i).getPath() + commodity.get(i).getDetail_img()%>" width="100px"/></td>	-->
										</tr>
										<%
												}
											}
											if( commodity.size() == 0){
										%>
										<tr>
											<td align="center" colspan="9">无商品信息</td>
										</tr>
										<%}%>
									</tbody>
								</table>
							</div>
							<div class="form-actions" align="center">
								<button type="button" class="btn btn-primary" onclick="closeviewcompang();">退出</button>
								<a href="<%=request.getContextPath()%>/browser/compang.do?status=<%=compang.getCheckStatus()%>" id="back_to_list" style="display:none"></a>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
</html>