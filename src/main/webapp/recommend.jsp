<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	List<Compang> compang = (List<Compang>)request.getAttribute("recommend_compang");
	
	User user = (User) session.getAttribute("user");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>推荐商户</title>
	</head>
	<script type="text/javascript">
		function recommend_compang(val,cid){
			$.ajax({
				url:"<%=request.getContextPath()%>/browser/recommendnumber.do",
				type:"post",
				dataType: "JSON",
				async:false,
				timeout: 3000,
				success: function(ret) {
					if(ret.canadd == 1){
						alert("请先取消一个推荐后才能再推荐");
						val.href="#";
					}else{
						val.href="<%=request.getContextPath()%>/browser/recommendcompang.do?cid=" + cid + "&showfirst=0";
					}
				},
				error: function(XMLRequest, textInfo) {
					if (textInfo != null) {
						alert(textInfo);
					}
				}
			});
		}
	</script>
	<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
	</style>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>商 户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">商户列表</a>
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>商户列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>编号</th>
											<th>公司名字</th>
											<th>类别</th>
											<th>子类</th>
											<th>发布日期</th>
											<th>期限</th>
											<th>到期日期</th>
											<th>区县</th>
											<th>认证</th>
											<th>状态</th>
											<th>推荐</th>
											<th>操作</th>
											<th>注</th>
										</tr>
									</thead>
									<tbody>
										<%
										if(compang != null){
											for(int i = 0;i < compang.size();i ++){
												if(compang.get(i).getAuthentication() == 0){
										%>
											<tr>
												<td><%=String.valueOf(compang.get(i).getNumber())%></td>
												<td><%=compang.get(i).getName()%></td>
												<td><%=compang.get(i).getSmallClass().getBigclass().getName()%></td>
												<td><%=compang.get(i).getSmallClass().getName()%></td>
												<%
													SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
													Calendar start_time = Calendar.getInstance();
													Calendar end_time = Calendar.getInstance();
													start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
													end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
													int month = 0;
													int start_year = start_time.get(Calendar.YEAR);
													int start_month = start_time.get(Calendar.MONTH);
													int end_year = end_time.get(Calendar.YEAR);
													int end_month = end_time.get(Calendar.MONTH);
													if(end_year > start_year){
														month = end_month + (end_year - start_year) * 12 - start_month;
													}else{
														if(end_month > start_month){
															month = end_month - start_month;
														}
													}
												%>
												<td><%=formatter.format(compang.get(i).getUpdateDatetime())%></td>
												<td><%=month%>个月</td>
												<td><%=formatter.format(compang.get(i).getExpirationtime())%></td>
												<%
													int pid = Integer.parseInt(compang.get(i).getArea());
													Place pl = placeDao.findPlaceByPid(pid);
												%>
												<td><%=pl.getArea()%></td>
												<%if(compang.get(i).getAuthentication() == 0){%>
												<td>已认证</td>
												<%}else{%>
												<td>未认证</td>
												<%}%>
												<%
												if(compang.get(i).getCheckStatus() == 0){
													if(month <= 1){
												%>
												<td>快到期</td>
												<%
													}else{
												%>
												<td>发布中</td>
												<%
													}
												}
												if(compang.get(i).getCheckStatus() == 1){
												%>
												<td>待审核</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 2){
												%>
												<td>不通过</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 3){
												%>
												<td>已关闭</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 4){
												%>
												<td>已过期</td>
												<%
												}
												%>
												<td>
												<%if(compang.get(i).getShowfirst() == 1){%>
												未
												<%}else{%>
												<font color="red">已推荐</font>
												<%}%>
												</td>
												<td class="taskOptions">
													<a href="<%=request.getContextPath()%>/browser/viewcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="预览"><i class="icon-eye-open"></i></a>
													<%if(compang.get(i).getShowfirst() == 0){%>
													<a href="<%=request.getContextPath()%>/browser/recommendcompang.do?cid=<%=compang.get(i).getCid()%>&showfirst=1" class="tip-top" data-original-title="取消推荐"><i class="icon-remove-circle"></i></a>
													<%}else{%>
													<a href="<%=request.getContextPath()%>/browser/recommendcompang.do?cid=<%=compang.get(i).getCid()%>&showfirst=0" onclick="recommend_compang(this,'<%=compang.get(i).getCid()%>')" class="tip-top" data-original-title="推荐"><i class="icon-ok-circle"></i></a>
													<%}%>
												</td>
												<td></td>
											</tr>
										<%
												}
											}
											if(compang.size() == 0){
										%>
											<tr>
												<td colspan="6">无推荐商户</td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
											</tr>
										<%
											}
										}
										%>
									</tbody>
								</table>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#recommend_menu"));
		});
	</script>
</html>