<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	
	//List<FourClass> four = (List<FourClass>)session.getAttribute("fourclass");
	//List<BigClass> big = (List<BigClass>)session.getAttribute("bigclass");
	//List<SmallClass> small = (List<SmallClass>)session.getAttribute("smallclass");
	
	
	List<BigClass> big = (List<BigClass>)request.getAttribute("bigclass");
	List<SmallClass> small = (List<SmallClass>)request.getAttribute("smallclass");
	Integer showbid = (Integer)request.getAttribute("showbid");
	//int show_bid = Integer.parseInt(showbid);
	User user = (User) session.getAttribute("user");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String area = "";
	int pid = Integer.parseInt(user.getAddress());
	Place pl = placeDao.findPlaceByPid(pid);
	if(user.getRole().getRid() <= 3){
		area = "全市";
	}else{
		area = pl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		
		
		<title>菜单</title>
		<style type="text/css">	
		.span2 {
			width: 90px;
		}
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
		</style>
		<script type="text/javascript">
			var alpha_num_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
			function randomString1(length){
				if(!length){
					length = Math.floor(Math.random() * alpha_num_chars.length);
				}
				var str = '';
				for(var i = 0; i < length; i++){
					str += alpha_num_chars[Math.floor(Math.random() * alpha_num_chars.length)];
				}  
				return str;  
			}
			function randomString() {  
				return '' + new Date().getTime();  
			}
			
			function showeditmenu1(bid,name,sort,path){
				document.getElementById("edit_two").style.display = "";
				document.getElementById("edit_two2").style.display = "";
				
				document.forms['doupload1'].getElementsByTagName("input").twomenu.value = name;
				document.forms['doupload1'].getElementsByTagName("input").sort.value = sort;
				document.forms['doupload1'].getElementsByTagName("input").bid.value = bid;
				document.forms['doupload1'].getElementsByTagName("input").filePath.value = path;
			}
			function closediv1(){
				document.getElementById("edit_two").style.display = "none";
				document.getElementById("edit_two2").style.display = "none";
			}
			
			function shownewmenu1(){
				document.getElementById("new_two").style.display = "";
				document.getElementById("new_two2").style.display = "";
			}
			function closediv2(){
				document.getElementById("new_two").style.display = "none";
				document.getElementById("new_two2").style.display = "none";
			}
			
			function showeditmenu2(sid,name,sort,path,show){
				document.getElementById("edit_three").style.display = "";
				document.getElementById("edit_three2").style.display = "";
				document.forms['doupload3'].getElementsByTagName("input").threemenu.value = name;
				document.forms['doupload3'].getElementsByTagName("input").sort.value = sort;
				document.forms['doupload3'].getElementsByTagName("input").sid.value = sid;
				document.forms['doupload3'].getElementsByTagName("input").filePath.value = path;
				if(show == 0){
					document.getElementsByName("ischeck")[0].checked = true;
				}else{
					document.getElementsByName("ischeck")[1].checked = true;
				}
			}
			function closediv3(){
				document.getElementById("edit_three").style.display = "none";
				document.getElementById("edit_three2").style.display = "none";
			}
			
			function shownewmenu2(name){
				document.getElementById("new_three").style.display = "";
				document.getElementById("new_three2").style.display = "";
				//document.forms['doupload4'].getElementsByTagName("label").twomenu.innerHTML = name;
				document.forms['doupload4'].getElementsByTagName("input").twomenu.value = name;
			}
			function closediv4(){
				document.getElementById("new_three").style.display = "none";
				document.getElementById("new_three2").style.display = "none";
			}
			
			function setInfo1(){
				var sort = document.forms['doupload1'].getElementsByTagName("input").sort.value;
				var bid = document.forms['doupload1'].getElementsByTagName("input").bid.value;
				var only = "";
				if(sort.length > 0){
					$.ajax({
						url:"<%=request.getContextPath()%>/browser/sortbig.do?sort=" + sort + "&bid=" + bid,
						type:"post",
						dataType: "JSON",
						async:false,
						timeout: 3000,
						success: function(ret) {
							only = ret.onlyone;
							if(only == 0){
								var form = document.forms['doupload1'];
								form.action = '<%=request.getContextPath() %>/browser/updatetwomenu.do';
								form.target = "_self";
								form.submit();
							}else{
								alert("该显示序号已有，请添加另外的序号！");
							}
						},
						error: function(XMLRequest, textInfo) {
							if (textInfo != null) {
								alert(textInfo);
							}
						}
					});
				}else{
					alert("请添加显示序号！");
				}
			}
			function uploadFile1(){
				var str = randomString();
				document.forms['doupload1'].getElementsByTagName("input").fileName.value = document.forms['doupload1'].getElementsByTagName("input").myFile.value;
				var filename = document.forms['doupload1'].getElementsByTagName("input").fileName.value;
				if(filename.length > 0){
					var num1 = filename.length;
					var num2 = filename.indexOf('.');
					var newname = filename.substr(num2,num1);
					newname = str + newname;
					document.forms['doupload1'].getElementsByTagName("input").fileName.value = newname;
					
					var form = document.forms['doupload1'];
					form.action = '<%=request.getContextPath() %>/browser/upload.do';
					form.target = "rfFrame";
					form.submit();
					alert("上传成功!");
				}
			}
			function setInfo2(){
				var sort = document.forms['doupload2'].getElementsByTagName("input").sort.value;
				var only = "";
				if(sort.length > 0){
					$.ajax({
						url:"<%=request.getContextPath()%>/browser/sortbig.do?sort=" + sort,
						type:"post",
						dataType: "JSON",
						async:false,
						timeout: 3000,
						success: function(ret) {
							only = ret.onlyone;
							if(only == 0){
								var form = document.forms['doupload2'];
								form.action = '<%=request.getContextPath() %>/browser/updatetwomenu.do';
								form.target = "_self";
								form.submit();
							}else{
								alert("该显示序号已有，请添加另外的序号！");
							}
						},
						error: function(XMLRequest, textInfo) {
							if (textInfo != null) {
								alert(textInfo);
							}
						}
					});
				}else{
					alert("请添加显示序号！");
				}
				
			}
			
			function uploadFile2(){
				//var num = Math.floor(Math.random() * ( 62 + 1));
				//var str = randomString(num);
				var str = randomString();
				document.forms['doupload2'].getElementsByTagName("input").fileName.value = document.forms['doupload2'].getElementsByTagName("input").myFile.value;
				var filename = document.forms['doupload2'].getElementsByTagName("input").fileName.value;
				if(filename.length > 0){
					var num1 = filename.length;
					var num2 = filename.indexOf('.');
					var newname = filename.substr(num2,num1);
					newname = str + newname;
					document.forms['doupload2'].getElementsByTagName("input").fileName.value = newname;
					
					var form = document.forms['doupload2'];
					form.action = '<%=request.getContextPath() %>/browser/upload.do';
					form.target = "rfFrame";
					form.submit();
					alert("上传成功!");
				}
			}
			
			function setInfo3(){
				var sort = document.forms['doupload3'].getElementsByTagName("input").sort.value;
				var sid = document.forms['doupload3'].getElementsByTagName("input").sid.value;
				var only = "";
				if(document.getElementsByName("ischeck")[0].checked){
					document.getElementById("showfirst").value = "0";
				}else{
					document.getElementById("showfirst").value = "1";
				}
				if(sort.length > 0){
					$.ajax({
						url:"<%=request.getContextPath()%>/browser/sortsmall.do?sort=" + sort + "&sid=" + sid,
						type:"post",
						dataType: "JSON",
						async:false,
						timeout: 3000,
						success: function(ret) {
							only = ret.onlyone;
							if(only == 0){
							/*	if(document.getElementsByName("ischeck")[0].checked){
								$.ajax({
									url:"<%=request.getContextPath()%>/browser/threemenushowfirst.do",
									type:"post",
									dataType: "JSON",
									async:false,
									timeout: 3000,
									success: function(ret) {
										var add = ret.add;
										if(add == 0){
											var form = document.forms['doupload3'];
											form.action = '<%=request.getContextPath() %>/browser/updatethreemenu.do';
											form.target = "_self";
											form.submit();
										}else{
											alert("热门精选已满，请先删除，然后再添加！");
										}
									},
									error: function(XMLRequest, textInfo) {
											if (textInfo != null) {
												alert(textInfo);
											}
										}
									});
								}*/
								var form = document.forms['doupload3'];
								form.action = '<%=request.getContextPath() %>/browser/updatethreemenu.do';
								form.target = "_self";
								form.submit();
							}else{
								alert("该显示序号已有，请添加另外的序号！");
							}
						},
						error: function(XMLRequest, textInfo) {
							if (textInfo != null) {
								alert(textInfo);
							}
						}
					});
				}else{
					alert("请添加显示序号！");
				}
				
			}
			function uploadFile3(){
				var str = randomString();
				document.forms['doupload3'].getElementsByTagName("input").fileName.value = document.forms['doupload3'].getElementsByTagName("input").myFile.value;
				var filename = document.forms['doupload3'].getElementsByTagName("input").fileName.value;
				if(filename.length > 0){
					var num1 = filename.length;
					var num2 = filename.indexOf('.');
					var newname = filename.substr(num2,num1);
					newname = str + newname;
					document.forms['doupload3'].getElementsByTagName("input").fileName.value = newname;
					
					var form = document.forms['doupload3'];
					form.action = '<%=request.getContextPath() %>/browser/upload.do';
					form.target = "rfFrame";
					form.submit();
					alert("上传成功!");
				}
			}
			
			function setInfo4(){
				//var sort = document.forms['doupload4'].getElementsByTagName("input").sort.value;
				//var bid = document.forms['doupload4'].getElementsByTagName("input").bid.value
				//var only = "";
				//if(sort.length > 0){
				/*	$.ajax({
						url:"<%=request.getContextPath()%>/browser/sortsmall.do?sort=" + sort + "&bid=" + bid,
						type:"post",
						dataType: "JSON",
						async:false,
						timeout: 3000,
						success: function(ret) {
							only = ret.onlyone;
							if(only == 0){
								var form = document.forms['doupload4'];
								form.action = '<%=request.getContextPath() %>/browser/updatethreemenu.do';
								form.target = "_self";
								form.submit();
							}else{
								alert("该显示序号已有，请添加另外的序号！");
							}
						},
						error: function(XMLRequest, textInfo) {
							if (textInfo != null) {
								alert(textInfo);
							}
						}
					});	*/
				//}else{
				//	alert("请添加显示序号！");
				//}
				var form = document.forms['doupload4'];
				form.action = '<%=request.getContextPath() %>/browser/updatethreemenu.do';
				form.target = "_self";
				form.submit();
			}
			function uploadFile4(){
				var str = randomString();
				document.forms['doupload4'].getElementsByTagName("input").fileName.value = document.forms['doupload4'].getElementsByTagName("input").myFile.value;
				var filename = document.forms['doupload4'].getElementsByTagName("input").fileName.value;
				if(filename.length > 0){
					var num1 = filename.length;
					var num2 = filename.indexOf('.');
					var newname = filename.substr(num2,num1);
					newname = str + newname;
					document.forms['doupload4'].getElementsByTagName("input").fileName.value = newname;
					
					var form = document.forms['doupload4'];
					form.action = '<%=request.getContextPath() %>/browser/upload.do';
					form.target = "rfFrame";
					form.submit();
					alert("上传成功!");
				}
			}
			function checkdeletetwomenu(val,bid,name){
				var r=confirm("你确定需要删除大类菜单" + name);
				if (r==true){
					val.href="<%=request.getContextPath() %>/browser/deltwomenu.do?bid=" + bid;
				}else{
					val.href="#";
				}
			}
			
			function checkdeletethreemenu(val,sid,name,bid){
				var r=confirm("你确定需要删除小类菜单" + name);
				if (r==true){
					val.href="<%=request.getContextPath() %>/browser/delthreemenu.do?sid=" + sid + "&bid=" + bid;
				}else{
					val.href="#";
				}
			}
		</script>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4>			
			</div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">菜单</a>
			</div>	
			<table style="width:100%;">
				<tr>
					<td>
						<%if(big.size() > 1){%>
						<ul class="thumbnails">
							<%for(int j = 0;j < big.size();j ++){
								if(big.get(j).getBid() != 1){
							%>
								<li class="span2">
									<a href="<%=request.getContextPath()%>/browser/smallclass.do?bid=<%=big.get(j).getBid()%>" <%if(showbid == big.get(j).getBid()){%>style="background:#A9A9A9"<%}%>class="thumbnail">
										<img src="<%=request.getContextPath()%>/<%=big.get(j).getPath() + big.get(j).getImage()%>" alt="" width="80px" height="80px"/>
										&nbsp;&nbsp;<%=big.get(j).getName()%>
									</a>
									<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 3)){%>
									<div class="actions">
										<a title="" href="#" onclick="showeditmenu1('<%=big.get(j).getBid()%>','<%=big.get(j).getName()%>','<%=big.get(j).getSort()%>','<%=big.get(j).getPath()%>')"><i class="icon-pencil icon-white"></i></a>
										<a title="" href="#" onclick="checkdeletetwomenu(this,'<%=big.get(j).getBid()%>','<%=big.get(j).getName()%>')"><i class="icon-remove icon-white"></i></a>
									</div>
									<%}%>
									<%if(user.getRole().getRid() == 2){%>
									<div class="actions">
										<a title="" href="#" onclick="showeditmenu1('<%=big.get(j).getBid()%>','<%=big.get(j).getName()%>','<%=big.get(j).getSort()%>','<%=big.get(j).getPath()%>')"><i class="icon-pencil icon-white"></i></a>
									</div>
									<%}%>
								</li>
							<%}
							}%>
						</ul>
						<%}else{%>
						没有菜单<br>
						<%}%>
						<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){%>						
						<ul class="thumbnails" style="display:none">
							<li class="span2">
								<a href="javascript:void(0)" class="thumbnail" onclick="shownewmenu1()">
									&nbsp;&nbsp;新增一级菜单
								</a>
							</li>
						</ul>
						<input type="button" value="新增一级菜单" onclick="shownewmenu1()">
						<%}%>
					</td>
				</tr>
				<tr>
					<td>
						<div class="widget-title" id="edit_two2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>修改一级菜单</h5>
						</div>
						<div class="widget-content nopadding" id="edit_two" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">菜单名称:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="twomenu" id="twomenu" value=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">显示顺序:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="sort" id="sort" value=""/>
									</div>
								</div>
								<div class="control-group" id="upfile" style="">
									<label class="control-label">上传图片:</label>
									<div class="controls">
										<input id="myFile" name="myFile" type="file" />
										<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile1()">
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo1();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closediv1();">返回</button>
								</div>
								<input id="bid" name="bid" type="hidden" value="">
								<input id="fileName" name="fileName" type="hidden" value="">
								<input id="filePath" name="filePath" type="hidden" value="">
							</form>
						</div>
						<div class="widget-title" id="new_two2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>新增一级菜单</h5>
						</div>
						<div class="widget-content nopadding" id="new_two" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
								<div class="control-group" id="upfile" style="">
									<label class="control-label">上传图片:</label>
									<div class="controls">
										<input id="myFile" name="myFile" type="file" />
										<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile2()">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">菜单名称:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="twomenu" id="twomenu" value=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">显示顺序:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="sort" id="sort" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo2();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closediv2();">返回</button>
								</div>
								<input id="fileName" name="fileName" type="hidden">
								<input id="filePath" name="filePath" type="hidden" value="img/xinjian/">
							</form>
						</div>
						<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
						</iframe>
					</td>
				</tr>
				<tr>
					<td>
						<%if(null != small && small.size() > 0){%>
						<ul class="thumbnails">
							<%for(int k = 0;k < small.size();k ++){
								if(small.get(k).getSid() != 1){
							%>
								<li class="span2">
									<a href="#" class="thumbnail">
										<img src="<%=request.getContextPath()%>/<%=small.get(k).getPath() + small.get(k).getImage()%>" alt="" width="80px" height="80px" style="display:none;"/>
										&nbsp;&nbsp;<%=small.get(k).getName()%>
									</a>
									<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 3)){%>
									<div class="actions">
										<a title="" href="#" onclick="showeditmenu2('<%=small.get(k).getSid()%>','<%=small.get(k).getName()%>','<%=small.get(k).getSort()%>','<%=small.get(k).getPath()%>','<%=small.get(k).getShowfirst()%>')"><i class="icon-pencil icon-white"></i></a>
										<a title="" href="#" onclick="checkdeletethreemenu(this,'<%=small.get(k).getSid()%>','<%=small.get(k).getName()%>','<%=showbid%>')"><i class="icon-remove icon-white"></i></a>
									</div>
									<%}%>
									<%if(user.getRole().getRid() == 2){%>
									<div class="actions">
										<a title="" href="#" onclick="showeditmenu2('<%=small.get(k).getSid()%>','<%=small.get(k).getName()%>','<%=small.get(k).getSort()%>','<%=small.get(k).getPath()%>')"><i class="icon-pencil icon-white"></i></a>
									</div>
									<%}%>
								</li>
							<%}
							}%>
						</ul>
						<%}%>
						<%
							if(big.size() > 1){
								if(small.size() == 0){
						%>
							没有相关菜单<br>
						<%	
							}
						if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){%>
						<ul class="thumbnails" style="display:none">
							<li class="span2">
							<%
								BigClassDao bigDao = (BigClassDao)SpringUtils.getBean(BigClassDao.class);
								BigClass bc =  bigDao.findBigClassByBid(showbid);
							%>
								<a href="#" class="thumbnail" onclick="shownewmenu2('<%=bc.getName()%>')">
									&nbsp;&nbsp;新增二级菜单
								</a>
							</li>
						</ul>
						<input type="button" value="新增二级菜单" onclick="shownewmenu2('<%=bc.getName()%>')">
						<%}
						}%>
					</td>
				</tr>
				<tr>
					<td>
						<div class="widget-title" id="edit_three2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>修改二级菜单</h5>
						</div>
						<div class="widget-content nopadding" id="edit_three" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload3" id="doupload3" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">二级菜单:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="threemenu" id="threemenu" value=""/>
									</div>
								</div>
								<div class="control-group" style="display:none">
									<label class="control-label">显示顺序:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="sort" id="sort" value=""/>
									</div>
								</div>
								<div class="control-group" style="display:none" style="display:none">
									<label class="control-label">热门精选:</label>
									<div class="controls">
										<input type="radio" id="ischeck" name="ischeck" style="margin: 0px 0 0;"/>&nbsp;&nbsp;是&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" id="ischeck" name="ischeck" style="margin: 0px 0 0;"/>&nbsp;&nbsp;否
									</div>
								</div>
								<div class="control-group" id="upfile">
									<label class="control-label">上传图片:</label>
									<div class="controls">
										<input id="myFile" name="myFile" type="file"/>
										<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile3()">
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo3();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closediv3();">返回</button>
								</div>
								<input id="sid" name="sid" type="hidden" value="">
								<input id="showfirst" name="showfirst" type="hidden">
								<input id="fileName" name="fileName" type="hidden" value="">
								<input id="filePath" name="filePath" type="hidden" value="">
							</form>
						</div>
						<div class="widget-title" id="new_three2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>新增二级菜单</h5>
						</div>
						<div class="widget-content nopadding" id="new_three" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload4" id="doupload4" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">所属一级菜单:</label>
									<div class="controls">
										<input type="button" id="twomenu" style="color:red;font-weight:bolder"/>
									<!--	<label class="control-label" id="twomenu"></label>	-->
									</div>
								</div>
								<div class="control-group" id="upfile" >
									<label class="control-label">上传图片:</label>
									<div class="controls">
										<input id="myFile" name="myFile" type="file" />
										<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile4()">
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">二级级菜单:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="threemenu" id="threemenu" value=""/>
									</div>
								</div>
								<div class="control-group" style="display:none">
									<label class="control-label">显示顺序:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="sort" id="sort" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo4();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closediv4();">返回</button>
								</div>
								<input id="bid" name="bid" type="hidden" value="<%=showbid%>">
								<input id="fileName" name="fileName" type="hidden" value="">
								<input id="filePath" name="filePath" type="hidden" value="img/xinzeng/zilei/">
								<input id="showfirst" name="showfirst" type="hidden" value="1">
							</form>
						</div>
						<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
						</iframe>
					</td>
				</tr>
			</table>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#menu_menu"));
		});
	</script>
</html>