<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	
	Compang compang = (Compang)request.getAttribute("compang");
	User user = (User) session.getAttribute("user");
	List<SmallClass> small = (List<SmallClass>)session.getAttribute("smallclass");
	List<BigClass> big = (List<BigClass>)session.getAttribute("bigclass");
%>

<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<script type="text/javascript">
			function setInfo1(){
				if(document.getElementById("upfile").style.display == "none"){
					var r=confirm("需要上传图片吗?");
					if (r==true){
						$("#file_path").show();
						$("#file_name").show();
						$("#upfile").show();
					}else{
						var chk1 = document.getElementById('show_yes');
						if(chk1.checked){
							document.getElementById("showfirst").value = "0";
						}
						var chk2 = document.getElementById('show_no');
						if(chk2.checked){
							document.getElementById("showfirst").value = "1";
						}
						var chk3 = document.getElementById('check_yes');
						if(chk3.checked){
							document.getElementById("checkstatus").value = "0";
						}
						var chk4 = document.getElementById('check_no');
						if(chk4.checked){
							document.getElementById("checkstatus").value = "1";
						}
						
						var form = document.forms['doupload'];
						form.action = '<%=request.getContextPath() %>/browser/updatecompang.do';
						form.target = "_self";
						form.submit();
					}
				}else{
					var chk1 = document.getElementById('show_yes');
					if(chk1.checked){
						document.getElementById("showfirst").value = "0";
					}else{
						document.getElementById("showfirst").value = "1";
					}
					var chk2 = document.getElementById('show_no');
					if(chk2.checked){
						document.getElementById("showfirst").value = "1";
					}
					var chk3 = document.getElementById('check_yes');
					if(chk3.checked){
						document.getElementById("checkstatus").value = "0";
					}else{
						document.getElementById("checkstatus").value = "1";
					}
					var chk4 = document.getElementById('check_no');
					if(chk4.checked){
						document.getElementById("checkstatus").value = "1";
					}
					
					var form = document.forms['doupload'];
					form.action = '<%=request.getContextPath() %>/browser/updatecompang.do';
					form.target = "_self";
					form.submit();
				}
			}
					
		function uploadFile1(){
			document.getElementById("fileName").value = document.getElementById("myFile1").value;
			
			var form = document.forms['doupload1'];
			form.action = '<%=request.getContextPath() %>/browser/upload.do';
			form.target = "rfFrame";
			form.submit();
			alert("上传成功!");
		}
		
		function setInfo2(){
			//var select1 = document.getElementById("sel1");
			//document.getElementById("setcid").value=select1.options[select1.selectedIndex].value;
			//var select2 = document.getElementById("sel2");
			//document.getElementById("filePath").value=select2.options[select2.selectedIndex].value;
			
			var form = document.forms['doupload1'];
			form.action = '<%=request.getContextPath() %>/browser/updatepicture.do';
			form.target = "_self";
			form.submit();
		}
		function uploadFile2(){
			//var select2 = document.getElementById("sel2");
			//document.getElementById("filePath").value=select2.options[select2.selectedIndex].value;
			//var filename = document.getElementById("fileName").value;
			//if(document.getElementById("file_name2").style.display != "none"){
			//	var filename2 = document.getElementById("fileName2").value;
			//	if(filename2.length > 0){
			
			document.getElementById("fileName").value = document.getElementById("myFile2").value;
			var form = document.forms['doupload2'];
			form.action = '<%=request.getContextPath() %>/browser/upload.do';
			form.target = "rfFrame";
			form.submit();
			alert("上传成功!");
			//	}else{
			//		alert("请输入图片名称");
			//	}
				return true;
			//}
			/*if(filename.length > 0){
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				//alert("上传成功!");
				var r=confirm("上传成功，需要上传大图片吗?");
				if (r==true){
					$("#file_name2").show();
				}
			}else{
				alert("请输入图片名称");
			}*/
		}
		
		function changeSmallList(){
			var bigid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			<%for(int i =0;i < big.size();i ++){
				int count = -1;
			%>
				var b<%=i%> = [];
			<%
				for(int j = 0;j < small.size();j ++) {
					
					if(big.get(i).getBid() == small.get(j).getBigclass().getBid()){
					count ++;
			%>
				var str = "<option value=\"<%=small.get(j).getSid()%>\"><%=small.get(j).getName()%></option>";
				b<%=i%>[<%=count%>] = str;
			<%
					}
				}
			}
			%>
			document.getElementById("sel2").options.length = 0;
			if(bigid == 1){
				for(var i = 0; i < b1.length; i ++){
					$("#sel2").append(b1[i]);
				}
			}
			if(bigid == 2){
				for(var i = 0; i < b2.length; i ++){
					$("#sel2").append(b2[i]);
				}
			}
			if(bigid == 3){
				for(var i = 0; i < b3.length; i ++){
					$("#sel2").append(b3[i]);
				}
			}
			if(bigid == 4){
				for(var i = 0; i < b4.length; i ++){
					$("#sel2").append(b4[i]);
				}
			}
			if(bigid == 5){
				for(var i = 0; i < b5.length; i ++){
					$("#sel2").append(b5[i]);
				}
			}
			if(bigid == 6){
				for(var i = 0; i < b6.length; i ++){
					$("#sel2").append(b6[i]);
				}
			}
			if(bigid == 7){
				for(var i = 0; i < b7.length; i ++){
					$("#sel2").append(b7[i]);
				}
			}
			if(bigid == 8){
				for(var i = 0; i < b8.length; i ++){
					$("#sel2").append(b8[i]);
				}
			}
		}
		
		function showfirstimage(){
			document.getElementById("first_image").style.display = "";
		}
		</script>
	</head>
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>编辑商户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/compang.do" class="tip-bottom">商户</a>
				<a href="#" class="current">编辑商户</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>编辑商户</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
									<div class="control-group">
										<ul class="thumbnails">		
											<li class="span2">
												<a href="#" class="thumbnail">
													<img src="<%=request.getContextPath() %>/<%=compang.getPath() + compang.getImage()%>" alt="" />
												</a>
												<div class="actions">
													<a title="" href="#" onclick="showfirstimage()"><i class="icon-pencil icon-white"></i></a>
													<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>
												</div>
											</li>
										</ul>
										<div class="controls" id="first_image" style="display:none">
											<input id="myFile1" name="myFile1" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile1()">
											<input id="fileName" name="fileName" type="hidden">
											<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">编号</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="number" id="number" value="<%=compang.getNumber()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所属大类</label>
										<div class="controls">
											<select id="sel1" onchange="changeSmallList()">
												<%for(BigClass bg:big) {
													if(bg.getBid() == compang.getSmallClass().getBigclass().getBid()){
												%>
												<option value="<%=bg.getBid()%>" selected><%=bg.getName()%></option>
												<%
													}else{
												%>
												<option value="<%=bg.getBid()%>"><%=bg.getName()%></option>
												<%
													}
												}%>
											</select>
										</div>
									</div>
									<div class="control-group" id="small_list" style="">
										<label class="control-label">所属子类</label>
										<div class="controls">
											<select id="sel2">
												<%for(SmallClass sa:small) {
													if(sa.getSid() == compang.getSmallClass().getSid()){
												%>
												<option value="<%=sa.getSid()%>" selected><%=sa.getName()%></option>
												<%
													}else if(sa.getBigclass().getBid() == compang.getSmallClass().getBigclass().getBid()){
												%>
												<option value="<%=sa.getSid()%>"><%=sa.getName()%></option>
												<%
													}
												}%>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">名称</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="com_name" id="com_name" value="<%=compang.getName()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">宣传语</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="title" id="title" value="<%=compang.getTitle()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">地址</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="address" id="address" value="<%=compang.getAddress()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">经度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="longitude" id="longitude" value="<%=compang.getLongitude()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">纬度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="latitude" id="latitude" value="<%=compang.getLatitude()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">服务区域</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="area" id="area" value="<%=user.getAddress()%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">拨打次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="teltime" id="teltime" value="<%=compang.getTeltime()%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">浏览次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="looktime" id="looktime" value="<%=compang.getLooktime()%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">内容</label>
										<div class="controls">
											<textarea name="content_str" id="contentstr"><%=compang.getContent().trim()%></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">电话号码</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="tel" id="tel" value="<%=compang.getTel()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">联系人</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="contact" id="contact" value="<%=compang.getContact()%>"/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">是否在前面显示</label>
										<div class="controls">
											<%
												String check_str1 = "";
												String check_str2 = "";
												if(compang.getShowfirst() == 0){
													check_str1 = "checked";
												}else{
													check_str2 = "checked";
												}
											%>
											<input type="checkbox" name="show_yes" id="show_yes" <%=check_str1%>/>是
											<input type="checkbox" name="show_no" id="show_no" <%=check_str2%>/>否
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">审核</label>
										<div class="controls">
											<%
												String check_str3 = "";
												String check_str4 = "";
												if(compang.getCheckStatus() == 0){
													check_str3 = "checked";
												}else{
													check_str4 = "checked";
												}
											%>
											<input type="checkbox" name="check_yes" id="check_yes" <%=check_str3%>/>通过
											<input type="checkbox" name="check_no" id="check_no" <%=check_str4%>/>不通过
										</div>
									</div>
									<div class="control-group" id="file_path" style="display:none">
										<label class="control-label">图片路径:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="filePath" id="filePath" value="<%=compang.getPath()%>" readonly/>*文件夹名称
										</div>
									</div>
									<div class="control-group" id="file_name" style="display:none">
										<label class="control-label">图片名称:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="fileName" id="fileName" value=""/>
										</div>
									</div>
									<div class="control-group" id="upfile" style="display:none">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile1" name="myFile1" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile1()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo1();">Save</button>
									</div>
									<input id="cid" name="cid" type="hidden" value="<%=compang.getCid()%>">
								</form>
								<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
								</iframe>
							</div>
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商户外景</h5>
							</div>
							<div class="widget-content nopadding" id="poicture_list">
								<ul class="thumbnails">
									<%
										PictureDao pictureDao = (PictureDao)SpringUtils.getBean(PictureDao.class);
										List<Picture> picture = pictureDao.findPictureByCid(compang.getCid());
										for(int i = 0;i < picture.size();i ++){
									%>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="<%=request.getContextPath() %>/<%=picture.get(i).getPath() + picture.get(i).getSmall()%>" alt="" />
										</a>
										<div class="actions">
											<a title="" href="#"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<%}%>
								</ul>
								<div align="center">
									<a id="newad" name="newad" href="#" onclick=""/>新建外景图片</a>
								</div>
							</div>
							<div class="widget-content nopadding" style="display:none" id="new_picture">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">描述:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="description" id="description" value=""/>
										</div>
									</div>
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile2" name="myFile2" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile2()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo2();">Save</button>
									</div>
									<input id="setcid" name="setcid" type="hidden">
									<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
									<input id="fileName" name="fileName" type="hidden">
									<input id="fileName2" name="fileName2" type="hidden">
								</form>
							</div>
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商品列表</h5>
							</div>
							<div class="row-fluid" id="commodity_list">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>名称</th>
											<th>所属商户</th>
											<th>型号</th>
											<th>规格</th>
											<th>价格</th>
											<th>内容</th>
											<th>图片</th>
											<th>详情图片</th>
											<th>操作</th>
										</tr>
										</thead>
										<tbody>
										<%
											CommodityDao commodityDao = (CommodityDao)SpringUtils.getBean(CommodityDao.class);
											List<Commodity> commodity = commodityDao.findCommoditysByCid(compang.getCid());
											if(commodity != null){
												for(int i = 0;i < commodity.size();i ++){
										%>
										<tr>
											<td><%=commodity.get(i).getName()%></td>
											<td><%=commodity.get(i).getCompang().getName()%></td>
											<td><%=commodity.get(i).getCode()%></td>
											<td><%=commodity.get(i).getNorm()%></td>
											<td><%=commodity.get(i).getPrice()%></td>
											<td><%=commodity.get(i).getContent()%></td>
											<td><%=commodity.get(i).getImage()%></td>
											<td><%=commodity.get(i).getDetail_img()%></td>
											<td class="taskOptions">
												<a href="<%=request.getContextPath()%>/browser/delcommodity.do?yid=<%=commodity.get(i).getYid()%>" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
												<a href="<%=request.getContextPath()%>/browser/getcommodity.do?yid=<%=commodity.get(i).getYid()%>" class="tip-top" data-original-title="改"><i class="icon-edit"></i></a>
											</td>
										</tr>
										<%
												}
											}
											if( commodity.size() == 0){
										%>
										<tr>
											<td align="center" colspan="9">该商户目前尚未添加任何商品</td>
										</tr>
										<%}%>
										</tbody>
									</table>
							</div>
							<div class="widget-content nopadding" id="edit_commodity" style="display:none">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload3" id="doupload3" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">商品名称</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="comm_name" id="comm_name" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">型号</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="code" id="code" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">规格</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="norm" id="norm" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">价格</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="price" id="price" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">内容</label>
										<div class="controls">
											<textarea name="content_str" id="content_str" style="width: 243px; height: 78px;"></textarea>
										</div>
									</div>
									<div class="control-group" id="upfile">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile3" name="myFile3" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile3()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo3();">Save</button>
									</div>
									<input id="setcid" name="setcid" type="hidden" value="<%=compang.getCid()%>">
									<input id="fileName1" name="fileName1" type="hidden">
									<input id="fileName2" name="fileName2" type="hidden">
									<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
									<input id="filePath2" name="filePath2" type="hidden" value="<%=compang.getPath()%>">
								</form>
							</div>
							<div align="center">
							<a id="newad" name="newad" href="#" onclick=""/>新建商品</a>
							</div>
						</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
</html>