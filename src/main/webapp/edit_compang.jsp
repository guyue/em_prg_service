<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	
	Compang compang = (Compang)request.getAttribute("compang");
	User user = (User) session.getAttribute("user");
	List<SmallClass> small = (List<SmallClass>)request.getAttribute("smallclass");
	List<BigClass> big = (List<BigClass>)request.getAttribute("bigclass");
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>

<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<script type="text/javascript">
		function randomString() {  
			return '' + new Date().getTime();  
		}
			
		function setInfo1(){
			if(document.getElementsByName("ischeck")[0].checked){
				document.getElementById("authentication").value = "0";
			}
			if(document.getElementsByName("ischeck")[1].checked){
				document.getElementById("authentication").value = "1";
			}
			var sid = document.getElementById("sel2").options[document.getElementById("sel2").selectedIndex].value;
			document.getElementById("setsid").value = sid;
			if(document.getElementById("sel3") != null){
				document.getElementById("area").value = document.getElementById("sel3").options[document.getElementById("sel3").selectedIndex].value;
			}
			
			var form = document.forms['doupload1'];
			form.action = '<%=request.getContextPath() %>/browser/updatecompang.do';
			form.target = "_self";
			form.submit();
		}
					
		function uploadFile1(){
			var str = randomString();
			document.forms['doupload1'].getElementsByTagName("input").fileName.value = document.getElementById("myFile").value;
			
			var filename = document.forms['doupload1'].getElementsByTagName("input").fileName.value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.forms['doupload1'].getElementsByTagName("input").fileName.value = newname;
				
				var form = document.forms['doupload1'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		
		function setInfo2(){
			var form = document.forms['doupload2'];
			form.action = '<%=request.getContextPath() %>/browser/updatepicture.do';
			form.target = "_self";
			form.submit();
		}
		function uploadFile2(){
			var str = randomString();
			document.forms['doupload2'].getElementsByTagName("input").fileName.value = document.forms['doupload2'].getElementsByTagName("input").myFile.value;
			var filename = document.forms['doupload2'].getElementsByTagName("input").fileName.value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.forms['doupload2'].getElementsByTagName("input").fileName.value = newname;
				
				var form = document.forms['doupload2'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
				return true;
			}
		}
		
		function setInfo3(){
			var form = document.forms['doupload3'];
			form.action = '<%=request.getContextPath() %>/browser/updatecommodity.do';
			form.target = "_self";
			form.submit();
		}
			
		function uploadFile3(){
			var str = randomString();
			document.forms['doupload3'].getElementsByTagName("input").fileName.value = document.forms['doupload3'].getElementsByTagName("input").myFile.value;
			
			var filename = document.forms['doupload3'].getElementsByTagName("input").fileName.value;
			/*if(filename.length > 0){
				document.forms['doupload3'].getElementsByTagName("input").fileName2.value = document.forms['doupload3'].getElementsByTagName("input").myFile.value;
				document.forms['doupload3'].getElementsByTagName("input").filePath2.value = document.forms['doupload3'].getElementsByTagName("input").filePath.value;
				filename = document.forms['doupload3'].getElementsByTagName("input").fileName2.value;
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.forms['doupload3'].getElementsByTagName("input").fileName2.value = newname;
			}else{
				document.forms['doupload3'].getElementsByTagName("input").fileName.value = document.forms['doupload3'].getElementsByTagName("input").myFile.value;
				filename = document.forms['doupload3'].getElementsByTagName("input").fileName.value;
				var num3 = filename.length;
				var num4 = filename.indexOf('.');
				var newname = filename.substr(num4,num3);
				newname = str + newname;
				document.forms['doupload3'].getElementsByTagName("input").fileName.value = newname;
			}*/
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.forms['doupload3'].getElementsByTagName("input").fileName.value = newname;
				document.forms['doupload3'].getElementsByTagName("input").fileName2.value = newname;
			
				var form = document.forms['doupload3'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		
		function changeSmallList(){
			var bigid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			$.ajax({
				url:"<%=request.getContextPath()%>/browser/changesmall.do?bid=" + bigid,
				type:"post",
				dataType: "JSON",
				async:false,
				timeout: 3000,
				success: function(ret) {
					if(ret.length > 0){
						document.getElementById("sel2").style.display = "";
						document.getElementById("sel2").options.length = 0;
						for(var i = 0; i < ret.length; i ++){
							$("#sel2").append(ret[i]);
						}
					}else{
						document.getElementById("sel2").style.display = "none";
					}
				},
				error: function(XMLRequest, textInfo) {
					if (textInfo != null) {
						alert(textInfo);
					}
				}
			});
		}
		
		function showfirstimage(){
			document.getElementById("first_image").style.display = "";
		}
		
		function addnewpicture(){
			//document.getElementById("poicture_list").style.display = "none";
			document.getElementById("new_picture").style.display = "";
		}
		
		function addnewcommodity(){
			//document.getElementById("commodity_list").style.display = "none";
			//document.getElementById("commodity_list2").style.display = "none";
			document.getElementById("new_commodity").style.display = "";
			document.getElementById("new_commodity2").style.display = "";
		}
		function closenewcommodity(){
			document.getElementById("commodity_list").style.display = "";
			document.getElementById("commodity_list2").style.display = "";
			document.getElementById("new_commodity").style.display = "none";
			document.getElementById("new_commodity2").style.display = "none";
		}
		function closenewpicture2(){
			document.getElementById("poicture_list").style.display = "";
			document.getElementById("new_picture").style.display = "none";
		}
		function findbaiduapi(){
			document.getElementById("baidu_url").target = "_blank";
			document.getElementById("baidu_url").click();
		}
		</script>
		<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
		.thumbnails .actions {
			margin-left: -14px;
			width: 15px;
		}
	</style>
	</head>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/js/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/lang/cn_utf8.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/calendar-setup.js"></script>
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>	
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>编辑商户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/compang.do" class="tip-bottom">商户列表</a>
				<a href="#" class="current">编辑商户</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>编辑商户</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal" />
									<div class="control-group">
										<ul class="thumbnails">		
											<li class="span2">
												<a href="#" class="thumbnail">
													<img src="<%=request.getContextPath() %>/<%=compang.getPath() + compang.getImage()%>" alt="" />
												</a>
												<div class="actions">
													<a title="" href="#" onclick="showfirstimage()"><i class="icon-pencil icon-white"></i></a>
												<!--	<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>	-->
												</div>
											</li>
										</ul>
										<div class="controls" id="first_image" style="display:none">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile1()">
											<input id="fileName" name="fileName" type="hidden">
											<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">编号</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="number" id="number" value="<%=String.valueOf(compang.getNumber())%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所属大类</label>
										<div class="controls">
											<select id="sel1" onchange="changeSmallList()">
												<%for(BigClass bg:big) {
													if(bg.getBid() == compang.getSmallClass().getBigclass().getBid()){
												%>
												<option value="<%=bg.getBid()%>" selected><%=bg.getName()%></option>
												<%
													}else{
												%>
												<option value="<%=bg.getBid()%>"><%=bg.getName()%></option>
												<%
													}
												}%>
											</select>
										</div>
									</div>
									<div class="control-group" id="small_list" style="">
										<label class="control-label">所属子类</label>
										<div class="controls">
											<select id="sel2">
												<%for(SmallClass sa:small) {
													if(sa.getSid() == compang.getSmallClass().getSid()){
												%>
												<option value="<%=sa.getSid()%>" selected><%=sa.getName()%></option>
												<%
													}else{
												%>
												<option value="<%=sa.getSid()%>"><%=sa.getName()%></option>
												<%
													}
												}%>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">名称</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="com_name" id="com_name" value="<%=compang.getName()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">宣传语</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="title" id="title" value="<%=compang.getTitle()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">地址</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="address" id="address" value="<%=compang.getAddress()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">经纬度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="long_lat" id="long_lat" value="<%=compang.getLongitude()%>,<%=compang.getLatitude()%>"/>
											<a href="http://api.map.baidu.com/lbsapi/getpoint/index.html" id="baidu_url"></a>
											<input type="button" onclick="findbaiduapi()" value="坐标取值">
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">经度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="longitude" id="longitude" value="<%=compang.getLongitude()%>"/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">纬度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="latitude" id="latitude" value="<%=compang.getLatitude()%>"/>
										</div>
									</div>
									<%
										int pid = Integer.parseInt(compang.getArea());
										String city = "";
										String area = "";
										List<Place> area_list = placeDao.findAreaByCity(newarea.getArea());
										
										Place pl = placeDao.findPlaceByPid(pid);
										city = pl.getCity();
										area = pl.getArea();
									%>
									<div class="control-group">
										<label class="control-label">所在城市</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="usercity" id="usercity" value="<%=city%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所在区域</label>
										<div class="controls">
											<%if(user.getRole().getRid() <= 3){%>
											<select id="sel3" onclick="" style="width:100px">
												<%for(int k = 0;k < area_list.size();k ++){
													if(area_list.get(k).getArea().equals(pl.getArea())){
												%>
													<option value="<%=area_list.get(k).getPid()%>" selected><%=area_list.get(k).getArea()%></option>
												<%}else{%>
													<option value="<%=area_list.get(k).getPid()%>"><%=area_list.get(k).getArea()%></option>
												<%}}%>
											</select>
											<%}else{%>
											<input type="text" style="width:25%;height:25px" name="userarea" id="userarea" value="<%=pl.getArea()%>" readonly/>
											<%}%>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">拨打次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="teltime" id="teltime" value="<%=compang.getTeltime()%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">浏览次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="looktime" id="looktime" value="<%=compang.getLooktime()%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">内容</label>
										<div class="controls">
											<textarea name="content_str" id="content_str"><%=compang.getContent()%></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">电话号码</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="tel" id="tel" value="<%=compang.getTel()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">联系人</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="contact" id="contact" value="<%=compang.getContact()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">到期时间</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="expiration_time" id="expiration_time" value="<%=formatter.format(compang.getExpirationtime())%>" readonly/>
											<img src="<%=request.getContextPath()%>/image/cal.gif" id="pdate_cal">
											<script type="text/javascript">
												Calendar.setup({ inputField : "expiration_time", ifFormat : "%Y/%m/%d", showsTime :false, button : "pdate_cal", singleClick : true, step : 1 });
											</script>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">是否认证：</label>
										<div class="controls">
											<%
												String ischeck = "checked";
												String uncheck = "checked";
												if(compang.getAuthentication() == 1){
													ischeck = "";
												}else{
													uncheck = "";
												}
											%>
											<input type="radio" id="ischeck" name="ischeck" style="margin: 0px 0 0;" <%=ischeck%>/>&nbsp;&nbsp;已认证&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="radio" id="ischeck" name="ischeck" style="margin: 0px 0 0;" <%=uncheck%>/>&nbsp;&nbsp;未认证
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">是否推荐:</label>
										<div class="controls">
											<%
												ischeck = "checked";
												uncheck = "checked";
												if(compang.getShowfirst() == (byte)1){
													ischeck = "";
												}else{
													uncheck = "";
												}
											%>
											<input name="showfirst" type="radio" value="0" <%=ischeck %> /> 是
											<input name="showfirst" type="radio" <%=uncheck %> value="1" /> 否
										</div>
									</div>
									<div class="form-actions" style="display:none">
										<button type="button" class="btn btn-primary" onclick="setInfo1();">保存</button>
									</div>
									<input id="cid" name="cid" type="hidden" value="<%=compang.getCid()%>">
									<input id="setsid" name="setsid" type="hidden">
									<input id="checkstatus" name="checkstatus" type="hidden" value="1">
									<input id="authentication" name="authentication" type="hidden">
									<input id="area" name="area" type="hidden" value="<%=compang.getArea()%>">
								</form>
								<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
								</iframe>
							</div>
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商户外景</h5>
							</div>
							<div class="widget-content nopadding" id="poicture_list">
								<ul class="thumbnails">
									<%
										PictureDao pictureDao = (PictureDao)SpringUtils.getBean(PictureDao.class);
										List<Picture> picture = pictureDao.findPictureByCid(compang.getCid());
										for(int i = 0;i < picture.size();i ++){
									%>
									<li class="span2">
										<a href="#" class="thumbnail">
											<img src="<%=request.getContextPath() %>/<%=picture.get(i).getPath() + picture.get(i).getSmall()%>" alt="" />
										</a>
										<div class="actions">
										<!--	<a title="" href="#" onclick="addnewpicture()"><i class="icon-pencil icon-white"></i></a>	-->
											<a title="" href="<%=request.getContextPath() %>/browser/delpicture.do?pid=<%=picture.get(i).getPid()%>&cid=<%=compang.getCid()%>"><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
									<%}%>
								</ul>
								<%if(picture.size() == 0){%>
								<div class="control-group">
									<label class="control-label">&nbsp;&nbsp;无外景图片</label>
								</div>
								<%}%>
							</div>
							<div class="widget-content nopadding" style="display:none" id="new_picture">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
									<div class="control-group" style="display:none">
										<label class="control-label">描述:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="description" id="description" value=""/>
										</div>
									</div>
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile2()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo2();">保存</button>
										<button type="button" class="btn btn-primary" onclick="closenewpicture2();">返回</button>
									</div>
									<input id="setcid" name="setcid" type="hidden" value="<%=compang.getCid()%>">
									<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
									<input id="fileName" name="fileName" type="hidden">
								</form>
							</div>
							<div align="center">
								<a id="newad" name="newad" href="#" onclick="addnewpicture()"/>新建外景</a>
							</div>
							<div class="widget-title" id="commodity_list2" style="display:none">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>商品列表</h5>
							</div>
							<div class="row-fluid" id="commodity_list" style="display:none">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>图片</th>
											<th>商品名称</th>
											<th>商品说明</th>
											<th>现价</th>
											<th>原价</th>
											<th>使用说明</th>
											<th>操作</th>
										</tr>
										</thead>
										<tbody>
										<%
											CommodityDao commodityDao = (CommodityDao)SpringUtils.getBean(CommodityDao.class);
											List<Commodity> commodity = commodityDao.findCommoditysByCid(compang.getCid());
											if(commodity != null){
												for(int i = 0;i < commodity.size();i ++){
													String code = commodity.get(i).getCode();
													if(code.length() > 15){
														code = code.substring(0,15) + "......";
													}
													String content = commodity.get(i).getContent();
													if(content.length() > 15){
														content = content.substring(0,15) + "......";
													}
										%>
										<tr>
											<td><img src="<%=request.getContextPath() %>/<%=commodity.get(i).getPath() + commodity.get(i).getImage()%>" width="80px"/></td>
											<td><%=commodity.get(i).getName()%></td>
											<td><%=code%></td>
											<td><%=commodity.get(i).getDeals()%></td>
											<td><%=commodity.get(i).getPrice()%></td>
											<td><%=content%></td>
											<td class="taskOptions">
												<a href="<%=request.getContextPath()%>/browser/delcommodity.do?yid=<%=commodity.get(i).getYid()%>&cid=<%=compang.getCid()%>" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
												<a href="<%=request.getContextPath()%>/browser/getcommodity.do?yid=<%=commodity.get(i).getYid()%>&cid=<%=compang.getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
											</td>
										</tr>
										<%
												}
											}
											if( commodity.size() == 0){
										%>
										<tr>
											<td align="center" colspan="9">无商品信息</td>
										</tr>
										<%}%>
										</tbody>
									</table>
							</div>
							<div class="widget-title" id="new_commodity2" style="display:none">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>添加商品</h5>
							</div>
							<div class="widget-content nopadding" id="new_commodity" style="display:none">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload3" id="doupload3" class="form-horizontal" />
									<div class="control-group" id="upfile">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile3()">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">商品名称</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="comm_name" id="comm_name" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">商品说明</label>
										<div class="controls">
											<textarea name="code" id="code" style="width: 243px; height: 78px;"></textarea>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">规格</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="norm" id="norm" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">现价</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="deals" id="deals" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">原价</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="price" id="price" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">使用说明</label>
										<div class="controls">
											<textarea name="comm_content" id="comm_content" style="width: 243px; height: 78px;"></textarea>
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo3();">保存</button>
										<button type="button" class="btn btn-primary" onclick="closenewcommodity();">取消</button>
									</div>
									<input id="setcid" name="setcid" type="hidden" value="<%=compang.getCid()%>">
									<input id="fileName" name="fileName" type="hidden">
									<input id="fileName2" name="fileName2" type="hidden">
									<input id="filePath" name="filePath" type="hidden" value="<%=compang.getPath()%>">
									<input id="filePath2" name="filePath2" type="hidden" value="<%=compang.getPath()%>">
								</form>
							</div>
							<div align="center" id="" style="display:none">
							<a id="newad" name="newad" href="#" onclick="addnewcommodity()"/>新建商品</a>
							</div>
							<div class="form-actions" align="center">
								<button type="button" class="btn btn-primary" onclick="setInfo1();">完成</button>
							</div>
						</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
</html>