<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	
	//List<FourClass> four = (List<FourClass>)session.getAttribute("fourclass");
	//List<BigClass> big = (List<BigClass>)session.getAttribute("bigclass");
	//List<SmallClass> small = (List<SmallClass>)session.getAttribute("smallclass");
	
	
	List<BigClass> big = (List<BigClass>)request.getAttribute("bigclass");
	List<SmallClass> small = (List<SmallClass>)request.getAttribute("smallclass");
	User user = (User) session.getAttribute("user");
	
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>菜单</title>
		<style type="text/css">	
		.span2 {
			width: 60px;
		}
		</style>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">菜单</a>
			</div>	
			<table style="width:100%;">
				<tr>
					<td width="30%">
						<table>
							<%for(int j = 0;j < big.size();j ++){%>
							<tr>
								<td>
								<%=big.get(j).getName()%>
								<ul class="thumbnails">	
									
									<li class="span2">
										<a href="<%=request.getContextPath()%>/browser/smallclass.do?bid=<%=big.get(j).getBid()%>" class="thumbnail">
											<img src="<%=request.getContextPath()%>/<%=big.get(j).getPath() + big.get(j).getImage()%>" alt="" width="50px" height="50px"/>
										</a>
										<div class="actions">
											<a title="" href="#" onclick="showfirstimage()"><i class="icon-pencil icon-white"></i></a>
											<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>
										</div>
									</li>
								</ul>
								</td>
							</tr>
							<%}%>
						</table>
					</td>
					<td width="70%">
						<ul class="thumbnails">
							<%for(int k = 0;k < small.size();k ++){%>
								<li class="span2">
									<a href="#" class="thumbnail">
										<img src="<%=request.getContextPath()%>/<%=small.get(k).getPath() + small.get(k).getImage()%>" alt="" width="50px" height="50px"/>
									</a>
									<div class="actions">
										<a title="" href="#" onclick="showfirstimage()"><i class="icon-pencil icon-white"></i></a>
										<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>
									</div>
								</li>
							<%}%>
						</ul>
					</td>
				</tr>
			</table>
			<!--
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>菜单列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>一级编号</th>
										<th>菜单名词</th>
										<th>二级编号</th>
										<th>菜单名词</th>
										<th>二级菜单图片</th>
										<th>三级编号</th>
										<th>菜单名称</th>
										<th>三级菜单图片</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
								
									<%
										if(four != null){
											for(int i = 0;i < four.size();i ++){
												for(int j = 0;j < big.size();j ++){
													if(four.get(i).getFid() == big.get(j).getFourclass().getFid()){
														for(int k = 0;k < small.size();k ++){
															if(small.get(k).getBigclass().getBid() == big.get(j).getBid()){
									%>
									<tr>
										<td><%=four.get(i).getFid()%></td>
										<td><%=four.get(i).getName()%></td>
										<td><%=four.get(i).getFid()%>-<%=big.get(j).getBid()%></td>
										<td><%=big.get(j).getName()%></td>
										<td><%=big.get(j).getImage()%></td>
										<td><%=four.get(i).getFid()%>-<%=big.get(j).getBid()%>-<%=small.get(k).getSid()%></td>
										<td><%=small.get(k).getName()%></td>
										<td><%=small.get(k).getImage()%></td>
										<td class="taskOptions">
											<a href="<%=request.getContextPath()%>/browser/deltwomenu.do?bid=<%=big.get(j).getBid()%>" class="tip-top" data-original-title="删除二级菜单"><i class="icon-remove"></i></a>
											<a href="<%=request.getContextPath()%>/browser/delthreemenu.do?sid=<%=small.get(k).getSid()%>" class="tip-top" data-original-title="删除三级菜单"><i class="icon-remove"></i></a>
											<a href="<%=request.getContextPath()%>/browser/getmenu.do?sid=<%=small.get(k).getSid()%>&bid=<%=big.get(j).getBid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
										</td>
									</tr>
									<%
															}
														}
													}
												}
											}
										}
									%>
								</tbody>
								</table>
						</div>	
						<div align="center">
						<a id="newad" name="newad" href="../new_menu.jsp" />新建菜单</a>
						</div>
					</div>					
				</div>
			</div>
			-->
			
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#menu_menu"));
		});
	</script>
</html>