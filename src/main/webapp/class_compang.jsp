<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%
	List<SmallClass> threemenu = (List<SmallClass>)request.getAttribute("smallclass");
	List<Compang> compang = (List<Compang>)request.getAttribute("compang");
	String sid = (String)request.getAttribute("sid");
	int sid_num = 0;
	if(null != sid){
		sid_num = Integer.parseInt(sid);
	}
	int total = 0;
	String total_num = (String)request.getAttribute("total");
	if(null != total_num){
		total = Integer.parseInt(total_num);
	}
	String start_num = (String)request.getAttribute("start");
	int start = 0;
	if(null != start_num){
		start = Integer.parseInt(start_num);
	}
	int nextnumber = 0;
	boolean shownext = false;
	if(total > start){
		if(start == 0){
			if(total > 5){
				nextnumber = 5;
			}else{
				shownext = true;
			}
		}else{
			if(start + 5 >= total){
				shownext = true;
			}else{
				nextnumber = total - start;
			}
		}
	}
	String showbylook = (String)request.getAttribute("showbylook");
	int area = 0;
	String area_num = (String)request.getAttribute("area");
	if(null != area_num){
		area = Integer.parseInt(area_num);
	}
%>
<html>
	<head>
		<title>易民网</title>
		<style type="text/css">
		#topbar {
			background: none repeat scroll 0 0 #f5f5f5;
			border-bottom: 1px solid #ddd;
			color: #666;
			font: 12px/2 Arial,Tahoma,"宋体";
			height: 37px;
			width: 100%;
		}
		#topbar .w, #topbar .warp {
			clear: both;
			line-height: 37px;
		}
		#topbar .w {
			background: none repeat scroll 0 0 #f5f5f5;
			border-bottom: 1px solid #ddd;
			height: 37px;
			position: relative;
		}
		.pos {
			position: relative;
			z-index: 999;
		}
		.bar_left {
			float: left;
			height: 27px;
			
			word-spacing: 1px;
		}
		.bar_right {
			float: right;
		}
		.bar_left h2 {
			color: red;
			display: inline;
			float: left;
			font-size: 16px;
			font-weight: 700;
		}
		.bar_left .tuan {
			display: inline-block;
			padding-right: 30px;
			position: relative;
		}
		.gap {
			color: #ccc;
			font-family: '宋体';
			margin: 0 6px;
		}
		.towdiv {
			float:left;
			position:relative;
		}
		.provinces {
			float: left;
			height: 30px;
			margin: 30px 0 0;
			padding: 0;
			position: relative;
			width: 100px;
		}
		.topcity {
			background: url("<%= request.getContextPath() %>/image/x.jpg") no-repeat scroll right 0px rgba(0, 0, 0, 0);
			cursor: pointer;
			float: left;
			font-size: 20px;
			line-height: 18px;
			padding: 0 30px 30px 90px;
			left: 200px;
		}
		#saerkey {
			-moz-border-bottom-colors: none;
			-moz-border-left-colors: none;
			-moz-border-right-colors: none;
			-moz-border-top-colors: none;
			background: none repeat scroll 0 0 #fff;
			border-color: #f78015 -moz-use-text-color #f78015 #f78015;
			border-image: none;
			border-style: solid none solid solid;
			border-width: 2px 0 2px 2px;
			float: left;
			position:relative;
			height: 30px;
			overflow: hidden;
			padding-left: 13px;
		}
		#key {
			background: none repeat scroll 0 0 #fff;
			float: left;
			height: 30px;
			line-height: 34px;
		}
		#keyword {
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
			border: 0 none;
			display: inline-block;
			float: left;
			font-size: 14px;
			height: 22px;
			line-height: 22px;
			overflow: hidden;
			padding: 4px 2px 4px 0;
			width: 444px;
		}
		#searchbar {
			left: 215px;
			line-height: 26px;
			overflow: hidden;
			position: absolute;
			top: 11px;
		}
		#searchbar i {
			font-style: normal;
		}
		.keyword {
			color: #c8c8c8;
		}
		#keyword.keyword2 {
			background-position: 0 50px;
			color: #292929;
		}
		.inputcon {
			float: left;
			position:relative;
			height: 34px;
			overflow: hidden;
		}
		.inputcon input:active {
			background: none repeat scroll 0 0 #eb6d13;
		}
		.clear, .line {
			clear: both;
			font-size: 0;
			height: 0;
			overflow: hidden;
		}
		.search-fix .search-no {
			display: none;
			visibility: hidden;
		}
		#hot, .hot2 {
			float: left;
		}
		#hot a, .hot2 a {
			color: #666;
			float: left;
			margin-right: 10px;
		}
		#hot a {
			display: none;
		}
		.hot3 {
			float: left;
		}
		.hot3 a {
			color: #666;
			float: left;
			margin-right: 10px;
		}
		input.btnal2, input.btnall {
			border: 0 none;
			color: #fff;
			cursor: pointer;
			float: left;
			font-size: 14px;
			font-weight: 700;
			height: 34px;
			line-height: 34px;
			margin: 0;
			overflow: hidden;
			padding: 0 20px;
			text-align: center;
		}
		input.btnall {
			background: none repeat scroll 0 0 #f78015;
		}
		body{ text-align:center;
			backgroud:#ffffff} 
		#title{margin:0 auto;border:0px solid #000;width:80%} 
		
		.style1 {color: #F64E4E}
		.style2 {color: #2260D1;float:left;}
		.style3 {float:left;}
		.style4 {
				display:block;
				width:60px;
				height:20px;
				border:1px solid #CCC;
				background:#FFF;
				text-decoration: none;
			}
		</style>
		<link rel="stylesheet" type="text/css" media="all" href="<%= request.getContextPath() %>/css/index.css"  />
		<link rel="stylesheet" type="text/css" media="all" href="<%= request.getContextPath() %>/css/test.css"  />
		<script type="text/javascript">
			
		</script>
	</head>
	<body>
		<div id="headTop">
			<div style="width:1000px;margin:auto;">
				<ul class="headTopUl clearfix">
					<div class="bar_left">
						<div id="first_page" class="towdiv">
							<span class="style1"><b>南京市</b></span><a target="_self" href=""> [切换城市]</a>
							<span class="gap">|</span>
							<a target="_self" href="">设为首页</a>
							<span class="gap">|</span>
							<a target="_self" href="">收藏易民</a>
						</div>
						
					</div>
					<div class="bar_right">
						
						<div id="about" class="towdiv">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a target="_blank" href="">客服热线</a>
							<span class="gap">|</span>
							<a target="_blank" href="<%= request.getContextPath() %>/phone.jsp">手机易民</a>
						</div>
					</div>
				</ul>
			</div>
		</div>
		<div id="headMin" >
			<div style="width:1000px;margin:auto;">
				<ul class="headConul clearfix">
					<li class="logoLi">
						<a id="logo" target="_blank" href="">
							<image style="height:35px" src="<%= request.getContextPath() %>/image/logo.png" title="欢迎来到易民服务站">
						</a>
					</li>
					
					<li class="searchLi">
						<form id="soso_form" target="_self" action="" onsubmit="">
							<input class="searchTxt" type="text" style="border-radius:3px; autocomplete="off" maxlength="140" name="w" value="请输入商品名、地址">
							<a id="soso_submit" class="searchBtn" href="javascript:;">搜索</a>
						</form>
					</li>
				</ul>
			</div>
		</div>
		<div style="width:1000px;margin:auto;">
			<table style="width:100%;" border="0" cellpadding="0" cellspacing="1">
				<tr style="height:35px;background-color:#e9e9e9">
					<td style="border-left:1px solid #e9e9e9;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">
						&nbsp;&nbsp;易民南京>易民社区>维修服务
					</td>
				</tr>
			</table>
			<section id="selection">
				<div class="mb10">
					<dl class="secitem pr">
						<dt>
						<span><%=threemenu.get(0).getBigclass().getName()%>：</span>
						</dt>
						<dd id="common_menu_catg">
							<%
								for(int j = 0;j < threemenu.size();j ++ ){
									if(null != sid){
										if(j == 0){
							%>
											<a href="<%= request.getContextPath() %>/browser/smallclassandcompang.do?bid=<%=threemenu.get(j).getBigclass().getBid()%>">全部</a>
							<%
										}else{
											if(threemenu.get(j).getSid() == sid_num){
							%>
											<a class="select" href="<%= request.getContextPath() %>/browser/smallclassandcompang.do?bid=<%=threemenu.get(j).getBigclass().getBid()%>&sid=<%=threemenu.get(j - 1).getSid()%>"><%=threemenu.get(j - 1).getName()%></a>
							<%
											}else{
							%>
											<a href="<%= request.getContextPath() %>/browser/smallclassandcompang.do?bid=<%=threemenu.get(j).getBigclass().getBid()%>&sid=<%=threemenu.get(j - 1).getSid()%>"><%=threemenu.get(j - 1).getName()%></a>
							<%
											}
										}
									}else{
										if(j == 0){
							%>
										<a class="select" href="<%= request.getContextPath() %>/browser/smallclassandcompang.do?bid=<%=threemenu.get(j).getBigclass().getBid()%>">全部</a>
							<%
										}else{
							%>
										<a href="<%= request.getContextPath() %>/browser/smallclassandcompang.do?bid=<%=threemenu.get(j).getBigclass().getBid()%>&sid=<%=threemenu.get(j -1).getSid()%>"><%=threemenu.get(j - 1).getName()%></a>
							<%
										}
									}
								}
							%>
						</dd>
					</dt>
					</dl>
					<dl class="secitem pr">
						<dt>
							<span>服务区域：</span>
						</dt>
						<dd id="common_menu_catg">
							<a class="<%= (area == 0?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=0">全部</a>
							<a class="<%= (area == 1?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=1&area=玄武区">玄武</a>
							<a class="<%= (area == 2?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=2&area=鼓楼区">鼓楼</a>
							<a class="<%= (area == 3?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=3&area=建邺区">建邺</a>
							<a class="<%= (area == 4?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=4&area=白下区">白下</a>
							<a class="<%= (area == 5?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=5&area=秦淮区">秦淮</a>
							<a class="<%= (area == 6?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=6&area=下关区">下关</a>
							<a class="<%= (area == 7?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=7&area=雨花台区">雨花台</a>
							<a class="<%= (area == 8?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=8&area=浦口区">浦口</a>
							<a class="<%= (area == 9?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=9&area=栖霞区">栖霞</a>
							<a class="<%= (area == 10?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=10&area=江宁区">江宁</a>
							<a class="<%= (area == 11?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=11&area=六合区">六合</a>
							<a class="<%= (area == 12?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=12&area=高淳区">高淳</a>
							<a class="<%= (area == 13?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=13&area=溧水区">溧水</a>
							<a class="<%= (area == 14?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=14&area=大厂区">大厂</a>
							<a class="<%= (area == 15?"select":"") %>" href="<%= request.getContextPath() %>/browser/compangbyarea.do?bid=<%=threemenu.get(0).getBigclass().getBid()%>&areanum=15&area=南京周边">南京周边</a>
						</dd>
					</dl>
				</div>
			</section>
			<table style="width:1000px;" border="0" cellpadding="0" cellspacing="1">
				<tr style="height:35px;background-color:#e9e9e9">
					<td style="border-left:1px solid #e9e9e9;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">
					<!--<td style="height:35px;float:left;">-->
					<span valign="middle">
						&nbsp;&nbsp;&nbsp;<a href="<%=request.getContextPath()%>/browser/pagemenu.do?fid=" style="text-decoration: none;color: #666;">默认↓</a>
						&nbsp;&nbsp;&nbsp;<a href="<%=request.getContextPath()%>/browser/getcompangbylook.do?fid=" style="text-decoration: none;color: #666;">浏览↓</a>
					</span>
					</td>
					<td style="height:36px;width:210px;background-color:#e9e9e9">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:790px;">
						<table	style="width:100%;" border="0" cellpadding="0" cellspacing="1">
							
							<%
								if(compang.size() > 0){
								String url = compang.get(0).getPath() + compang.get(0).getImage();
							%>
							<tr style="height:60px">
								<td style="border-left:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">
								<a href="<%=request.getContextPath()%>/browser/companginfo.do?cid=<%=compang.get(0).getCid()%>">
								&nbsp;&nbsp;<img src="<%= request.getContextPath() %>/<%=url%>">
								</a>
								</td>
								<td style="text-align:left;border-bottom:1px solid #e9e9e9;">
								<span style="line-height:1.5">
									<a href="<%=request.getContextPath()%>/browser/companginfo.do?cid=<%=compang.get(0).getCid()%>">
									<font color="blue" style=""><%=compang.get(0).getTitle()%></font><br>
									</a>
									<%=compang.get(0).getName()%><br>
									所在区域:<%=compang.get(0).getAddress()%><br>
									浏览次数:<%=compang.get(0).getLooktime()%><br>
								</span>
								</td>
								<td style="border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;"><font color="red"><%=compang.get(0).getTel()%></font></td>
							</tr>
							<%}else{%>
							<tr>
								<td>没有相关信息</td>
							</tr>
							<%
							}
							if(compang.size() > 1){
								String url = compang.get(1).getPath() + compang.get(1).getImage();
							%>
							<tr style="height:60px">
								<td style="border-left:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">&nbsp;&nbsp;<img src="<%= request.getContextPath() %>/<%=url%>"></td>
								<td style="text-align:left;border-bottom:1px solid #e9e9e9;">
								<span style="line-height:1.5">
									<font color="blue" style=""><%=compang.get(1).getTitle()%></font><br>
									<%=compang.get(1).getName()%><br>
									所在区域:<%=compang.get(1).getAddress()%><br>
									浏览次数:<%=compang.get(1).getLooktime()%><br>
								</span>
								</td>
								<td style="border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;"><font color="red"><%=compang.get(1).getTel()%></font></td>
							</tr>
							<%}else{%>
							<tr>
								<td>没有相关信息</td>
							</tr>
							<%
							}
							if(compang.size() > 2){
								String url = compang.get(2).getPath() + compang.get(2).getImage();
							%>
							<tr>
								<td style="border-left:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">&nbsp;&nbsp;<img src="<%= request.getContextPath() %>/<%=url%>"></td>
								<td style="text-align:left;border-bottom:1px solid #e9e9e9;">
								<span style="line-height:1.5">
									<font color="blue"><%=compang.get(2).getTitle()%></font><br>
									<%=compang.get(2).getName()%><br>
									所在区域:<%=compang.get(2).getAddress()%><br>
									浏览次数:<%=compang.get(2).getLooktime()%><br>
								</span>
								</td>
								<td style="border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;"><font color="red"><%=compang.get(2).getTel()%></font></td>
							</tr>
							<%}else{%>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<%}
							if(compang.size() > 3){
								String url = compang.get(3).getPath() + compang.get(3).getImage();
							%>
							<tr>
								<td style="border-left:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">&nbsp;&nbsp;<img src="<%= request.getContextPath() %>/<%=url%>"></td>
								<td style="text-align:left;border-bottom:1px solid #e9e9e9;">
								<span style="line-height:1.5">
									<font color="blue"><%=compang.get(3).getTitle()%></font><br>
									<%=compang.get(3).getName()%><br>
									所在区域:<%=compang.get(3).getAddress()%><br>
									浏览次数:<%=compang.get(3).getLooktime()%><br>
								</span>
								</td>
								<td style="border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;"><font color="red"><%=compang.get(3).getTel()%></font></td>
							</tr>
							<%}else{%>
							<tr>
								<td></td>
							</tr>
							<%}
							if(compang.size() > 4){
								String url = compang.get(4).getPath() + compang.get(4).getImage();
							%>
							<tr>
								<td style="border-left:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;text-align:left;">&nbsp;&nbsp;<img src="<%= request.getContextPath() %>/<%=url%>"></td>
								<td style="text-align:left;border-bottom:1px solid #e9e9e9;">
								<span style="line-height:1.5">
									<font color="blue"><%=compang.get(4).getTitle()%></font><br>
									<%=compang.get(4).getName()%><br>
									所在区域:<%=compang.get(4).getAddress()%><br>
									浏览次数:<%=compang.get(4).getLooktime()%><br>
								</span>
								</td>
								<td style="border-bottom:1px solid #e9e9e9;border-right:1px solid #e9e9e9;"><font color="red"><%=compang.get(4).getTel()%></font></td>
							</tr>
							<%}else{%>
							<tr>
								<td></td>
							</tr>
							
							<%}
								if(!shownext){
									if(showbylook == "1"){
							%>
							<tr>
								<td colspan="3" align="center"><a class="style4" disabled="disabled" href="<%= request.getContextPath() %>/browser/nextcompang.do?fid=&number=10&start=<%=nextnumber%>&showbylook=1">下一页</a></td>
							</tr>
							<%		}else{
							%>
								<tr>
									<td colspan="3" align="center"><a class="style4" disabled="disabled" href="<%= request.getContextPath() %>/browser/nextcompang.do?fid=&number=10&start=<%=nextnumber%>">下一页</a></td>
								</tr>
							<%
								}
							}%>
						</table>
					</td>
					<td style="width:210px;">
						
					</td>
				</tr>
			</table>
		</div>
		
		<div style="1000px;margin:8px 0 0 0;">
			<a href="">
				<img width="1000px" height="100px" src="/mobile/image/6.jpg">
			</a>
		</div>
		<div class="footer-btm" style="width:100%;filter:alpha(opacity=70);position:absolute;opacity: 1;">
			<table width="1000px" style="margin:auto;">
				<tr>
					
					<td style="width:60%;text-align:left">
						<font color="white">
						<a href=""><font color="white">关于易民</font></a><span class="gap">|</span><a href=""><font color="white">招商热线</font></a><span class="gap">|</span><a href=""><font color="white">法律声明</font></a>
						<br>
						</font>
					</td>
					<td>
						<font color="white">
						<span style="float:right">
						苏ICP备	13023235号
						</span>
						</font>
					</td>
					<!--
					<td style="width:10%">
						<img style="height:60px" src="<%= request.getContextPath() %>/image/8.jpg">
					</td>
					<td style="width:16%;text-align:left">
						<font color="white">
						 扫描二维码，即可与易民微信互动，还有惊爆优惠等你来拿！
						 </font>
					</td>
					<td style="width:4%">&nbsp;</td>
					-->
				</tr>
			</table>
		</div>
	</body>
</html>