<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	BigClass bigclass = (BigClass)request.getAttribute("bigclass");
	SmallClass smallclass = (SmallClass)request.getAttribute("smallclass");
	
	User user = (User) session.getAttribute("user");
	
%>

<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<script type="text/javascript">
		function setInfo(){			
			var form = document.forms['doupload2'];
			form.action = '<%=request.getContextPath() %>/browser/updatetwomenu.do';
			form.target = "_self";
			form.submit();
		}
		function setInfo2(){
			var form = document.forms['doupload3'];
			form.action = '<%=request.getContextPath() %>/browser/updatethreemenu.do';
			form.target = "_self";
			form.submit();
		}
		function uploadFile(){
			//var filename = document.getElementById("fileName").value;
			document.getElementById("fileName").value = document.getElementById("myFile").value;
			//if(filename.length > 0){
				var form = document.forms['doupload2'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			//}else{
			//	alert("请输入图片名称");
			//}
		}
		function uploadFile2(){
			//var filename2 = document.getElementById("fileName2").value;
			document.getElementById("fileName2").value = document.getElementById("myFile").value;
			//if(filename2.length > 0){
				var form = document.forms['doupload3'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			//}else{
			//	alert("请输入图片名称");
			//}
		}
		</script>
	</head>
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<!--
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 首页</a>
			<ul id="leftMenu">
				<li id="home_menu" class="active"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-home"></i> <span>首页</span></a></li>
				<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-th-large"></i> <span>广告</span></a></li>
				<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单</span></a></li>
				<li id="settings_menu" onclick="setActiveClass(this)" class="submenu" style="display:none"><a href="#"><i class="icon icon-cog"></i> <span>设置</span> <span class="label">2</span></a>
					<ul>
						<li id="settings_menu" ><a href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span>修改密码</span></a></li>
						<li id="profile_menu" ><a href="<%=request.getContextPath()%>/anonymous_profile.jsp"><i class="icon icon-user"></i> <span>Update Profile</span></a></li>
					</ul>
				</li>
				<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do"><i class="icon icon-tasks"></i> <span>商户</span></a></li>	
				<li id="service_menu"><a href="<%=request.getContextPath()%>/browser/service.do"><i class="icon icon-tasks"></i> <span>服务区域</span></a></li>	
				<li id="commodity_menu"><a href="<%=request.getContextPath()%>/browser/commodity.do"><i class="icon icon-tasks"></i> <span>商品</span></a></li>
				<li id="picture_menu"><a href="<%=request.getContextPath()%>/browser/picture.do"><i class="icon icon-tasks"></i> <span>外景</span></a></li>
				<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
			</ul>
		</div>
		-->
		<div id="content">
			<div id="content-header">
				<h1>编辑菜单</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/menu.do" class="tip-bottom">菜单</a>
				<a href="#" class="current">编辑菜单</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>编辑菜单</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">一级菜单</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="menu" id="menu" value="易民生活" readonly/>
									</div>
								</div>
								</form>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">二级菜单:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="twomenu" id="twomenu" value="<%=bigclass.getName()%>"/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">图片名称:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="fileName" id="fileName" value=""/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">图片路径:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="filePath" id="filePath" value="<%=bigclass.getPath()%>" readonly/>*文件夹名称
										</div>
									</div>
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo();">Save</button>
									</div>
									<input id="bid" name="bid" type="hidden" value="<%=bigclass.getBid()%>">
								</form>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload3" id="doupload3" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">三级菜单:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="threemenu" id="threemenu" value="<%=smallclass.getName()%>"/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">图片名称:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="fileName2" id="fileName2" value=""/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">图片路径:</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="filePath2" id="filePath2" value="<%=smallclass.getPath()%>" readonly/>*文件夹名称
										</div>
									</div>
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile2()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo2();">Save</button>
									</div>
									<input id="sid" name="sid" type="hidden" value="<%=smallclass.getSid()%>">
								</form>
							</div>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#menu_menu"));
		});
	</script>
</html>