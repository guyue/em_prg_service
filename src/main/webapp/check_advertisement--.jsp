<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=GBK"%>

<html>
<% 
	String ctx = (String) request.getContextPath();
	
	List<Advertisement> ad = (List<Advertisement>)session.getAttribute("advers");
	User user = (User) session.getAttribute("user");
	
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>审核广告</title>
		<script type="text/javascript">
			function setInfo(){
				var select1 = document.getElementById("sel1");
				document.getElementById("typeid").value=select1.options[select1.selectedIndex].value;
				
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/updatead.do';
				form.target = "_self";
				form.submit();
			}
		</script>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<!--
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 首页</a>
			<ul id="leftMenu">
				<li id="home_menu" class="active"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-home"></i> <span>首页</span></a></li>
				<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-th-large"></i> <span>广告</span></a></li>
				<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单</span></a></li>
				<li id="settings_menu" onclick="setActiveClass(this)" class="submenu" style="display:none"><a href="#"><i class="icon icon-cog"></i> <span>设置</span> <span class="label">2</span></a>
					<ul>
						<li id="settings_menu" ><a href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span>修改密码</span></a></li>
						<li id="profile_menu" ><a href="<%=request.getContextPath()%>/anonymous_profile.jsp"><i class="icon icon-user"></i> <span>Update Profile</span></a></li>
					</ul>
				</li>
				<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do"><i class="icon icon-tasks"></i> <span>商户</span></a></li>	
				<li id="service_menu"><a href="<%=request.getContextPath()%>/browser/service.do"><i class="icon icon-tasks"></i> <span>服务区域</span></a></li>	
				<li id="commodity_menu"><a href="<%=request.getContextPath()%>/browser/commodity.do"><i class="icon icon-tasks"></i> <span>商品</span></a></li>
				<li id="picture_menu"><a href="<%=request.getContextPath()%>/browser/picture.do"><i class="icon icon-tasks"></i> <span>外景</span></a></li>
				<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
			</ul>
		</div>
		-->
		<div id="content">
			<div id="content-header">
				<h1>广 告</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">广 告</a>
			</div>
			<div align="right">	
				<input type="hidden" value="" id="price">
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>广告列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>广告类型</th>
										<th>商户名称</th>
										<th>图片</th>
									<!--	<th>路径</th>	-->
										<th>状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									<%if(ad != null){
									for (int i = 0;i < ad.size();i ++){
										CompangDao compangDao = (CompangDao)SpringUtils.getBean(CompangDao.class);
										Compang compang = compangDao.findCompangByCid(ad.get(i).getTypeId());
									%>
									<tr>
										<td>商户</td>
										<td><%=compang.getName()%></td>
										<td><%=ad.get(i).getImage()%></td>
									<!--	<td><%=ad.get(i).getPath()%></td>	-->
										<%if(ad.get(i).getCheckstatus() == 1){%>
										<td>审核中</td>
										<%}else{%>
										<td>审核通过</td>
										<%}%>
										<td class="taskOptions">
											<a href="<%=request.getContextPath()%>/browser/delavert.do?aid=<%=ad.get(i).getAid()%>" onclick="" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
											<a href="<%=request.getContextPath()%>/browser/checkadver.do?aid=<%=ad.get(i).getAid()%>&checkstatus=0" onclick="" class="tip-top" data-original-title="通过"><i class="icon-ok-sign"></i></a>
											<a href="<%=request.getContextPath()%>/browser/checkadver.do?aid=<%=ad.get(i).getAid()%>&checkstatus=1" onclick="" class="tip-top" data-original-title="未通过"><i class="icon-remove-sign"></i></a>
										</td>
									</tr>
									<%}
									}%>
								</tbody>
								</table>
						</div>
					</div>					
				</div>
			</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#checkad_menu"));
		});
	</script>
</html>