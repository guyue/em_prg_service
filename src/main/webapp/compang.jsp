<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	List<Compang> compang = (List<Compang>)session.getAttribute("compang");
	
	User user = (User) session.getAttribute("user");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		area = "全市";
	}else{
		area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
	
	int allnum = (Integer)session.getAttribute("totalcompang");
	int showpage = 0;
	if((allnum % 10) == 0){
		showpage = allnum / 10;
	}else{
		showpage = (allnum / 10) + 1;
	}
	String showpage_num = (String)session.getAttribute("page");
	//session.setAttribute("page",null);
	String enterbtn_num = (String)session.getAttribute("enterbtnnum");
	//session.setAttribute("enterbtnnum",null);
%>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>商户列表</title>
	</head>
	<style type="text/css">	
	.widget-box {
		background: none repeat scroll 0 0 #f9f9f9;
		border-left: 1px solid #cdcdcd;
		border-right: 1px solid #cdcdcd;
		border-top: 1px solid #cdcdcd;
		clear: both;
		margin-bottom: 16px;
		margin-top: 0px;
		position: relative;
	}
	#header .tile {
		height: 61px;
		left: 215px;
		line-height: 600px;
		overflow: hidden;
		position: relative;
		top: -20px;
		width: 391px;
	}
	</style>
	<script type="text/javascript">
		function showreview(name,cid){
			document.getElementById("cid").value = cid;
			document.getElementById("compangname").value = name;
			document.getElementById("review_compang").style.display="";
			document.getElementById("review_compang2").style.display="";
		}
		function closereview(){
			document.getElementById("review_compang").style.display="none";
			document.getElementById("review_compang2").style.display="none";
		}
		
		function setInfo(){
			var checkid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			document.getElementById("checkstatus").value = checkid;
			
			if(document.getElementById("reason_box").style.display == ""){
				var str = document.getElementById("reason").value;
				if(str.length == 0){
					alert("请添加不通过的原因！");
					return false;
				}
			}
			var form = document.forms['doupload'];
			form.action = '<%=request.getContextPath() %>/browser/compangstatus.do';
			form.target = "_self";
			form.submit();
		}
		
		function checkcompangstatus(){
			if(document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value == "2"){
				document.getElementById("reason_box").style.display = "";
			}else{
				document.getElementById("reason_box").style.display = "none";
			}
		}
		
		function checkdeletecompang(val,cid,name){
			var r=confirm("你确定需要删除" + name);
			if (r==true){
				val.href="<%=request.getContextPath()%>/browser/delcompang.do?cid=" + cid;
			}else{
				val.href="#";
			}
		}
	</script>
	<body onload="showinpage();">
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4>			
			</div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>商 户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">商户列表</a>
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>商户列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table width="100%" border="1" bordercolor="#E5E5E5" id="emdata">
									<thead>
										<tr bgcolor="#EDEDED" height="30px">
											<th>编号</th>
											<th>公司名字</th>
											<th>类别</th>
											<th>子类</th>
											<th>发布日期</th>
											<th>期限(月)</th>
											<th>到期时间</th>
											<th>区县</th>
											<th>外景</th>
											<th>产品</th>
											<th>认证</th>
											<th>状态</th>
											<th>是否推荐</th>
											<th>操作</th>
											<th>注</th>
										</tr>
									</thead>
									<tbody>
										<%
										boolean showcompang = false;
										if(compang != null){
											for(int i = 0;i < compang.size();i ++){
												if(compang.get(i).getCheckStatus() != 3){
												showcompang = true;
										%>
											<tr class="gradeX" height="40px">
												<td><%=String.valueOf(compang.get(i).getNumber())%></td>
												<td><%=compang.get(i).getName()%></td>
												<td><%=compang.get(i).getSmallClass().getBigclass().getName()%></td>
												<td><%=compang.get(i).getSmallClass().getName()%></td>
												<%
													SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
													Calendar start_time = Calendar.getInstance();
													Calendar end_time = Calendar.getInstance();
													start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
													end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
													int month = 0;
													int start_year = start_time.get(Calendar.YEAR);
													int start_month = start_time.get(Calendar.MONTH);
													int end_year = end_time.get(Calendar.YEAR);
													int end_month = end_time.get(Calendar.MONTH);
													if(end_year > start_year){
														month = end_month + (end_year - start_year) * 12 - start_month;
													}else{
														if(end_month > start_month){
															month = end_month - start_month;
														}
													}
													PictureDao pictureDao = (PictureDao)SpringUtils.getBean(PictureDao.class);
													List<Picture> picture = pictureDao.findPictureByCid(compang.get(i).getCid());
													CommodityDao commodityDao = (CommodityDao)SpringUtils.getBean(CommodityDao.class);
													List<Commodity> commodity = commodityDao.findCommoditysByCid(compang.get(i).getCid());
												%>
												<td><%=formatter.format(compang.get(i).getUpdateDatetime())%></td>
												<td><%=month%></td>
												<td><%=formatter.format(compang.get(i).getExpirationtime())%></td>
												<%
													int pid = Integer.parseInt(compang.get(i).getArea());
													Place pl = placeDao.findPlaceByPid(pid);
												%>
											<!--	<td><%=pl.getCity()%></td>	-->
												<td><%=pl.getArea()%></td>
												<td><%=picture.size()%></td>
												<td><%=commodity.size()%></td>
												<%if(compang.get(i).getAuthentication() == 0){%>
												<td><font color="red">已认证</font></td>
												<%}else{%>
												<td>未认证</td>
												<%}%>
												<%
												if(compang.get(i).getCheckStatus() == 0){
													if(month <= 1){
												%>
												<td>快到期</td>
												<%
													}else{
												%>
												<td>发布中</td>
												<%
													}
												}
												if(compang.get(i).getCheckStatus() == 1){
												%>
												<td><font color="blue">待审核</font></td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 2){
												%>
												<td>不通过</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 3){
												%>
												<td>已关闭</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 4){
												%>
												<td>已过期</td>
												<%
												}
												String recomendStr = "未推荐";
												if (compang.get(i).getShowfirst() == (byte)0) {
													recomendStr = "已推荐";
												}
												%>
												<td><%=recomendStr %></td>
												<td class="taskOptions">
													<a href="<%=request.getContextPath()%>/browser/viewcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="预览"><i class="icon-eye-open"></i></a>
													<%if(user.getRole().getRid() == 1){%>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
													<a href="<%=request.getContextPath()%>/browser/reviewcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>
													<a href="<%=request.getContextPath()%>/browser/compangstatus.do?cid=<%=compang.get(i).getCid()%>&checkstatus=3" class="tip-top" data-original-title="删除"><i class="icon-trash"></i></a>
													<%}%>
													<%if(user.getRole().getRid() == 2){%>
													<a href="<%=request.getContextPath()%>/browser/reviewcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>
													<a href="<%=request.getContextPath()%>/browser/compangstatus.do?cid=<%=compang.get(i).getCid()%>&checkstatus=3" class="tip-top" data-original-title="删除"><i class="icon-trash"></i></a>
													<%}%>
													<%if(user.getRole().getRid() == 3){%>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
													<a href="<%=request.getContextPath()%>/browser/compangstatus.do?cid=<%=compang.get(i).getCid()%>&checkstatus=3" class="tip-top" data-original-title="删除"><i class="icon-trash"></i></a>
													<%}%>
													<%if(user.getRole().getRid() == 4){%>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
													<a href="<%=request.getContextPath()%>/browser/compangstatus.do?cid=<%=compang.get(i).getCid()%>&checkstatus=3" class="tip-top" data-original-title="删除"><i class="icon-trash"></i></a>
													<%}%>
													<%if(user.getRole().getRid() == 5){%>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
													<%}%>
												</td>
												<td></td>
											</tr>
										<%
												}
											}
											if(!showcompang){
										%>
											<tr>
												<td colspan="14">无商户信息</td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
											</tr>
										<%
											}
										}
										%>
									</tbody>
								</table>
								<div align="center" id="pagenum" class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix" style="height: 60px">
									<input type="hidden" value="<%=allnum%>" id="compang_num" name="compang_num">
									<div id="DataTables_Table_0_filter" class="dataTables_filter" style="position: relative;">
										<label>
										搜 索:
										<input type="text" id="DataTables_Table_0" style="height: 18px" onKeyUp="fun();"/>
										</label>
									</div>
									<div id="DataTables_Table_0_paginate" class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers" style="text-align:center;">
									<%if(showpage >= 1){%>
									<a id="DataTables_Table_0_first" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">首页</a>
									<a id="DataTables_Table_0_previous" class="previous fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">上一页</a>
									
									<span>
									<a class="fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" id="DataTables_Table_0_1" onclick="test(this)">1</a>
									<%if(showpage >= 2){%>
									<a class="fg-button ui-button ui-state-default" tabindex="0" id="DataTables_Table_0_2" onclick="test(this)">2</a>
									<%}%>
									<%if(showpage >= 3){%>
									<a class="fg-button ui-button ui-state-default" tabindex="0" id="DataTables_Table_0_3" onclick="test(this)">3</a>
									<%}%>
									<%if(showpage >= 4){%>
									<a class="fg-button ui-button ui-state-default" tabindex="0" id="DataTables_Table_0_4" onclick="test(this)">4</a>
									<%}%>
									<%if(showpage >= 5){%>
									<a class="fg-button ui-button ui-state-default" tabindex="0" id="DataTables_Table_0_5" onclick="test(this)">5</a>
									<%}%>
									</span>
									<%if(showpage == 1){%>
									<a id="DataTables_Table_0_next" class="next fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">下一页</a>
									<a id="DataTables_Table_0_last" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">末页</a>
									<%}else{%>
									<a id="DataTables_Table_0_next" class="next fg-button ui-button ui-state-default" tabindex="0" onclick="test(this)">下一页</a>
									<a id="DataTables_Table_0_last" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default" tabindex="0" onclick="test(this)">末页</a>
									<%}
									}else{%>
									<a id="DataTables_Table_0_first" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">首页</a>
									<a id="DataTables_Table_0_previous" class="previous fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">上一页</a>	
									<a id="DataTables_Table_0_next" class="next fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">下一页</a>
									<a id="DataTables_Table_0_last" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default ui-state-disabled" tabindex="0" onclick="test(this)">末页</a>
									<%}%>
									</div>
								</div>
							</div>
						</div>		
						<div align="center">
							<%
								if(user.getRole().getRid() != 2){
									if(user.getRole().getRid() == 5){
										if(compang.size() == 0){
							%>
							<a id="newad" name="newad" href="<%=request.getContextPath()%>/browser/newcompang.do" />新建商户</a>
							<%
										}
									}else{
							%>
							<a id="newad" name="newad" href="<%=request.getContextPath()%>/browser/newcompang.do" />新建商户</a>	
							<%
									}
								}
							%>
						</div> 
						<div class="widget-title" id="review_compang2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>商户审核</h5>
						</div>
						<div class="widget-content nopadding" id="review_compang" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">商户名称:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="compangname" id="compangname" value="" readonly/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">状态:</label>
									<div class="controls">
										<select id="sel1" onchange="checkcompangstatus()">
											<option value="0">发布中</option>
											<option value="2">不通过</option>
											<option value="3">关闭</option>
											<option value="4">已过期</option>
										</select>
									</div>
								</div>
								<div class="control-group" id="reason_box" style="display:none">
									<label class="control-label">原因</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="reason" id="reason" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closereview();">返回</button>
								</div>
								<input id="cid" name="cid" type="hidden">
								<input id="checkstatus" name="checkstatus" type="hidden">
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});		
	</script>
<script type="text/javascript">
function showinpage(){
	var show_page = 0;
	var totalpage = <%=showpage%>;
	var enter_btn = <%=enterbtn_num%>;
	<%
		if(null != showpage_num && showpage_num.length() > 0){
	%>
		show_page = <%=Integer.parseInt(showpage_num)%>;
	<%
		}
	%>
	if(totalpage > 5){
		if(enter_btn == 1){
			if(show_page > 2){
				if((show_page - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page - 2);
					$("#DataTables_Table_0_2").html(show_page - 1);
					$("#DataTables_Table_0_3").html(show_page);
					$("#DataTables_Table_0_4").html(show_page + 1);
					$("#DataTables_Table_0_5").html(show_page + 2);
				}
			}else{
				if(show_page == 1){
					$("#DataTables_Table_0_1").html(show_page);
					$("#DataTables_Table_0_2").html(show_page + 1);
					$("#DataTables_Table_0_3").html(show_page + 2);
					$("#DataTables_Table_0_4").html(show_page + 3);
					$("#DataTables_Table_0_5").html(show_page + 4);
				}else{
					$("#DataTables_Table_0_1").html(show_page - 1);
					$("#DataTables_Table_0_2").html(show_page);
					$("#DataTables_Table_0_3").html(show_page + 1);
					$("#DataTables_Table_0_4").html(show_page + 2);
					$("#DataTables_Table_0_5").html(show_page + 3);
				}
			}
		}
		if(enter_btn == 2){
			if(show_page > 2){
				if((show_page - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page - 2);
					$("#DataTables_Table_0_2").html(show_page - 1);
					$("#DataTables_Table_0_3").html(show_page);
					$("#DataTables_Table_0_4").html(show_page + 1);
					$("#DataTables_Table_0_5").html(show_page + 2);
				}
			}
		}
		if(enter_btn == 4){
			if((show_page + 2) <= totalpage){
				$("#DataTables_Table_0_1").html(show_page - 2);
				$("#DataTables_Table_0_2").html(show_page - 1);
				$("#DataTables_Table_0_3").html(show_page);
				$("#DataTables_Table_0_4").html(show_page + 1);
				$("#DataTables_Table_0_5").html(show_page + 2);
			}else{
				$("#DataTables_Table_0_1").html(show_page - 3);
				$("#DataTables_Table_0_2").html(show_page - 2);
				$("#DataTables_Table_0_3").html(show_page - 1);
				$("#DataTables_Table_0_4").html(show_page);
				$("#DataTables_Table_0_5").html(show_page + 1);
			}
		}
		if(enter_btn == 5){
			if((show_page + 2) <= totalpage){
				$("#DataTables_Table_0_1").html(show_page - 2);
				$("#DataTables_Table_0_2").html(show_page - 1);
				$("#DataTables_Table_0_3").html(show_page);
				$("#DataTables_Table_0_4").html(show_page + 1);
				$("#DataTables_Table_0_5").html(show_page + 2);
			}else{
				if(show_page == totalpage){
					$("#DataTables_Table_0_1").html(show_page - 4);
					$("#DataTables_Table_0_2").html(show_page - 3);
					$("#DataTables_Table_0_3").html(show_page - 2);
					$("#DataTables_Table_0_4").html(show_page - 1);
					$("#DataTables_Table_0_5").html(show_page);
				}else{
					$("#DataTables_Table_0_1").html(show_page - 3);
					$("#DataTables_Table_0_2").html(show_page - 2);
					$("#DataTables_Table_0_3").html(show_page - 1);
					$("#DataTables_Table_0_4").html(show_page);
					$("#DataTables_Table_0_5").html(show_page + 1);
				}
			}
		}
		if(enter_btn == 3){
			if((show_page - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page -2);
					$("#DataTables_Table_0_2").html(show_page - 1);
					$("#DataTables_Table_0_3").html(show_page);
					$("#DataTables_Table_0_4").html(show_page + 1);
					$("#DataTables_Table_0_5").html(show_page + 2);
				}
		}
	}
	if(null != enter_btn){
		if(enter_btn >= 4){
			if(show_page == totalpage){
				enter_btn = 5;
			}else{
				if(show_page + 1 == totalpage){
					enter_btn = 4;
				}else{
					enter_btn = 3;
				}
			}
		}else{
			if(show_page >= 3){
				enter_btn = 3;
			}
		}
	}
	var a;
	if(enter_btn == 2){
		if((show_page == totalpage) || (show_page >= totalpage)){
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			if(enter_btn > totalpage){
				$("#DataTables_Table_0_" + totalpage).addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_2").addClass("ui-state-disabled");
			}
			$("#DataTables_Table_0_next").addClass("ui-state-disabled");
			$("#DataTables_Table_0_last").addClass("ui-state-disabled");
		}else{
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_2").addClass("ui-state-disabled");
		}
	}
	if(enter_btn == 3){
		if((show_page == totalpage) || (show_page >= totalpage)){
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			if(enter_btn > totalpage){
				$("#DataTables_Table_0_" + totalpage).addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_3").addClass("ui-state-disabled");
			}
			$("#DataTables_Table_0_next").addClass("ui-state-disabled");
			$("#DataTables_Table_0_last").addClass("ui-state-disabled");
		}else{
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_3").addClass("ui-state-disabled");
		}
	}
	if(enter_btn == 4){
		if((show_page == totalpage) || (show_page >= totalpage)){
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			if(enter_btn > totalpage){
				$("#DataTables_Table_0_" + totalpage).addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_4").addClass("ui-state-disabled");
			}
			$("#DataTables_Table_0_next").addClass("ui-state-disabled");
			$("#DataTables_Table_0_last").addClass("ui-state-disabled");
		}else{
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_4").addClass("ui-state-disabled");
		}
	}
	if(enter_btn == 5){
		if((show_page == totalpage) || (show_page >= totalpage)){
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			if(enter_btn > totalpage){
				$("#DataTables_Table_0_" + totalpage).addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_5").addClass("ui-state-disabled");
			}
			$("#DataTables_Table_0_next").addClass("ui-state-disabled");
			$("#DataTables_Table_0_last").addClass("ui-state-disabled");
		}else{
			$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
			$("#DataTables_Table_0_5").addClass("ui-state-disabled");
		}
	}	
}

var cleartime = '';
function fun(){
  clearTimeout(cleartime);
  cleartime = setTimeout("searchData()",500);
}
function searchData(){
	//alert($("#DataTables_Table_0").val());
	$.ajax({
		url:"<%=request.getContextPath()%>/browser/compangbypage.do?search=" + $("#DataTables_Table_0").val(),
		type:"post",
		dataType: "JSON",
		async:false,
		timeout: 3000,
		success: function(ret) {
			var formsdata = ret.html;
			formsdata = "<thead><tr bgcolor=\"#EDEDED\" height=\"30px\"><th>编号</th><th>公司名字</th><th>类别</th><th>子类</th><th>发布日期</th><th>期限</th><th>到期时间</th><th>区县</th><th>外景</th><th>产品</th><th>认证</th><th>状态</th><th>操作</th><th>注</th></tr></thead>" + formsdata;
			formsdata = formsdata.replace(new RegExp("weirenzheng", 'g'),"未认证");
			formsdata = formsdata.replace(new RegExp("yirenzheng", 'g'),"已认证");
			formsdata = formsdata.replace(new RegExp("fabuzhong", 'g'),"发布中");
			formsdata = formsdata.replace(new RegExp("kuaidaoqi", 'g'),"快到期");
			formsdata = formsdata.replace(new RegExp("daishenhe", 'g'),"待审核");
			formsdata = formsdata.replace(new RegExp("butongguo", 'g'),"不通过");
			formsdata = formsdata.replace(new RegExp("yulan", 'g'),"预览");
			formsdata = formsdata.replace(new RegExp("xiugai", 'g'),"修改");
			formsdata = formsdata.replace(new RegExp("shenhe", 'g'),"审核");
			formsdata = formsdata.replace(new RegExp("shanchu", 'g'),"删除");
			formsdata = formsdata.replace(new RegExp("No data available in table", 'g'),"无商户信息");
			
			if(formsdata.length > 0){				
				$("#emdata").html(formsdata);
				var showpage = ret.showpage;
				var a;
				var showlast = false;
				var hasmore = false;
				if(showpage == 1){
					$("#DataTables_Table_0_first").addClass("ui-state-disabled");
					$("#DataTables_Table_0_previous").addClass("ui-state-disabled");
					
					a = document.getElementById("DataTables_Table_0_2");
					if(a != null){
						$("#DataTables_Table_0_2").show();
						hasmore = true;
						$("#DataTables_Table_0_1").html(Number($("#DataTables_Table_0_2").html()) - 1);
						if($("#DataTables_Table_0_2").hasClass("ui-state-disabled")){
							$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
							showlast = true;
						}
					}
					a = document.getElementById("DataTables_Table_0_3");
					if(a != null){
						$("#DataTables_Table_0_3").show();
						hasmore = true;
						if($("#DataTables_Table_0_3").hasClass("ui-state-disabled")){
							$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
							showlast = true;
						}
					}
					a = document.getElementById("DataTables_Table_0_4");
					if(a != null){
						$("#DataTables_Table_0_4").show();
						hasmore = true;
						if($("#DataTables_Table_0_4").hasClass("ui-state-disabled")){
							$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
							showlast = true;
						}
					}
					a = document.getElementById("DataTables_Table_0_5");
					if(a != null){
						$("#DataTables_Table_0_5").show();
						hasmore = true;
						if($("#DataTables_Table_0_4").hasClass("ui-state-disabled")){
							$("#DataTables_Table_0_1").removeClass("ui-state-disabled");
							showlast = true;
						}
					}
					if(hasmore){
						if(showlast){
							$("#DataTables_Table_0_next").addClass("ui-state-disabled");
							$("#DataTables_Table_0_last").addClass("ui-state-disabled");
						}else{
							$("#DataTables_Table_0_next").removeClass("ui-state-disabled");
							$("#DataTables_Table_0_last").removeClass("ui-state-disabled");
						}
					}
				}else{
					$("#DataTables_Table_0_first").addClass("ui-state-disabled");
					$("#DataTables_Table_0_previous").addClass("ui-state-disabled");
					$("#DataTables_Table_0_1").html(1);
					$("#DataTables_Table_0_1").addClass("ui-state-disabled");
					a = document.getElementById("DataTables_Table_0_2");
					if(a != null){
						$("#DataTables_Table_0_2").hide();
					}
					a = document.getElementById("DataTables_Table_0_3");
					if(a != null){
						$("#DataTables_Table_0_3").hide();
					}
					a = document.getElementById("DataTables_Table_0_4");
					if(a != null){
						$("#DataTables_Table_0_4").hide();
					}
					a = document.getElementById("DataTables_Table_0_5");
					if(a != null){
						$("#DataTables_Table_0_5").hide();
					}
					$("#DataTables_Table_0_next").addClass("ui-state-disabled");
					$("#DataTables_Table_0_last").addClass("ui-state-disabled");
				}
			}
		},
		error: function(XMLRequest, textInfo) {
			if (textInfo != null) {
				alert(textInfo);
			}
		}
	});
}
function getcompangs(num,page,btn){
	$.ajax({
		url:"<%=request.getContextPath()%>/browser/compangbypage.do?pagenumber="+num + "&page=" + page + "&enterbtnnum=" + btn,
		type:"post",
		dataType: "JSON",
		async:false,
		timeout: 3000,
		success: function(ret) {
			var formsdata = ret.html;
			formsdata = "<thead><tr bgcolor=\"#EDEDED\" height=\"30px\"><th>编号</th><th>公司名字</th><th>类别</th><th>子类</th><th>发布日期</th><th>期限</th><th>到期时间</th><th>区县</th><th>外景</th><th>产品</th><th>认证</th><th>状态</th><th>操作</th><th>注</th></tr></thead>" + formsdata;
			formsdata = formsdata.replace(new RegExp("weirenzheng", 'g'),"未认证");
			formsdata = formsdata.replace(new RegExp("yirenzheng", 'g'),"已认证");
			formsdata = formsdata.replace(new RegExp("fabuzhong", 'g'),"发布中");
			formsdata = formsdata.replace(new RegExp("kuaidaoqi", 'g'),"快到期");
			formsdata = formsdata.replace(new RegExp("daishenhe", 'g'),"待审核");
			formsdata = formsdata.replace(new RegExp("butongguo", 'g'),"不通过");
			formsdata = formsdata.replace(new RegExp("yulan", 'g'),"预览");
			formsdata = formsdata.replace(new RegExp("xiugai", 'g'),"修改");
			formsdata = formsdata.replace(new RegExp("shenhe", 'g'),"审核");
			formsdata = formsdata.replace(new RegExp("shanchu", 'g'),"删除");
			formsdata = formsdata.replace(new RegExp("No data available in table", 'g'),"无商户信息");
			
			if(formsdata.length > 0){
				$("#emdata").html(formsdata);
			}
		},
		error: function(XMLRequest, textInfo) {
			if (textInfo != null) {
				alert(textInfo);
			}
		}
	});
}

function test(num){
	var em_num = Number($("#compang_num").val());//eforms total
	var islast = 0;//pages total
	if((em_num % 10) == 0){
		islast = em_num / 10;
	}else{
		islast = Math.floor(em_num / 10) + 1;
	}
	
	var t = num.id;//page id
	var num1 = t.length;
	var num2 = t.lastIndexOf("_") + 1;
	var id_num = t.substring(num2, num1);//String id num
	
	var class_length = $(".ui-state-disabled").length;
	var show_button_id;
	for(var i = 0;i < class_length;i ++){
		show_button_id = $(".ui-state-disabled")[i].id;
		if(t == show_button_id){
			return false;
		}
	}
	var num3 = show_button_id.length;
	var num4 = show_button_id.lastIndexOf("_") + 1;
	var button_id = show_button_id.substring(num4, num3);
	if(button_id == 1){
		$("#DataTables_Table_0_first").removeClass("ui-state-disabled");
		$("#DataTables_Table_0_previous").removeClass("ui-state-disabled");
	}
	if(button_id == "last"){
		$("#DataTables_Table_0_next").removeClass("ui-state-disabled");
		$("#DataTables_Table_0_last").removeClass("ui-state-disabled");
	}
	var int_button_id;
	var int_button_num;
	if(button_id != "last"){
		int_button_id = Number(button_id);
		int_button_num = Number($("#"+show_button_id).html());
	}else{
		show_button_id = $(".ui-state-disabled")[0].id;
		var num5 = show_button_id.length;
		var num6 = show_button_id.lastIndexOf("_") + 1;
		var button_id1 = show_button_id.substring(num6, num5);
		int_button_id = Number(button_id1);
		int_button_num = Number($("#"+show_button_id).html());
	}	
	
	var page_num = $(num).html();//String show page num	
	if(id_num == "first"){
		id_num = "1";
		page_num = "1";
	}
	if(id_num == "previous"){
		if(int_button_num >= 4) {
			id_num = "4";
		}else{
			if((int_button_num -1) > 0){
				id_num = int_button_num - 1;
			}
		}
		if((int_button_num -1) > 0){
			page_num = int_button_num - 1;
		}
	}
	if(id_num == "next"){
		if(int_button_id >= 3){
			id_num = "4";
		}else{
			if((int_button_id + 1) < 4){
				id_num = int_button_id + 1;
			}
		}
		if((int_button_num + 1) <= islast){
			page_num = int_button_num + 1;
		}
	}
	if(id_num == "last"){
		if(islast >= 2){
			id_num = "2";
		}
		if(islast >= 3){
			id_num = "3";
		}
		if(islast >= 4){
			id_num = "4";
		}
		if(islast >= 5){
			id_num = "5";
		}
		page_num = islast;
	}
	var show_page_num = Number(page_num);//int show page num
	var int_id_num = Number(id_num);//int id num	
	
	if(int_id_num >= 4 ){
		$("#DataTables_Table_0_" + int_button_id).removeClass("ui-state-disabled");
		if(show_page_num == islast){
			if(document.getElementById("DataTables_Table_0_5") != null){
				$("#DataTables_Table_0_5").addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_4").addClass("ui-state-disabled");
			}
		}else{
			if((show_page_num + 1) == islast){
				$("#DataTables_Table_0_4").addClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_3").addClass("ui-state-disabled");
			}
		}
	}else{
		if(show_page_num >= 3){
			$("#DataTables_Table_0_" + int_button_id).removeClass("ui-state-disabled");
			$("#DataTables_Table_0_3").addClass("ui-state-disabled");
		}else{
			if(show_page_num > int_id_num){
				$("#DataTables_Table_0_" + show_page_num).addClass("ui-state-disabled");
				$("#DataTables_Table_0_" + int_button_id).removeClass("ui-state-disabled");
			}else{
				$("#DataTables_Table_0_" + int_id_num).addClass("ui-state-disabled");
				$("#DataTables_Table_0_" + int_button_id).removeClass("ui-state-disabled");
			}
		}
	}
	if(int_id_num == 1){
		if(show_page_num == 1){
			$("#DataTables_Table_0_first").addClass("ui-state-disabled");
			$("#DataTables_Table_0_previous").addClass("ui-state-disabled");
		}
	}
	if(show_page_num == islast){
		$("#DataTables_Table_0_next").addClass("ui-state-disabled");
		$("#DataTables_Table_0_last").addClass("ui-state-disabled");
	}
	if(islast > 5){
		if(int_id_num == 1){
			if(show_page_num > 2){
				if((show_page_num - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page_num -2);
					$("#DataTables_Table_0_2").html(show_page_num - 1);
					$("#DataTables_Table_0_3").html(show_page_num);
					$("#DataTables_Table_0_4").html(show_page_num + 1);
					$("#DataTables_Table_0_5").html(show_page_num + 2);
				}
			}else{
				if(show_page_num == 1){
					$("#DataTables_Table_0_1").html(show_page_num);
					$("#DataTables_Table_0_2").html(show_page_num + 1);
					$("#DataTables_Table_0_3").html(show_page_num + 2);
					$("#DataTables_Table_0_4").html(show_page_num + 3);
					$("#DataTables_Table_0_5").html(show_page_num + 4);
				}else{
					$("#DataTables_Table_0_1").html(show_page_num - 1);
					$("#DataTables_Table_0_2").html(show_page_num);
					$("#DataTables_Table_0_3").html(show_page_num + 1);
					$("#DataTables_Table_0_4").html(show_page_num + 2);
					$("#DataTables_Table_0_5").html(show_page_num + 3);
				}
			}
		}
		if(int_id_num == 2){
			if(show_page_num > 2){
				if((show_page_num - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page_num -2);
					$("#DataTables_Table_0_2").html(show_page_num - 1);
					$("#DataTables_Table_0_3").html(show_page_num);
					$("#DataTables_Table_0_4").html(show_page_num + 1);
					$("#DataTables_Table_0_5").html(show_page_num + 2);
				}
			}
		}
		if(int_id_num == 4){
			if((show_page_num + 2) <= islast){
				$("#DataTables_Table_0_1").html(show_page_num -2);
				$("#DataTables_Table_0_2").html(show_page_num - 1);
				$("#DataTables_Table_0_3").html(show_page_num);
				$("#DataTables_Table_0_4").html(show_page_num + 1);
				$("#DataTables_Table_0_5").html(show_page_num + 2);
			}
		}
		if(int_id_num == 5){
			if((show_page_num + 2) <= islast){
				$("#DataTables_Table_0_1").html(show_page_num -2);
				$("#DataTables_Table_0_2").html(show_page_num - 1);
				$("#DataTables_Table_0_3").html(show_page_num);
				$("#DataTables_Table_0_4").html(show_page_num + 1);
				$("#DataTables_Table_0_5").html(show_page_num + 2);
			}else{
				if(show_page_num == islast){
					$("#DataTables_Table_0_1").html(show_page_num - 4);
					$("#DataTables_Table_0_2").html(show_page_num - 3);
					$("#DataTables_Table_0_3").html(show_page_num - 2);
					$("#DataTables_Table_0_4").html(show_page_num - 1);
					$("#DataTables_Table_0_5").html(show_page_num);
				}else{
					$("#DataTables_Table_0_1").html(show_page_num - 3);
					$("#DataTables_Table_0_2").html(show_page_num - 2);
					$("#DataTables_Table_0_3").html(show_page_num - 1);
					$("#DataTables_Table_0_4").html(show_page_num);
					$("#DataTables_Table_0_5").html(show_page_num + 1);
				}
			}
		}
		if(int_id_num == 3){
			if((show_page_num - 2) > 0){
					$("#DataTables_Table_0_1").html(show_page_num -2);
					$("#DataTables_Table_0_2").html(show_page_num - 1);
					$("#DataTables_Table_0_3").html(show_page_num);
					$("#DataTables_Table_0_4").html(show_page_num + 1);
					$("#DataTables_Table_0_5").html(show_page_num + 2);
				}
		}
	}
	getcompangs((show_page_num-1) * 10,show_page_num,int_id_num);
}
</script>
</html>