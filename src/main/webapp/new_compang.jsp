<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	User user = (User) session.getAttribute("user");
	List<SmallClass> small = (List<SmallClass>)request.getAttribute("smallclass");
	List<BigClass> big = (List<BigClass>)request.getAttribute("bigclass");
	int maxnum = (Integer)request.getAttribute("maxnum");
	maxnum = maxnum + 1;
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>

<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<script type="text/javascript">
		function randomString() {  
			return '' + new Date().getTime();  
		}
		function setInfo(){
			var select1 = document.getElementById("sel2");
			document.getElementById("setsid").value=select1.options[select1.selectedIndex].value;
			if(document.getElementById("sel3") != null){
				document.getElementById("area").value = document.getElementById("sel3").options[document.getElementById("sel3").selectedIndex].value;
			}
			var long_lat = $("#long_lat").val();
			
			if(long_lat.length <= 0){
				alert("请输入经纬度！");
			}else{
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/updatecompang.do';
				form.target = "_self";
				form.submit();
			}
		}
					
		function uploadFile(){
			var str = randomString();
			document.getElementById("fileName").value = document.getElementById("myFile").value;
			
			var filename = document.getElementById("fileName").value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.getElementById("fileName").value = newname;
				
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		
		function changeSmallList(){
			var bigid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			$.ajax({
				url:"<%=request.getContextPath()%>/browser/changesmall.do?bid=" + bigid,
				type:"post",
				dataType: "JSON",
				async:false,
				timeout: 3000,
				success: function(ret) {
					if(ret.length > 0){
						document.getElementById("sel2").style.display = "";
						document.getElementById("sel2").options.length = 0;
						for(var i = 0; i < ret.length; i ++){
							$("#sel2").append(ret[i]);
						}
					}else{
						document.getElementById("sel2").style.display = "none";
					}
				},
				error: function(XMLRequest, textInfo) {
					if (textInfo != null) {
						alert(textInfo);
					}
				}
			});
		}
		function closenew(){
			document.getElementById("back_to_list").click();
			//history.back();
		}
		function findbaiduapi(){
			document.getElementById("baidu_url").target = "_blank";
			document.getElementById("baidu_url").click();
		}
		</script>
		<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
	</style>
	</head>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/js/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/lang/cn_utf8.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/calendar/calendar-setup.js"></script>
<!--	<script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/js/My97DatePicker/WdatePicker.js"></script>-->
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>	
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>添加商户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/compang.do" class="tip-bottom">商户列表</a>
				<a href="#" class="current">添加商户</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>添加商户</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile()">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所属大类</label>
										<div class="controls">
											<select id="sel1" onchange="changeSmallList()">
												<%for(BigClass bg:big){
												%>
												<option value="<%=bg.getBid()%>"><%=bg.getName()%></option>
												<%
												}%>
											</select>
										</div>
									</div>
									<div class="control-group" id="small_list" style="">
										<label class="control-label">所属子类</label>
										<div class="controls">
											<select id="sel2">
												<%
												if(big.size() > 0){
													for(SmallClass sa:small) {
														if(sa.getBigclass().getBid() == big.get(0).getBid()){
												%>
												<option value="<%=sa.getSid()%>"><%=sa.getName()%></option>
												<%
														}
													}
												}
												%>
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">编号</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="number" id="number" value="<%=maxnum%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">名称</label>
										<div class="controls">
											<%if(user.getRole().getRid() == 5){%>
											<input type="text" style="width:25%;height:25px" name="com_name" id="com_name" value="<%=user.getName()%>" readonly/>
											<%}else{%>
											<input type="text" style="width:25%;height:25px" name="com_name" id="com_name" value=""/>
											<%}%>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">宣传语</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="title" id="title" value=""/>
										</div>
									</div>
									<%
										//String login_place = (String)session.getAttribute("login_place");
										int pid = Integer.parseInt(user.getAddress());
										String city = "";
										String area = "";
										List<Place> area_list = placeDao.findAreaByCity(newarea.getArea());
										
										Place pl = placeDao.findPlaceByPid(pid);
										if(pid == 1){
											city = newarea.getArea();
										}else{
											city = pl.getCity();
										}
										area = pl.getArea();
									%>
									<div class="control-group">
										<label class="control-label">所在城市</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="usercity" id="usercity" value="<%=city%>" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">所在区域</label>
										<div class="controls">
											<%if(user.getRole().getRid() <= 3){%>
											<select id="sel3" onclick="" style="width:100px">
											<%for(int k = 0;k < area_list.size();k ++){%>
												<option value="<%=area_list.get(k).getPid()%>"><%=area_list.get(k).getArea()%></option>
											<%}%>
											</select>
											<%}else{%>
											<input type="text" style="width:25%;height:25px" name="userarea" id="userarea" value="<%=area%>" readonly/>
											<%}%>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">地址</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="address" id="address" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">经纬度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="long_lat" id="long_lat" value=""/>
											<a href="http://api.map.baidu.com/lbsapi/getpoint/index.html" id="baidu_url"></a>
											<input type="button" onclick="findbaiduapi()" value="坐标取值">
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">经度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="longitude" id="longitude" value=""/>
										</div>
									</div>
									<div class="control-group" style="display:none">
										<label class="control-label">纬度</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="latitude" id="latitude" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">拨打次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="teltime" id="teltime" value="0" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">浏览次数</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="looktime" id="looktime" value="0" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">内容</label>
										<div class="controls">
											<textarea name="content_str" id="contentstr" style="width: 243px; height: 78px;"></textarea>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">电话号码</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="tel" id="tel" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">联系人</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="contact" id="contact" value=""/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">到期时间</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="expiration_time" id="expiration_time" value="" readonly/>
										<!--	<input type="text" style="width:25%;height:25px" name="expiration_time" id="expiration_time" onClick="WdatePicker()" readonly/>		-->
											<img src="<%=request.getContextPath()%>/image/cal.gif" id="pdate_cal">
											<script type="text/javascript">
												Calendar.setup({ inputField : "expiration_time", ifFormat : "%Y/%m/%d", showsTime :false, button : "pdate_cal", singleClick : true, step : 1 });
											</script>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">是否推荐:</label>
										<div class="controls">
											<input type="radio" name="showfirst" value="0"> 是
											<input type="radio" name="showfirst" value="1" checked> 否
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
										<button type="button" class="btn btn-primary" onclick="closenew();">退出</button>
										<a href="<%=request.getContextPath()%>/browser/compang.do" id="back_to_list" style="display:none"></a>
									</div>
									<input id="area" name="area" type="hidden" value="<%=user.getAddress()%>">
									<input id="checkstatus" name="checkstatus" type="hidden" value="1">
									<input id="setsid" name="setsid" type="hidden">
									<input id="filePath" name="filePath" type="hidden" value="img/xinjian/">
									<input id="fileName" name="fileName" type="hidden">
								</form>
								<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
								</iframe>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
</html>