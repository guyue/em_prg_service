<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="GBK"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<html>
<% 
	String ctx = (String) request.getContextPath();
	
	List<Advertisement> ad = (List<Advertisement>)session.getAttribute("advers");
	User user = (User) session.getAttribute("user");
	List<Compang> ad_compang = (List<Compang>)request.getAttribute("ad_compang");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
	String ad_type = (String)request.getAttribute("ad_type");
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		
		<title>广告</title>
		<style type="text/css">
			#header .tile {
				height: 61px;
				left: 215px;
				line-height: 600px;
				overflow: hidden;
				position: relative;
				top: -20px;
				width: 391px;
			}
		</style>
		<script type="text/javascript">
		function randomString() {  
			return '' + new Date().getTime();  
		}
		function setInfo(){
			var select1 = document.getElementById("sel1");
			document.getElementById("typeid").value=select1.options[select1.selectedIndex].value;
			
			var form = document.forms['doupload'];
			form.action = '<%=request.getContextPath() %>/browser/updatead.do';
			form.target = "_self";
			form.submit();
		}
					
		function uploadFile(){
			var str = randomString();
			document.getElementById("fileName").value = document.getElementById("myFile").value;
			
			var filename = document.getElementById("fileName").value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.getElementById("fileName").value = newname;
				
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		
		function addnewadvertisement(){
			/*$.ajax({
				url:"<%=request.getContextPath()%>/browser/advernum.do",
				type:"post",
				dataType: "JSON",
				async:false,
				timeout: 3000,
				success: function(ret) {
					if(ret.canadd == 1){
						alert("请先删除一个现有广告后才能再推荐");
					}else{
						document.getElementById("new_advertisement").style.display="";
						document.getElementById("new_advertisement2").style.display="";
					}
				},
				error: function(XMLRequest, textInfo) {
					if (textInfo != null) {
						alert(textInfo);
					}
				}
			});*/
			document.getElementById("new_advertisement").style.display="";
			document.getElementById("new_advertisement2").style.display="";
			
		}
		function closenewad(){
			document.getElementById("new_advertisement").style.display="none";
			document.getElementById("new_advertisement2").style.display="none";
		}
		
		function showreview(name,aid){
			document.getElementById("aid").value = aid;
			document.getElementById("advertisementname").innerHTML = name;
			document.getElementById("review_advertisement").style.display="";
			document.getElementById("review_advertisement2").style.display="";
		
		}
		function closereview(){
			document.getElementById("review_advertisement").style.display="none";
			document.getElementById("review_advertisement2").style.display="none";
		}
		
		function setInfo2(){
			var checkid = document.getElementById("sel2").options[document.getElementById("sel2").selectedIndex].value;
			//document.getElementById("checkstatus").value = checkid;
			document.forms['doupload2'].getElementsByTagName("input").checkstatus.value = checkid;
			
			if(document.getElementById("reason_box").style.display == ""){
				var str = document.getElementById("reason").value;
				if(str.length == 0){
					alert("请添加不通过的原因！");
					return false;
				}
			}
			var form = document.forms['doupload2'];
			form.action = '<%=request.getContextPath() %>/browser/adverstatus.do';
			form.target = "_self";
			form.submit();
		}
		
		function checkadvertisementstatus(){
			if(document.getElementById("sel2").options[document.getElementById("sel2").selectedIndex].value == "2"){
				document.getElementById("reason_box").style.display = "";
			}else{
				document.getElementById("reason_box").style.display = "none";
			}
		}
		</script>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>广 告</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">广 告</a>
			</div>
			<div align="right">	
				<input type="hidden" value="" id="price">
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>广告列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>类别</th>
										<th>编号</th>
										<th>公司名称</th>
										<th>类别</th>
										<th>子类</th>
										<th>区县</th>
										<th>图片</th>
										<th>认证</th>
										<th>状态</th>
										<th>操作</th>
										<th>注</th>
									</tr>
								</thead>
								<tbody>
									<%
									CompangDao compangDao = (CompangDao)SpringUtils.getBean(CompangDao.class);
									if(ad != null){
									for (int i = 0;i < ad.size();i ++){
										Compang compang = compangDao.findCompangByCidWithoutStatus(ad.get(i).getTypeId());
										
										SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
										Calendar start_time = Calendar.getInstance();
										Calendar end_time = Calendar.getInstance();
										start_time.setTimeInMillis(compang.getUpdateDatetime().getTime());
										end_time.setTimeInMillis(compang.getExpirationtime().getTime());
										int month = 0;
										int start_year = start_time.get(Calendar.YEAR);
										int start_month = start_time.get(Calendar.MONTH);
										int end_year = end_time.get(Calendar.YEAR);
										int end_month = end_time.get(Calendar.MONTH);
										if(end_year > start_year){
											month = end_month + (end_year - start_year) * 12 - start_month;
										}else{
											if(end_month > start_month){
												month = end_month - start_month;
											}
										}
										int pid = Integer.parseInt(compang.getArea());
										Place pl = placeDao.findPlaceByPid(pid);
									%>
									<tr>
										<td>商户</td>
										<td><%=compang.getNumber()%></td>
										<td><%=compang.getName()%></td>
										<td><%=compang.getSmallClass().getBigclass().getName()%></td>
										<td><%=compang.getSmallClass().getName()%></td>
										<td><%=pl.getArea()%></td>
										<td>
											<img src="<%=request.getContextPath() %>/<%=ad.get(i).getPath() +ad.get(i).getImage()%>" width="130px"/>
										</td>
										<%if(compang.getAuthentication() == 0){%>
										<td>已认证</td>
										<%}else{%>
										<td>未认证</td>
										<%}%>
										<%
										if(ad.get(i).getCheckstatus() == 0){
											if(month <= 1){
										%>
										<td>快到期</td>
										<%
											}else{
										%>
										<td>发布中</td>
										<%
											}
										}
										if(ad.get(i).getCheckstatus() == 1){
										%>
										<td>待审核</td>
										<%
										}
										if(ad.get(i).getCheckstatus() == 2){
										%>
										<td>不通过</td>
										<%
										}
										if(ad.get(i).getCheckstatus() == 3){
										%>
										<td>已关闭</td>
										<%
										}
										if(ad.get(i).getCheckstatus() == 4){
										%>
										<td>已过期</td>
										<%
										}
										%>
										<td class="taskOptions">
											<%if(user.getRole().getRid() == 1){%>
											<a href="#" onclick="showreview('<%=compang.getName()%>','<%=ad.get(i).getAid()%>')" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>
											<a href="<%=request.getContextPath()%>/browser/delavert.do?aid=<%=ad.get(i).getAid()%>&status=<%=ad_type%>" onclick="" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
											<%}%>
											<%if(user.getRole().getRid() == 2){%>
											<a href="#" onclick="showreview('<%=compang.getName()%>','<%=ad.get(i).getAid()%>')" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>
											<%}%>
											<%if(user.getRole().getRid() == 3){%>
											<a href="<%=request.getContextPath()%>/browser/delavert.do?aid=<%=ad.get(i).getAid()%>&status=<%=ad_type%>" onclick="" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
											<%}%>
										</td>
										<td></td>
									</tr>
									<%}
										if(ad.size() == 0){
									%>
										<tr>
											<td colspan="5">无广告信息</td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
											<td style="display:none"></td>
										</tr>
									<%
										}
									}%>
								</tbody>
								</table>
						</div>
						<%if(null != ad_compang && ad_compang.size() > 0){%>
							<div align="center">
								<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 2) || (user.getRole().getRid() == 3)){%>
								<a id="newad" name="newad" href="#" onclick="addnewadvertisement()"/>新建广告</a>
								<%}%>
							</div>
						<%}%>
						<div class="widget-title" id="new_advertisement2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>新建广告</h5>
						</div>
						<div class="widget-content nopadding" id="new_advertisement" style="display:none">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
									<div class="control-group">
										<label class="control-label">广告类型</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="ad_type" id="ad_type" value="商户" readonly/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">商户</label>
										<div class="controls">
											<select id="sel1" style="width:300px">
												<%
												for(int j = 0;j < ad_compang.size();j ++) {
													if(ad_compang.get(j).getAuthentication() == 0){
												%>
													<option value="<%=ad_compang.get(j).getCid()%>"><%=ad_compang.get(j).getNumber() + "." + ad_compang.get(j).getName()%></option>
												<%
													}
												}%>
											</select>
										</div>
									</div>
									<div class="control-group" id="upfile" style="">
										<label class="control-label">上传图片:</label>
										<div class="controls">
											<input id="myFile" name="myFile" type="file"/>
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile()">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
										<button type="button" class="btn btn-primary" onclick="closenewad();">返回</button>
									</div>
									<input id="typeid" name="typeid" type="hidden">
									<input id="checkstatus" name="checkstatus" type="hidden" value="1">
									<input id="status" name="status" type="hidden" value="<%=ad_type%>">
									<input id="filePath" name="filePath" type="hidden" value="img/ad/">
									<input id="fileName" name="fileName" type="hidden" value="img/ad/">
								</form>
								<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
								</iframe>
						</div>
						<div class="widget-title" id="review_advertisement2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>广告审核</h5>
						</div>
						<div class="widget-content nopadding" id="review_advertisement" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">商户名称:</label>
									<label class="control-label" id="advertisementname"></label>
								</div>
								<div class="control-group">
									<label class="control-label">状态:</label>
									<div class="controls">
										<select id="sel2" onchange="checkadvertisementstatus()">
											<option value="0" >发布中</option>
											<option value="2" >不通过</option>
											<option value="3" >关闭</option>
											<option value="4" >已过期</option>
										</select>
									</div>
								</div>
								<div class="control-group" id="reason_box" style="display:none">
									<label class="control-label">原因</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="reason" id="reason" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo2();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closereview();">返回</button>
								</div>
								<input id="aid" name="aid" type="hidden">
								<input id="checkstatus" name="checkstatus" type="hidden">
								<input id="status" name="status" type="hidden" value="<%=ad_type%>">
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
					</div>	
					
				</div>
			</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			<%if(ad_type.equals("0")){%>
			setActiveClass($("#ad_menu"));
			<%}else if(ad_type.equals("1")){%>
			setActiveClass($("#pc_menu"));
			<%}%>
		});
	</script>
</html>