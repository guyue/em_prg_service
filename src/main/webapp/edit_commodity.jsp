<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<%	
	String ctx =request.getContextPath();
	Compang compang = (Compang)request.getAttribute("compang");
	Commodity commodity = (Commodity)request.getAttribute("commodity");
	
	User user = (User) session.getAttribute("user");

	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
<html>
	<head>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">

<script src="<%=ctx%>/js/jquery.min.js"></script>
<script src="<%=ctx%>/js/jquery.uniform.js"></script>
<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>

<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
<script src="<%=ctx%>/js/global.js"></script>
<script src="<%=ctx%>/js/select2.js"></script>
<script src="<%=ctx%>/js/jquery.validate.js"></script>
<script src="<%=ctx%>/js/jquery.wizard.js"></script>

		<title>易民生活</title>
		<script type="text/javascript">
		function randomString() {  
			return '' + new Date().getTime();  
		}
		function setInfo(){
			var select1 = document.getElementById("sel1");
			//document.getElementById("setcid").value=select1.options[select1.selectedIndex].value;
			
			var form = document.forms['doupload'];
			form.action = '<%=request.getContextPath() %>/browser/updatecommodity.do';
			form.target = "_self";
			form.submit();
		}
					
		function uploadFile1(){
			var str = randomString();
			document.getElementById("fileName").value = document.getElementById("myFile").value;
			document.getElementById("fileName2").value = document.getElementById("myFile").value;
			
			var filename = document.getElementById("fileName").value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.getElementById("fileName").value = newname;
				document.getElementById("fileName2").value = newname;
					
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		function uploadFile2(){
			var str = randomString();
			document.getElementById("fileName2").value = document.getElementById("myFile2").value;
			
			var filename = document.getElementById("fileName2").value;
			if(filename.length > 0){
				var num1 = filename.length;
				var num2 = filename.indexOf('.');
				var newname = filename.substr(num2,num1);
				newname = str + newname;
				document.getElementById("fileName2").value = newname;
				
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/upload.do';
				form.target = "rfFrame";
				form.submit();
				alert("上传成功!");
			}
		}
		
		function showfirstimage(){
			document.getElementById("first_image").style.display = "";
		}
		
		function showdetailimage(){
			document.getElementById("detail_image").style.display = "";
		}
		
		function closeedit(){
			document.getElementById("back_to_compang").click();
		}
		</script>
		<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
	</style>
	</head>
	<body onload="">
	<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font></h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul><br>
			<font color="#999" size="2px"><%=role%></font>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>编辑商品</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> 首页</a>
				<a href="<%=request.getContextPath()%>/browser/compang.do" class="tip-bottom">商户</a>
				<a href="#" class="current">编辑商品</a>
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>编辑商品</h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<%=request.getContextPath() %>/" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
									<div class="control-group">
										<ul class="thumbnails">		
											<li class="span2">
												<a href="#" class="thumbnail">
													<img src="<%=request.getContextPath() %>/<%=commodity.getPath() + commodity.getImage()%>"/>
												</a>
												<div class="actions">
													<a title="" href="#" onclick="showfirstimage()"><i class="icon-pencil icon-white"></i></a>
													<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>
												</div>
											</li>
										</ul>
										<div class="controls" id="first_image" style="display:none">
											<input id="myFile" name="myFile" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile1()">
											<input id="fileName" name="fileName" type="hidden">
											<input id="filePath" name="filePath" type="hidden" value="<%=commodity.getPath()%>">
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">商品名称</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="comm_name" id="comm_name" value="<%=commodity.getName()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">商品说明</label>
										<div class="controls">
											<textarea name="code" id="code" style="width: 243px; height: 78px;"><%=commodity.getCode()%></textarea>
										</div>
									</div>
									<div class="control-group"  style="display:none">
										<label class="control-label">规格</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="norm" id="norm" value="<%=commodity.getNorm()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">现价</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="deals" id="deals" value="<%=commodity.getDeals()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">原价</label>
										<div class="controls">
											<input type="text" style="width:25%;height:25px" name="price" id="price" value="<%=commodity.getPrice()%>"/>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">使用说明</label>
										<div class="controls">
											<textarea name="comm_content" id="comm_content" style="width: 243px; height: 78px;"><%=commodity.getContent()%></textarea>
										</div>
									</div>
									<div class="control-group">
										<ul class="thumbnails">		
											<li class="span2">
												<a href="#" class="thumbnail">
													<img src="<%=request.getContextPath() %>/<%=commodity.getPath() + commodity.getDetail_img()%>"/>
												</a>
												<div class="actions" style="display:none">
													<a title="" href="#" onclick="showdetailimage()"><i class="icon-pencil icon-white"></i></a>
													<a title="" href="#" onclick=""><i class="icon-remove icon-white"></i></a>
												</div>
											</li>
										</ul>
										<div class="controls" id="detail_image" style="display:none">
											<input id="myFile2" name="myFile2" type="file" />
											<input type="button" name="uploadfile" value="上传" id="uploadfile" onclick="uploadFile2()">
											<input id="fileName2" name="fileName2" type="hidden">
											<input id="filePath2" name="filePath2" type="hidden" value="<%=commodity.getPath()%>">
										</div>
									</div>
									<div class="form-actions">
										<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
										<button type="button" class="btn btn-primary" onclick="closeedit();">退出</button>
										<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.getCid()%>" id="back_to_compang" style="display:none"></a>
									</div>
									<input id="setcid" name="setcid" type="hidden" value="<%=compang.getCid()%>">
									<input id="yid" name="yid" type="hidden" value="<%=commodity.getYid()%>">
								</form>
								<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
								</iframe>
							</div>
						</div>						
					</div>
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
</html>