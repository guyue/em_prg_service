<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<html>
	<head>
		<title>易民网</title>
		<style type="text/css">
		#topbar {
			background: none repeat scroll 0 0 #f5f5f5;
			border-bottom: 1px solid #ddd;
			color: #666;
			font: 12px/2 Arial,Tahoma,"宋体";
			height: 37px;
			width: 100%;
		}
		#topbar .w, #topbar .warp {
			clear: both;
			line-height: 37px;
		}
		#topbar .w {
			background: none repeat scroll 0 0 #f5f5f5;
			border-bottom: 1px solid #ddd;
			height: 37px;
			position: relative;
		}
		.pos {
			position: relative;
			z-index: 999;
		}
		.bar_left {
			float: left;
			height: 27px;
			padding-left: 10px;
			word-spacing: 1px;
		}
		.bar_right {
			float: right;
		}
		.bar_left h2 {
			color: red;
			display: inline;
			float: left;
			font-size: 16px;
			font-weight: 700;
		}
		.bar_left .tuan {
			display: inline-block;
			padding-right: 30px;
			position: relative;
		}
		.gap {
			color: #ccc;
			font-family: '宋体';
			margin: 0 6px;
		}
		.towdiv {
			float:left;
			position:relative;
		}
		.provinces {
			float: left;
			height: 30px;
			margin: 30px 0 0;
			padding: 0;
			position: relative;
			width: 100px;
		}
		.topcity {
			background: url("<%= request.getContextPath() %>/image/x.jpg") no-repeat scroll right 0px rgba(0, 0, 0, 0);
			cursor: pointer;
			float: left;
			font-size: 20px;
			line-height: 18px;
			padding: 0 30px 30px 90px;
			left: 200px;
		}
		#saerkey {
			-moz-border-bottom-colors: none;
			-moz-border-left-colors: none;
			-moz-border-right-colors: none;
			-moz-border-top-colors: none;
			background: none repeat scroll 0 0 #fff;
			border-color: #f78015 -moz-use-text-color #f78015 #f78015;
			border-image: none;
			border-style: solid none solid solid;
			border-width: 2px 0 2px 2px;
			float: left;
			position:relative;
			height: 30px;
			overflow: hidden;
			padding-left: 13px;
		}
		#key {
			background: none repeat scroll 0 0 #fff;
			float: left;
			height: 30px;
			line-height: 34px;
		}
		#keyword {
			background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
			border: 0 none;
			display: inline-block;
			float: left;
			font-size: 14px;
			height: 22px;
			line-height: 22px;
			overflow: hidden;
			padding: 4px 2px 4px 0;
			width: 444px;
		}
		#searchbar {
			left: 215px;
			line-height: 26px;
			overflow: hidden;
			position: absolute;
			top: 11px;
		}
		#searchbar i {
			font-style: normal;
		}
		.keyword {
			color: #c8c8c8;
		}
		#keyword.keyword2 {
			background-position: 0 50px;
			color: #292929;
		}
		.inputcon {
			float: left;
			position:relative;
			height: 34px;
			overflow: hidden;
		}
		.inputcon input:active {
			background: none repeat scroll 0 0 #eb6d13;
		}
		.clear, .line {
			clear: both;
			font-size: 0;
			height: 0;
			overflow: hidden;
		}
		.search-fix .search-no {
			display: none;
			visibility: hidden;
		}
		#hot, .hot2 {
			float: left;
		}
		#hot a, .hot2 a {
			color: #666;
			float: left;
			margin-right: 10px;
		}
		#hot a {
			display: none;
		}
		.hot3 {
			float: left;
		}
		.hot3 a {
			color: #666;
			float: left;
			margin-right: 10px;
		}
		input.btnal2, input.btnall {
			border: 0 none;
			color: #fff;
			cursor: pointer;
			float: left;
			font-size: 14px;
			font-weight: 700;
			height: 34px;
			line-height: 34px;
			margin: 0;
			overflow: hidden;
			padding: 0 20px;
			text-align: center;
		}
		input.btnall {
			background: none repeat scroll 0 0 #f78015;
		}
		body{ text-align:center} 
		#title{margin:0 auto;border:0px solid #000;width:80%} 
		
		</style>
		<link rel="stylesheet" type="text/css" media="all" href="<%= request.getContextPath() %>/css/index.css"  />
		<script type="text/javascript">
		</script>
	</head>
	<body>
		<div id="headTop">
			<ul class="headTopUl clearfix">
				<div class="bar_left">
					<div id="first_page" class="towdiv">
						<a target="_self" href="">易民首页</a>
						<span class="gap">|</span>
						<a target="_self" href="">设为首页</a>
						<span class="gap">|</span>
						<a target="_self" href="">加入收藏</a>
					</div>
					<div id="login" class="towdiv">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a target="_self" href="">登录</a>
						<span class="gap">|</span>
						<a target="_self" href="">注册</a>
					</div>
				</div>
				<div class="bar_right">
					<div id="date_time" class="towdiv">
						<script type="text/javascript" language="javascript">
							var week; 
							var	today = new Date();
							if(new Date().getDay()==0) week="星期日"
							if(new Date().getDay()==1) week="星期一"
							if(new Date().getDay()==2) week="星期二" 
							if(new Date().getDay()==3) week="星期三"
							if(new Date().getDay()==4) week="星期四"
							if(new Date().getDay()==5) week="星期五"
							if(new Date().getDay()==6) week="星期六"
							function getFullYear(d) {
								var yr = d.getYear();
								if (yr < 1000)
								yr += 1900;
								return yr;
							} 
							document.write(getFullYear(today)+"年"+(new Date().getMonth()+1)+"月"+new Date().getDate()+"日 "+week);
						</script>
					</div>
					<div id="about" class="towdiv">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<a target="_blank" href="">关于易民</a>
						<span class="gap">|</span>
						<a target="_blank" href="<%= request.getContextPath() %>/phone.jsp">手机易民</a>
					</div>
				</div>
			</ul>
		</div>
		<div id="headMin">
			<ul class="headConul clearfix">
				<li class="logoLi">
					<a id="logo" target="_blank" href="">
						<image src="<%= request.getContextPath() %>/image/logo.jpg" title="欢迎来到易民服务站">
					</a>
				</li>
				<li class="cityBox">
					<div class="topcity" onmouseover="ShowDivCity()" onmouseout="HiddenDivCity()">南京</div>
				</li>
				<li class="searchLi">
					<form id="soso_form" target="_self" action="" onsubmit="">
						<input class="searchTxt" type="text" autocomplete="off" maxlength="140" name="w" value="请输入商品名、地址">
						<a id="soso_submit" class="searchBtn" href="javascript:;">搜索</a>
						<div class="hotkey" style="display:block" data-title="data_T">
							<a href="" style="display:block">关键字1</a>
							<a href="" style="display:block">关键字2</a>
							<a href="" style="display:block">关键字3</a>
						</div>
					</form>
				</li>
			</ul>
		</div>
		<div id="title" style="width:80%;align:center;display:none">
			<div class="towdiv">
				<a id="logo" target="_blank" href="">
					<image src="<%= request.getContextPath() %>/image/logo.jpg" title="欢迎来到易民服务站">
				</a>
			</div>
			<div class="provinces">
				<div class="topcity" onmouseover="ShowDivCity()" onmouseout="HiddenDivCity()">南京</div>
				<form target="_self" action="" onsubmit="">
					<div id="searchbar">
						<div id="saerkey" style="width:80%">
							<span id="key">
								<input id="keyword" class="keyword" type="text" autocomplete="off" onfocus="if(this.value=='请输入类别名称或关键字')this.value='',this.className='keyword2'" onkeyup="" onblur="if(this.value=='')this.value='请输入类别名称或关键字',this.className='keyword'" value="请输入类别名称或关键字" name="key">
							</span>
						</div>
						<div class="inputcon">
							<input id="searchbtn" class="btnall" type="submit" onmouseout="this.className='btnall'" onmousemove="this.className='btnal2'" value="搜索" name="button">
						</div>
						<div class="clear"></div>
						<div class="search-no">
							<span id="hot">
								<a href="" style="display:block">关键字1</a>
								<a href="" style="display:block">关键字2</a>
								<a href="" style="display:block">关键字3</a>
							</span>
							<span class="hot2">
								<a href="" style="display:block">关键字4</a>
								<a href="" style="display:block">关键字5</a>
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="headNav">
			<ul id="navList" class="navUl clearfix">
				<li date-nav="index">
					<a gae="click_channel1_Nav" href="">易民社区</a>
				</li>
				<li date-nav="lehuo">
					<a gae="click_channel2_Nav" href="">易民乐活</a>
				</li>
				<li date-nav="daogou">
					<a gae="click_channel2_Nav" href="">易民导购</a>
				</li>
				<li date-nav="shangwu">
					<a gae="click_channel2_Nav" href="">易民商务</a>
				</li>
				<li date-nav="zhaoping">
					<a gae="click_channel2_Nav" href="">易民招聘</a>
				</li>
				<li date-nav="zixun">
					<a gae="click_channel2_Nav" href="">易民资讯</a>
				</li>
				<li date-nav="gongyi">
					<a gae="click_channel2_Nav" href="">易民公益</a>
				</li>
			</ul>
		</div>
		<div style="width:75%;margin:auto;">
			<table style="width:100%;">
				<tr>
					<td style="width:64%">
						<div id="wowoFocus" class="focuswowo" >
							<ul style="height: 540px;">
								<li >
									<a target="_blank" gae="AD_MORE-SY-TP-00000615-4-11" href="" >
										<img  height="90"  original="<%= request.getContextPath() %>/image/1.jpg" src="<%= request.getContextPath() %>/image/1.jpg" visibility="hidden" style="width:100%">
									</a>
								</li>
								<li>
									<a target="_blank" gae="AD_MORE-SY-TP-00000615-4-12" href="">
										<img class="lazyload"  height="90" original="<%= request.getContextPath() %>/image/2.jpg" src="<%= request.getContextPath() %>/image/2.jpg" visibility="hidden" style="">
									</a>
								</li>
								
							</ul>
						</div>
					</td>
					<td></td>
					<td style="width:35%">
						<div>
							<a target="_blank" gae="AD_MORE-SY-TP-00000615-4-11" href="" >
								<img  height="90"  original="<%= request.getContextPath() %>/image/2.jpg" src="<%= request.getContextPath() %>/image/2.jpg" visibility="hidden" style="width:100%">
							</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table bgcolor="#FFFFFF" width="100%">
							<tbody>
								<tr>
									<td style="height:40px">
										<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px">易民社区</font></a>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
										<hr style="margin-top:-5px;" color="#FFA500" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:20%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						
					</td>
					<td rowspan="7">
						<table width="100%">
							<tr>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/3.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/4.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/5.jpg"></a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/3.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/4.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/5.jpg"></a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/3.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/4.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/5.jpg"></a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/3.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/4.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/5.jpg"></a>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
							</tr>
							<tr>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/3.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/4.jpg"></a>
								</td>
								<td>
									<a href=""><img src="<%= request.getContextPath() %>/image/5.jpg"></a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;
					</td>
					<td>
					</td>
					
				</tr>
				<tr>
					<td>
						<table bgcolor="#FFFFFF" width="100%">
							<tbody>
								<tr>
									<td style="height:40px;">
										<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px">易民乐活</font></a>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
										<hr style="margin-top:-5px;" color="#FFA500" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:20%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						
					</td>
					
				</tr>
				<tr>
					<td>&nbsp;
					</td>
					<td>
					</td>
					
				</tr>
				<tr>
					<td>
						<table bgcolor="#FFFFFF" width="100%">
							<tbody>
								<tr>
									<td style="height:40px">
										<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px">易民导购</font></a>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
										<hr style="margin-top:-5px;" color="#FFA500" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:20%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						
					</td>
					
				</tr>
				<tr>
					<td>&nbsp;
					</td>
					<td>
					</td>
					
				</tr>
				<tr>
					<td>
						<table bgcolor="#FFFFFF" width="100%">
							<tbody>
								<tr>
									<td style="height:40px;">
										<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px">易民商务</font></a>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td>
										<hr style="margin-top:-5px;" color="#FFA500" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
									<td>
										<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:17%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
								<tr>
									<td style="width:16%;height:40px">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td> 
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:16%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
									<td style="width:20%">
										<a href="#"><font color="0099FF" size="4px">维修服务</font></a>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						
					</td>
					
				</tr>
			</table>
		</div>
		<div style="width:75%;margin:0 auto;">
			<a href="">
				<img src="<%= request.getContextPath() %>/image/6.jpg" width="100%" height="100px">
			</a>
		</div>
		<div style="width:75%;margin:0 auto;">
			<table width="100%" bgcolor="#FFFFFF">
				<tr>
					<td style="height:40px;">
						<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px" >易民招聘</font></a>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right">
						<a href="">
							<img src="<%= request.getContextPath() %>/image/7.jpg">
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin-top:-5px;" color="#FFA500" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
				</tr>
				<tr>
					<td style="20%"><font size="4px">职位名称</font></td>
					<td></td>
					<td style="">公司名称</td>
					<td></td>
					<td style="">工作地点</td>
					<td></td>
					<td style="">更新日期</td>
					<td></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">销售经理1名</font></a></td>
					<td></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">易民江苏传媒有限公司广告南京分公司</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">浦口区</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-07-10</font></a></td>
					<td></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">销售经理1名</font></a></td>
					<td></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">易民江苏传媒有限公司广告南京分公司</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">浦口区</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-07-10</font></a></td>
					<td></td>
				</tr><tr>
					<td><a href="#"><font color="0099FF" size="4px">销售经理1名</font></a></td>
					<td></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">易民江苏传媒有限公司广告南京分公司</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">浦口区</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-07-10</font></a></td>
					<td></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">销售经理1名</font></a></td>
					<td></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">易民江苏传媒有限公司广告南京分公司</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">浦口区</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-07-10</font></a></td>
					<td></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">销售经理1名</font></a></td>
					<td></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">易民江苏传媒有限公司广告南京分公司</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">浦口区</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-07-10</font></a></td>
					<td></td>
				</tr>
			</table>
		</div>
		&nbsp;
		<div style="width:75%;margin:0 auto;">
			<table width="100%" bgcolor="#FFFFFF">
				<tr>
					<td style="height:40px;">
						<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px" >易民资讯</font></a>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right">
						<a href="">
							<img src="<%= request.getContextPath() %>/image/7.jpg">
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin-top:-5px;" color="#FFA500" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
				</tr>
				<tr>
					<td style="20%"><font size="4px">类别名称</font></td>
					<td style="" colspan="2">资讯内容简述</td>
					<td></td>
					<td style="">作者</td>
					<td>发帖日期</td>
					<td style="">浏览次数</td>
					<td>回帖数量</td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
			</table>
		</div>
		&nbsp;
		<div style="width:75%;margin:0 auto;">
			<a href="">
				<img src="<%= request.getContextPath() %>/image/6.jpg" width="100%" height="100px">
			</a>
		</div>
		&nbsp;
		<div style="width:75%;margin:0 auto;">
			<table width="100%" bgcolor="#FFFFFF">
				<tr>
					<td style="height:40px;">
						<a href="#" style="text-decoration:none;"><font color="#FFC125" size="5px" >易民公益</font></a>
					</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td style="text-align:right">
						<a href="">
							<img src="<%= request.getContextPath() %>/image/7.jpg">
						</a>
					</td>
				</tr>
				<tr>
					<td>
						<hr style="margin-top:-5px;" color="#FFA500" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
					<td>
						<hr style="margin-top:-5px;" color="#D3D3D3" size="3px">
					</td>
				</tr>
				<tr>
					<td style="20%"><font size="4px">类别名称</font></td>
					<td style="" colspan="2">资讯内容简述</td>
					<td></td>
					<td style="">作者</td>
					<td>发帖日期</td>
					<td style="">浏览次数</td>
					<td>回帖数量</td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
				<tr>
					<td><a href="#"><font color="0099FF" size="4px">类别</font></a></td>
					<td colspan="2"><a href="#"><font color="0099FF" size="4px">谁知道盱眙到扬州最迟一班车司机的电话。</font></a></td>
					<td></td>
					<td><a href="#"><font color="0099FF" size="4px">静静的风</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">2013-04-02</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">30次</font></a></td>
					<td><a href="#"><font color="0099FF" size="4px">28条</font></td>
				</tr>
			</table>
		</div>
		<div class="footer-btm" style="width:100%;filter:alpha(opacity=70);position:absolute;opacity: 0.7;">
			<table>
				<tr>
					<td style="width:10%">&nbsp;</td>
					<td style="width:60%;text-align:left">
						<font color="white">
						<a href=""><font color="white">首页</font></a><span class="gap">|</span><a href=""><font color="white">客服服务</font></a><span class="gap">|</span><a href=""><font color="white">隐私政策</font></a><span class="gap">|</span>
						<a href=""><font color="white">广告服务</font></a><span class="gap">|</span><a href=""><font color="white">网站地图</font></a><span class="gap">|</span><a href=""><font color="white">意见反馈</font></a><span class="gap">|</span>
						<a href=""><font color="white">不良信息举报</font></a> &nbsp;&nbsp;&nbsp;易民生活网版权所有©1997-2014<br>
						京ICP备	12025925号电信业务审批[2001]字第233号函京公安网安备110105000322
						</font>
					</td>
					<td style="width:10%">
						<img src="<%= request.getContextPath() %>/image/8.jpg">
					</td>
					<td style="width:16%;text-align:left">
						<font color="white">
						 扫描二维码，即可与易民微信互动，还有惊爆优惠等你来拿！
						 </font>
					</td>
					<td style="width:4%">&nbsp;</td>
				</tr>
			</table>
		</div>
	</body>
</html>