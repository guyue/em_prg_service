<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<% 
	User user = (User) session.getAttribute("user");	
%>
<div id="sidebar">
	<a href="#" class="visible-phone"><i class="icon icon-home"></i>后台首页</a>
	<ul id="leftMenu">
		<li id="home_menu" class="active"><a href="<%=request.getContextPath()%>/browser/statusnum.do"><i class="icon icon-home"></i> <span>后台首页</span></a></li>
		<%if(user.getRole().getRid() == 1){%>
		<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单类别</span></a></li>
	<!--	<li id="hot_menu"><a href="<%=request.getContextPath()%>/browser/hotmenu.do"><i class="icon icon-th"></i> <span>热门精选</span></a></li>	-->
		<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do?showfirst=0"><i class="icon icon-tasks"></i> <span>商户列表</span></a></li>	
		<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=0"><i class="icon icon-th-large"></i> <span>手机广告</span></a></li>
		<li id="pc_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=1"><i class="icon icon-th-large"></i> <span>网页广告</span></a></li>
	<!--	<li id="recommend_menu"><a href="<%=request.getContextPath()%>/browser/recommend.do"><i class="icon icon-tasks"></i> <span>推荐商户</span></a></li>		-->
		<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
		<li id="user_menu"><a href="<%=request.getContextPath()%>/browser/alluser.do"><i class="icon icon-tasks"></i> <span>权限分配</span></a></li>
		<li id="recover_menu"><a href="<%=request.getContextPath()%>/browser/recover.do"><i class="icon icon-trash"></i> <span>回收站</span></a></li>	
		<li id="place_menu"><a href="<%=request.getContextPath()%>/openplace.jsp"><i class="icon icon-globe"></i> <span>区域设置</span></a></li>	
		<%}%>
		<%if(user.getRole().getRid() == 2){%>
		<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单类别</span></a></li>
		<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do?showfirst=0"><i class="icon icon-tasks"></i> <span>商户列表</span></a></li>	
		<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=0"><i class="icon icon-th-large"></i> <span>手机广告</span></a></li>
		<li id="pc_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=1"><i class="icon icon-th-large"></i> <span>网页广告</span></a></li>
	<!--	<li id="recommend_menu"><a href="<%=request.getContextPath()%>/browser/recommend.do"><i class="icon icon-tasks"></i> <span>推荐商户</span></a></li>		-->
		<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
		<li id="recover_menu"><a href="<%=request.getContextPath()%>/browser/recover.do"><i class="icon icon-trash"></i> <span>回收站</span></a></li>	
		<%}%>
		<%if(user.getRole().getRid() == 3){%>
		<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单类别</span></a></li>
	<!--	<li id="hot_menu"><a href="<%=request.getContextPath()%>/browser/hotmenu.do"><i class="icon icon-th"></i> <span>热门精选</span></a></li>		-->
		<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do?showfirst=0"><i class="icon icon-tasks"></i> <span>商户列表</span></a></li>	
		<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=0"><i class="icon icon-th-large"></i> <span>手机广告</span></a></li>
		<li id="pc_menu"><a href="<%=request.getContextPath()%>/browser/advers.do?status=1"><i class="icon icon-th-large"></i> <span>网页广告</span></a></li>
	<!--	<li id="recommend_menu"><a href="<%=request.getContextPath()%>/browser/recommend.do"><i class="icon icon-tasks"></i> <span>推荐商户</span></a></li>		-->
		<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
		<li id="recover_menu"><a href="<%=request.getContextPath()%>/browser/recover.do"><i class="icon icon-trash"></i> <span>回收站</span></a></li>	
		<%}%>
		<%if(user.getRole().getRid() == 4){%>
		<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do?showfirst=0"><i class="icon icon-tasks"></i> <span>商户列表</span></a></li>	
		<li id="recover_menu"><a href="<%=request.getContextPath()%>/browser/recover.do"><i class="icon icon-trash"></i> <span>回收站</span></a></li>	
		<%}%>
		<%if(user.getRole().getRid() == 5){%>
		<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do?showfirst=0"><i class="icon icon-tasks"></i> <span>商户列表</span></a></li>	
		<%}%>
		<li id="password_menu"><a href="<%=request.getContextPath()%>/changepassword.jsp"><i class="icon icon-cog"></i> <span>修改密码</span></a></li>	
	</ul>
</div>