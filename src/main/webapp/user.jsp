<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	User user = (User) session.getAttribute("user");
	List<User> user_list = (List<User>) request.getAttribute("all_user");

	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>用户列表</title>
	</head>
	<script type="text/javascript">
	function showeditrole(uid,name){
		document.forms['doupload1'].getElementsByTagName("input").uid.value = uid;
		document.forms['doupload1'].getElementsByTagName("input").username.value = name;
		
		document.getElementById("edit_role").style.display="";
		document.getElementById("edit_role2").style.display="";
	}
	function closeeditrole(){
		document.getElementById("edit_role").style.display="none";
		document.getElementById("edit_role2").style.display="none";
	}
	
	function showedituser(uid,name,pass,address,role,tel){
		document.forms['doupload2'].getElementsByTagName("input").uid.value = uid;
		document.forms['doupload2'].getElementsByTagName("input").username.value = name;
		document.forms['doupload2'].getElementsByTagName("input").password.value = pass;
		document.forms['doupload2'].getElementsByTagName("input").address.value = address;
		//if(role == "1"){
			//document.forms['doupload2'].getElementsByTagName("input").role_name.value = "一级";
		//}else 
		if(role == "2"){
			//document.getElementById("sel1").selectedIndex = 1;
			document.getElementById("sel3").options[3].selected = "seleceted";
			//document.forms['doupload2'].getElementsByTagName("input").role_name.value = "二级";
		}else if(role == "3"){
			//document.getElementById("sel1").selectedIndex = 2;
			document.getElementById("sel3").options[2].selected = "seleceted";
			//document.forms['doupload2'].getElementsByTagName("input").role_name.value = "三级";
		}else if(role == "4"){
			//document.getElementById("sel1").selectedIndex = 3;
			document.getElementById("sel3").options[1].selected = "seleceted";
			//document.forms['doupload2'].getElementsByTagName("input").role_name.value = "四级";
		}else if(role == "5"){
			//document.getElementById("sel1").selectedIndex = 4;
			document.getElementById("sel3").options[0].selected = "seleceted";
			//document.forms['doupload2'].getElementsByTagName("input").role_name.value = "五级";
		}
		document.forms['doupload2'].getElementsByTagName("input").tel.value = tel;
		document.forms['doupload2'].getElementsByTagName("input").role.value = role;
		
		document.getElementById("edit_user").style.display="";
		document.getElementById("edit_user2").style.display="";
	}
	function closeedituser(){
		document.getElementById("edit_user").style.display="none";
		document.getElementById("edit_user2").style.display="none";
	}
	
	function shownewuser(){
		document.getElementById("new_user").style.display="";
		document.getElementById("new_user2").style.display="";
	}
	function closenewuser(){
		document.getElementById("new_user").style.display="none";
		document.getElementById("new_user2").style.display="none";
	}
	
	function setInfo(){
		var roleid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
		document.forms['doupload1'].getElementsByTagName("input").role.value = roleid;
		var form = document.forms['doupload1'];
		form.action = '<%=request.getContextPath() %>/browser/changerole.do';
		form.target = "_self";
		form.submit();
	}
	function setInfo2(){
		var address = document.getElementById("sel4").options[document.getElementById("sel4").selectedIndex].value;
		document.forms['doupload2'].getElementsByTagName("input").address.value = address;
		var roleid = document.getElementById("sel3").options[document.getElementById("sel3").selectedIndex].value;
		document.forms['doupload2'].getElementsByTagName("input").role.value = roleid;
		
		var name = document.forms['doupload2'].getElementsByTagName("input").username.value;
		var uid = document.forms['doupload2'].getElementsByTagName("input").uid.value;
		$.ajax({
			url:"<%=request.getContextPath()%>/browser/checkname.do?checkname=" + name + "&uid=" + uid,
			type:"post",
			dataType: "JSON",
			async:false,
			timeout: 3000,
			success: function(ret) {
				if(ret.ckeck == "0"){
					var form = document.forms['doupload2'];
					form.action = '<%=request.getContextPath() %>/browser/updateuser.do';
					form.target = "_self";
					form.submit();
				}else{
					document.getElementById("checkname2").innerHTML = "<font color='red'>*用户名重复,请重新输入</font>";
				}
			},
			error: function(XMLRequest, textInfo) {
				if (textInfo != null) {
					alert(textInfo);
				}
			}
		});
		
		
	}
	function setInfo3(){
		var roleid = document.getElementById("sel2").options[document.getElementById("sel2").selectedIndex].value;
		document.forms['doupload3'].getElementsByTagName("input").role.value = roleid;
		
		var address = document.getElementById("sel6").options[document.getElementById("sel6").selectedIndex].value;
		document.forms['doupload3'].getElementsByTagName("input").address.value = address;
		
		var name = document.forms['doupload3'].getElementsByTagName("input").username.value;
		$.ajax({
			url:"<%=request.getContextPath()%>/browser/checkname.do?checkname=" + name,
			type:"post",
			dataType: "JSON",
			async:false,
			timeout: 3000,
			success: function(ret) {
				if(ret.ckeck == "0"){
					var form = document.forms['doupload3'];
					form.action = '<%=request.getContextPath() %>/browser/updateuser.do';
					form.target = "_self";
					form.submit();
				}else{
					document.getElementById("checkname").innerHTML = "<font color='red'>*用户名重复,请重新输入</font>";
				}
			},
			error: function(XMLRequest, textInfo) {
				if (textInfo != null) {
					alert(textInfo);
				}
			}
		});
	}
	function changecity(){
		var city = document.getElementById("sel3").options[document.getElementById("sel3").selectedIndex].value;
		if(city == "全部"){
			document.getElementById("sel4").options[0].selected = true;
		}else{
			$.ajax({
					url:"<%=request.getContextPath()%>/browser/getarea.do?city=" + city,
					type:"post",
					dataType: "JSON",
					async:false,
					timeout: 3000,
					success: function(ret) {
						if(ret.length > 0){
							document.getElementById("sel4").options.length = 1;
							for(var i = 0; i < ret.length; i ++){
								$("#sel4").append(ret[i]);
							}
						}
					},
					error: function(XMLRequest, textInfo) {
						if (textInfo != null) {
							alert(textInfo);
						}
					}
			});
		}
	}
	function changecity2(){
		var city = document.getElementById("sel5").options[document.getElementById("sel5").selectedIndex].value;
		if(city == "全部"){
			document.getElementById("sel6").options[0].selected = true;
		}else{
			$.ajax({
					url:"<%=request.getContextPath()%>/browser/getarea.do?city=" + city,
					type:"post",
					dataType: "JSON",
					async:false,
					timeout: 3000,
					success: function(ret) {
						if(ret.length > 0){
							document.getElementById("sel6").options.length = 1;
							for(var i = 0; i < ret.length; i ++){
								$("#sel6").append(ret[i]);
							}
						}
					},
					error: function(XMLRequest, textInfo) {
						if (textInfo != null) {
							alert(textInfo);
						}
					}
			});
		}
	}
	function deleteUser(val,uid,name){
		var r=confirm("你确定需要删除" + name + "吗？");
		if (r==true){
			val.href="<%=request.getContextPath()%>/browser/deluser.do?uid=" + uid;
		}else{
			val.href="#";
		}
	}
	</script>
	<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
	</style>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>	
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>			
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>权限分配</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">权限分配</a>
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>权限分配</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>用户名</th>
											<th>权限</th>
											<th>区域</th>
											<th>电话</th>
											<th>操作</th>
										</tr>
										</thead>
									<tbody>
										<%
										if(user_list != null){
											for(int i = 0;i < user_list.size();i ++){
												if(user_list.get(i).getRole().getRid() != 1){
										%>
											<tr>
												<td><%=user_list.get(i).getName()%></td>
												<td>
													<%if(user_list.get(i).getRole().getRid() == 1){%>
														一级权限
													<%}%>
													<%if(user_list.get(i).getRole().getRid() == 2){%>
														二级权限
													<%}%>
													<%if(user_list.get(i).getRole().getRid() == 3){%>
														三级权限
													<%}%>
													<%if(user_list.get(i).getRole().getRid() == 4){%>
														四级权限
													<%}%>
													<%if(user_list.get(i).getRole().getRid() == 5){%>
														五级权限
													<%}%>
												</td>
												<td>
												<%
													int pid = Integer.parseInt(user_list.get(i).getAddress());
													Place pl = placeDao.findPlaceByPid(pid);
													if( pid == 1){
												%>
													<%=newarea.getArea() + pl.getProvince()%>
												<%
													}else{
												%>
													<%=pl.getCity() + pl.getArea()%>
												<%
													}
												%>
												</td>
												<td><%=user_list.get(i).getPhone()%></td>
												<td class="taskOptions">
												<!--	<a href="#" class="tip-top" onclick="showeditrole('<%=user_list.get(i).getUid()%>','<%=user_list.get(i).getName()%>')" data-original-title="权限分配"><i class="icon-pencil"></i></a>	-->
													<a href="#" class="tip-top" onclick="showedituser('<%=user_list.get(i).getUid()%>','<%=user_list.get(i).getName()%>','<%=user_list.get(i).getPassword()%>','<%=user_list.get(i).getAddress()%>','<%=user_list.get(i).getRole().getRid()%>','<%=user_list.get(i).getPhone()%>')" data-original-title="修改用户"><i class="icon-edit"></i></a>
													<a href="#" class="tip-top" onclick="deleteUser(this,'<%=user_list.get(i).getUid()%>','<%=user_list.get(i).getName()%>')" data-original-title="删除"><i class="icon-remove"></i></a>
												</td>
											</tr>
										<%
												}
											}
											if(user_list.size() == 1 && user_list.get(0).getRole().getRid() == 1){
										%>
											<tr>
												<td colspan="5">无用户信息</td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
											</tr>
										<%
											}
										}
										%>
									</tbody>
								</table>
							</div>
						</div>
							<div align="center">
								<a href="#" class="tip-top" onclick="shownewuser()" data-original-title="添加用户"><i class="icon-user"></i>添加用户</a>
							</div>
						<div class="widget-title" id="edit_role2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>权限分配</h5>
						</div>
						<div class="widget-content nopadding" id="edit_role" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload1" id="doupload1" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">用户名:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="username" id="username" value="" readonly/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">分配权限:</label>
									<div class="controls">
										<select id="sel1" onchange="checkrole()">
											<%if(user.getRole().getRid() <= 5){%>
											<option value="5">五级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 4){%>
											<option value="4">四级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 3){%>
											<option value="3">三级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 2){%>
											<option value="2">二级</option>
											<%}%>
										</select>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closeeditrole();">返回</button>
								</div>
								<input id="uid" name="uid" type="hidden">
								<input id="role" name="role" type="hidden">
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
						<div class="widget-title" id="edit_user2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>修改用户</h5>
						</div>
						<div class="widget-content nopadding" id="edit_user" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">用户名:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="username" id="username" value=""/><label id="checkname2"></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">密码</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="password" id="password" value=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">范围</label>
									<div class="controls">
										<%
											List<Place> area = placeDao.findAreaByCity(newarea.getArea());
										%>
										<select id="sel4" onclick="" style="width:100px">
											<option value="1" selected>全市</option>
											<%for(int k = 0;k < area.size();k ++){%>
												<option value="<%=area.get(k).getPid()%>"><%=area.get(k).getArea()%></option>
											<%}%>
										</select>
									</div>
								</div>
								<div class="control-group" style="display:none">
									<label class="control-label">权限:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="role_name" id="role_name" value="" readonly/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">分配权限:</label>
									<div class="controls">
										<select id="sel3" onchange="checkrole()">
											<%if(user.getRole().getRid() <= 5){%>
											<option value="5">五级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 4){%>
											<option value="4">四级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 3){%>
											<option value="3">三级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 2){%>
											<option value="2">二级</option>
											<%}%>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">电话</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="tel" id="tel" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo2();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closeedituser();">返回</button>
								</div>
								<input id="uid" name="uid" type="hidden">
								<input id="role" name="role" type="hidden">
								<input id="address" name="address" type="hidden">
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
						<div class="widget-title" id="new_user2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>添加用户</h5>
						</div>
						<div class="widget-content nopadding" id="new_user" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload3" id="doupload3" class="form-horizontal">
								<div class="control-group">
									<label class="control-label">用户名:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="username" id="username" value=""/><label id="checkname"></label>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">密码(默认)</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="password" id="password" value="123456" readonly/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">范围</label>
									<div class="controls">
										<select id="sel6" onclick="" style="width:100px">
											<option value="1" selected>全市</option>
											<%for(int k = 0;k < area.size();k ++){%>
												<option value="<%=area.get(k).getPid()%>"><%=area.get(k).getArea()%></option>
											<%}%>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">分配权限:</label>
									<div class="controls">
										<select id="sel2">
											<%if(user.getRole().getRid() <= 5){%>
											<option value="5">五级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 4){%>
											<option value="4">四级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 3){%>
											<option value="3">三级</option>
											<%}%>
											<%if(user.getRole().getRid() <= 2){%>
											<option value="2">二级</option>
											<%}%>
										</select>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">电话</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="tel" id="tel" value=""/>
									</div>
								</div>
								<input id="role" name="role" type="hidden">
								<input id="address" name="address" type="hidden">
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo3();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closenewuser();">返回</button>
								</div>
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#user_menu"));
		});
	</script>
</html>				