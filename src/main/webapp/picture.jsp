<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	List<Picture> picture = (List<Picture>)request.getAttribute("picture");
	
	User user = (User) session.getAttribute("user");
	
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title></title>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<!--
		<div id="sidebar">
			<a href="#" class="visible-phone"><i class="icon icon-home"></i> 首页</a>
			<ul id="leftMenu">
				<li id="home_menu" class="active"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-home"></i> <span>首页</span></a></li>
				<li id="ad_menu"><a href="<%=request.getContextPath()%>/browser/advers.do"><i class="icon icon-th-large"></i> <span>广告</span></a></li>
				<li id="menu_menu"><a href="<%=request.getContextPath()%>/browser/menu.do"><i class="icon icon-th"></i> <span>菜单</span></a></li>
				<li id="settings_menu" onclick="setActiveClass(this)" class="submenu" style="display:none"><a href="#"><i class="icon icon-cog"></i> <span>设置</span> <span class="label">2</span></a>
					<ul>
						<li id="settings_menu" ><a href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span>修改密码</span></a></li>
						<li id="profile_menu" ><a href="<%=request.getContextPath()%>/anonymous_profile.jsp"><i class="icon icon-user"></i> <span>Update Profile</span></a></li>
					</ul>
				</li>
				<li id="compang_menu"><a href="<%=request.getContextPath()%>/browser/compang.do"><i class="icon icon-tasks"></i> <span>商户</span></a></li>	
				<li id="service_menu"><a href="<%=request.getContextPath()%>/browser/service.do"><i class="icon icon-tasks"></i> <span>服务区域</span></a></li>	
				<li id="commodity_menu"><a href="<%=request.getContextPath()%>/browser/commodity.do"><i class="icon icon-tasks"></i> <span>商品</span></a></li>
				<li id="picture_menu"><a href="<%=request.getContextPath()%>/browser/picture.do"><i class="icon icon-tasks"></i> <span>外景</span></a></li>
				<li id="contect_menu"><a href="<%=request.getContextPath()%>/browser/contect.do"><i class="icon icon-tasks"></i> <span>联系方式</span></a></li>
			</ul>
		</div>
		-->
		<div id="content">
			<div id="content-header">
				<h1>商户外景</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">商户外景</a>
			</div>
			<div align="right">	
				<input type="hidden" value="" id="price">
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>外景列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>商户</th>
										<th>小图</th>
										<th>大图</th>
										<th>路径</th>
										<th>描述</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
								
									<%
										if(picture != null){
											for(int i = 0;i < picture.size();i ++){
									%>
									<tr>
										<td><%=picture.get(i).getCompang().getName()%></td>
										<td><%=picture.get(i).getSmall()%></td>
										<td><%=picture.get(i).getBig()%></td>
										<td><%=picture.get(i).getPath()%></td>
										<td><%=picture.get(i).getDescription()%></td>
										<td class="taskOptions">
											<a href="<%=request.getContextPath()%>/browser/delpicture.do?pid=<%=picture.get(i).getPid()%>" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
											<a href="<%=request.getContextPath()%>/browser/getpicture.do?pid=<%=picture.get(i).getPid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
										</td>
									</tr>
									<%
											}
										}
									%>
								</tbody>
								</table>
						</div>		
						<div align="center">
						<a id="newad" name="newad" href="../new_picture.jsp" />新建商户外景</a>
						</div>
					</div>					
				</div>
			</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#picture_menu"));
		});
	</script>
</html>