<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<%
	String ctx = (String) request.getContextPath();
	List<BigClass> big = (List<BigClass>)request.getAttribute("bigclass");
	List<SmallClass> small = (List<SmallClass>)request.getAttribute("smallclass");
	User user = (User) session.getAttribute("user");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String area = "";
	int pid = Integer.parseInt(user.getAddress());
	Place pl = placeDao.findPlaceByPid(pid);
	if(user.getRole().getRid() <= 3){
		area = "全市";
	}else{
		area = pl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
	<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
	<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
	<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
	<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
	<script src="<%=ctx%>/js/jquery.min.js"></script>
	<script src="<%=ctx%>/js/jquery.uniform.js"></script>
	<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
	<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
	<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
	<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
	<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
	<script src="<%=ctx%>/js/global.js"></script>
	<script src="<%=ctx%>/js/select2.js"></script>
	<script src="<%=ctx%>/js/jquery.validate.js"></script>
	<script src="<%=ctx%>/js/jquery.wizard.js"></script>
	<title>热门精选</title>
	<style type="text/css">	
	.span2 {
		width: 90px;
	}
	#header .tile {
		height: 61px;
		left: 215px;
		line-height: 600px;
		overflow: hidden;
		position: relative;
		top: -20px;
		width: 391px;
	}
	.thumbnails .actions {
		margin-left: -14px;
		width: 15px;
	}
	</style>
	<script type="text/javascript">
	function changeSmallList(){
		var bigid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
		$.ajax({
			url:"<%=request.getContextPath()%>/browser/changesmall.do?bid=" + bigid,
			type:"post",
			dataType: "JSON",
			async:false,
			timeout: 3000,
			success: function(ret) {
				if(ret.length > 0){
					document.getElementById("sel2").style.display = "";
					document.getElementById("sel2").options.length = 0;
					for(var i = 0; i < ret.length; i ++){
						$("#sel2").append(ret[i]);
					}
				}else{
					document.getElementById("sel2").style.display = "none";
				}
			},
			error: function(XMLRequest, textInfo) {
				if (textInfo != null) {
					alert(textInfo);
				}
			}
		});
	}
	function setInfo(){
		var select1 = document.getElementById("sel2");
		document.getElementById("sid").value=select1.options[select1.selectedIndex].value;
		document.getElementById("showfirst").value="0";
		
		$.ajax({
			url:"<%=request.getContextPath()%>/browser/threemenushowfirst.do",
			type:"post",
			dataType: "JSON",
			async:false,
			timeout: 3000,
			success: function(ret) {
				var add = ret.add;
				if(add == 0){
					var form = document.forms['doupload'];
					form.action = '<%=request.getContextPath() %>/browser/setthreemenu.do';
					form.target = "_self";
					form.submit();
				}else{
					alert("热门精选已满，请先删除，然后再添加！");
				}
			},
			error: function(XMLRequest, textInfo) {
				if (textInfo != null) {
					alert(textInfo);
				}
			}
		});
	}
		
	function shohotmenu(){
		document.getElementById("new_hot").style.display = "";
		document.getElementById("new_hot2").style.display = "";
	}
	function closediv(){
		document.getElementById("new_hot").style.display = "none";
		document.getElementById("new_hot2").style.display = "none";
	}
	</script>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=area%></font></h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="<%=request.getContextPath()%>/"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul><br>
			<font color="#999" size="2px"><%=role%></font>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">热门精选</a>
			</div>
			<ul class="thumbnails">
				<%
				int count_hot = 0;
				for(int j = 0;j < small.size();j ++){
					if(small.get(j).getShowfirst() == 0){
				%>
				<li class="span2">
					<a href="#" class="thumbnail">
						<img src="<%=request.getContextPath()%>/<%=small.get(j).getPath() + small.get(j).getImage()%>" alt="" width="80px" height="80px"/>
						&nbsp;&nbsp;<%=small.get(j).getName()%>
					</a>
					<div class="actions">
						<a title="" href="<%=request.getContextPath() %>/browser/setthreemenu.do?sid=<%=small.get(j).getSid()%>&showfirst=1"><i class="icon-remove icon-white"></i></a>
					</div>
				</li>
				<%
					count_hot ++;
					}
				}%>
			</ul>
			<%if(count_hot == 0){%>
				无热门精选</br>
			<%}%>
			<input type="button" value="新增热门精选" onclick="shohotmenu()">
			<div class="widget-title" id="new_hot" style="display:none">
				<span class="icon">
					<i class="icon-align-justify"></i>									
				</span>
				<h5>新增热门精选</h5>
			</div>
			<div class="widget-content nopadding" id="new_hot2" style="display:none">
				<form action="" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal">
					<div class="control-group">
						<label class="control-label">一级菜单:</label>
						<div class="controls">
							<select id="sel1" onchange="changeSmallList()">
								<%for(BigClass bg:big){
									if(bg.getBid() != 1){
								%>
								<option value="<%=bg.getBid()%>"><%=bg.getName()%></option>
								<%
									}
								}%>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">二级级菜单:</label>
						<div class="controls">
							<select id="sel2">
								<%
								if(big.size() > 1){
									for(SmallClass sa:small) {
										if(sa.getBigclass().getBid() == big.get(1).getBid()){
								%>
								<option value="<%=sa.getSid()%>"><%=sa.getName()%></option>
								<%
										}
									}
								}
								%>
							</select>
						</div>
					</div>
					<div class="form-actions">
						<button type="button" class="btn btn-primary" onclick="setInfo();">增加</button>
						<button type="button" class="btn btn-primary" onclick="closediv();">返回</button>
					</div>
					<input id="sid" name="sid" type="hidden">
					<input id="showfirst" name="showfirst" type="hidden">
					<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
					</iframe>
				</form>
			</div>
		</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#hot_menu"));
		});
	</script>
</html>