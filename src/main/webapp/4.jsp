<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	List<Compang> compang = (List<Compang>)session.getAttribute("compang");
	
	User user = (User) session.getAttribute("user");
	
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title></title>
	</head>
	<style type="text/css">	
	.widget-box {
		background: none repeat scroll 0 0 #f9f9f9;
		border-left: 1px solid #cdcdcd;
		border-right: 1px solid #cdcdcd;
		border-top: 1px solid #cdcdcd;
		clear: both;
		margin-bottom: 16px;
		margin-top: 0px;
		position: relative;
	}
	</style>
	<script type="text/javascript">
		function showreview(name,cid){
			document.getElementById("cid").value = cid;
			document.getElementById("compangname").innerHTML = name;
			document.getElementById("review_compang").style.display="";
			document.getElementById("review_compang2").style.display="";
		}
		function closereview(){
			document.getElementById("review_compang").style.display="none";
			document.getElementById("review_compang2").style.display="none";
		}
		
		function setInfo(){
			var checkid = document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value;
			document.getElementById("checkstatus").value = checkid;
			
			if(document.getElementById("reason_box").style.display == ""){
				var str = document.getElementById("reason").value;
				if(str.length == 0){
					alert("请添加不通过的原因！");
					return false;
				}else{
					return true;
				}
			}
			var form = document.forms['doupload'];
			form.action = '<%=request.getContextPath() %>/browser/';
			form.target = "_self";
			form.submit();
		}
		
		function checkcompangstatus(){
			if(document.getElementById("sel1").options[document.getElementById("sel1").selectedIndex].value == "2"){
				document.getElementById("reason_box").style.display = "";
			}else{
				document.getElementById("reason_box").style.display = "none";
			}
		}
	</script>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>商 户</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/advers.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">商户列表</a>
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>商户列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
									<thead>
										<tr>
											<th>编号</th>
											<th>公司名字</th>
											<th>发布日期</th>
											<th>发布期限</th>
											<th>到期时间</th>
											<th>是否认证</th>
											<th>类别</th>
											<th>子类</th>
											<th>发布城市</th>
											<th>发布区域</th>
											<th>状态</th>
											<th>操作</th>
										</tr>
									</thead>
									<tbody>
										<%
										if(compang != null){
											for(int i = 0;i < compang.size();i ++){
										%>
											<tr>
												<td><%=compang.get(i).getNumber()%></td>
												<td><%=compang.get(i).getName()%></td>
												<%
													SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
													Calendar start_time = Calendar.getInstance();
													Calendar end_time = Calendar.getInstance();
													start_time.setTimeInMillis(compang.get(i).getUpdateDatetime().getTime());
													end_time.setTimeInMillis(compang.get(i).getExpirationtime().getTime());
													int month = 0;
													int start_year = start_time.get(Calendar.YEAR);
													int start_month = start_time.get(Calendar.MONTH);
													int end_year = end_time.get(Calendar.YEAR);
													int end_month = end_time.get(Calendar.MONTH);
													if(end_year > start_year){
														month = end_month + (end_year - start_year) * 12 - start_month;
													}else{
														if(end_month > start_month){
															month = end_month - start_month;
														}
													}
												%>
												<td><%=formatter.format(compang.get(i).getUpdateDatetime())%></td>
												<td><%=month%>个月</td>
												<td><%=formatter.format(compang.get(i).getExpirationtime())%></td>
												<%if(compang.get(i).getAuthentication() == 0){%>
												<td>已认证</td>
												<%}else{%>
												<td>未认证</td>
												<%}%>
												<td><%=compang.get(i).getSmallClass().getBigclass().getName()%></td>
												<td><%=compang.get(i).getSmallClass().getName()%></td>
												<%
													ServiceAreaDao serviceDao = (ServiceAreaDao)SpringUtils.getBean(ServiceAreaDao.class);
													List<ServiceArea> area = serviceDao.findServideAresByCid(compang.get(i).getCid());
												%>
												<td>南京</td>
												<td><%=area.get(0).getArea()%></td>
												<%
												if(compang.get(i).getCheckStatus() == 0){
													if(month <= 1){
												%>
												<td>快到期</td>
												<%
													}else{
												%>
												<td>发布中</td>
												<%
													}
												}
												if(compang.get(i).getCheckStatus() == 1){
												%>
												<td>待审核</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 2){
												%>
												<td>不通过</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 3){
												%>
												<td>已关闭</td>
												<%
												}
												if(compang.get(i).getCheckStatus() == 4){
												%>
												<td>已过期</td>
												<%
												}
												%>
												<td class="taskOptions">
													<a href="<%=request.getContextPath()%>/browser/viewcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="预览"><i class="icon-eye-open"></i></a>
												<!--	<a href="#" onclick="showMessageBox(this,'<%=compang.get(i).getName()%>','<%=compang.get(i).getCid()%>')" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>	-->
													<a href="#" onclick="showreview('<%=compang.get(i).getName()%>','<%=compang.get(i).getCid()%>')" class="tip-top" data-original-title="审核"><i class="icon-check"></i></a>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
													<a href="<%=request.getContextPath()%>/browser/delcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
													<a href="<%=request.getContextPath()%>/browser/getcompang.do?cid=<%=compang.get(i).getCid()%>" class="tip-top" data-original-title="关闭"><i class="icon-move"></i></a>
												</td>
											</tr>
										<%
											}
										}
										%>
									</tbody>
								</table>
							</div>
						</div>		
						<div align="center">
						<a id="newad" name="newad" href="../new_compang.jsp" />新建商户</a>
						</div> 
						<div class="widget-title" id="review_compang2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>商户审核</h5>
						</div>
						<div class="widget-content nopadding" id="review_compang" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">商户名称:</label>
									<label class="control-label" id="compangname"></label>
								</div>
								<div class="control-group">
									<label class="control-label">状态:</label>
									<div class="controls">
										<select id="sel1" onchange="checkcompangstatus()">
											<option value="0" selected>发布中</option>
											<option value="2" selected>不通过</option>
											<option value="3" selected>已关闭</option>
											<option value="4" selected>已过期</option>
										</select>
									</div>
								</div>
								<div class="control-group" id="reason_box" style="display:none">
									<label class="control-label">原因</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="reason" id="reason" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closereview();">返回</button>
								</div>
								<input id="cid" name="cid" type="hidden">
								<input id="checkstatus" name="checkstatus" type="hidden">
							</form>
							<iframe style="display:none" id="rfFrame" name="rfFrame" src="about:blank">
							</iframe>
						</div>
					</div>					
				</div>
			</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#compang_menu"));
		});
	</script>
	
	<script type="text/javascript">
            var isIe=(document.all)?true:false;  
            var winArray = [];  
            function showMessageBox(content,name,cid)  
            {  
                var back=document.createElement("div");  
                var styleStr="top:0px;left:0px;position:absolute;background:#666;width:100%;height:100%;";  
                styleStr+=(isIe)?"filter:alpha(opacity=0);":"opacity:0;";  
                back.style.cssText=styleStr;  
                document.body.appendChild(back);  
                showBackground(back,50);  
                winArray.push(back);  
  
                var mesW=document.createElement("div");  
                mesW.className="mesWindow";  
                mesW.innerHTML="<div style=\"background:#CCCCCC; color:#000;filter:alpha(opacity=100); height:20px\"><div style=\"font-size:20px;float: left;\"><span style=\"font-size:12px\" id=\"msg2\">商户审核</span></div>" + 
				"<div align=\"right\"><a href=\"javascript:void(0)\" onclick=\"closeWindow(this);\" class=\"tip-top\" data-original-title=\"Close\"><i class=\"icon-remove\"></i></a></div></div>" +
				"<div class=\"widget-box\"><div class=\"widget-title\"><span class=\"icon\"><i class=\"icon-align-justify\"></i></span><h5>审核</h5></div>" +
				"<div class=\"widget-content nopadding\" align=\"center\"><form action=\"\" id=\"form-wizard\" method=\"post\"><div class=\"control-group\"><label class=\"control-label\">"+name+"</label><label class=\"control-label\"></label></div>" +
				"<div class=\"control-group\"><label class=\"control-label\">发布状态</label><select id=\"sel1\"><option value=\"0\" selected>发布中</option><option value=\"2\" selected>不通过</option><option value=\"3\" selected>已关闭</option><option value=\"4\" selected>已过期</option></select></div>"+
				"<div class=\"control-group\" id=\"reason_box\" style=\"display:none\"><div class=\"controls\"><label class=\"control-label\">原因</label><input type=\"text\" style=\"width:25%;height:25px\" name=\"reason\" id=\"reason\" value=\"\"/></div></div>"+
				"<div class=\"form-actions\" align=\"center\"><input id=\"submit\" class=\"btn btn-danger\" type=\"button\" value=\"提交\" onclick=\"checkstatus();\" style=\"height:30px;width:150px;\" /></div>"+
				"</form></div></div>";  
                styleStr="left:600px;top:50px;position:absolute;width:350px;height:300px;";  
                mesW.style.cssText=styleStr;  
                document.body.appendChild(mesW);
            }  
            function showBackground(obj,endInt)  
            {  
                if(isIe)  
                {  
                    obj.filters.alpha.opacity+=1;  
                    if(obj.filters.alpha.opacity<endInt)  
                    {  
                        setTimeout(function(){showBackground(obj,endInt)},5);  
                    }  
                }else{  
                    var al=parseFloat(obj.style.opacity);al+=0.01;  
                    obj.style.opacity=al;  
                    if(al<(endInt/100))  
                    {setTimeout(function(){showBackground(obj,endInt)},5);}  
                }  
            }  
            function closeWindow(my)  
            {  
                $(my).parents(".mesWindow").remove();  
                $(winArray.pop()).remove();
            }
        </script>
</html>