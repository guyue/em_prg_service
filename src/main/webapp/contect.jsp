<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page import="com.androidservice.dao.*"%>
<%@ page import="com.androidservice.bean.*"%>
<%@ page import="com.androidservice.util.SpringUtils"%>
<%@ page import="java.sql.*, java.text.*, java.lang.*"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<% 
	String ctx = (String) request.getContextPath();
	List<Contect> contect = (List<Contect>)request.getAttribute("contect");
	
	User user = (User) session.getAttribute("user");
	
	PlaceDao placeDao = (PlaceDao)SpringUtils.getBean(PlaceDao.class);
	OpenAreaDao openareaDao = (OpenAreaDao)SpringUtils.getBean(OpenAreaDao.class);
	String place = "";
	if(request.getContextPath().contains("mobile")){
		place = "nj";
	}else{
		place = request.getContextPath();
		place = place.substring(1,place.length());
	}
	OpenArea newarea = openareaDao.findOpenAreaByAbridge(place);
	String new_area = "";
	int newpid = Integer.parseInt(user.getAddress());
	Place newpl = placeDao.findPlaceByPid(newpid);
	if(user.getRole().getRid() <= 3){
		new_area = "全市";
	}else{
		new_area = newpl.getArea();
	}
	String role = "";
	if(user.getRole().getRid() == 1){
		role = "一级权限";
	}
	if(user.getRole().getRid() == 2){
		role = "二级权限";
	}
	if(user.getRole().getRid() == 3){
		role = "三级权限";
	}
	if(user.getRole().getRid() == 4){
		role = "四级权限";
	}
	if(user.getRole().getRid() == 5){
		role = "五级权限";
	}
%>
	<head>
	
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/fullcalendar.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.main.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/select2.css" />
		<link rel="stylesheet" href="<%=ctx%>/css/unicorn.grey.css" class="skin-color" />
		<link href="<%=ctx%>/img/Oscar.ico" rel="shortcut icon">
		
		<script src="<%=ctx%>/js/excanvas.min.js"></script>
		<script src="<%=ctx%>/js/jquery.min.js"></script>
		<script src="<%=ctx%>/js/jquery.uniform.js"></script>
		<script src="<%=ctx%>/js/jquery.ui.custom.js"></script>
		<script src="<%=ctx%>/js/bootstrap.min.js"></script>
		<script src="<%=ctx%>/js/select2.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.js"></script>
		<script src="<%=ctx%>/js/jquery.dataTables.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.tables.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.min.js"></script>
		<script src="<%=ctx%>/js/jquery.flot.resize.min.js"></script>
		<script src="<%=ctx%>/js/jquery.peity.min.js"></script>
		<script src="<%=ctx%>/js/fullcalendar.min.js"></script>
		<script src="<%=ctx%>/js/unicorn.dashboard.js"></script>
		<script src="<%=ctx%>/js/global.js"></script>
		<script src="<%=ctx%>/js/select2.js"></script>
		
		<script src="<%=ctx%>/js/jquery.validate.js"></script>
		<script src="<%=ctx%>/js/jquery.wizard.js"></script>
		<script src="<%=ctx%>/js/unicorn.wizard.js"></script>
		
		<script type="text/javascript" src="<%=ctx%>/js/cvi_busy_lib.js"></script>
		<title>联系人</title>
		<script type="text/javascript">
			function addeditcontect(aid,address,content,tel){
				content = content.replace("<br>","\r\n");
				document.getElementById("aid").value = aid;
				document.forms['doupload'].getElementsByTagName("input").area.value = address;
				document.forms['doupload'].getElementsByTagName("textarea").content_str.value = content;
				document.forms['doupload'].getElementsByTagName("input").tel.value = tel;
				document.getElementById("edit_contect").style.display="";
				document.getElementById("edit_contect2").style.display="";
			}
			function closeeditcontect(){
				document.getElementById("edit_contect").style.display="none";
				document.getElementById("edit_contect2").style.display="none";
			}
			
			function addnewcontect(){
				document.getElementById("new_contect").style.display="";
				document.getElementById("new_contect2").style.display="";
			}
			function closenewcontect(){
				document.getElementById("new_contect").style.display="none";
				document.getElementById("new_contect2").style.display="none";
			}
			
			function setInfo(){
				var form = document.forms['doupload'];
				form.action = '<%=request.getContextPath() %>/browser/updatecontect.do';
				form.target = "_self";
				form.submit();
			}
			
			function setInfo2(){
				var form = document.forms['doupload2'];
				form.action = '<%=request.getContextPath() %>/browser/updatecontect.do';
				form.target = "_self";
				form.submit();
			}
		</script>
		<style type="text/css">
		#header .tile {
			height: 61px;
			left: 215px;
			line-height: 600px;
			overflow: hidden;
			position: relative;
			top: -20px;
			width: 391px;
		}
	</style>
	</head>
	<body>
		<div id="header">
			<h1><a href="<%=request.getContextPath()%>/dashboard.jsp">易民生活</a></h1>		
			<div class="tile"><h4><font color="#999">权限区域：<%=newarea.getArea()%>&nbsp;<%=new_area%></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<font color="#999"><%=role%></font>
			</h4></div>
		</div>
		<script type="text/javascript">
			$("#header > h1").css("backgroundImage","url('<%=request.getContextPath()%>/img/logo.png')");
		</script>
		<div id="user-nav" class="navbar navbar-inverse">
			<ul class="nav btn-group">
				<li class="btn btn-inverse" style=""><a title="" href="#"><i class="icon icon-user"></i> <span class="text"><%=user.getName()%></span></a></li>
				<li class="btn btn-inverse" style="display:none"><a title="" href="<%=request.getContextPath()%>/anonymous_changepassword.jsp"><i class="icon icon-cog"></i> <span class="text">设置</span></a></li>
				
				<li class="btn btn-inverse" id="logoutAction"><a title="" href="<%=request.getContextPath()%>/logout.jsp"><i class="icon icon-share-alt"></i> <span class="text">退出</span></a></li>
				<li class="btn btn-inverse" id="loginAction" style="display:none"><a title="" href="javascript:void(0)" onclick="showMessageBox(this)"><i class="icon icon-share-alt"></i> <span class="text">登录</span></a></li>
			</ul>
		</div>
		<jsp:include page="sidebar.jsp" flush="true" />
		<div id="content">
			<div id="content-header" style="display:none">
				<h1>联系方式</h1>
			</div>
			<div id="breadcrumb">
				<a href="<%=request.getContextPath()%>/browser/statusnum.do" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>&nbsp;首页</a>
				<a href="javascript:void(0)" class="current">联系方式</a>
			</div>
			<div align="right">	
				<input type="hidden" value="" id="price">
			</div>		
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>联系列表</h5><div class="buttons"></div></div>
							<div class="row-fluid">
								<table class="table table-bordered data-table">
								<thead>
									<tr>
										<th>权限范围</th>
										<th>联系内容</th>
										<th>联系电话</th>
										<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 3)){%>
										<th>操作</th>
										<%}%>
									</tr>
								</thead>
								<tbody>
								
									<%
										if(contect != null){
											for(int i = 0;i < contect.size();i ++){
												String ctr_content = contect.get(i).getContent();
												ctr_content=ctr_content.replaceAll("\r\n","<br>");
									%>
									<tr>
										<td><%=contect.get(i).getAddress()%></td>
										<td><%=contect.get(i).getContent()%></td>
										<td><%=contect.get(i).getTelphone()%></td>
										<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 3)){%>
										<td class="taskOptions">
											<a href="<%=request.getContextPath()%>/browser/delcontect.do?aid=<%=contect.get(i).getAid()%>" class="tip-top" data-original-title="删除"><i class="icon-remove"></i></a>
											<a href="#" onclick="addeditcontect('<%=contect.get(i).getAid()%>','<%=contect.get(i).getAddress()%>','<%=ctr_content%>','<%=contect.get(i).getTelphone()%>')" class="tip-top" data-original-title="修改"><i class="icon-edit"></i></a>
										</td>
										<%}%>
									</tr>
									<%
											}
											if(contect.size() == 0){
									%>
											<tr>
												<td colspan="4">无联系方式</td>
												<td style="display:none"></td>
												<td style="display:none"></td>
												<td style="display:none"></td>
											</tr>
									<%
											}
										}
									%>
								</tbody>
								</table>
						</div>
						<%if((user.getRole().getRid() == 1) || (user.getRole().getRid() == 3)){%>
						<div align="center">
							<a id="newad" name="newad" href="#" onclick="addnewcontect()"/>新建联系方式</a>
						</div>
						<%}%>
						<div class="widget-title" id="edit_contect2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>编辑联系方式</h5>
						</div>
						<div class="widget-content nopadding" id="edit_contect" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload" id="doupload" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">权限范围:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="area" id="area" value=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系内容:</label>
									<div class="controls">
										<textarea name="content_str" id="content_str" style="width: 243px; height: 78px;"></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系电话:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="tel" id="tel" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closeeditcontect();">返回</button>
								</div>
								<input id="aid" name="aid" type="hidden">
							</form>
						</div>
						<div class="widget-title" id="new_contect2" style="display:none">
							<span class="icon">
								<i class="icon-align-justify"></i>									
							</span>
							<h5>添加联系方式</h5>
						</div>
						<div class="widget-content nopadding" id="new_contect" style="display:none">
							<form action="" method="post" enctype="multipart/form-data"  name="doupload2" id="doupload2" class="form-horizontal" />
								<div class="control-group">
									<label class="control-label">权限范围:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="area" id="area" value=""/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系内容:</label>
									<div class="controls">
										<textarea name="content_str" id="content_str" style="width: 243px; height: 78px;"></textarea>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">联系电话:</label>
									<div class="controls">
										<input type="text" style="width:25%;height:25px" name="tel" id="tel" value=""/>
									</div>
								</div>
								<div class="form-actions">
									<button type="button" class="btn btn-primary" onclick="setInfo2();">保存</button>
									<button type="button" class="btn btn-primary" onclick="closenewcontect();">返回</button>
								</div>
							</form>
						</div>
					</div>					
				</div>
			</div>
	</body>
	<script type="text/javascript">
		$(document).ready(function() {
			setActiveClass($("#contect_menu"));
		});
	</script>
</html>