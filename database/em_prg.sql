/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Version : 50627
 Source Host           : localhost
 Source Database       : em

 Target Server Version : 50627
 File Encoding         : utf-8

 Date: 06/01/2016 18:08:18 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `advertisement`
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement` (
  `aid` int(12) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(12) NOT NULL DEFAULT '0',
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `checkstatus` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `bigclass`
-- ----------------------------
DROP TABLE IF EXISTS `bigclass`;
CREATE TABLE `bigclass` (
  `bid` int(12) NOT NULL AUTO_INCREMENT,
  `fid` int(12) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `four_fk` (`fid`),
  CONSTRAINT `four_fk` FOREIGN KEY (`fid`) REFERENCES `fourclass` (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `bigclass`
-- ----------------------------
BEGIN;
INSERT INTO `bigclass` VALUES ('1', '1', '餐饮美食', 'canyin.png', 'img/canyin/', '0'), ('2', '1', '教育培训', 'jiaoyu.png', 'img/jiaoyu/', '0'), ('3', '1', '汽车服务', 'qiche.png', 'img/qiche/', '0'), ('4', '1', '商超百货', 'shangchao.png', 'img/shangchao/', '0'), ('5', '1', '维修服务', 'weixiu.png', 'img/weixiu/', '0'), ('6', '1', '医疗养生', 'yiliao.png', 'img/yiliao/', '0'), ('7', '1', '装修服务', 'zhuangxiu.png', 'img/zhuangxiu/', '0'), ('8', '1', '其他更多', 'qita.png', 'img/qita/', '0'), ('9', '1', 'test', '1464767329373.png', 'img/xinjian/', '2');
COMMIT;

-- ----------------------------
--  Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `cid` int(12) NOT NULL AUTO_INCREMENT,
  `uid` int(12) NOT NULL DEFAULT '0',
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `type_id` int(12) NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `commentator` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `score` int(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`),
  KEY `comments_cs_uid` (`uid`),
  CONSTRAINT `comments_cs_uid` FOREIGN KEY (`uid`) REFERENCES `users` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `commodity`
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity` (
  `yid` int(12) NOT NULL AUTO_INCREMENT,
  `cid` int(12) NOT NULL DEFAULT '0',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `norm` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `deals` varchar(64) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'CURRENT_TIMESTAMP',
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail_img` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail_path` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`yid`),
  KEY `gid` (`cid`),
  CONSTRAINT `commodity_cy_cid` FOREIGN KEY (`cid`) REFERENCES `compangs` (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `compangs`
-- ----------------------------
DROP TABLE IF EXISTS `compangs`;
CREATE TABLE `compangs` (
  `cid` int(12) NOT NULL AUTO_INCREMENT,
  `sid` int(12) NOT NULL DEFAULT '0',
  `uid` int(12) NOT NULL,
  `number` int(16) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `manage` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `distance` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` int(11) DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `authentication` tinyint(1) NOT NULL DEFAULT '1',
  `recommend` tinyint(1) NOT NULL DEFAULT '1',
  `preferential` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `looktime` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teltime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `commentsize` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `tel` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `area` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slogan` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `stars` int(12) DEFAULT NULL,
  `grade` int(12) DEFAULT '0',
  `showfirst` tinyint(1) NOT NULL DEFAULT '1',
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiration_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checkstatus` tinyint(1) NOT NULL DEFAULT '1',
  `reason` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`cid`),
  UNIQUE KEY `compangs_name` (`name`),
  KEY `compangs_co_cid` (`sid`),
  CONSTRAINT `compangs_co_cid` FOREIGN KEY (`sid`) REFERENCES `smallclass` (`sid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `compangs`
-- ----------------------------
BEGIN;
INSERT INTO `compangs` VALUES ('1', '9', '1', '0', 'nanjding', '15nian', '', 'zhujianglu', '', '120', '30', '0', '1', '1', 'dongzhukeji.jpg', 'img/weixiu/dongzhukeji/', '0', '0', '0', 'diannaoweixiu', '13851702512', 'ding', 'zhujangrode', '1', '15years', '0', '0', '0', '2016-06-01 18:08:05', '2016-05-26 00:22:54', '0', ''), ('2', '19', '1', '0', 'nanjingchanghai ', 'changwei', '', 'daqiaobeilu9', '', '120', '33', '0', '1', '1', 'changhai.jpg', 'img/yiliao/changhaiyiyuan/', '0', '0', '0', 'changhai', '13952069795', 'lixue', 'daqiaonorthrode', '1', 'changwi', '0', '0', '0', '2016-06-01 18:07:58', '2016-05-31 00:22:58', '0', ''), ('3', '2', '1', '1', '浦东大厦', '浦东大厦', '', '泰山新村泰西路2号浦东大厦', '', '119', '32', '0', '1', '1', '1464078092214.jpg', 'img/xinjian/', '0', '0', '0', '餐饮美食，桑拿洗浴', '18012076049', '王先生', '泰山新村泰西路2号浦东大厦', '1', '浦东大厦', '0', '0', '1', '2016-06-01 18:08:00', '2017-05-11 00:00:00', '1', ''), ('5', '2', '1', '1', '浦东大厦23232', '浦东大厦', '', '泰山新村泰西路2号浦东大厦', '', '119', '32', '0', '1', '1', '1464078164257.jpg', 'img/xinjian/', '0', '0', '0', '餐饮美食，桑拿洗浴', '18012076049', '王先生', '泰山新村泰西路2号浦东大厦', '1', '浦东大厦', '0', '0', '1', '2016-06-01 18:08:03', '2017-05-11 00:00:00', '1', '');
COMMIT;

-- ----------------------------
--  Table structure for `contect`
-- ----------------------------
DROP TABLE IF EXISTS `contect`;
CREATE TABLE `contect` (
  `aid` int(12) NOT NULL AUTO_INCREMENT,
  `address` text COLLATE utf8_unicode_ci,
  `telphone` text COLLATE utf8_unicode_ci,
  `qq` text COLLATE utf8_unicode_ci,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` int(11) DEFAULT NULL,
  `latitude` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `feedbacks`
-- ----------------------------
DROP TABLE IF EXISTS `feedbacks`;
CREATE TABLE `feedbacks` (
  `fid` int(12) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci,
  `phone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `fourclass`
-- ----------------------------
DROP TABLE IF EXISTS `fourclass`;
CREATE TABLE `fourclass` (
  `fid` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fid`),
  UNIQUE KEY `fourclass_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `fourclass`
-- ----------------------------
BEGIN;
INSERT INTO `fourclass` VALUES ('1', '易民生活');
COMMIT;

-- ----------------------------
--  Table structure for `openarea`
-- ----------------------------
DROP TABLE IF EXISTS `openarea`;
CREATE TABLE `openarea` (
  `oid` int(12) NOT NULL AUTO_INCREMENT,
  `area` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `abridge` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `packagename` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isopen` tinyint(1) DEFAULT NULL,
  `visit` int(11) NOT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `openarea`
-- ----------------------------
BEGIN;
INSERT INTO `openarea` VALUES ('1', '南京', 'nj', 'nj', 'nj', '0', '1'), ('2', '盐城', 'yc', 'yc', 'yc', '0', '1');
COMMIT;

-- ----------------------------
--  Table structure for `opinion`
-- ----------------------------
DROP TABLE IF EXISTS `opinion`;
CREATE TABLE `opinion` (
  `oid` int(12) NOT NULL AUTO_INCREMENT,
  `content` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `picture`
-- ----------------------------
DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture` (
  `pid` int(12) NOT NULL AUTO_INCREMENT,
  `cid` int(12) NOT NULL DEFAULT '0',
  `small` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `big` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pid`),
  KEY `picture_pd_cid` (`cid`),
  CONSTRAINT `picture_pd_gid` FOREIGN KEY (`cid`) REFERENCES `compangs` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `picture`
-- ----------------------------
BEGIN;
INSERT INTO `picture` VALUES ('1', '1', 'dianmian1.jpg', 'dianmian1.jpg', 'img/weixiu/dongzhukeji/', '店铺外景'), ('2', '1', 'dianmian2.jpg', 'dianmian2.jpg', 'img/weixiu/dongzhukeji/', '店铺外景'), ('3', '1', 'dianmian3.jpg', 'dianmian3.jpg', 'img/weixiu/dongzhukeji/', '店铺外景'), ('4', '1', 'dianmian4.jpg', 'dianmian4.jpg', 'img/weixiu/dongzhukeji/', '店铺外景'), ('5', '2', 'dianmian1.jpg', 'dianmian1.jpg', 'img/yiliao/changhaiyiyuan/', '店铺外景'), ('6', '2', 'dianmian2.jpg', 'dianmian2.jpg', 'img/yiliao/changhaiyiyuan/', '店铺外景'), ('7', '2', 'dianmian3.jpg', 'dianmian3.jpg', 'img/yiliao/changhaiyiyuan/', '店铺外景'), ('8', '2', 'dianmian4.jpg', 'dianmian4.jpg', 'img/yiliao/changhaiyiyuan/', '店铺外景'), ('9', '5', '1464078215349.jpg', '1464078215349.jpg', 'img/xinjian/', '');
COMMIT;

-- ----------------------------
--  Table structure for `place`
-- ----------------------------
DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `pid` int(12) NOT NULL AUTO_INCREMENT,
  `province` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `place`
-- ----------------------------
BEGIN;
INSERT INTO `place` VALUES ('1', '江苏', '南京', '浦口区'), ('2', '江苏', '南京', '玄武区'), ('3', '江苏', '盐城', '盐都区');
COMMIT;

-- ----------------------------
--  Table structure for `recordhistory`
-- ----------------------------
DROP TABLE IF EXISTS `recordhistory`;
CREATE TABLE `recordhistory` (
  `rid` int(12) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `operation` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `recordhistory`
-- ----------------------------
BEGIN;
INSERT INTO `recordhistory` VALUES ('1', 'admin', 'Add a new compang name for 浦东大厦23232', '2016-05-24 16:23:19'), ('2', 'admin', 'Add a new picture for compang name is 浦东大厦23232', '2016-05-24 16:23:37'), ('3', 'admin', 'Add a new two menu for name is test', '2016-06-01 15:48:52');
COMMIT;

-- ----------------------------
--  Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `rid` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `roles_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `roles`
-- ----------------------------
BEGIN;
INSERT INTO `roles` VALUES ('2', 'ad1'), ('3', 'ad2'), ('4', 'ad3'), ('5', 'ad4'), ('1', 'admin');
COMMIT;

-- ----------------------------
--  Table structure for `service_area`
-- ----------------------------
DROP TABLE IF EXISTS `service_area`;
CREATE TABLE `service_area` (
  `sid` int(12) NOT NULL AUTO_INCREMENT,
  `cid` int(12) NOT NULL DEFAULT '0',
  `area` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `service_area_sa_sid` (`cid`),
  CONSTRAINT `service_area_sa_cid` FOREIGN KEY (`cid`) REFERENCES `compangs` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `service_area`
-- ----------------------------
BEGIN;
INSERT INTO `service_area` VALUES ('1', '1', '玄武区'), ('2', '2', '浦口区');
COMMIT;

-- ----------------------------
--  Table structure for `shangxun`
-- ----------------------------
DROP TABLE IF EXISTS `shangxun`;
CREATE TABLE `shangxun` (
  `xid` int(12) NOT NULL AUTO_INCREMENT,
  `sid` int(12) NOT NULL DEFAULT '0',
  `shangxun_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `looktime` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `comment_size` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `showfirst` tinyint(1) NOT NULL DEFAULT '1',
  `checkstatus` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`xid`),
  KEY `shangxun_sx_sid` (`sid`),
  CONSTRAINT `shangxun_sx_sid` FOREIGN KEY (`sid`) REFERENCES `smallclass` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `smallclass`
-- ----------------------------
DROP TABLE IF EXISTS `smallclass`;
CREATE TABLE `smallclass` (
  `sid` int(12) NOT NULL AUTO_INCREMENT,
  `bid` int(12) NOT NULL DEFAULT '0',
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `showfirst` tinyint(1) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `smallclass_sc_sid` (`bid`),
  CONSTRAINT `smallclass_sc_sid` FOREIGN KEY (`bid`) REFERENCES `bigclass` (`bid`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `smallclass`
-- ----------------------------
BEGIN;
INSERT INTO `smallclass` VALUES ('1', '1', '中餐', 'zhongcan.png', 'img/canyin/zilei/', '0', '0'), ('2', '1', '西餐', 'xican.png', 'img/canyin/zilei/', '0', '0'), ('3', '2', '家教机构', 'jiajiao.png', 'img/jiaoyu/zilei/', '0', '0'), ('4', '2', '文体培训', 'wenti.png', 'img/jiaoyu/zilei/', '0', '0'), ('5', '2', '职业培训', 'zhiye.png', 'img/jiaoyu/zilei/', '0', '0'), ('6', '3', '汽车保险', 'baoxian.png', 'img/qiche/zilei/', '0', '0'), ('7', '3', '维修保养', 'baoyang.png', 'img/qiche/zilei/', '0', '0'), ('8', '4', '超市', 'chaoshi.png', 'img/shangchao/zilei/', '0', '0'), ('9', '5', '电脑维修', 'diannao.png', 'img/weixiu/zilei/', '0', '0'), ('10', '5', '电瓶车维修', 'dianpingche.png', 'img/weixiu/zilei/', '0', '0'), ('11', '5', '防水处理', 'fangshui.png', 'img/weixiu/zilei/', '0', '0'), ('12', '5', '房屋维修', 'fangwu.png', 'img/weixiu/zilei/', '0', '0'), ('13', '5', '家电维修', 'jiadian.png', 'img/weixiu/zilei/', '0', '0'), ('14', '5', '开锁换锁', 'kaisuo.png', 'img/weixiu/zilei/', '0', '0'), ('15', '5', '摩托车维修', 'motuoche.png', 'img/weixiu/zilei/', '0', '0'), ('16', '5', '疏通管道', 'guandao.png', 'img/weixiu/zilei/', '0', '0'), ('17', '5', '水电维修', 'shuidian.png', 'img/weixiu/zilei/', '0', '0'), ('18', '6', '药房药店', 'yaodian.png', 'img/yiliao/zilei/', '0', '0'), ('19', '6', '医院诊所', 'yiyuan.png', 'img/yiliao/zilei/', '0', '0'), ('20', '7', '装修', 'zhuangxiu.png', 'img/zhuangxiu/zilei/', '0', '0'), ('21', '1', '火锅', 'huoguo.png', 'img/canyin/zilei/', '0', '0'), ('22', '1', '酒吧', 'jiuba.png', 'img/canyin/zilei/', '0', '0'), ('23', '1', '咖啡馆', 'kafeiguan.png', 'img/canyin/zilei/', '0', '0'), ('24', '1', '快餐外卖', 'kuaican.png', 'img/canyin/zilei/', '0', '0'), ('25', '1', '烧烤', 'shaokao.png', 'img/canyin/zilei/', '0', '0'), ('26', '2', '亲子教育', 'qinzi.png', 'img/jiaoyu/zilei/', '0', '0'), ('27', '2', '会计培训', 'kuaiji.png', 'img/jiaoyu/zilei/', '0', '0'), ('28', '2', '外语培训', 'waiyu.png', 'img/jiaoyu/zilei/', '0', '0'), ('29', '2', '学历教育', 'xueli.png', 'img/jiaoyu/zilei/', '0', '0'), ('30', '2', '电脑培训', 'diannao.png', 'img/jiaoyu/zilei/', '0', '0'), ('31', '2', '其他培训', 'qita.png', 'img/jiaoyu/zilei/', '0', '0'), ('32', '3', '租车', 'zuche.png', 'img/qiche/zilei/', '0', '0'), ('33', '3', '过户上牌', 'guohushangpai.png', 'img/qiche/zilei/', '0', '0'), ('34', '3', '驾校', 'jiaxiao.png', 'img/qiche/zilei/', '0', '0'), ('35', '3', '年检验车', 'nianjian.png', 'img/qiche/zilei/', '0', '0'), ('36', '3', '陪驾代驾', 'peijia.png', 'img/qiche/zilei/', '0', '0'), ('37', '3', '汽车美容', 'meirong.png', 'img/qiche/zilei/', '0', '0'), ('38', '4', '箱包皮具', 'xiangbao.png', 'img/shangchao/zilei/', '0', '0'), ('39', '4', '烟酒', 'yanjiu.png', 'img/shangchao/zilei/', '0', '0'), ('40', '4', '珠宝砖石', 'zhubao.png', 'img/shangchao/zilei/', '0', '0'), ('41', '4', '茶叶', 'chaye.png', 'img/shangchao/zilei/', '0', '0'), ('42', '4', '床上用品', 'chaungshang.png', 'img/shangchao/zilei/', '0', '0'), ('43', '4', '服装鞋帽', 'fuzhuang.png', 'img/shangchao/zilei/', '0', '0'), ('44', '4', '化妆洗护', 'huazhuang.png', 'img/shangchao/zilei/', '0', '0'), ('45', '4', '家电数码', 'jiadian.png', 'img/shangchao/zilei/', '0', '0'), ('46', '4', '书店', 'shudian.png', 'img/shangchao/zilei/', '0', '0'), ('47', '5', '数码维修', 'shuma.png', 'img/weixiu/zilei/', '0', '0'), ('48', '6', '足疗按摩', 'zuliao.png', 'img/yiliao/zilei/', '0', '0'), ('49', '6', '膏帖', 'gaotie.png', 'img/yiliao/zilei/', '0', '0'), ('50', '6', '美容美体', 'meirong.png', 'img/yiliao/zilei/', '0', '0'), ('51', '6', '养生理疗', 'yangsheng.png', 'img/yiliao/zilei/', '0', '0'), ('52', '6', '整容', 'zhengrong.png', 'img/yiliao/zilei/', '0', '0'), ('53', '7', '家具', 'jiaju.png', 'img/zhuangxiu/zilei/', '0', '0'), ('54', '7', '建材', 'jiancai.png', 'img/zhuangxiu/zilei/', '0', '0'), ('55', '8', '保洁服务', 'baojie.png', 'img/qita/zilei/', '0', '0'), ('56', '8', '物品回收', 'huishou.png', 'img/qita/zilei/', '0', '0'), ('57', '8', '休闲娱乐', 'xiuxian.png', 'img/qita/zilei/', '0', '0'), ('59', '8', '月嫂保姆', 'yuesao.png', 'img/qita/zilei/', '0', '0'), ('60', '8', '搬家服务', 'banjia.png', 'img/qita/zilei/', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `number` int(12) NOT NULL DEFAULT '0',
  `uuid` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(12) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Records of `users`
-- ----------------------------
BEGIN;
INSERT INTO `users` VALUES ('1', '123', '123', '123', 'admin', 'admin', '123@12com.com', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `version`
-- ----------------------------
DROP TABLE IF EXISTS `version`;
CREATE TABLE `version` (
  `vid` int(12) NOT NULL AUTO_INCREMENT,
  `ver` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `update_content` text COLLATE utf8_unicode_ci,
  `url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `become_effective` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`vid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
